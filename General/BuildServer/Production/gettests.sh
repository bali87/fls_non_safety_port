#!/bin/bash

# scripts takes two arguments and returns number of occurrences
# 'cmocka_unit_test' in a provided c-file (the result should be
# always equal to number of tests declared as a tests array). the
# arguments are as follows:
# - first is a c-file with cmocka tests
# - second is a number to be subtracted from the result

TESTS=`grep cmocka_unit_test $1 | grep -v -e "//.*cmocka_unit_test" | wc -l | cut -d" " -f1`

if [[ -n "$2" ]]; then
    TESTS=$(($TESTS - $2))
fi

echo -n $TESTS

