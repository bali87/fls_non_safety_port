#!/bin/bash
# Copyright 2018 OptoForce
#   created:  Tue 02 Jun 2015 09:40:47 AM CEST
#   modified: Thu, Jun  4, 2015 10:31:38 PM

# script runs xc16 compiler with modified arguments:
#  - performs naive find replace with -Wl,--wrap= directive
#  - concatenate '-o output_file' to '-ooutput_file'

if [[ "$#" == "0" ]]; then
    echo "No arguments provided."
    exit 1
fi

##### BASH_CLEANUP module {{{
# make temp file to store all commands to run when scripts exits
BASH_CLEANUP_FILE=`mktemp`

# register a command to be executed when exit
function BASH_FINALLY() {
  echo "$*" >> $BASH_CLEANUP_FILE
}

# clean up function
function BASH_CLEANUP() {
    # execute all registered commands (in reverse order)
    tac $BASH_CLEANUP_FILE | bash
    # remove clean up script itself
    rm $BASH_CLEANUP_FILE
}

# actual bind a clean up function to EXIT event
trap BASH_CLEANUP EXIT
##### }}}

# explicit clear variables
SYMBOLS=""
ARGS=""
CONCATENATE_LAST_ARG=""

# iterate over all command line arguments and extract all '-Wl,--wrap='
# directives, eg. for the following command:
#   $ ./script.sh -Wl,--wrap=foo -Wl,--wrap=bar -ggdb -o main main.c
# loop will set following variables:
#   ARGS=="-ggdb -o main main.c"
#   SYMBOLS=="foo bar"
for arg in $*; do
    # check if current argument is a wrap directive
    if [[ ${arg} =~ -Wl,--wrap=* ]]; then
        SYMBOL=${arg/-Wl,--wrap=/}
        SYMBOLS="${SYMBOLS} ${SYMBOL}"
    else 
        ARGS="${ARGS} ${arg}"
    fi
done

# iterate over all arguments and perform replace all c-files and h-files by
# changeing all occurencies of 'symbol' to '__wrap_symbol'
for f in ${ARGS}; do
    # check for c-file or h-file
    if [[ (${f} == *.c) || (${f} == *.h) ]]; then
        # make a local copy
        cp ${f} ${f}.bak
        # add clean up command (will be triggered at the end of the script)
        BASH_FINALLY mv ${f}.bak ${f}
        # for all symbols passed with '-Wl,--wrap=' directive
        for s in ${SYMBOLS}; do
            # replace all occurencies in file
            sed -e "s/\<${s}\>/__wrap_&/g" -i ${f}
        done
    fi
done

SANITIZED_ARGS=""

for arg in ${ARGS}; do
 
  if [[ (${arg} == *.elf) ]]; then
       CMAKE_ELF_OUTPUT=${arg}
	   CMAKE_HEX_OUTPUT=${arg/.elf/.hex}
  fi
    
  

    # use windows path (desired path form is c:/dir/subdir/)
    arg=${arg/\/cygdrive\/c/c:}
	arg=${arg/\/cygdrive\/d/d:}
        
    SANITIZED_ARGS="${SANITIZED_ARGS} ${arg}"
done


# run modified command
echo "Executing modified linker command:"
echo "xc16-gcc ${SANITIZED_ARGS}"
xc16-gcc ${SANITIZED_ARGS} || EXIT=1
echo "xc16-bin2hex ${CMAKE_ELF_OUTPUT} -a -omf=elf"
xc16-bin2hex ${CMAKE_ELF_OUTPUT} -a -omf=elf
echo "Normalizing hex file:"
"C:/Program Files (x86)/Microchip/MPLABX/v5.05/mplab_platform/platform/../mplab_ide/modules/../../bin/hexmate" --edf="C:/Program Files (x86)/Microchip/MPLABX/v5.05/mplab_platform/platform/../mplab_ide/modules/../../dat/en_msgs.txt" ${CMAKE_HEX_OUTPUT} -osHEX.App.hex
echo "\"C:/Program Files (x86)/Microchip/MPLABX/v5.05/mplab_platform/platform/../mplab_ide/modules/../../bin/hexmate\" --edf=\"C:/Program Files (x86)/Microchip/MPLABX/v5.05/mplab_platform/platform/../mplab_ide/modules/../../dat/en_msgs.txt\" ${CMAKE_HEX_OUTPUT} -osHEX.App.hex"

rm Main.hex
echo "Creating unified hex file:"
"C:/Program Files (x86)/Microchip/MPLABX/v5.05/mplab_platform/platform/../mplab_ide/modules/../../bin/hexmate" --edf="C:/Program Files (x86)/Microchip/MPLABX/v5.05/mplab_platform/platform/../mplab_ide/modules/../../dat/en_msgs.txt" -break=55E00 sHEX.App.hex ../../../Bootloader/Opto_Bootloader24.X.production.hex -osHEX.Unified.hex

exit ${EXIT}

