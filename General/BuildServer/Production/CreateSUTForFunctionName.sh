#!/bin/bash
# $1 - SUI Relative Path (clion: determined on current file)
# $2 - UnitName_FunctionName (clion: passed by selection)
# $3 - Project SUT root directory (currently empty and not used)

#UnitPath=`echo -n $1 | sed -e "s#[^/]*/[^/]*/##"`
#UnitPath -- SUI path without two consecutive / / 
UnitPath=$1

UnitName=${2/_*/}
FunctionName=${2/*_/}

FullFunctionName=$2

ProjectSUTDirectory=$3

MockFile=${UnitName}SUTMock.c
FunctionFile=${UnitName}SUT${FunctionName}.c

echo $UnitName
echo $FunctionName
echo $MockFile
echo $FunctionFile

TemplatesRelativePath=./.idea/fileTemplates
SUTMockFileTemplate="CSUTMockFileForOneTestedUnit.c"
SUTFunctionFileTemplate="CSUTTestFileForOneFunction.c"

#SUTSourceRelativePath=../../../Software/SP6-9_SUT/WP6-9.5.2_SVS/Sources/${ProjectSUTDirectory}
SUTSourceRelativePath=../../../Software/UnitTesting/TestSpec/
SUTUnitRelativePath=${SUTSourceRelativePath}/${UnitPath}

#echo UnitName $UnitName
#echo FunctionName $FunctionName
#echo UnitPath $UnitPath
#echo SourceRelPath ${SUTSourceRelativePath}/${UnitPath}

function sanitize_clion_template() {
  sed -e "s/#\[\[//g" -e "s/\]\]#//g" -i $1
}

function replace_placeholder() {
  sed -e "s/\${${1}}/${2}/g" -i ${3}
}

echo "Creating SUT for ${FullFunctionName} in path ${SUTUnitRelativePath}"

mkdir -p ${SUTUnitRelativePath}

echo "Checking if corresponding Mock file (${MockFile}) already exists ..."

OutputCfile=${SUTUnitRelativePath}/${MockFile}
if [[ -f ${OutputCfile} ]]; then
    echo "... file already exists."
else 
    cp "${TemplatesRelativePath}/${SUTMockFileTemplate}" ${OutputCfile}
    sanitize_clion_template ${OutputCfile}
    replace_placeholder TestedUnitName ${UnitName} ${OutputCfile}
    replace_placeholder FunctionName ${FunctionName} ${OutputCfile}
    echo "... creating corresponding Mock file."
fi    

echo "Checking if corresponding test file (${FunctionFile}) exists ..."
OutputCfile=${SUTUnitRelativePath}/${FunctionFile}
if [[ -f ${OutputCfile} ]]; then
    echo "... file already exists."
else 
    cp "${TemplatesRelativePath}/${SUTFunctionFileTemplate}" ${OutputCfile}
    sanitize_clion_template ${OutputCfile}
    replace_placeholder TestedUnitName ${UnitName} ${OutputCfile}
    replace_placeholder FunctionName ${FunctionName} ${OutputCfile}
    echo "... creating corresponding test file."
fi    

echo "... finished."

