/**
 * @file
 *
 * Unit that manages the computation of the system delay /Unit that provides library functions for computation of delay.
 * fixme update the comment.
 * 
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */

#[[#include]]# "Std.h"

#if (${HEADER_FILENAME})
#[[#include]]# "${HEADER_FILENAME}"
#end


/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticDefines Static defines
 */


 
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



 /** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */



/** @} */
/* ================================================================================================================== */
 
/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */ 



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */ 



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */ 



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */ 



/** @} */
