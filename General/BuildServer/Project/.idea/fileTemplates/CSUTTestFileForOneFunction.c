/**
 * @file
 *
 * Unit that provides unit tests for the unit ${TestedUnitName}.
 * 
 * @version #[[$Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#[[#include]]# "Std.h"

#[[#include]]# "CmockaFramework.h"

#[[#include]]# "${TestedUnitName}.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */

static int ${TestedUnitName}SUT${FunctionName}_Setup(void ** state);
static void ${TestedUnitName}SUT${FunctionName}_Test0(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest ${TestedUnitName}SUT${FunctionName}_Tests[] = 
{
	cmocka_unit_test(${TestedUnitName}SUT${FunctionName}_Test0)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */

/**
 * Initialization of tested unit.
 */
static int ${TestedUnitName}SUT${FunctionName}_Setup(void ** state)
{

    // fixme for units that have _Init(), not for testing of _Init function itself.
    // ${TestedUnitName}_Init();
    return 0;
}

/**
 * @test ${TestedUnitName}SUT${FunctionName}_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ${TestedUnitName}SUT${FunctionName}_Test0(void ** state)
{

    /* given */
    uint8_t retVal;
    /* when */
    retVal = ${TestedUnitName}_${FunctionName}();
    /* then */
    assert_int_equal(retVal, 0U);


    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(${TestedUnitName}SUT${FunctionName}_Tests, NULL, NULL)

/** @} */
