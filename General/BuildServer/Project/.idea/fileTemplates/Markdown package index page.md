@dir .
@brief Component Temperature, handling the determination of target relay state based on current temperature. 