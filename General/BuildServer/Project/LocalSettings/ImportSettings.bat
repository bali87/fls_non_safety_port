@echo off



echo please close clion is closed before running this script.

echo copying exida.xml with live snippets ...
mkdir %USERPROFILE%\.CLion2017.2
mkdir %USERPROFILE%\.CLion2017.2\config

mkdir %USERPROFILE%\.CLion2017.2\config\colors
mkdir %USERPROFILE%\.CLion2017.2\config\keymaps
mkdir %USERPROFILE%\.CLion2017.2\config\options
mkdir %USERPROFILE%\.CLion2017.2\config\templates
mkdir %USERPROFILE%\.CLion2017.2\config\tools

copy /y exida_colors.icls %USERPROFILE%\.CLion2017.2\config\colors

copy /y "Default copy.xml" %USERPROFILE%\.CLion2017.2\config\keymaps

copy /y "keymap.xml" %USERPROFILE%\.CLion2017.2\config\options
copy /y "colors.scheme.xml" %USERPROFILE%\.CLion2017.2\config\options
copy /y "editor.codeinsight.xml" %USERPROFILE%\.CLion2017.2\config\options
copy /y "ui.lnf.xml" %USERPROFILE%\.CLion2017.2\config\options
copy /y "editor.xml" %USERPROFILE%\.CLion2017.2\config\options
copy /y "other.xml" %USERPROFILE%\.CLion2017.2\config\options

copy /y exida.xml %USERPROFILE%\.CLion2017.2\config\templates

copy /y "External Tools.xml" %USERPROFILE%\.CLion2017.2\config\tools

del /f ..\.idea\workspace.xml

echo ... finished, now you can start clion.
@pause