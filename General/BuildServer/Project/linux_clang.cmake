message("Using instrumented clang compiler on linux ...")

set(CMAKE_SYSTEM_NAME Linux)

set(CMAKE_C_COMPILER "clang")

set(CMAKE_C_FLAGS
  "-std=c99 -ggdb -O0 -fsanitize=integer -fno-sanitize-recover=integer -fsanitize=undefined -fsanitize=signed-integer-overflow -fsanitize=unsigned-integer-overflow -fsanitize=shift -fsanitize=enum -fsanitize=null -fsanitize=float-cast-overflow -fsanitize=float-divide-by-zero -fsanitize=alignment -fno-sanitize-recover=undefined"
    CACHE STRING "" FORCE)

set(CMOCKA_LIB ../../../Software/UnitDesignAndImpl/Implementation/Libraries/Cmocka/libcmocka.so.0)

