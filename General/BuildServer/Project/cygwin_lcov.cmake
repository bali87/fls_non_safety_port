message("Using gcc compiler with coverge on cygwin ...")

set(CMAKE_SYSTEM_NAME Cygwin)

set(CMAKE_C_COMPILER gcc)

set(CMAKE_C_FLAGS
    "-std=c99 -ggdb -O0 -D__PIC24FJ256GA702__ -fprofile-arcs -ftest-coverage"
    CACHE STRING "" FORCE)

set(CMOCKA_LIB ../../../Software/UnitDesignAndImpl/Implementation/Libraries/Cmocka/libcmocka.dll.a)
set(CMOCKA_LIB_DLL ../../../Software/UnitDesignAndImpl/Implementation/Libraries/Cmocka/cygcmocka-0.dll)

