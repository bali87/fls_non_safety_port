message("Using instrumented gcc compiler on linux ...")

set(CMAKE_SYSTEM_NAME Linux)

set(CMAKE_C_COMPILER "gcc_asan")

set(CMAKE_C_FLAGS
    "-std=c99 -ggdb -O0 -fsanitize=address -fno-common -fno-omit-frame-pointer"
    CACHE STRING "" FORCE)

set(CMOCKA_LIB ../../../Software/UnitDesignAndImpl/Implementation/Libraries/Cmocka/libcmocka.so.0)

