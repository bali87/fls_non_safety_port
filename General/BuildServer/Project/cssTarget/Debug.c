/**********************************************************************
* (C) Copyright 2017, OptoForce Ltd.
* -------------------------------------------------------------------
* \verbatim
*
* OPTOFORCE LIMITED PROPRIETARY INFORMATION
*
* Property of OptoForce ltd, Unauthorized reproduction and/or
* distribution is strictly prohibited. This product is protected
* under copyright law and trade secret law as an unpublished
* work
* (C) Copyright OptoForce. All rights reserved.
* \endverbatim
* -------------------------------------------------------------------
*
* -------------------------------------------------------------------
* FILE DESCRIPTION
* -------------------------------------------------------------------
* File: my_utils.c
* Component: -
* Module: -
* Generator: -
*
* Description: Utils for code testing and debugging block
*
*---------------------------------------------------------------------
*---------------------------------------------------------------------
* Author: Vincze Balazs
*---------------------------------------------------------------------
* Revision History
*---------------------------------------------------------------------
* Version   Date     Author  Change       ID Description
*---------------------------------------------------------------------
* 00.01.00 2017. szept. 18. Vincze Balazs 0000000000000 Initial Version
*-------------------------------------------------------------------*/

#include <Debug.h>
#include "can.h"
#include "sci.h"
#include <string.h>


char _debug_tmp='\0';
static char hex_chr_lut[] = {"0123456789ABCDEF\0"};
/* Local Defines */
#define CAN_DEBUG_TX_FRAME_NUM  8U          /* From  MSGBOX 49 to 64 */
#define CAN_DEBUG_MSGBOX_START  49U
#define CAN_DEBUG_FRAME_ID      0x200



int32_t Debug_WriteCAN(char * pBuff,uint32_t buffLen)
{
uint16_t txIndex = 0u;
uint16_t txDLC=0u;
uint8_t msgboxIterator=0u;
uint16_t block_flag_curr=0u;
uint32_t byteIterator=0u;
int32_t retVal = 0;
    if ( (buffLen > 0u) && (buffLen<=256u) )        /* data_length = 1..256*/
    {
        while (buffLen>0u)
        {
                /* Search Last free messagebox from top id to bottom */
                msgboxIterator = CAN_DEBUG_TX_FRAME_NUM;  /* Start from MSGBOX 49, second integer(uint32_t) of TXRQ */
                while(msgboxIterator > 0u)
                {

                    block_flag_curr = (uint16_t)((canREG1->TXRQx[1] & 0X0F00U)& (1u << (msgboxIterator - 1u)));

                    if(block_flag_curr) /* If found break with last free */
                    {
                       break;
                    }
                    msgboxIterator--;
                }
                if((block_flag_curr)&&(0 == msgboxIterator))
                {
                    msgboxIterator = 1;
                }
               if(msgboxIterator == CAN_DEBUG_TX_FRAME_NUM)
                {

                    /*PUTS("\r\n!!!TX FIFO TELI BLOKKOL!\r\n");
                    PUTCHAR('B');
                   print_word_hex(  block_flag_curr);
              NEWLINE();
                   print_byte_hex(cnt);*/

                }
                /* Fill Free area from bottom to top */
                while(msgboxIterator<(CAN_DEBUG_TX_FRAME_NUM))
                {
                    if (buffLen>=8) txDLC = 8; else txDLC = buffLen;
                    while ((canREG1->IF1STAT & 0x80U) == 0x80U)
                    {
                    } /* Wait */
                    canREG1->IF1MSK  = 0xC0000000U | (uint32)((uint32)((uint32)0x000007FFU & (uint32)0x000007FFU) << (uint32)18U);
                    canREG1->IF1ARB  = (uint32)0x80000000U | (uint32)0x00000000U | (uint32)0x20000000U | (uint32)((uint32)((uint32)CAN_DEBUG_FRAME_ID & (uint32)0x000007FFU) << (uint32)18U);
                    canREG1->IF1MCTL = 0x00001000U | (uint32)0x00000000U | (uint32)0x00000000U | (uint32)0x00000000U | (uint32)txDLC;
                    canREG1->IF1CMD  = (uint8) 0xF8U;
                    canREG1->IF1NO   = (CAN_DEBUG_MSGBOX_START + msgboxIterator);
                    while ((canREG1->IF1STAT & 0x80U) == 0x80U)
                    {
                    } /* Wait */

                      /** - Configure IF1 for
                      *     - Message direction - Write
                      *     - Data Update
                      *     - Start Transmission
                      */
                    canREG1->IF1CMD = 0x87U;

                      /** - Copy TX data into IF1 */
                      for (byteIterator = 0U; byteIterator < txDLC; byteIterator++)
                      {
                          /*SAFETYMCUSW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
                          canREG1->IF1DATx[byteIterator] = pBuff[txIndex];
                          /*SAFETYMCUSW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
                          /*SAFETYMCUSW 567 S MR:17.1,17.4 <APPROVED> "Pointer increment needed" */
                          txIndex++;
                      }
                      /** - Copy TX data into message box */
                      /*SAFETYMCUSW 93 S MR: 6.1,6.2,10.1,10.2,10.3,10.4 <APPROVED> "LDRA Tool issue" */
                      canREG1->IF1NO = (uint8) (CAN_DEBUG_MSGBOX_START + msgboxIterator);
                      //tx_index+=tx_DLC;
                      buffLen-=txDLC;
                      if(buffLen==0u)break;
                      msgboxIterator++;
                }



        }
        if(buffLen > 0u)
        {
           /*PUTS("Nem fert el!");
           NEWLINE();
           PUTS("FLG=");
           print_word_hex(  block_flag_curr);
           NEWLINE();
           PUTS("CNT=");

           print_byte_hex(cnt);
           PUTS("LEN=");
           print_num_dec(data_length);
           NEWLINE();

           PUTS("START_LEN=");
           print_num_dec(start_len);
           NEWLINE();
           PUTS("TX_INDX=");
           print_num_dec(tx_index);

           while(1);*/
        }
    }


return (retVal);
}
int32_t Debug_WriteSCI(char * pBuff,uint32_t buffLen)
{
int32_t retVal = 0;
uint32_t i = 0x0U;
   while(i < buffLen)
   {
       sciSendByte(scilinREG, pBuff[i]);
       i++;
   }
return (retVal);
}
/**
 * @fn void print_byte_hex(uint8_t dat)
 * @param dat [in] 8bit data to serial port
 */
void print_byte_hex(uint8_t dat)
{
    _puts("0x");
    PUTCHAR(hex_chr_lut[(dat/16u)]);
    PUTCHAR(hex_chr_lut[(dat%16u)]);
    PUTCHAR('\n');
}
/**
 * @fn void print_word_hex(uint16_t dat)
 * @param dat dat [in] 16bit data to serial port
 */
void print_word_hex(uint16_t dat)
{
    PUTS("0x");
    PUTCHAR(hex_chr_lut[(((dat>>8)&0xffu)/16u)]);
    PUTCHAR(hex_chr_lut[(((dat>>8)&0xffu)%16u)]);
    PUTCHAR(hex_chr_lut[(((dat)&0xffu)/16u)]);
    PUTCHAR(hex_chr_lut[(((dat)&0xffu)%16u)]);

}
/**
 * @fn void print_int_hex(uint32_t dat)
 * @param dat dat [in] 32bit data to serial port
 */
void print_int_hex(uint32_t dat)
{

   PUTS("0x");

   PUTCHAR(hex_chr_lut[(((dat>>24)&0xffu)/16u)]);
   PUTCHAR(hex_chr_lut[(((dat>>24)&0xffu)%16u)]);
   PUTCHAR(hex_chr_lut[(((dat>>16)&0xffu)/16u)]);
   PUTCHAR(hex_chr_lut[(((dat>>16)&0xffu)%16u)]);
   PUTCHAR(hex_chr_lut[(((dat>>8)&0xffu)/16u)]);
   PUTCHAR(hex_chr_lut[(((dat>>8)&0xffu)%16u)]);
   PUTCHAR(hex_chr_lut[(((dat)&0xffu)/16u)]);
   PUTCHAR(hex_chr_lut[(((dat)&0xffu)%16u)]);

}
/**
 * @fn void print_byte_dec(uint8_t dat)
 * @param dat [in] 8bit data to serial port
 */
void print_byte_dec(uint8_t dat)
{
   PUTCHAR(hex_chr_lut[(dat/100u)]);
   PUTCHAR(hex_chr_lut[(dat%100u)/10u]);
   PUTCHAR(hex_chr_lut[(dat%10u)]);
   PUTCHAR('\n');
}
/**
 * @fn void print_num_dec(uint32_t dat)
 * @param [in] 32 bit data to serial port
 */
void print_num_dec(uint32_t dat)
{
uint32_t remain=dat;
uint32_t divider = 1000000u;
char str[11] = {0u};
uint8_t i=0u;
    while(divider > 0u)
    {
        str[i++] = (char)hex_chr_lut[(remain/divider)];
        remain = (remain % divider);
        divider = divider / 10u;
    }
    i = 0u;
    while(str[i] == '0')
    {
        i++;
        if(9u == i)
        {
            break;
        }
    }
    PUTS(&str[i]);
}
void _puts(char * ptr)
{
uint32_t indx=0u;
    while(ptr[indx]!='\0')
    {
        PUTCHAR(ptr[indx]);
        indx++;
    }
}
