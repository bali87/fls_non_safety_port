/**
* @file SignalFilter.c
* @author Vincze Balazs
* @date 2017. okt. 25.
* @brief Implementation file of module XXXX
* @version V 1.0
*/
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */
#include <ExtADCDrv.h>
#include "SignalFilter.h"
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 */

/**
 * Description.
 */ 
/*#define TEMPLATE_DEF (20U) */

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */

/**
 * Short description for EA and Doxygen.
 *
 * Detailed description for Doxygen, possibly several lines.
 * And several sentences.
 */
#if 0
typedef struct _Template_BarT;
{

    /**
     * Short description Doxygen.
     *
     * Detailed description for Doxygen, possibly several lines.
     * And several sentences.
     */
    uint8 bar;

} Template_BarT;
#endif
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */

/**
 * Description.
 */ 
/* const uint8 Template_val = 0U; */

/**@}*/
/* ================================================================================================================== */
 
/**
 * @addtogroup StaticVariablesInitialized Static variables initialized
 *  @{
 */ 


/**
 * Short description for EA and Doxygen.
 *
 * Detailed description for Doxygen, possibly several lines.
 * And several sentences.
 */
/*static int8 Template_StaticData;*/

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariablesNonInitialized Static variables non-initialized
 * @{
 */ 

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */ 

/* static uint8 SignalFilter_StaticFoo (int8 templateFoo); */

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */ 
/**
 * Short description of the global variable for EA and Doxygen.
 *
 * Detailed description for Doxygen, possibly several lines.
 * And several sentences.
 *
 *  @startuml
 *    Sender->Receiver  : Command()
 *    Sender<--Receiver : Ack()
 *  @enduml 
 *
 * @param[in] templateFoo Description for Doxygen.
 * @return Description for Doxygen.
 *
 */
uint8_t SignalFilter_Init(void)
{
	// TODO: Get filter paramaters from EEPROM????
    /* Initialize variables. */

	return (0u);
}

/**@}*/

