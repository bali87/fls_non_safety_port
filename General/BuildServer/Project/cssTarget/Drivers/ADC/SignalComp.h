#ifndef DRIVERS_ADC_SIGNALCOMP_H_
#define DRIVERS_ADC_SIGNALCOMP_H_

/**
* @file SignalComp.h
* @author Vincze Balazs
* @date 2017. okt. 25.
* @brief Implementation file of module XXXX
* @version V 1.0
*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */
    #include "stdint.h"
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */

/**
 * Short description.
 *
 * Detailed description for Doxygen, possibly several lines.
 * And several sentences.
 */
/* #define TEMPLATE_PI 3.14 */

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */

/**
 * Short description.
 *
 * Detailed description for Doxygen, possibly several lines.
 * And several sentences.
 */
#if 0
typedef struct
{

    /**
     * Short description Doxygen.
     *
     * Detailed description for Doxygen, possibly several lines.
     * And several sentences.
     */
    uint8 bar;

} Template_FooT;
#endif
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */

/**
 * Description.
 */ 
/* const uint8 Template_gVal = 0U; */

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */



extern uint8_t SignalComp_Init(void);
/*
extern void Template_HandleSchedule (void);

extern uint8 Template_GlobalFoo (int8 templateFoo);

*/
/**@}*/
#endif /* DRIVERS_ADC_SIGNALCOMP_H_ */
