var adc_8c =
[
    [ "_adc_rx_pending_data_t", "struct__adc__rx__pending__data__t.html", "struct__adc__rx__pending__data__t" ],
    [ "adc_rx_pending_data_t", "adc_8c.html#aa78a6798995eeea5c3dc0acbfcaf17f4", null ],
    [ "adc_init", "adc_8c.html#a399802a44e0b01613106be0b04b7108a", null ],
    [ "adc_receive_fsm", "adc_8c.html#a4145281fd6809c76b271dcd39cda39b5", null ],
    [ "clear_conv_flag", "adc_8c.html#a7585b344f3a9880d6cfce9cca73abd97", null ],
    [ "is_conv_ready", "adc_8c.html#a635d5fa307997e768f8fa9f58ff1091f", null ],
    [ "ad7124_handler", "adc_8c.html#acd19c874278431e3f7979a807436e447", null ],
    [ "adc_init_status", "adc_8c.html#af85e1d79c7c8aa1991c9087a493ba3a5", null ],
    [ "my_ad7124", "adc_8c.html#a70314a4fec2987a250bbe3d45b2769c2", null ],
    [ "regNr", "adc_8c.html#a5b8092aa6a64137393d27f2b94b2f260", null ],
    [ "ret", "adc_8c.html#ab9ba0a236922319ce742a327f6ce5b2a", null ],
    [ "rx_pending_data", "adc_8c.html#af665c23d9ad0f51cd1ebae140683b352", null ],
    [ "sample", "adc_8c.html#a26adade2cc728132b49d34103346b631", null ],
    [ "spi_test_cfg", "adc_8c.html#a96865fff4cb7b1f87989bde871546b60", null ]
];