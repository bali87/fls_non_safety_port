/* OPTOFORCE COMMUNICATION PROTOCOL(OCP) LIBRARY v0.1.0 **********************
 *
 *  File:   opto_com.c
 *  Author: gspr
 *
 *  Created on 2016. j�nius 25., 11:30
 *
 * ***************************************************************************
 */

#include <Debug.h>
#include "opto_com.h"
/*#include "globals.h"*/
#include <string.h>    

/*Private function prototipes*/
static EP_State EP_ProcessRx(EP_TypeDef *ep);
static EP_State EP_ProcessTx(EP_TypeDef *ep);
static void 	EP_Reset_Machine(EP_Machine_TypeDef *m);
static void 	EP_Reset_Callbacks(EP_TypeDef *ep);


static uint8_t i;



/*Function declarations*/
EP_State EP_Init(EP_TypeDef *ep, EP_Read_Cb rb, EP_Write_Cb wb,EP_Transport_Init_Cb ti, EP_Trans_Tasks_Cb tc)
{
EP_State ret = EP_ERROR;
    if( (rb == 0) || (wb == 0) || (ti ==0) || (tc == 0))
    {
        ret = EP_ERROR;
    }
    else
    {
        ep->sys_cb.read_buffer = rb;
        ep->sys_cb.write_buffer = wb;
        ep->sys_cb.transport_init = ti;
        ep->sys_cb.transport_tasks = tc;
        EP_Reset_Machine(&ep->machine);
        EP_Reset_Callbacks(ep);
        ep->sys_cb.transport_init();

        ep->tx_bytes = 0u;
        ep->tx_fifo_size = 0u;
        ret = EP_OK;
    }
    return ret;
}

EP_State EP_Subscribe_Cb(EP_TypeDef *ep, uint8_t rw, uint16_t addr, EP_Packet_Cb cb)
{
EP_State ret = EP_ERROR;
  for(i = 0u; i < CALLBACKS; i++)
  {
	if( (uint16_t)0u == ep->packet_cbs[i].p_address)
	{
		ep->packet_cbs[i].p_address = addr;
                ep->packet_cbs[i].p_rw = rw;
		ep->packet_cbs[i].callback = cb;
		ret = EP_OK;
		break;
	}
  }
  return ret;
}

EP_State EP_Unsubscribe_Cb(EP_TypeDef *ep, uint8_t rw, uint16_t addr)
{
EP_State ret = EP_ERROR;
  for (i = 0u; i < CALLBACKS; i++) {
	if (ep->packet_cbs[i].p_address == addr) {
		ep->packet_cbs[i].p_address = 0u;
                ep->packet_cbs[i].p_rw = 0u;
		ep->packet_cbs[i].callback = (EP_Packet_Cb)0u;
		ret = EP_OK;
		break;
	}
  }
  return ret;
}

EP_State EP_Send(EP_TypeDef *ep, uint8_t data_length, uint8_t data[])
{
EP_State ret = EP_ERROR;

        if(0u == data_length)
        {
            ret = EP_OK;
        }
        else
        {
            if ((ep->tx_fifo_size + (int16_t)data_length ) < (int16_t)TX_FIFO_SIZE)
            {
                for(i = 0u; i < data_length; i++)
                {
                    ep->tx_fifo[ep->tx_fifo_size++] = data[i];
                }

            ret = EP_OK;
            }
        }

return ret;
}

EP_State EP_Send_Packet(EP_TypeDef *ep, uint8_t rw, uint16_t addr, uint8_t data_length, uint8_t data[])
{
static uint16_t chk = 0u;
static int16_t tfs = 0;
static uint8_t payload_length = 0u;
EP_State ret = EP_ERROR;
        chk = 0u;
        tfs = ep->tx_fifo_size;
        payload_length = 0u;

        if((uint8_t)READ_REQUEST == rw)
        {
            payload_length = (uint8_t)0u;
        }
        else
        {
            payload_length = data_length;
        }
        if ((ep->tx_fifo_size + ((int16_t)payload_length + 6) ) < (int16_t)TX_FIFO_SIZE)
        {
            ep->tx_fifo[tfs++] = rw;
            ep->tx_fifo[tfs++] = (uint8_t)(addr>>8);
            ep->tx_fifo[tfs++] = (uint8_t)addr;
            ep->tx_fifo[tfs++] = data_length;

            for(i = 0u; i < payload_length; i++)
            {
                ep->tx_fifo[tfs++] = data[i];
            }

            for(i = 0u; i < payload_length + 4u; i++)
            {
                chk += (uint16_t)ep->tx_fifo[ep->tx_fifo_size + (int16_t)i];
            }

            ep->tx_fifo[tfs++] = (uint8_t)(chk>>8);
            ep->tx_fifo[tfs++] = (uint8_t)chk;

            ep->tx_fifo_size = (int16_t)tfs;

            ret = EP_OK;
        }
    

 	return (ret)
 	        ;
}

EP_State EP_Process(EP_TypeDef *ep)
{
EP_State ep_state = EP_ProcessRx(ep);
	ep_state += EP_ProcessTx(ep);

 	return ((ep_state) ? EP_ERROR : EP_OK);
}

EP_State EP_Tasks(EP_TypeDef *ep)
{
    if(ep->sys_cb.transport_tasks != 0)
    {
        ep->sys_cb.transport_tasks();
    }
}

static EP_State EP_ProcessRx(EP_TypeDef *ep)
{
uint16_t j=0u;
static uint8_t in_byte;
  ep->rx_bytes = ep->sys_cb.read_buffer(ep->rx_buffer);     /*  Call CAN_Read_Buffer() */

  for(j = 0u; j < ep->rx_bytes; j++ )
  {

    in_byte = ep->rx_buffer[j];

    switch(ep->machine.rec_phase)
    {
      case EP_PROCESS_HEADER:
			memcpy(&(ep->rx_packet_header[0]), &(ep->rx_packet_header[1]), 3u);
			ep->rx_packet_header[3] = in_byte;

			if((170u == ep->rx_packet_header[0]) || (171u == ep->rx_packet_header[0]))
            {
				ep->machine.rec_rw = ep->rx_packet_header[0];
				ep->machine.rec_addr = ((uint16_t)(ep->rx_packet_header[1]<<8) | (uint16_t)(ep->rx_packet_header[2]));
				ep->machine.rec_payload_length = ep->rx_packet_header[3];

				ep->machine.calc_ch_summ = 0u;

				for(i = 0u; i < 4u; i++)
                {
                    ep->machine.calc_ch_summ += ep->rx_packet_header[i];
				}

                if((ep->machine.rec_payload_length) && (ep->machine.rec_rw != READ_REQUEST))
                {
                    ep->machine.rec_phase = EP_PROCESS_PAYLOAD;
                }
                else
                {
                    ep->machine.rec_phase = EP_READ_CHECKSUMM_HIGH;   
                }

				ep->machine.rec_pos = 0u;

				if(ep->machine.rec_payload_length > (uint8_t)RX_BUFFER_SIZE)
                {
                    ep->state.comerror |= RX_BUFFER_OVERFLOW;
                    ep->machine.rec_phase = EP_PROCESS_HEADER;
				}
			}
		break;

      case EP_PROCESS_PAYLOAD:
			ep->machine.rec_payload[ ep->machine.rec_pos++ ] = in_byte;
			ep->machine.calc_ch_summ += in_byte;

			if(ep->machine.rec_pos == ep->machine.rec_payload_length)
                        {
				ep->machine.rec_phase = EP_READ_CHECKSUMM_HIGH;
			}

            if(255u == ep->machine.rec_pos)
            {
                ep->state.comerror |= RX_BUFFER_OVERFLOW;
            }
      break;

      case EP_READ_CHECKSUMM_HIGH:
			ep->machine.rec_ch_summ = (uint16_t)( ((uint16_t)in_byte)<<8 );
                        ep->machine.rec_phase = EP_READ_CHECKSUMM_LOW;
      break;

      case EP_READ_CHECKSUMM_LOW:
			ep->machine.rec_ch_summ |= (uint16_t)in_byte;
			ep->machine.rec_phase = EP_HANDLE_CALLBACKS;
                        //break;

      case EP_HANDLE_CALLBACKS:
			if(ep->machine.calc_ch_summ == ep->machine.rec_ch_summ)
            {
				for (i = 0u; i < CALLBACKS; i++)
                {
					if ((ep->packet_cbs[i].p_address == ep->machine.rec_addr || ep->packet_cbs[i].p_address == 0xFFFF) && ep->packet_cbs[i].p_rw == ep->machine.rec_rw && ep->packet_cbs[i].callback)
                    {
						ep->packet_cbs[i].callback(ep->machine.rec_rw, ep->machine.rec_addr, ep->machine.rec_payload_length, ep->machine.rec_payload);
						//break;
					}
				}
			}
            else
            {
                if(WRITE_REQUEST == ep->machine.rec_rw)
                {
                    ep->state.comerror |= WRITE_CRC_ERROR;
                }
                else
                {
                    ;
                }
                if(READ_REQUEST == ep->machine.rec_rw)
                {
                    ep->state.comerror |= READ_CRC_ERROR;
                }
                else
                {
                    ;
                }
            }
			EP_Reset_Machine(&ep->machine);
      break;
      default:
      break;
    }
  }
  
  return (EP_OK);
}

static EP_State EP_ProcessTx(EP_TypeDef *ep)
{
EP_State tx_state = EP_ERROR;
    if(ep->tx_fifo_size > 0)
    {
        //print_num_dec(ep->tx_fifo_size);
        //NEWLINE();
        if (ep->tx_fifo_size >= (int16_t)FRAME_SIZE)
        {
                ep->tx_bytes = FRAME_SIZE;
        }
        else
        {
                ep->tx_bytes = ep->tx_fifo_size;
        }

        memcpy(ep->tx_buffer, ep->tx_fifo, ep->tx_bytes);
        tx_state = ep->sys_cb.write_buffer(ep->tx_buffer, ep->tx_bytes);

        if(tx_state == EP_OK)
        {
            memmove(ep->tx_fifo, &ep->tx_fifo[ep->tx_bytes], ep->tx_fifo_size - ep->tx_bytes);
            ep->tx_fifo_size -= (int16_t)ep->tx_bytes;

            if (ep->tx_fifo_size < 0)
            {
                 ep->tx_fifo_size = 0;
            }
        }


    }

return (tx_state);
}

static void EP_Reset_Machine(EP_Machine_TypeDef *m)
{
    m->rec_phase = EP_PROCESS_HEADER;
    m->rec_rw = 0u;
    m->rec_addr = 0u;
    m->rec_payload_length = 0u;
    m->rec_ch_summ = 0u;
    m->calc_ch_summ = 0u;
    m->rec_pos = 0u;
}

static void EP_Reset_Callbacks(EP_TypeDef *ep)
{
  for(i = 0u; i < CALLBACKS; i++)
  {
	ep->packet_cbs[i].p_rw = 0u;
	ep->packet_cbs[i].p_address = 0u;
	ep->packet_cbs[i].callback = (EP_Packet_Cb)0u;
  }

}
