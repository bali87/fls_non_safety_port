/* OPTOFORCE COMMUNICATION PROTOCOL(OCP) LIBRARY v1.0.3 **********************
 * 
 *  File:   opto_com.h
 *  Author: gspr
 *
 *  Created on 2016. j�nius 25., 11:30
 *
 * 
 * THIS LIBRARY IS A LIGHT WEIGHT VERSION OF THE INTER CHIP COMMUNICATION
 * PROTOCOL(ICCP) LIBRARY MADE FOR OPTOFORCE DEVICES.
 *
 * THE LIBRARY USES CALLBACK FUNCTIONS TO INTERACT WIDTH THE APPLICATION LAYER
 * BASED ON THE MEMORY ADDRESS AND THE READ/WRITE FIELD OF THE PACKET.
 *
 *      STRUCTURE:
 *
 *                       --------------------
 *                      /    APPLICATION    /
 *                       --------------------
 *                                |
 *                       --------------------
 *                      /        OCP        /
 *                       --------------------
 *                                |
 *                       --------------------
 *                      /      OPTOLIB      /
 *                       --------------------
 *
 * THE OCP ALSO USES CALLBACKS TO USE THE INTERFACES OF THE USER DEFINED
 * OPTOLIB OR ANY OTHER PERIPHERAL LYBRARY (USART, SPI, I2C, CAN).
 *
 *      PACKET STRUCTURE:
 *
 *      | RW | ADDR_H | ADDR_L | PL_LENGTH | PAYLOAD | CHSUM_H | CHSUM_L |
 *
 * SPECIFICATION: TODO
 *
 * USING THE LIBRARY: TODO
 *
 *****************************************************************************/

#ifndef OPTO_COM_H_
#define OPTO_COM_H_

#ifdef	__cplusplus
extern "C" {
#endif

#include <inttypes.h>
    
#define USB_ERR      0b00000001
#define CAN_ERR      0b00000010
#define UART_ERR     0b00000011
    
/**
* OCP defines
* ****************************************************************************
*/
#define CALLBACKS                           40u
#define RX_BUFFER_SIZE                      255u
#define TX_BUFFER_SIZE                      1024u
#define TX_FIFO_SIZE                        2048u
#define FRAME_SIZE                          64u     /* 128*/

/**
* Read/write dafines
* ****************************************************************************
*/
#define READ_REQUEST                        171u
#define WRITE_REQUEST                       170u
#define AUTO_SEND_REQUEST                   170u

/**
* OD mapp addresses (partial)
* ****************************************************************************
*/
#define DAQ_IDENTITY_OBJ_ADDR               10
#define DAQ_CONFIGURATION_OBJ_ADDR          50
#define DAQ_EEPROM_OBJ_ADDR                 60
#define DAQ_ERROR_OBJ_ADDR                  80
#define DAQ_TPDO_SETTINGS_OBJ_ADDR          100
#define DAQ_NOMINAL_CAPACITIES_OBJ_ADDR     350
#define DAQ_COUNTS_OBJ_ADDR                 450
#define DAQ_TPDO_OBJ_ADDR                   1800
#define DAQ_KEEP_ALIVE_ADDR                 40000

 /**
* OD object sizes in bytes (partial)
* ****************************************************************************
*/
#define DAQ_IDENTITY_OBJ_SIZE               16
#define DAQ_CONFIGURATION_OBJ_SIZE          6
#define DAQ_EEPROM_OBJ_SIZE                 14
#define DAQ_ERROR_OBJ_SIZE                  16
#define DAQ_TPDO_SETTINGS_OBJ_SIZE          4
#define DAQ_NOMINAL_CAPACITIES_OBJ_SIZE     14
#define DAQ_COUNTS_OBJ_SIZE                 14
#define DAQ_TPDO_OBJ_SIZE                   16

//All addresses
#define ADDRESS_ALL                         0xFFFF

#define WRITE_CRC_ERROR                     200u
#define READ_CRC_ERROR                      201u
#define RX_TIMEOT                           202u
#define RX_BUFFER_OVERFLOW                  220u

/* TODO: Rewrite this packet format to the OPTOFORCE protocol
********************************************************************************************************************
** PACKET STRUCTURE:
** |		SOH			|		 ID			|		 CMD		|		 LEN		|		STX
** start of heading 8                   packet ID 16                            command	8                       length of data 8                start of text
** |				DATA					        |		 ETX		|		 CS		|		EOT			|
** data to send 8 * 255 max.                                                    end of text 8                   checksumm 8                     end of transmission 8
*/
/**
* EP State enumeration typedef
*/
typedef enum
{
	EP_OK = 0,
	EP_WARNING,
	EP_ERROR,
	EP_BUSY

} EP_State;

/*Endpoint transport layer init callback*/
typedef void(*EP_Transport_Init_Cb)(void);

/*Endpoint transport layer peripheral tasks callback*/
typedef void(*EP_Trans_Tasks_Cb)(void);

/*Endpoint callback funtion to handle the rx buffer of the endpoint*/
typedef uint16_t(*EP_Read_Cb)(uint8_t rx_buffer[]);

/*Endpoint callback funtion to handle the tx buffer of the endpoint*/
typedef EP_State(*EP_Write_Cb)(uint8_t *tx_buffer, uint8_t length);

/*Endpoint callback function to handle a received packet*/
typedef void(*EP_Packet_Cb)(uint8_t rw, uint16_t addr, uint8_t data_length, uint8_t *data);

/**
* State machine phase enumeration typedef
*/
typedef enum
{
	EP_PROCESS_HEADER = 0,
	EP_PROCESS_PAYLOAD,
	EP_READ_CHECKSUMM_HIGH,
        EP_READ_CHECKSUMM_LOW,
	EP_HANDLE_CALLBACKS

} EP_Machine_Phase;

/**
* System callback function typedef
*/
typedef struct _EP_Scb
{
    EP_Read_Cb              read_buffer;
    EP_Write_Cb             write_buffer;
    EP_Transport_Init_Cb    transport_init;
    EP_Trans_Tasks_Cb       transport_tasks;
            
} EP_Scb_TypeDef;

/**
* Rx packet callback function typedef
*/
typedef struct _EP_Pcb
{
        uint8_t         p_rw;
        uint16_t        p_address;
        EP_Packet_Cb    callback;

} EP_Pcb_TypeDef;

/**
* State machine typedef
*/
typedef struct _EP_Machine
{
	EP_Machine_Phase    rec_phase;
	uint8_t             rec_pos;
        uint8_t             rec_rw;
	uint16_t            rec_addr;
	uint8_t             rec_payload_length;
        uint8_t             rec_payload[RX_BUFFER_SIZE];
	uint16_t            rec_ch_summ;
	uint16_t            calc_ch_summ;

} EP_Machine_TypeDef;

typedef struct _EP_Error
{
    uint8_t comerror;
    uint8_t txerror;
    uint8_t errorcrtl;

} EP_Error_TypeDef;

/**
* Endpoint typedef
*/
typedef struct _EP
{
	uint8_t             rx_buffer[RX_BUFFER_SIZE];
	uint8_t             rx_packet_header[4];
        uint8_t             tx_buffer[TX_BUFFER_SIZE];
        uint8_t             tx_fifo[TX_FIFO_SIZE];
        int16_t             tx_fifo_size;
        uint16_t             rx_bytes;
        uint16_t             tx_bytes;
        EP_Scb_TypeDef      sys_cb;
	EP_Pcb_TypeDef      packet_cbs[CALLBACKS];
	EP_Machine_TypeDef  machine;
        EP_Error_TypeDef    state;

} EP_TypeDef;

/**
* Functions
* ****************************************************************
*/
EP_State EP_Init(EP_TypeDef *ep, EP_Read_Cb rb, EP_Write_Cb wb,EP_Transport_Init_Cb ti, EP_Trans_Tasks_Cb tc);
EP_State EP_Subscribe_Cb(EP_TypeDef *ep, uint8_t rw, uint16_t addr, EP_Packet_Cb cb);
EP_State EP_Unsubscribe_Cb(EP_TypeDef *ep, uint8_t rw, uint16_t addr);
EP_State EP_Send(EP_TypeDef *ep, uint8_t data_length, uint8_t data[]);
EP_State EP_Send_Packet(EP_TypeDef *ep, uint8_t rw, uint16_t addr, uint8_t data_length, uint8_t data[]);
EP_State EP_Process(EP_TypeDef *ep);
EP_State EP_Tasks(EP_TypeDef *ep);

#ifdef	__cplusplus
}
#endif

#endif /* OPTO_COM_H_ */
