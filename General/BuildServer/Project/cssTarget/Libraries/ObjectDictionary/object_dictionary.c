#include "Libraries/ObjectDictionary/object_dictionary.h"


#define SET_TPDO(OFFSET,T_ADDR,T_LEN,T_EN) OD16w(OD_TPDO_SETTINGS_ADDR+OFFSET*4, T_ADDR); OD[OD_TPDO_SETTINGS_ADDR+OFFSET*4+2] = T_LEN;	OD[OD_TPDO_SETTINGS_ADDR+OFFSET*4+3] = T_EN;

unsigned char TPDO_req = 0;
unsigned int  OD_TPDO_Addr = 1800;
unsigned char OD_TPDO_Len = 0;
unsigned char TPDO_En = 1;		

unsigned char OD[OD_SIZE];
uint8_t OD_TPDO_speed = 10u;
uint16_t OD_TPDO_Msg_Cnt;

const char * serial = "SAFETYS0";

void OD_Init_Dictionary()
{
uint8_t i=0;
#if 0
    const char* temp_serial = _expand(SENSOR);
    strcpy(daq.DQserial,temp_serial);               /* This initialize DQserial value to the SENSOR */
                                                   /*+ \0 + 32 (if 6 char long otherwise just SENSOR) */
#endif
    OD[OD_IDENTITY_ADDR+0] = 64;            /*PRODUCT_TYPE;           Product Type - OMD */
    OD[OD_IDENTITY_ADDR+1] = 3;             /*SENSOR_NUMBER;         Sensor number */
    OD[OD_IDENTITY_ADDR+2] = 0b00000010;    /*ComType;         CommType - USB */
    OD[OD_IDENTITY_ADDR+3] = 160;           /*hw_version;         Hw version */
    OD[OD_IDENTITY_ADDR+4] = 206;           /*FIRMWARE_VER;      Fw version */
    OD[OD_IDENTITY_ADDR+5] = 105;           /*PROTOCOL_VER;          Protocol version */
    OD[OD_IDENTITY_ADDR+6] = 60;            /*hw_type;           Actual PCB type read from the resistor divider */
    OD[OD_IDENTITY_ADDR+7] = 60;            /*HW_TYPE;              Required HW_Type by the firmware */
    for(i=0;i<8;i++)
    {
        OD[(OD_IDENTITY_ADDR+8+i)] = serial[i];
    }
    SET_TPDO(0, OD_TPDO_ADDR, OD_TPDO_Len, TPDO_En);/* TPDO settings  */
    SET_TPDO(1,1200,4,1);                           /* T_REGION_1   Sample counter and Status */
    SET_TPDO(2,1400,12,1);                      /* T_REGION_2   FFx,FFy,FFz,Tx,Ty,Tz */
    SET_TPDO(3,1500,10*4,0);                    /* T_REGION_3   rS1,rS2,rS3,rS4,rT * 4 */
    SET_TPDO(4,1600,10*4,0);                    /* T_REGION_4   cS1,cS2,cS3,cS4,cT * 4 */
    SET_TPDO(5,1608,2,0);                       /* T_REGION_5   cT_1 */
    SET_TPDO(6,1618,2,0);                       /* T_REGION_6   cT_2 */
    SET_TPDO(7,1628,2,0);                       /* T_REGION_7   cT_3 */
    SET_TPDO(8,1638,2,0);                       /* T_REGION_8   cT_4 */
#if 0
    MAP_STRING8(OD_IDENTITY_ADDR+8, daq.DQserial);  /* DAQ's serial */
            
    OD[OD_CONFIG_ADDR+0] = OD_TPDO_speed;           /* TPDO speed - disabled */
    OD[OD_CONFIG_ADDR+1] = FILTER;                       /* Filter - 15 Hz */
    OD[OD_CONFIG_ADDR+2] = 0;                       /* Zero - 0 - Unzero all, 255 - Zero all */

    OD[OD_ERROR_ADDR+12] = DIV_HW_Version;
    OD[OD_ERROR_ADDR+13] = DIV_HW_Type;

    daq.CompMODE = 0;
    
#ifdef GainCMP_1
    daq.CompMODE += 1;
#endif
#ifdef TempCMP_1
    daq.CompMODE += 2;
#endif
#ifdef LinCMP_1
    daq.CompMODE += 4;
#endif
#if POLY_COMP == 1
    daq.CompMODE += 8;
#endif
#if CT_COMP == 1
    daq.CompMODE += 16;
#endif
#if COMP_TYPE == PCA
    daq.CompMODE += 32;
#endif
    
    OD[OD_CONFIG_ADDR+4] = daq.CompMODE;            /* Compensation Mode 0 - RAW, 1 - Gain, 2 - Temp, 4 - Lin */
    OD[OD_CONFIG_ADDR+5] = 0;                       /* Debug Mode 0 - Release, 5 - Release + Temperature 10 - Debug */
       
    OD16w(OD_EEPROM_ADDR+0, NODE_ID_RX);             /* NODE_ID_RX */
    OD16w(OD_EEPROM_ADDR+2, NODE_ID_TX);             /* NODE_ID_TX */
    
    OD[OD_EEPROM_ADDR+4+0] = EEPROM[0];
    OD[OD_EEPROM_ADDR+4+1] = EEPROM[1];
    OD[OD_EEPROM_ADDR+4+2] = EEPROM[2];
    OD[OD_EEPROM_ADDR+4+3] = EEPROM[3];

    OD[OD_EEPROM_ADDR+8] = CAN_speed;
    OD[OD_EEPROM_ADDR+9] = 0;                       /* Has been removed after POC from EEver 2 */

    OD16w(OD_EEPROM_ADDR+10,POWER_ON_Counter);      /* POWER_ON_Counter */
    OD16w(OD_EEPROM_ADDR+12,UART_speed);            /* UART_speed */

    OD[OD_EEPROM_ADDR + 15] = Axis_Conv;

    OD[OD_ERROR_ADDR+0] = 0;                        /* ComErr - no error */
    OD[OD_ERROR_ADDR+1] = 0;                        /* DQ_Error - no error */
    OD[OD_ERROR_ADDR+2] = 0;                        /* DQ_Error - no error */
    OD[OD_ERROR_ADDR+3] = 0;                        /* DQ_Error - no error */
    OD[OD_ERROR_ADDR+14] = HW_VER_FROM;             /* DQ_Error - HW_Version_FROM */
    OD[OD_ERROR_ADDR+15] = HW_VER_TO;               /* DQ_Error - HW_Version_TO */

    SET_TPDO(0, OD_TPDO_ADDR, OD_TPDO_Len, TPDO_En);/* TPDO settings */
    SET_TPDO(1,1200,4,1);                           /* T_REGION_1   Sample counter and Status */

    if (PRODUCT_TYPE == 31)
    {
        SET_TPDO(2,1300,6*1,1);                     /* T_REGION_2   Fx,Fy,Fz * Snum */
        SET_TPDO(3,1500,10*1,0);                    /* T_REGION_3   rS1,rS2,rS3,rS4,rT * Snum */
        SET_TPDO(4,1600,10*1,0);                    /* T_REGION_4   cS1,cS2,cS3,cS4,cT * Snum */
        SET_TPDO(5,1608,2,0);                       /* T_REGION_5   cT_1 */
    }
    else if (PRODUCT_TYPE == 34)                    /*TODO If API supports then 34 should use Snum instead of hardwiring */
    {
        SET_TPDO(2,1300,6*4,1);                     /* T_REGION_2   Fx,Fy,Fz * Snum */
        SET_TPDO(3,1500,10*4,0);                    /* T_REGION_3   rS1,rS2,rS3,rS4,rT * Snum */
        SET_TPDO(4,1600,10*4,0);                    /* T_REGION_4   cS1,cS2,cS3,cS4,cT * Snum */
        SET_TPDO(5,1608,2,0);                       /* T_REGION_5   cT_1 */
        SET_TPDO(6,1618,2,0);                       /* T_REGION_6   cT_2 */
        SET_TPDO(7,1628,2,0);                       /* T_REGION_7   cT_3 */
        SET_TPDO(8,1638,2,0);                       /* T_REGION_8   cT_4 */
    }
    else if (PRODUCT_TYPE == 64)
    {
        SET_TPDO(2,1400,12,1);                      /* T_REGION_2   FFx,FFy,FFz,Tx,Ty,Tz */
        SET_TPDO(3,1500,10*4,0);                    /* T_REGION_3   rS1,rS2,rS3,rS4,rT * 4 */
        SET_TPDO(4,1600,10*4,0);                    /* T_REGION_4   cS1,cS2,cS3,cS4,cT * 4 */
        SET_TPDO(5,1608,2,0);                       /* T_REGION_5   cT_1 */
        SET_TPDO(6,1618,2,0);                       /* T_REGION_6   cT_2 */
        SET_TPDO(7,1628,2,0);                       /* T_REGION_7   cT_3 */
        SET_TPDO(8,1638,2,0);                       /* T_REGION_8   cT_4 */
    }

    OD16w(OD_PROPERTIES_OMD1_ADDR+0,1111);                      /* OMD1 status */
    MAP_STRING8(OD_PROPERTIES_OMD1_ADDR+2, daq.OMD1serial);          /* OMD1 serial */

    OD16w(OD_PROPERTIES_OMD1_ADDR+12,2222);                     /* OMD2 status */
    MAP_STRING8(OD_PROPERTIES_OMD1_ADDR+14, daq.OMD2serial);         /* OMD2 serial */

    OD16w(OD_PROPERTIES_OMD1_ADDR+24,3333);                     /* OMD3 status */
    MAP_STRING8(OD_PROPERTIES_OMD1_ADDR+26, daq.OMD3serial);         /* OMD3 serial */

    OD16w(OD_PROPERTIES_OMD1_ADDR+36,4444);                     /* OMD4 status */
    MAP_STRING8(OD_PROPERTIES_OMD1_ADDR+38, daq.OMD4serial);         /* OMD4 serial */

    OD16w(OD_PROPERTIES_OMD1_ADDR+72,6666);                     /* HEX1 status */
    MAP_STRING8(OD_PROPERTIES_OMD1_ADDR+74, daq.HEX1serial);         /* HEX1 serial */
#endif
}
#if 0
void OD_Update_Dictionary()
{

    MAP_OD_1(OD_PROPERTIES_OMD1_ADDR, SNR_Omd[1].status.word);
    MAP_OD_1(OD_PROPERTIES_OMD2_ADDR, SNR_Omd[2].status.word);
    MAP_OD_1(OD_PROPERTIES_OMD3_ADDR, SNR_Omd[3].status.word);
    MAP_OD_1(OD_PROPERTIES_OMD4_ADDR, SNR_Omd[4].status.word);

    MAP_OD_1(OD_PROPERTIES_HEX_ADDR, SNR_Hex[1].status.word);
    MAP_OD_2(OD_RELEASE_SAMPLE_CTR_ADDR, OD_TPDO_Msg_Cnt, SNR_Global_Stat.word);

    MAP_OD_3(OD_RELEASE_CTS_OMD1_ADDR, SNR_Omd[1].zFx, SNR_Omd[1].zFy, SNR_Omd[1].zFz );
    MAP_OD_3(OD_RELEASE_CTS_OMD2_ADDR, SNR_Omd[2].zFx, SNR_Omd[2].zFy, SNR_Omd[2].zFz );
    MAP_OD_3(OD_RELEASE_CTS_OMD3_ADDR, SNR_Omd[3].zFx, SNR_Omd[3].zFy, SNR_Omd[3].zFz );
    MAP_OD_3(OD_RELEASE_CTS_OMD4_ADDR, SNR_Omd[4].zFx, SNR_Omd[4].zFy, SNR_Omd[4].zFz );

    MAP_OD_6(OD_RELEASE_CTS_HEX_ADDR, SNR_Hex[1].zFx, SNR_Hex[1].zFy, SNR_Hex[1].zFz, SNR_Hex[1].zTx, SNR_Hex[1].zTy, SNR_Hex[1].zTz);

    MAP_OD_5(OD_DEBUG_RAW_OMD1_ADDR, SNR_Omd[1].rS[1], SNR_Omd[1].rS[2], SNR_Omd[1].rS[3], SNR_Omd[1].rS[4], SNR_Omd[1].rS[5] );
    MAP_OD_5(OD_DEBUG_RAW_OMD2_ADDR, SNR_Omd[2].rS[1], SNR_Omd[2].rS[2], SNR_Omd[2].rS[3], SNR_Omd[2].rS[4], SNR_Omd[2].rS[5] );
    MAP_OD_5(OD_DEBUG_RAW_OMD3_ADDR, SNR_Omd[3].rS[1], SNR_Omd[3].rS[2], SNR_Omd[3].rS[3], SNR_Omd[3].rS[4], SNR_Omd[3].rS[5] );
    MAP_OD_5(OD_DEBUG_RAW_OMD4_ADDR, SNR_Omd[4].rS[1], SNR_Omd[4].rS[2], SNR_Omd[4].rS[3], SNR_Omd[4].rS[4], SNR_Omd[4].rS[5] );
                  
    MAP_OD_5(OD_DEBUG_COMP_OMD1_ADDR, SNR_Omd[1].cS[1], SNR_Omd[1].cS[2], SNR_Omd[1].cS[3], SNR_Omd[1].cS[4], SNR_Omd[1].cS[5] );
    MAP_OD_5(OD_DEBUG_COMP_OMD2_ADDR, SNR_Omd[2].cS[1], SNR_Omd[2].cS[2], SNR_Omd[2].cS[3], SNR_Omd[2].cS[4], SNR_Omd[2].cS[5] );
    MAP_OD_5(OD_DEBUG_COMP_OMD3_ADDR, SNR_Omd[3].cS[1], SNR_Omd[3].cS[2], SNR_Omd[3].cS[3], SNR_Omd[3].cS[4], SNR_Omd[3].cS[5] );
    MAP_OD_5(OD_DEBUG_COMP_OMD4_ADDR, SNR_Omd[4].cS[1], SNR_Omd[4].cS[2], SNR_Omd[4].cS[3], SNR_Omd[4].cS[4], SNR_Omd[4].cS[5] );

    OD16w(OD_EEPROM_ADDR+10, POWER_ON_Counter);

#if (COMM_TYPE & USB)
    OD[OD_ERROR_ADDR+0] |= ocp_usb_ep.state.comerror;
    OD[OD_ERROR_ADDR+2] |= ocp_usb_ep.state.txerror;
    OD[OD_ERROR_ADDR+3] |= ocp_usb_ep.state.errorcrtl;
#endif
#if (COMM_TYPE & UART)
    OD[OD_ERROR_ADDR+0] |= ocp_uart_ep.state.comerror;
    OD[OD_ERROR_ADDR+2] |= ocp_uart_ep.state.txerror;
    OD[OD_ERROR_ADDR+3] |= ocp_uart_ep.state.errorcrtl;
#endif
#if (COMM_TYPE & CAN)
    OD[OD_ERROR_ADDR+0] |= ocp_can_ep.state.comerror;
    OD[OD_ERROR_ADDR+2] |= ocp_can_ep.state.txerror;
    OD[OD_ERROR_ADDR+3] |= ocp_can_ep.state.errorcrtl;
#endif
    
    OD[OD_ERROR_ADDR+1] = daq.DAQError;

    OD16w(OD_ERROR_ADDR+4, (1000 - iter_us));
    OD16w(OD_ERROR_ADDR+6, (1000 - iter_max_us));
    
#if   HW_TYPE_4CH_5x == 1
    OD16w(OD_ERROR_ADDR+8, daq.ref_5V);
    OD16w(OD_ERROR_ADDR+10, daq.ref_4V);
#else
    OD16w(OD_ERROR_ADDR+8, daq.ref_5V);
    OD16w(OD_ERROR_ADDR+10, daq.ref_4V);    /*OD16w(OD_ERROR_ADDR+10, ref_4V); */
#endif
            
    OD[OD_ERROR_ADDR+12] = DIV_HW_Version;
    OD[OD_ERROR_ADDR+13] = DIV_HW_Type;

    OD[OD_CONFIG_ADDR+4] = daq.CompMODE;

    OD_TPDO_Addr = OD16r(OD_TPDO_SETTINGS_ADDR+0);	/* TPDO Addr */
    OD_TPDO_Len = OD[OD_TPDO_SETTINGS_ADDR+2];

    OD_TPDO_Len = OD_Generate_TPDO_Mapping(8);
    if (OD_TPDO_Len > 240) OD_TPDO_Len = 240;
    OD[OD_TPDO_SETTINGS_ADDR+2] = OD_TPDO_Len;
}

void Update_OD_from_EEPROM()
{
    NODE_ID_RX = EE16r(16+0) ;    /*NODE_ID_RX read from EEPROM */
    NODE_ID_TX = EE16r(16+2);     /*NODE_ID_TX from EEPROM */


    if ((NODE_ID_RX<=0x0) || (NODE_ID_RX>0x7ff)) NODE_ID_RX = 0x101;
    if ((NODE_ID_TX<=0x0) || (NODE_ID_TX>0x7ff)) NODE_ID_TX = 0x100;


    EE16w(16+0,NODE_ID_RX);
    EE16w(16+2,NODE_ID_TX);

    CAN_speed = EEdata[16+4];
    UART_speed = EE16r(16+6);
    Axis_Conv = EEdata[16+14];    /*OD[OD_EEPROM_ADDR + 15] = EEdata[16+14]; */

    OD_TPDO_speed = EEdata[16+9];
    FILTER = EEdata[16+10];
    ComType = EEdata[16+12];

    EEdata[16+12] = ComType;
    POWER_ON_Counter =  EE16r(14);
}

uint8_t OD_Generate_TPDO_Mapping(uint8_t max_addresses)
{
    OD_TPDO_Len = 0;
    TPDO_En = OD[OD_TPDO_SETTINGS_ADDR+3];
    if (TPDO_En>0)				/* Globally disable the TPDO by TPDO_En */
    {
        int m;
        for (m=0; m<max_addresses; m++)		/* Up to "max_addresses" addresses are used */
        {
            int T_Addr;
            int T_Len;
            int T_En;
            T_Addr = OD16r(OD_TPDO_SETTINGS_ADDR+4+(m*4));
            T_Len = OD[OD_TPDO_SETTINGS_ADDR+6+(m*4)];
            T_En = OD[OD_TPDO_SETTINGS_ADDR+7+(m*4)];
            if (T_En>0)     /* If this region is enabled copy the area */
            { 
                    memcpy(&OD[OD_TPDO_Addr+OD_TPDO_Len],&OD[T_Addr],T_Len);
                    OD_TPDO_Len += T_Len;
            }	
        } 
    }
    return OD_TPDO_Len;
}
#endif
