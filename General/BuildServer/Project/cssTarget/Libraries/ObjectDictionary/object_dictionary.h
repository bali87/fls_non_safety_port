/* 
 * File:   object_dictionary.h
 * Author: Pinkoczi Gaspar
 *
 * Created on 2017. janu�r 17., 16:43
 */

#ifndef OBJECT_DICTIONARY_H
#define	OBJECT_DICTIONARY_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <inttypes.h>

/* Object dictionarry size */
#define OD_SIZE     4096

/* DAQ Object addresses */
#define OD_IDENTITY_ADDR            10
#define OD_CONFIG_ADDR              50
#define OD_EEPROM_ADDR              60
#define OD_ERROR_ADDR               80
#define OD_TPDO_SETTINGS_ADDR       100
#define OD_TPDO_REGION_ONE_ADDR     104
#define OD_TPDO_REGION_TWO_ADDR     108
#define OD_TPDO_REGION_THREE_ADDR   112
#define OD_TPDO_REGION_FOUR_ADDR    116
#define OD_TPDO_REGION_FIVE_ADDR    120
#define OD_TPDO_REGION_SIX_ADDR     124
#define OD_TPDO_REGION_SEVEN_ADDR   128
#define OD_TPDO_REGION_EIGHT_ADDR   132
#define OD_CALIBRATION_ADDR         150
#define OD_RESTART_ADDR             168

/* Sensor Properties object addresses */
#define OD_PROPERTIES_OMD1_ADDR     200
#define OD_PROPERTIES_OMD2_ADDR     212
#define OD_PROPERTIES_OMD3_ADDR     224
#define OD_PROPERTIES_OMD4_ADDR     236
#define OD_PROPERTIES_HEX_ADDR      272

/* Nominal Capacity object addresses */
#define OD_NC_SETT_ADDR             300
#define OD_NC_OMD1_ADDR             302
#define OD_NC_OMD2_ADDR             310
#define OD_NC_OMD3_ADDR             318
#define OD_NC_OMD4_ADDR             326
#define OD_NC_HEX_ADDR              350

/* Counts object addresses */
#define OD_CT_OMD1_ADDR             400
#define OD_CT_OMD2_ADDR             408
#define OD_CT_OMD3_ADDR             416
#define OD_CT_OMD4_ADDR             424
#define OD_CT_HEX_ADDR              450

/* Gain/offset object addresses */
#define OD_GAIN_OS_SETTINGS_ADDR    600
#define OD_GAIN_OS_OMD1_ADDR        602
#define OD_GAIN_OS_OMD2_ADDR        612
#define OD_GAIN_OS_OMD3_ADDR        622
#define OD_GAIN_OS_OMD4_ADDR        632

/* Gain multiplier object addresses */
#define OD_GAIN_MULT_SETTINGS_ADDR  668
#define OD_GAIN_MULT_OMD1_ADDR      670
#define OD_GAIN_MULT_OMD2_ADDR      678
#define OD_GAIN_MULT_OMD3_ADDR      686
#define OD_GAIN_MULT_OMD4_ADDR      694

/* Temp offset object addresses */
#define OD_TEMP_OS_SETTINGS_ADDR    724
#define OD_TEMP_OS_OMD1_ADDR        726
#define OD_TEMP_OS_OMD2_ADDR        734
#define OD_TEMP_OS_OMD3_ADDR        742
#define OD_TEMP_OS_OMD4_ADDR        750

/* Temp multiplier object addresses */
#define OD_TEMP_MULT_SETTINGS_ADDR  780
#define OD_TEMP_MULT_OMD1_ADDR      782
#define OD_TEMP_MULT_OMD2_ADDR      790
#define OD_TEMP_MULT_OMD3_ADDR      798
#define OD_TEMP_MULT_OMD4_ADDR      806

/* Temp quadratic object addresses */
#define OD_TEMP_QUAD_SETTINGS_ADDR  836
#define OD_TEMP_QUAD_OMD1_ADDR      838
#define OD_TEMP_QUAD_OMD2_ADDR      846
#define OD_TEMP_QUAD_OMD3_ADDR      854
#define OD_TEMP_QUAD_OMD4_ADDR      862

/* Lin offset object addresses */
#define OD_LIN_OS_SETTINGS_ADDR     892
#define OD_LIN_OS_OMD1_ADDR         894
#define OD_LIN_OS_OMD2_ADDR         902
#define OD_LIN_OS_OMD3_ADDR         910
#define OD_LIN_OS_OMD4_ADDR         918

/* Lin multiplier object addresses */
#define OD_LIN_MULT_SETTINGS_ADDR   948
#define OD_LIN_MULT_OMD1_ADDR       950
#define OD_LIN_MULT_OMD2_ADDR       958
#define OD_LIN_MULT_OMD3_ADDR       966
#define OD_LIN_MULT_OMD4_ADDR       974

/* Lin quadratic object addresses */
#define OD_LIN_QUAD_SETTINGS_ADDR   1004
#define OD_LIN_QUAD_OMD1_ADDR       1006
#define OD_LIN_QUAD_OMD2_ADDR       1014
#define OD_LIN_QUAD_OMD3_ADDR       1022
#define OD_LIN_QUAD_OMD4_ADDR       1030

/* Release object addresses */
#define OD_RELEASE_SAMPLE_CTR_ADDR  1200
#define OD_RELEASE_GEN_STAT_ADDR    1202
#define OD_RELEASE_CTS_OMD1_ADDR    1300
#define OD_RELEASE_CTS_OMD2_ADDR    1306
#define OD_RELEASE_CTS_OMD3_ADDR    1312
#define OD_RELEASE_CTS_OMD4_ADDR    1318
#define OD_RELEASE_CTS_HEX_ADDR     1400

/* Debug object addresses */
#define OD_DEBUG_RAW_OMD1_ADDR      1500
#define OD_DEBUG_RAW_OMD2_ADDR      1510
#define OD_DEBUG_RAW_OMD3_ADDR      1520
#define OD_DEBUG_RAW_OMD4_ADDR      1530
#define OD_DEBUG_COMP_OMD1_ADDR     1600
#define OD_DEBUG_COMP_OMD2_ADDR     1610
#define OD_DEBUG_COMP_OMD3_ADDR     1620
#define OD_DEBUG_COMP_OMD4_ADDR     1630

/* TPDO object address */
#define OD_TPDO_ADDR                1800
    
/* Challenge address */
#define OD_CHALLENGE_ADDR           4022u

#define MAP_STRING8(offset,str_arr)   OD[offset+0]=str_arr[0]; OD[offset+1]=str_arr[1]; OD[offset+2]=str_arr[2]; OD[offset+3]=str_arr[3]; OD[offset+4]=str_arr[4]; OD[offset+5]=str_arr[5]; OD[offset+6]=str_arr[6]; OD[offset+7]=str_arr[7];

#define LO_Low(param)  ((char *)&param)[0]
#define LO_High(param) ((char *)&param)[1]
#define HI_Low(param)  ((char *)&param)[2]
#define HI_High(param) ((char *)&param)[3]

#define NO_ERROR  0b000
#define DAQ_ERROR 0b001
#define COM_ERROR 0b010

#define EEPROM_ERROR    0b001
#define POWER_5V_ERROR  0b010
#define REF_4V_ERROR    0b100



/* Write 16 bits to the object dictionary */
#define OD16w(START,VAR16) do{tmp = VAR16; OD[START] = LO_High(tmp); OD[START+1] = tmp;}while(0);

/* Read 16 bits from the object dictionary */
#define OD16r(START)	OD[START] * 256 + OD[START+1]

/* Mapping functions */
#define MAP_OD_1(STR,V1) OD16w(STR,V1);
#define MAP_OD_2(STR,V1,V2) OD16w(STR,V1); OD16w(STR+2,V2);
#define MAP_OD_3(STR,V1,V2,V3) OD16w(STR,V1); OD16w(STR+2,V2); OD16w(STR+4,V3);
#define MAP_OD_4(STR,V1,V2,V3,V4) OD16w(STR,V1); OD16w(STR+2,V2); OD16w(STR+4,V3); OD16w(STR+6,V4);
#define MAP_OD_5(STR,V1,V2,V3,V4,V5) OD16w(STR,V1); OD16w(STR+2,V2); OD16w(STR+4,V3); OD16w(STR+6,V4); OD16w(STR+8,V5);
#define MAP_OD_6(STR,V1,V2,V3,V4,V5,V6) OD16w(STR,V1); OD16w(STR+2,V2); OD16w(STR+4,V3); OD16w(STR+6,V4); OD16w(STR+8,V5); OD16w(STR+10,V6);
#define MAP_OD_7(STR,V1,V2,V3,V4,V5,V6,V7) OD16w(STR,V1); OD16w(STR+2,V2); OD16w(STR+4,V3); OD16w(STR+6,V4); OD16w(STR+8,V5); OD16w(STR+10,V6); OD16w(STR+12,V7);

/* Object Dictionary */
extern unsigned char OD[OD_SIZE];

unsigned int tmp;
extern uint8_t OD_TPDO_speed;
extern uint16_t OD_TPDO_Msg_Cnt;

extern unsigned char TPDO_req;
extern unsigned int  OD_TPDO_Addr;
extern unsigned char OD_TPDO_Len;
extern unsigned char TPDO_En;

/* Init the Object dictionary */
void OD_Init_Dictionary(void);

/* Update from variable */
void OD_Update_Dictionary(void);

/* Update from eeprom */
void Update_OD_from_EEPROM(void);

uint8_t OD_Generate_TPDO_Mapping(uint8_t max_addresses);

#ifdef	__cplusplus
}
#endif

#endif	/* OBJECT_DICTIONARY_H */

