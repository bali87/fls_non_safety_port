/**********************************************************************
* (C) Copyright 2017, OptoForce Ltd.
* -------------------------------------------------------------------
* \verbatim
*
* OPTOFORCE LIMITED PROPRIETARY INFORMATION
*
* Property of OptoForce ltd, Unauthorized reproduction and/or
* distribution is strictly prohibited. This product is protected
* under copyright law and trade secret law as an unpublished
* work
* (C) Copyright OptoForce. All rights reserved.
* \endverbatim
* -------------------------------------------------------------------
*
* -------------------------------------------------------------------
* FILE DESCRIPTION
* -------------------------------------------------------------------
* File: my_utils.h
* Component: -
* Module: -
* Generator: -
*
* Description: Utils for code testing and debugging interface
*
*---------------------------------------------------------------------
*---------------------------------------------------------------------
* Author: Vincze Balazs
*---------------------------------------------------------------------
* Revision History
*---------------------------------------------------------------------
* Version   Date     Author  Change       ID Description
*---------------------------------------------------------------------
* 00.01.00 2017. szept. 18. Vincze Balazs 0000000000000 Initial Version
*-------------------------------------------------------------------*/
#ifndef DEBUG_H_
#define DEBUG_H_


#include <string.h>
#include <inttypes.h>
#include <stdio.h>
#include "../../../../Software/UnitDesignAndImpl/Implementation/Sources/L4MicrocontrollerAbstraction/CommunicationDrivers/CAN/CANDrv.h"
extern char _debug_tmp;
int32_t Debug_WriteSCI(char * pBuff,uint32_t buffLen);
int32_t Debug_WriteCAN(char * pBuff,uint32_t buffLen);

#if 0
#define PUTCHAR(_chr)       do{ _debug_tmp = _chr;Debug_WriteCAN(&_debug_tmp,1U);  }while(0)                                                        /* Macro to send one byte on SCI port */
#define PUTS(_str)          do{ Debug_WriteCAN(_str,strlen(_str));    }while(0)
#elif 0
#define PUTCHAR(_chr)       do{ _debug_tmp = _chr;Debug_WriteSCI(&_debug_tmp,1U);  }while(0)                                                       /* Macro to send one byte on SCI port */
#define PUTS(_str)          do{ Debug_WriteSCI(_str,strlen(_str));    }while(0)
#elif   0
#define PUTCHAR(_chr)       do{ _debug_tmp = _chr;CAN_Write_Buffer(NODE_ID_TX,(unsigned char *)&_debug_tmp,1U);  }while(0)                                                       /* Macro to send one byte on SCI port */
#define PUTS(_str)          do{ CAN_Write_Buffer(NODE_ID_TX,(unsigned char *)_str,strlen(_str));    }while(0)
#else
#define PUTCHAR(_chr)       do{putchar(_chr);  }while(0)                                                       /* Macro to send one byte on SCI port */
#define PUTS(_str)          do{ puts(_str);   }while(0)
#endif


#define ERROR()             do{ _puts("\r\n!!!UNHANDLED ERROR!!!\r\n");_puts((char *)__FILE__);_puts("\r\nFunction: ");\
                                _puts((char *)__FUNCTION__);_puts("\r\nLine: ");print_num_dec(__LINE__);}while(0)
#define ERROR_HALT()        do{    ERROR();do{;}while(1);  }while(0)                                                            /* Simple infinitive loop macro */
#define SPACE()             do{ PUTCHAR(' ');}while(0)
#define NEWLINE()           do{ _puts("\r\n" );}while(0)

void print_byte_hex(uint8_t dat);
void print_byte_dec(uint8_t dat);
void print_word_hex(uint16_t dat);
void print_int_hex(uint32_t dat);
void _puts(char * ptr);
void print_num_dec(uint32_t dat);
#endif /* DEBUG_H_ */
