################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/ADC/SignalComp.c \
../Drivers/ADC/SignalFilter.c 

C_DEPS += \
./Drivers/ADC/SignalComp.d \
./Drivers/ADC/SignalFilter.d 

OBJS += \
./Drivers/ADC/SignalComp.obj \
./Drivers/ADC/SignalFilter.obj 

OBJS__QUOTED += \
"Drivers\ADC\SignalComp.obj" \
"Drivers\ADC\SignalFilter.obj" 

C_DEPS__QUOTED += \
"Drivers\ADC\SignalComp.d" \
"Drivers\ADC\SignalFilter.d" 

C_SRCS__QUOTED += \
"../Drivers/ADC/SignalComp.c" \
"../Drivers/ADC/SignalFilter.c" 


