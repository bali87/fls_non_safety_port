message("Using xc16 compiler on windows (with cygwin) ...")

set(CMAKE_SYSTEM_NAME Linux)

set(PIC24 TRUE)
set(TARGET PIC24)
set(GCC TRUE)

SET(CMAKE_CXX_OUTPUT_EXTENSION ".o")
set(CMAKE_C_OUTPUT_EXTENSION ".o")

set(CMAKE_C_COMPILER "${PROJECT_SOURCE_DIR}/../Production/xc16.sh")
set(XC_LINKER "${PROJECT_SOURCE_DIR}/../Production/xc16linker_sim.sh")
set(CMAKE_C_COMPILER_WORKS TRUE CACHE INTERNAL "" FORCE)
set(CMAKE_C_COMPILER_FORCED TRUE)
set(CMAKE_USE_RELATIVE_PATHS TRUE)


set(XC_CHIP "24FJ256GA702")


set(CMAKE_XC_COMPILE_FLAGS "${CMAKE_C_FLAGS} -mcpu=${XC_CHIP} -MMD -mno-eds-warn -g -omf=elf -fast-math -DXPRJ_default=default -legacy-libc -std=gnu99 -mfillupper=0 -mlarge-code -O1 -msmart-io=1 -Wall -msfr-warn=off")

set(CMAKE_XC_LINK_FLAGS "${CMAKE_C_FLAGS} -mcpu=${XC_CHIP} -omf=elf -fast-math -legacy-libc -std=gnu99 -Wl,--local-stack,,--defsym=__MPLAB_BUILD=1,--script=p24FJ256GA702.gld,--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--gc-sections,--fill-upper=0,--stackguard=16,--no-ivt,--no-force-link,--smart-io,--report-mem,--memorysummary,memoryfiles.xml ") 


set(CMAKE_C_CREATE_STATIC_LIBRARY "xc16-ar -omf=elf -r <TARGET> <OBJECTS> <LINK_FLAGS>")

set(CMAKE_C_COMPILE_OBJECT "<CMAKE_C_COMPILER> <SOURCE> -o <OBJECT> -c ${CMAKE_XC_COMPILE_FLAGS} <INCLUDES> <DEFINES> <FLAGS>")

set(DEBUG_SUFFIX "elf")
set(CMAKE_C_LINK_EXECUTABLE "${XC_LINKER} -o <TARGET>.${DEBUG_SUFFIX} <OBJECTS> ${CMAKE_XC_LINK_FLAGS} <LINK_LIBRARIES>")
set(CMOCKA_LIB ../../../Software/UnitDesignAndImpl/Implementation/Libraries/Cmocka/libcmocka.o)


   
