message("Using ti-arm-none-eabi compiler on windows (with cygwin) ...")


set(CMAKE_SYSTEM_NAME Linux)



set(TARGET RM48)
set(GCC TRUE)

SET(CMAKE_CXX_OUTPUT_EXTENSION ".o")
set(CMAKE_C_OUTPUT_EXTENSION ".o")



#set(CMAKE_C_COMPILER "${PROJECT_SOURCE_DIR}/../Production/armgcc.sh")
set(CMAKE_C_COMPILER "C:/ti/ccsv7/tools/compiler/ti-cgt-arm_17.9.0.STS/bin/armcl")


#set(CMAKE_ARMGCC_LINKER "${PROJECT_SOURCE_DIR}/../Production/armlinker.sh")
set(CMAKE_ARMGCC_LINKER "C:/ti/ccsv7/tools/compiler/ti-cgt-arm_17.9.0.STS/bin/armcl")

#set(CMAKE_ARMGCC_ARCHIVER "${PROJECT_SOURCE_DIR}/../Production/armar.sh")
set(CMAKE_ARMGCC_ARCHIVER "C:/ti/ccsv7/tools/compiler/ti-cgt-arm_17.9.0.STS/bin/armar")
#set(CMAKE_ASM_COMPILER "${PROJECT_SOURCE_DIR}/../Production/armasm.sh")

#SET(CMAKE_ASM_COMPILER "${TOOLCHAIN_BIN_DIR}/${TARGET_TRIPLET}-gcc${TOOL_EXECUTABLE_SUFFIX}")

#set(CMAKE_MAKE_PROGRAM="make -j 4)
#add_definitions(-D_COMPILING_NEWLIB)

set(CMAKE_C_COMPILER_WORKS TRUE CACHE INTERNAL "" FORCE)
set(CMAKE_C_COMPILER_FORCED TRUE)
set(CMAKE_USE_RELATIVE_PATHS TRUE)

#set(XC_OPTIMIZATION "-O3")
#set(XC_OPTIMIZATION "")


#set(TARGET_CORE "cortex-m0")

#set(TARGET_CHIP "STM32F091")
#set(TARGET_CHIP2 "STM32F091RCTx")

#set(PROJECT_CONF_DEFINES "-DBACDL_MSTP '-DMAX_APDU=206' '-DMAX_TSM_TRANSACTIONS=4' '-DWRITE_PROPERTY=1' '-DUSE_XPT2046=1' '-DUSE_LCD=1' '-DUSE_MCP9800=1' -'DMAX_ADDRESS_CACHE=32'")



set(CMAKE_ARMGCC_COMPILE_FLAGS "-mv7R4 --code_state=32 --float_support=VFPv3D16 -me  ${PROJECT_CONF_DEFINES} ${CMAKE_C_FLAGS}  -g --c89 --c++03 --strict_ansi --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi")

#\"../source/sys_link.cmd\" -z -m"fls_firmware.map" --xml_link_info="fls_firmware_linkInfo.xml"  -o "fls_firmware.out" "./Debug.obj" "./my_timer.obj" "./ocp_can_ep.obj" "./ocp_usr.obj" "./process.obj" "./Drivers/ADC/AdcInstance.obj" "./Drivers/ADC/SignalComp.obj" "./Drivers/ADC/SignalFilter.obj" "./Drivers/CAN/opto_can.obj" "./Drivers/GPIO/GPIO.obj" "./Drivers/SPI/SPIDriver.obj" "./Libraries/OCP/opto_com.obj" "./Libraries/ObjectDictionary/object_dictionary.obj" "./source/can.obj" "./source/dabort.obj" "./source/esm.obj" "./source/gio.obj" "./source/notification.obj" "./source/pinmux.obj" "./source/rti.obj" "./source/sci.obj" "./source/spi.obj" "./source/sys_core.obj" "./source/sys_dma.obj" "./source/sys_intvecs.obj" "./source/sys_main.obj" "./source/sys_mpu.obj" "./source/sys_pcr.obj" "./source/sys_phantom.obj" "./source/sys_pmm.obj" "./source/sys_pmu.obj" "./source/sys_selftest.obj" "./source/sys_startup.obj" "./source/sys_vim.obj" "./source/system.obj" 
set(CMAKE_ARMGCC_LINK_FLAGS "${CMAKE_C_FLAGS}    -mv7R4 --code_state=32 --float_support=VFPv3D16 -me -g --c89 --c++03 --strict_ansi --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi  --heap_size=0x800 --stack_size=0x800 -i\"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_17.9.0.STS/lib\" -i\"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_17.9.0.STS/include\" --reread_libs --diag_wrap=off --display_error_number --warn_sections --rom_model -z   -lrtsv7R4_T_le_v3D16_eabi.lib ") 


set(CMAKE_C_CREATE_STATIC_LIBRARY "arm-none-eabi-ar -r <TARGET> <OBJECTS>")# <LINK_FLAGS>")

set(CMAKE_C_COMPILE_OBJECT "<CMAKE_C_COMPILER> ${CMAKE_ARMGCC_COMPILE_FLAGS} <INCLUDES> --preproc_with_compile --preproc_dependency=\"<OBJECT>.d_raw\" -o <OBJECT> -c <SOURCE>  <DEFINES> <FLAGS>")
#SET(CMAKE_ASM_COMPILE_OBJECT "<CMAKE_ASM_COMPILER> -mcpu=cortex-m0 -mthumb -mfloat-abi=soft -o <OBJECT>   -c <SOURCE>")


set(DEBUG_SUFFIX "out")
set(CMAKE_C_LINK_EXECUTABLE "${CMAKE_ARMGCC_LINKER} ${CMAKE_ARMGCC_LINK_FLAGS} -o \"<TARGET>.${DEBUG_SUFFIX}\" <OBJECTS> <LINK_LIBRARIES>")
set(CMOCKA_LIB ../../../Software/UnitDesignAndImpl/Implementation/Libraries/Cmocka/libcmocka.o)



	