message("Using gcc compiler with coverge on linux ...")

set(CMAKE_SYSTEM_NAME Linux)

set(CMAKE_C_COMPILER gcc)

set(CMAKE_C_FLAGS
    "-std=c99 -ggdb -O0 -fprofile-arcs -ftest-coverage"
    CACHE STRING "" FORCE)

set(CMOCKA_LIB ../../../Software/UnitDesignAndImpl/Implementation/Libraries/Cmocka/libcmocka.so.0)

