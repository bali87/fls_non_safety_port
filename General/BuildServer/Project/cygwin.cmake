message("Using gcc compiler on cygwin ...")

set(CMAKE_SYSTEM_NAME Cygwin)

set(GCC TRUE)

set(CMAKE_C_COMPILER gcc)

set(CMAKE_C_FLAGS
    "-g -std=c99 -Wall -D__PIC24FJ256GA702__"
    CACHE STRING "" FORCE)

set(CMOCKA_LIB ../../../Software/UnitDesignAndImpl/Implementation/Libraries/Cmocka/libcmocka.dll.a)
set(CMOCKA_LIB_DLL ../../../Software/UnitDesignAndImpl/Implementation/Libraries/Cmocka/cygcmocka-0.dll)

#message(FATAL_ERROR)lll


