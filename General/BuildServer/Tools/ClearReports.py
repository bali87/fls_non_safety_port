# -*- coding: utf-8 -*-
""" Remove all reports. """

import sys
import os
import shutil

def delete(filepath):
    """ Delete file. """

    print("Removing %s ..." % filepath)
    try:
        if os.path.exists(filepath):
            os.remove(filepath)
        else:
            print("File %s does not exist ..." % filepath)
    except OSError:
        print("There was an error while removing %s ...", filepath)
        sys.exit(1)

def delete_folder(filepath):
    """ Delete folder. """

    print("Removing %s ..." % filepath)
    try:
        if os.path.exists(filepath):
            shutil.rmtree(filepath)
        else:
            print("Directory %s does not exist ..." % filepath)
    except OSError:
        print("There was an error while removing %s ...", filepath)
        sys.exit(1)

def main(interactive_mode=None):
    """ Main. """

    print("Removing all reports ...")

    # SUDI
    delete('../../../P6_PDSL/SP6-8_SUDI/WP6-8.5.1_SUDS/WP6-8.5.1_SUDS.chm')
    delete('../../../P6_PDSL/SP6-8_SUDI/WP6-8.5.2_SUI/WP6-8.5.2_SUI_HostBuild.txt')
    #delete('../../../P6_PDSL/SP6-8_SUDI/WP6-8.5.2_SUI/WP6-8.5.2_SUI_SimulatorBuild.txt')
    #delete('../../../P6_PDSL/SP6-8_SUDI/WP6-8.5.2_SUI/WP6-8.5.2_SUI_TargetBuild.txt')

    delete('../../../P6_PDSL/SP6-8_SUDI/WP6-8.5.3_SVR-R/6-8.5.3 Software verification report - code metrics.chm')
    delete('../../../P6_PDSL/SP6-8_SUDI/WP6-8.5.3_SVR-R/6-8.5.3 Software verification report - MISRA report.xlsx')

    # SUT
    delete('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.2_SVS/WP6-9.5.2_SVS.chm')

    #delete_folder('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/Host')
    #delete_folder('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/Simulator')
    #delete_folder('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/Target')

    delete('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/6-9.5.3 Software verification report - address sanitizer runtime output (linux gcc 32 bit).txt')
    delete('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/6-9.5.3 Software verification report - dr memory runtime output (linux gcc 32 bit).txt')
    delete('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/6-9.5.3 Software verification report - results of unit testing (linux gcc 32 bit).txt')

    delete('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/WP6-9.5.3_SVR-R_HostBuild.txt')
    delete('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/WP6-9.5.3_SVR-R_HostOutput.txt')

    #delete('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/WP6-9.5.3_SVR-R_SimulatorBuild.txt')
    #delete('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/WP6-9.5.3_SVR-R_SimulatorOutput.txt')
    #delete('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/WP6-9.5.3_SVR-R_TargetBuild.txt')
    #delete('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/WP6-9.5.3_SVR-R_TargetOutput.txt')

    delete('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/6-9.5.3 Software verification report - code coverage.chm')

    # SIT
    # delete_folder('../../../P6_PDSL/SP6-10_SIT/WP6-10.5.4_SVR-R/Host')
    # delete_folder('../../../P6_PDSL/SP6-10_SIT/WP6-10.5.4_SVR-R/Simulator')
    # delete_folder('../../../P6_PDSL/SP6-10_SIT/WP6-10.5.4_SVR-R/Target')

#    delete('../../../P6_PDSL/SP6-10_SIT/WP6-10.5.2_SVS-R/WP6-10.5.2_SVS-R.chm')
#
#    delete('../../../P6_PDSL/SP6-10_SIT/WP6-10.5.4_SVR-R/WP6-10.5.4_SVR-R_AddressSanitizerOutput.txt')
#    delete('../../../P6_PDSL/SP6-10_SIT/WP6-10.5.4_SVR-R/WP6-10.5.4_SVR-R_DrMemoryOutput.txt')
#
#    delete('../../../P6_PDSL/SP6-10_SIT/WP6-10.5.4_SVR-R/WP6-10.5.4_SVR-R_HostBuild.txt')
#    delete('../../../P6_PDSL/SP6-10_SIT/WP6-10.5.4_SVR-R/WP6-10.5.4_SVR-R_HostOutput.txt')
#
#    delete('../../../P6_PDSL/SP6-10_SIT/WP6-10.5.4_SVR-R/WP6-10.5.4_SVR-R_SimulatorBuild.txt')
#    delete('../../../P6_PDSL/SP6-10_SIT/WP6-10.5.4_SVR-R/WP6-10.5.4_SVR-R_SimulatorOutput.txt')
#
#    delete('../../../P6_PDSL/SP6-10_SIT/WP6-10.5.4_SVR-R/WP6-10.5.4_SVR-R_TargetBuild.txt')
#    delete('../../../P6_PDSL/SP6-10_SIT/WP6-10.5.4_SVR-R/WP6-10.5.4_SVR-R_TargetOutput.txt')
#
#    delete('../../../P6_PDSL/SP6-10_SIT/WP6-10.5.4_SVR-R/WP6-10.5.4_SVR-R_TestCoverage.chm')

    # VSSR
    # delete_folder('../../../P6_PDSL/SP6-11_VSSR/WP6-11.5.3_SVR-R/Host')
    # delete_folder('../../../P6_PDSL/SP6-11_VSSR/WP6-11.5.3_SVR-R/Simulator')
    # delete_folder('../../../P6_PDSL/SP6-11_VSSR/WP6-11.5.3_SVR-R/Target')

#    delete('../../../P6_PDSL/SP6-11_VSSR/WP6-11.5.2_SVS-R/WP6-11.5.2_SVS-R.chm')
#
#    delete('../../../P6_PDSL/SP6-11_VSSR/WP6-11.5.3_SVR-R/WP6-11.5.3_SVR-R_AddressSanitizerOutput.txt')
#    delete('../../../P6_PDSL/SP6-11_VSSR/WP6-11.5.3_SVR-R/WP6-11.5.3_SVR-R_DrMemoryOutput.txt')
#
#    delete('../../../P6_PDSL/SP6-11_VSSR/WP6-11.5.3_SVR-R/WP6-11.5.3_SVR-R_HostOutput.txt')
#    delete('../../../P6_PDSL/SP6-11_VSSR/WP6-11.5.3_SVR-R/WP6-11.5.3_SVR-R_HostBuild.txt')
#
#    delete('../../../P6_PDSL/SP6-11_VSSR/WP6-11.5.3_SVR-R/WP6-11.5.3_SVR-R_SimulatorOutput.txt')
#    delete('../../../P6_PDSL/SP6-11_VSSR/WP6-11.5.3_SVR-R/WP6-11.5.3_SVR-R_SimulatorBuild.txt')
#
#    delete('../../../P6_PDSL/SP6-11_VSSR/WP6-11.5.3_SVR-R/WP6-11.5.3_SVR-R_TargetOutput.txt')
#    delete('../../../P6_PDSL/SP6-11_VSSR/WP6-11.5.3_SVR-R/WP6-11.5.3_SVR-R_TargetBuild.txt')
#
#    delete('../../../P6_PDSL/SP6-11_VSSR/WP6-11.5.3_SVR-R/WP6-11.5.3_SVR-R_TestCoverage.chm')

    # IIT
    # delete_folder('../../../P4_PDSL/SP4-8_IIT/WP4-8.5.3_ITR/Host')
    # delete_folder('../../../P4_PDSL/SP4-8_IIT/WP4-8.5.3_ITR/Simulator')
    # delete_folder('../../../P4_PDSL/SP4-8_IIT/WP4-8.5.3_ITR/Target')

#    delete('../../../P4_PDSL/SP4-8_IIT/WP4-8.5.2_ITS/WP4-8.5.2_ITS.chm')
#
#    delete('../../../P4_PDSL/SP4-8_IIT/WP4-8.5.3_ITR/WP4-8.5.3_ITR_AddressSanitizerOutput.txt')
#    delete('../../../P4_PDSL/SP4-8_IIT/WP4-8.5.3_ITR/WP4-8.5.3_ITR_DrMemoryOutput.txt')
#
#    delete('../../../P4_PDSL/SP4-8_IIT/WP4-8.5.3_ITR/WP4-8.5.3_ITR_HostBuild.txt')
#    delete('../../../P4_PDSL/SP4-8_IIT/WP4-8.5.3_ITR/WP4-8.5.3_ITR_HostOutput.txt')
#
#    delete('../../../P4_PDSL/SP4-8_IIT/WP4-8.5.3_ITR/WP4-8.5.3_ITR_SimulatorBuild.txt')
#    delete('../../../P4_PDSL/SP4-8_IIT/WP4-8.5.3_ITR/WP4-8.5.3_ITR_SimulatorOutput.txt')
#
#    delete('../../../P4_PDSL/SP4-8_IIT/WP4-8.5.3_ITR/WP4-8.5.3_ITR_TargetBuild.txt')
#    delete('../../../P4_PDSL/SP4-8_IIT/WP4-8.5.3_ITR/WP4-8.5.3_ITR_TargetOutput.txt')
#
#    delete('../../../P4_PDSL/SP4-8_IIT/WP4-8.5.3_ITR/WP4-8.5.3_ITR_TestCoverage.chm')

    print('All reports have been deleted or did not exist.')

    if not interactive_mode and interactive_mode == True:
        input('Press Enter to continue ...')


if __name__ == '__main__':
    if len(sys.argv) == 2 and sys.argv[1] == 'INTERACTIVE_MODE':
        main(True)
    else:
        main()

