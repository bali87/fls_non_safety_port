<?xml version="1.0" encoding="UTF-8"?>
<Site BuildName="CYGWIN-c++.exe"
	BuildStamp="20150525-1453-Experimental"
	Name="box"
	Generator="ctest-3.1.2"
	CompilerName="/usr/bin/c++.exe"
	OSName="CYGWIN_NT-6.1-WOW"
	Hostname="box"
	OSRelease="2.0.2(0.287/5/3)"
	OSVersion="2015-05-08 17:03"
	OSPlatform="i686"
	Is64Bits="0"
	VendorString="GenuineIntel"
	VendorID="Intel Corporation"
	FamilyID="6"
	ModelID="30"
	ProcessorCacheSize="256"
	NumberOfLogicalCPU="4"
	NumberOfPhysicalCPU="1"
	TotalVirtualMemory="0"
	TotalPhysicalMemory="255"
	LogicalProcessorsPerPhysical="4"
	ProcessorClockFrequency="2664"
>
<Testing>
	<StartDateTime>__DATE__</StartDateTime>
	<StartTestTime>__TIME__</StartTestTime>
	<TestList>
		<Test>__NAME__</Test>
	</TestList>
	<Test Status="__STATUS__">
		<Name>__NAME__</Name>
		<Path>./__PATH__/__NAME__</Path>
		<FullName>./__PATH__/__NAME__</FullName>
		<FullCommandLine>./__PATH__/__NAME__</FullCommandLine>
		<Results>
			<NamedMeasurement type="text/string" name="Exit Code"><Value>__STATUS_CAMEL__</Value></NamedMeasurement>
			<NamedMeasurement type="text/string" name="Exit Value"><Value>__ERROR_CODE__</Value></NamedMeasurement>
			<NamedMeasurement type="numeric/double" name="Execution Time"><Value>0.00</Value></NamedMeasurement>
			<NamedMeasurement type="text/string" name="Completion Status"><Value>Completed</Value></NamedMeasurement>
      <NamedMeasurement type="text/string" name="Command Line"><Value>./__PATH__/__NAME__</Value></NamedMeasurement>
			<Measurement>
				<Value>
__OUTPUT__
        </Value>
			</Measurement>
        </Results>
	</Test>
	<EndDateTime>__DATE__</EndDateTime>
	<EndTestTime>__TIME__</EndTestTime>
<ElapsedMinutes>0</ElapsedMinutes></Testing>
</Site>
