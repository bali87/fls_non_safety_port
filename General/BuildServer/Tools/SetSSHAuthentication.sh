#!/bin/bash

if [ ! $# = 2 ]; then
	echo "Generate and copy public key to server:"
	echo " ./SetSSHAuthentication.sh USER HOST"
	exit
fi

USER=$1
HOST=$2

KEY=$HOME/.ssh/id_rsa
KEYPUB=$KEY.pub
if [ -e $KEYPUB ]; then
    echo 'key already exists.'
else 
	mkdir -p ~/.ssh
    ssh-keygen -f $KEY -t rsa
	chmod 600 $KEY
fi
ssh $USER@$HOST 'mkdir -p .ssh'
cat $KEYPUB | ssh $USER@$HOST 'cat >> .ssh/authorized_keys'

