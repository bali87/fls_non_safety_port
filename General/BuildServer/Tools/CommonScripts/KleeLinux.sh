#!/bin/bash

OUTPUT_DIR=$1
OUTPUT=$2
SUBPHASE=$3

cd ${OUTPUT_DIR}/Klee

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

run_klee ${OUTPUT_DIR} ${OUTPUT} ${SUBPHASE}

