#!/bin/bash

OUTPUT_DIR=$1
OUTPUT=$2
SUBPHASE=$3

cd ${OUTPUT_DIR}/DrMemory

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

dr_memory "${OUTPUT_DIR}" "${OUTPUT}" ${SUBPHASE}

