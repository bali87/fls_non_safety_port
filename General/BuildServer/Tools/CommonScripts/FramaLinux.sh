#!/bin/bash

OUTPUT_DIR=$1
OUTPUT=$2
SUBPHASE=$3

cd ${OUTPUT_DIR}/Frama-c

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

run_frama ${OUTPUT_DIR} ${OUTPUT} ${SUBPHASE}

