#!/bin/bash

OUTPUT_DIR=$1
OUTPUT=$2
SUBPHASE=$3

cd ${OUTPUT_DIR}/ClangInstrumented

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

clang_instrumented "${OUTPUT_DIR}" "${OUTPUT}" ${SUBPHASE}

