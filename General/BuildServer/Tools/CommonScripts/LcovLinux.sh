#!/bin/bash

OUTPUT_DIR=$1
OUTPUT=$2
SUBPHASE=$3
INCLUDE_TESTS=$4

cd ${OUTPUT_DIR}

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

coverage "${OUTPUT_DIR}" "${OUTPUT}" ${SUBPHASE} ${INCLUDE_TESTS}

