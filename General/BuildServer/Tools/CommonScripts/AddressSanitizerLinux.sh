#!/bin/bash


OUTPUT_DIR=$1
OUTPUT=$2
SUBPHASE=$3

cd ${OUTPUT_DIR}/AddressSanitizer

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

address_sanitizer "${OUTPUT_DIR}" "${OUTPUT}" ${SUBPHASE}

