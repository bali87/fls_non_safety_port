#!/bin/bash

ISO_ROOT=/home/bmw/bmw/etoe

find $ISO_ROOT -name "build-tmp" -type d -exec rm -fr {} \;
find $ISO_ROOT -name "b" -type d -exec rm -fr {} \;
find $ISO_ROOT -name "tmp_*" -type f -exec rm {} \;
find $ISO_ROOT -name "Test.xml" -type f -exec rm {} \;

