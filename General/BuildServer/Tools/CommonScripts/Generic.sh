# this option is needed to handle last non-zero exit code of pipeline (eg. make | tee)
set -o pipefail

export ISO_ROOT=../../../../..
export CMAKELISTS_DIR=${ISO_ROOT}/General/BuildServer/Project

export CYGWIN_TOOLCHAIN_FILE=${CMAKELISTS_DIR}/cygwin.cmake
export LINUX_TOOLCHAIN_FILE=${CMAKELISTS_DIR}/linux.cmake
export LINUX_ASAN_TOOLCHAIN_FILE=${CMAKELISTS_DIR}/linux_asan.cmake
export LINUX_CLANG_TOOLCHAIN_FILE=${CMAKELISTS_DIR}/linux_clang.cmake
export LINUX_DRMEM_TOOLCHAIN_FILE=${CMAKELISTS_DIR}/linux_drmem.cmake
export LINUX_LCOV_TOOLCHAIN_FILE=${CMAKELISTS_DIR}/linux_lcov.cmake
export CYGWIN_LCOV_TOOLCHAIN_FILE=${CMAKELISTS_DIR}/cygwin_lcov.cmake
export MCU_TOOLCHAIN_FILE=${CMAKELISTS_DIR}/ti-arm-none-eabi.cmake
export MCU_SIM_TOOLCHAIN_FILE=${CMAKELISTS_DIR}/xc16_sim.cmake
  
export ISO_ROOT_REMOTE_CALL=../../../..

export HOST=localhost
export USER=optoforce
export RSA_KEY=General/BuildServer/Tools/RSA_key
export KEY=${ISO_ROOT_REMOTE_CALL}/${RSA_KEY}


#path to svn repository on remote host (it should be already checked out)
export PATH_TO_REPO=/home/jenkins/gripper/

export TIMEOUT=600s

function enable_interactive_mode() {
    if [[ "$1" == "INTERACTIVE_MODE" ]]; then
        export INTERACTIVE_MODE=TRUE
    else 
        export INTERACTIVE_MODE=FALSE
    fi
}

function pause() {
    if [[ "${INTERACTIVE_MODE}" == "TRUE" ]]; then
        echo "Press Enter to continue ..."
        read
    fi
}

function create_and_cd_build_dir() {
    BUILD_DIR=b
    mkdir -p ${BUILD_DIR}
    cd ${BUILD_DIR}
}

function check() {
    if [[ "${EXIT}" == "1" ]]; then
        echo "... running $1 failed."
        pause
        exit 1
    else
        echo "... running $1 done."
        pause
    fi
}

function check_output() {
    echo "Checking output $1 ..."
    if [[ -f $1 ]]; then 
        echo "... output $1 has been created."
        pause
    else
        echo "... output $1 does not exist."
        pause
        exit 1
    fi
}

function check_deleted() {
    echo "Checking if $1 has been deleted ..."
    if [[ -f "$1" ]]; then 
        echo "... file $1 has not been deleted."
        pause
        exit 1
    else
        echo "... file $1 has been deleted."
        pause
    fi
}

function collect_hex() {
    echo "Collect hex files ..."
    find . -name "*.hex" -exec cp {} ../ \; || EXIT=1
    find . -name "*.hex" -exec echo "... {} has been created." \; || EXIT=1
    if [[ "${EXIT}" == "1" ]]; then 
        echo "... hex output does not exist."
        exit 1
        pause
    else
        echo "... hex output has been created."
        pause
    fi
}

function run_simulation() {
    HEX=$1
    SIMULATION_OUTPUT=$2
    SIMULATION_DEBUG=$3

    SIMULATION_TEMPLATE=../../../../../General/BuildServer/Tools/RunSimulationTemplate.txt
    SIMULATION_INPUT=RunConfigTmp.txt

    echo "Removing old simulation input file for ${HEX} ..."
    rm -fr ${SIMULATION_INPUT}

    echo "Creating simulation input file for ${HEX} ..."
    cp ${SIMULATION_TEMPLATE} ${SIMULATION_INPUT}
    check_output ${SIMULATION_INPUT}

    echo "Running sed for ${HEX} ..."
    sed -e "s#__PROGRAM__#${HEX}#" -i ${SIMULATION_INPUT} || EXIT=1
    sed -e "s#__OUTPUT__#${SIMULATION_OUTPUT}#" -i ${SIMULATION_INPUT} || EXIT=1
    check "sed"
    
    echo "Running simulation ${HEX} ..."
    mdb.bat ${SIMULATION_INPUT} 2>&1 | tee -a ${SIMULATION_DEBUG} || EXIT=1
    check "simulation ${HEX}"
}

function bamboo_frama_test_xml() {
    # eg. bamboo_test_xml P6_PDSL/SPX-Y_ZZ/WP_DIR WP_output.txt TEST_NAME FAILED

    echo "Creating Test.xml file ..."

    # output template
    P1_PATH=../../../../../General/BuildServer/Tools/
    OUTPUT_TEMPLATE_XML=${P1_PATH}/BambooGenerateTestXmlTemplate.txt
    OUTPUT_SED=BambooGenerateTestXml.sed
    # output file
    OUTPUT_XML=Test.xml

    INPUT_PATH=$1
    INPUT_FILE=$2
    INPUT_NAME=$3

    STATUS=passed
    STATUS_CAMEL=Passed
    ERROR_CODE=0

    FRAMA=""
    for FAIL in "Unknown" "\[wp\] warning" "\[rte\] warning" "\[val\] warning"; do
        if grep -q "${FAIL}" ${INPUT_FILE}; then
            STATUS=failed
            STATUS_CAMEL=Failed
            ERROR_CODE=1
            if [[ -n $FRAMA ]]; then
              FRAMA="${FRAMA}, \"${FAIL}\""
            else
              FRAMA="\"${FAIL}\""
            fi        
        else 
            echo $FAIL not found
        fi
    done

    rm -f output.txt

    if [[ -n ${FRAMA} ]]; then
        echo "FramaC output has been tested for presence of following keywords:"      >> output.txt
        echo "  ${FRAMA}"                                                             >> output.txt
        echo "in its output, which means that there is an error in the source code."  >> output.txt
        echo ""                                                                       >> output.txt
    fi

    cp ${OUTPUT_TEMPLATE_XML} ${OUTPUT_XML}
    check_output ${OUTPUT_XML}

    sed -e "s#&#\&amp;#g; s#<#\&lt;#g; s#>#\&gt;#g" ${INPUT_FILE} >> output.txt
    check_output output.txt

    echo "Running sed ..."
    sed -e "s#__DATE__#`date`#" -i ${OUTPUT_XML} || EXIT=1
    sed -e "s#__TIME__#`date`#" -i ${OUTPUT_XML} || EXIT=1
    sed -e "s#__NAME__#${INPUT_NAME}#" -i ${OUTPUT_XML} || EXIT=1
    sed -e "s#__PATH__#${INPUT_PATH}#" -i ${OUTPUT_XML} || EXIT=1
    sed -e "s#__STATUS__#${STATUS}#" -i ${OUTPUT_XML} || EXIT=1
    sed -e "s#__STATUS_CAMEL__#${STATUS_CAMEL}#" -i ${OUTPUT_XML} || EXIT=1
    sed -e "s#__ERROR_CODE__#${ERROR_CODE}#" -i ${OUTPUT_XML} || EXIT=1
    cp ${P1_PATH}/${OUTPUT_SED} ${OUTPUT_SED} || EXIT=1
    sed -f ${OUTPUT_SED} -i ${OUTPUT_XML} || EXIT=1
    check "sed"
    
    echo "Removind temporary files ..."
    rm -f output.txt ${OUTPUT_SED}

    check_output Test.xml
}

function bamboo_test_xml() {
    # eg. bamboo_test_xml P6_PDSL/SPX-Y_ZZ/WP_DIR WP_output.txt TEST_NAME FAILED

    echo "Creating Test.xml file ..."

    # output template
    P1_PATH=../../../../../General/BuildServer/Tools/
    OUTPUT_TEMPLATE_XML=${P1_PATH}/BambooGenerateTestXmlTemplate.txt
    OUTPUT_SED=BambooGenerateTestXml.sed
    # output file
    OUTPUT_XML=Test.xml

    INPUT_PATH=$1
    INPUT_FILE=$2
    INPUT_NAME=$3
    shift 3
    FAIL_REGEX_CONDITION=$*

    cp ${OUTPUT_TEMPLATE_XML} ${OUTPUT_XML}
    check_output ${OUTPUT_XML}

    sed -e "s#&#\&amp;#g; s#<#\&lt;#g; s#>#\&gt;#g" "${INPUT_FILE}" > output.txt
    check_output output.txt

    STATUS=passed
    STATUS_CAMEL=Passed
    ERROR_CODE=0

    for FAIL in "${FAIL_REGEX_CONDITION}"; do
        if grep -q -e "${FAIL}" output.txt; then
            STATUS=failed
            STATUS_CAMEL=Failed
            ERROR_CODE=1
        fi
    done

    echo "Running sed ..."
    sed -e "s#__DATE__#`date`#" -i ${OUTPUT_XML} || EXIT=1
    sed -e "s#__TIME__#`date`#" -i ${OUTPUT_XML} || EXIT=1
    sed -e "s#__NAME__#${INPUT_NAME}#" -i ${OUTPUT_XML} || EXIT=1
    sed -e "s#__PATH__#${INPUT_PATH}#" -i ${OUTPUT_XML} || EXIT=1
    sed -e "s#__STATUS__#${STATUS}#" -i ${OUTPUT_XML} || EXIT=1
    sed -e "s#__STATUS_CAMEL__#${STATUS_CAMEL}#" -i ${OUTPUT_XML} || EXIT=1
    sed -e "s#__ERROR_CODE__#${ERROR_CODE}#" -i ${OUTPUT_XML} || EXIT=1
    cp ${P1_PATH}/${OUTPUT_SED} ${OUTPUT_SED} || EXIT=1
    sed -f ${OUTPUT_SED} -i ${OUTPUT_XML} || EXIT=1
    check "sed"
    
    echo "Removind temporary files ..."
    rm -f output.txt ${OUTPUT_SED}

    check_output Test.xml
}

function remote_call() {
    SCRIPT="$1"
    # relative path from ISO_ROOT
    OUTPUT_DIR="$2"
    # output file
    OUTPUT="$3"
    SUBPHASE="$4"
    EXTRA_FLAG=$5

    echo "Triggering remote exection ..."
    ssh ${USER}@${HOST} -i ${KEY} \
        "cd ${PATH_TO_REPO}; \
         svn update --non-interactive; \
         ${SCRIPT} ${OUTPUT_DIR} \"tmp_${OUTPUT}\"" ${SUBPHASE} ${EXTRA_FLAG} || EXIT=1
    check "triggering remote execution"

    if [[ -n "$OUTPUT" ]]; then 
        echo "Getting the result ..."
        scp -i ${KEY} ${USER}@${HOST}:"${PATH_TO_REPO}/${OUTPUT_DIR}/\"tmp_${OUTPUT}\"" \
            "${ISO_ROOT_REMOTE_CALL}/${OUTPUT_DIR}/${OUTPUT}" || EXIT=1
        check "getting the result"
        check_output ${ISO_ROOT_REMOTE_CALL}/${OUTPUT_DIR}/"${OUTPUT}"
    fi
}

function copy_remote_host() {
    FILE="$1"
    REMOTE_HOST_DIR="$2"

    echo "Triggering copy on remote host ..."
    scp -i ${KEY} ${FILE} ${USER}@${HOST}:"${PATH_TO_REPO}/${REMOTE_HOST_DIR}/" || EXIT=1
}

function dr_memory() {
    OUTPUT_DIR="$1"
    OUTPUT="$2"
    SUBPHASE="$3"

    export PATH=${PATH}:/home/bmw/bmw/etoe/General/BuildServer/Production
    
    echo "Running DrMemory on remote host (linux) ..."

    create_and_cd_build_dir

    echo "Running cmake ..."
    cmake -DCMAKE_TOOLCHAIN_FILE=${LINUX_DRMEM_TOOLCHAIN_FILE} \
          -DSUBPHASE=${SUBPHASE} ${CMAKELISTS_DIR} || EXIT=1
    check "cmake"

    echo "Running make"
    make || EXIT=1
    check "make"

    REPORT=${ISO_ROOT}/${OUTPUT_DIR}/${OUTPUT}

    echo "Removing old files ..."
    rm -fr "${REPORT}" drmemory_logdir
    check_deleted "${REPORT}"

    mkdir -p drmemory_logdir

    for EXEC in `find . -perm /u=x,g=x,o=x -type f`; do
        if [[ ($EXEC == *.sh) || ($EXEC == *.dll) || ($EXEC == *.so) || \
              ($EXEC == *.so.*) || ($EXEC == a.out) || ($EXEC == *.bin) ]]; then
            echo "skipping file ..." > /dev/null
        else
            echo "${EXEC}" >> "${REPORT}"
            echo "=======================" >> "${REPORT}"
            echo "" >> "${REPORT}"
            /opt/DrMemory-Linux-1.8.0-8/bin/drmemory -logdir drmemory_logdir \
                -- ${EXEC} 2>&1 | tee -a "${REPORT}"
            echo "" >> "${REPORT}"
            echo "" >> "${REPORT}"
        fi
    done;

    echo "Removing old file ..."
    rm -fr "~/Test.xml"
    check_deleted "~/Test.xml"

    echo "Fixing xml tags formatting ..."
    sed -e "s/</\\&lt;/g" -e "s/>/\\&gt;/g" -i "../../${OUTPUT}"

    echo "Generating ctest file ..."
    bamboo_test_xml "${OUTPUT_DIR}" "../../${OUTPUT}" DrMemory "[^\\ 0]\\+0* total" 
    check_output "Test.xml"

    mv Test.xml ~/Test.xml

    echo "... running DrMemory on remote host (linux) done."
}

function trigger_dr_memory() {
    OUTPUT_DIR="$1"
    OUTPUT="$2"
    SUBPHASE="$3"
    SCRIPT=./General/BuildServer/Tools/CommonScripts/DrMemoryLinux.sh

    echo "Running DrMemory on remote host ..."

    echo "Running the remote script ..."
    remote_call "${SCRIPT}" "${OUTPUT_DIR}" "${OUTPUT}" "${SUBPHASE}" || EXIT=1
    check "the remote script"

    echo "Removing old file ..."
    rm -fr Test.xml

    echo "Getting the test result ..."
    scp -i ${KEY} ${USER}@${HOST}:Test.xml . || EXIT=1
    check_output Test.xml

    echo "... running DrMemory on remote host done."
    pause
}

function address_sanitizer() {
    OUTPUT_DIR="$1"
    OUTPUT="$2"
    SUBPHASE="$3"

    export PATH=${PATH}:/home/bmw/bin:/home/bmw/bmw/etoe/General/BuildServer/Production
    export LIBRARY_PATH=/usr/local/lib64
    export LD_LIBRARY_PATH=/usr/local/lib64

    echo "Running Address Sanitizer on remote host (linux) ..."

    create_and_cd_build_dir

    echo "Running cmake ..."
    cmake -DCMAKE_TOOLCHAIN_FILE=${LINUX_ASAN_TOOLCHAIN_FILE} \
          -DSUBPHASE=${SUBPHASE} ${CMAKELISTS_DIR} || EXIT=1
    check "cmake"

    echo "Running make ..."
    make || EXIT=1
    check "make"
    
    REPORT=${ISO_ROOT}/${OUTPUT_DIR}/${OUTPUT}

    echo "Removing old files ..."
    rm -f "${REPORT}"
    find . -name "Test.xml" -exec rm {} \;
    check_deleted "${REPORT}"

    ctest -VV -T Test >> "${REPORT}"

    find . -name "Test.xml" -exec sed -e "s/<Name>/&AddressSanitizer_/g" -i {} \;
    find . -name "Test.xml" -exec sed -e "s/_SVS\//&AddressSanitizer_/g" -i {} \;
    find . -name "Test.xml" -exec sed -e "s/_SVS-R\//&AddressSanitizer_/g" -i {} \;
    find . -name "Test.xml" -exec sed -e "s/_ITS\//&AddressSanitizer_/g" -i {} \;
  
    tar -cvf ~/asan.tar `find . -name "Test.xml"`
}

function trigger_address_sanitizer() {
    OUTPUT_DIR="$1"
    OUTPUT="$2"
    SUBPHASE=$3
    SCRIPT=./General/BuildServer/Tools/CommonScripts/AddressSanitizerLinux.sh

    echo "Running Address Sanitizer on remote host (linux) ..."

    echo "Removing old file ..."
    rm -fr Testing
    rm -f asan.tar
    find . -name "Test.xml" -exec rm {} \;

    echo "Running the remote script ..."
    remote_call "${SCRIPT}" "${OUTPUT_DIR}" "${OUTPUT}" "${SUBPHASE}" || EXIT=1
    check "the remote script"

    echo "Getting the test result ..."
    scp -i ${KEY} ${USER}@${HOST}:asan.tar . || EXIT=1
    check_output asan.tar

    tar -xvf asan.tar
    echo "Removing old file ..."
    rm -f asan.tar

    echo "... running Address Sanitizer on remote host (linux) done."
    pause
}

function clang_instrumented() {
    OUTPUT_DIR="$1"
    OUTPUT="$2"
    SUBPHASE=$3

    export PATH=${PATH}:/home/uwt/bin
    export LD_LIBRARY_PATH=/usr/local/lib64

    echo "Running Clang Instrumented compilation on remote host (linux) ..."

    create_and_cd_build_dir

    echo "Running cmake ..."
    cmake -DCMAKE_TOOLCHAIN_FILE=${LINUX_CLANG_TOOLCHAIN_FILE} \
          -DSUBPHASE=${SUBPHASE} ${CMAKELISTS_DIR} || EXIT=1
    check "cmake"

    echo "Running make ..."
    make || EXIT=1
    check "make"
    
    REPORT=${ISO_ROOT}/${OUTPUT_DIR}/${OUTPUT}

    echo "Removing old files ..."
    rm -f "${REPORT}"
    find . -name "Test.xml" -exec rm {} \;
    check_deleted "${REPORT}"

    ctest -VV -T Test >> "${REPORT}"

    find . -name "Test.xml" -exec sed -e "s/<Name>/&ClangInstrumented_/g" -i {} \;
    find . -name "Test.xml" -exec sed -e "s/_SVS\//&ClangInstrumented_/g" -i {} \;
    find . -name "Test.xml" -exec sed -e "s/_SVS-R\//&ClangInstrumented_/g" -i {} \;
    find . -name "Test.xml" -exec sed -e "s/_ITS\//&ClangInstrumented_/g" -i {} \;
  
    tar -cvf ~/clang.tar `find . -name "Test.xml"`
}

function trigger_clang_instrumented() {
    OUTPUT_DIR="$1"
    OUTPUT="$2"
    SUBPHASE="$3"
    SCRIPT=./General/BuildServer/Tools/CommonScripts/ClangInstrumentedLinux.sh

    echo "Running Clang Instrumented on remote host (linux) ..."

    echo "Removing old file ..."
    rm -fr Testing
    rm -f clang.tar
    find . -name "Test.xml" -exec rm {} \;

    echo "Running the remote script ..."
    remote_call "${SCRIPT}" "${OUTPUT_DIR}" "${OUTPUT}" ${SUBPHASE} || EXIT=1
    check "the remote script"

    echo "Getting the test result ..."
    scp -i ${KEY} ${USER}@${HOST}:clang.tar . || EXIT=1
    check_output clang.tar

    tar -xvf clang.tar
    echo "Removing old file ..."
    rm -f clang.tar

    echo "... running Clang Instrumented on remote host (linux) done."
    pause
}


function coverage() {
    OUTPUT_DIR="$1"
    OUTPUT="$2"
    SUBPHASE="$3"
    INCLUDE_TESTS="$4"

    export PATH=${PATH}:/home/jenkins/workspace/gripper/Generation/General/BuildServer/Production

    echo "Running coverage report generation on remote host (linux) ..."

    create_and_cd_build_dir

    echo "Removing old files ..."
    rm -fr ./coverage-report

    echo "Running cmake ..."
    cmake -DCMAKE_TOOLCHAIN_FILE=${LINUX_LCOV_TOOLCHAIN_FILE} \
          -DSUBPHASE=${SUBPHASE} ${CMAKELISTS_DIR} || EXIT=1
    check "cmake"

    echo "Running make ..."
    make || EXIT=1
    check "make"

    echo "Running tests ..."
    ctest -VV -T Test

    if [[ -z "${INCLUDE_TESTS}" ]]; then
        GCOV_ROOT=./Implementation
    else 
        GCOV_ROOT=.
    fi

    # suppress cmocka_main
    find ${GCOV_ROOT} -name 'cmocka_main.*.gcda' -exec rm -f {} \;
    find ${GCOV_ROOT} -name 'cmocka_main.*.gcno' -exec rm -f {} \;

    find ${GCOV_ROOT} -name '*.gcda' | xargs gcov
    lcov --capture --no-list-full-path --directory ${GCOV_ROOT} \
         --output-file ./coverage.info
    genhtml ./coverage.info --output-directory ./coverage-report

    zip -r "../${OUTPUT}" ./coverage-report
    check_output "../${OUTPUT}"
}

function trigger_coverage() {
    OUTPUT_DIR="$1"
    OUTPUT="$2"
    SUBPHASE="$3"
    REPORT="$4"
    INCLUDE_TESTS=$5
    
    SCRIPT=./General/BuildServer/Tools/CommonScripts/LcovLinux.sh
    
    echo "Running coverage report generation on remote host ..."

    echo "Removing old files ..."
    rm -fr ${OUTPUT} index.chm coverage-report

    echo "Running the remote script ..."
    remote_call "${SCRIPT}" "${OUTPUT_DIR}" "${OUTPUT}" "${SUBPHASE}" "${INCLUDE_TESTS}" || EXIT=1
    check "the remote script"

    echo "Unzipping coverage report files ..."
    unzip "${OUTPUT}" || EXIT=1
    check "unzip coverage report files"

    echo "Removing old file ..."
    rm -fr "${OUTPUT}"

    echo "Generating CHM file ..."
    hhc ./index.hhp # || EXIT=1
    # SUPPRESS hhc error code check
    echo "... generating CHM file finished."
    check_output index.chm

    mv ./index.chm "../${REPORT}"
    check_output "../${REPORT}"

    echo "Removing old files ..."
    rm -fr coverage-report

    echo "... running coverage report generation on remote host done."
    pause
}

function build_simulator() {
    OUTPUT_DIR=$1
    OUTPUT=$2
    SUBPHASE=$3

    echo "Running compilation for simulator on remote host (linux) ..."

    create_and_cd_build_dir
    
    export PATH=${PATH}:/home/bmw/bmw/etoe/General/BuildServer/Production:/opt/microchip/xc8/v1.34/bin

    echo "Running cmake ..."
    cmake -DCMAKE_TOOLCHAIN_FILE=${MCU_TOOLCHAIN_FILE} \
          -DSUBPHASE=${SUBPHASE} ${CMAKELISTS_DIR} \
          2>&1 | tee ../../${OUTPUT} || EXIT=1
    check "cmake"

    echo "Running make ..."
    make 2>&1 | tee ../../${OUTPUT} || EXIT=1
    check "make"

    echo "... running compilation for simulator on remote host (linux) done."
}

function trigger_build_simulator() {
    OUTPUT_DIR=$1
    OUTPUT=$2
    SUBPHASE=$3
    SCRIPT=./General/BuildServer/Tools/CommonScripts/BuildSimulatorLinux.sh

    echo "Running compilation on remote host for simulator (linux) ..."

    echo "Running the remote script ..."
    remote_call ${SCRIPT} ${OUTPUT_DIR} ${OUTPUT} ${SUBPHASE} || EXIT=1
    check "the remote script"

    echo "... running compilation on remote host for simulator (linux) done."
    pause
}


function run_frama() {
    OUTPUT_DIR=$1
    OUTPUT=$2
    SUBPHASE=$3

    echo "Running Frama-c on remote host (linux) ..."

    INPUT_DIR=/home/jenkins/workspace/gripper/Generation/Software/UnitDesignAndImpl/Implementation/Sources/

    create_and_cd_build_dir

    FILES=`find $INPUT_DIR -name "*.c"`
    for file in `find $INPUT_DIR -name "*.c"`; do
      sed -e "/^#pragma config.*/ d" -i $file
    done
    for arg in `find $INPUT_DIR -type d`; do
      include_dir="$include_dir -I$arg" 
    done
    for arg in `find $INPUT_DIR/../../Libraries/Cmocka -type d`; do
      include_dir="$include_dir -I$arg" 
    done

    frama-c -cpp-extra-args="$include_dir $DEFINES" \
      -rte -then -rte-precond -print -wp -wp-proof alt-ergo \
      -warn-unsigned-overflow \
      -warn-unsigned-downcast \
      -warn-signed-overflow \
      -warn-signed-downcast \
      -rte-div \
      -rte-mem \
      -rte-shift \
      -rte-float-to-int \
      -rte-all \
      -val \
      -machdep=x86_64 \
      $FILES 2>&1 | tee ../../$OUTPUT

   # svn revert --depth=infinity $INPUT_DIR

    bamboo_frama_test_xml ${OUTPUT_DIR} ../../${OUTPUT} FramaC
    rm -fr ~/FramaTest.tar
    find . -name "Test.xml" | xargs tar -cvf ~/FramaTest.tar

    echo "... running Frama-c on remote host (linux) done."
}

function trigger_run_frama() {
    OUTPUT_DIR=$1
    OUTPUT=$2
    SUBPHASE=$3

    SCRIPT=./General/BuildServer/Tools/CommonScripts/FramaLinux.sh

    echo "Running tests on remote host for simulator (linux) ..."

    echo "Running the remote script ..."
    remote_call ${SCRIPT} ${OUTPUT_DIR} ${OUTPUT} ${SUBPHASE} || EXIT=1
    check "the remote script"
    
    echo "Getting the test result ..."
    scp -i ${KEY} ${USER}@${HOST}:FramaTest.tar . || EXIT=1
    check_output FramaTest.tar

    tar -xvf FramaTest.tar
    echo "Removing old file ..."
    rm -f FramaTest.tar

    echo "... running tests on remote host for simulator (linux) done."
    pause
}

function trigger_run_frama_sui() {
    OUTPUT_DIR=$1
    OUTPUT=$2
    SUBPHASE=$3

    SCRIPT=./Software/UnitDesignAndImpl/Analysis/Frama-c/FramaLinux.sh
    echo "Running tests on remote host for simulator (linux) ..."

    echo "Running the remote script ..."
    remote_call ${SCRIPT} ${OUTPUT_DIR} ${OUTPUT} ${SUBPHASE} || EXIT=1
    check "the remote script"

    echo "Getting the test result ..."
    scp -i ${KEY} ${USER}@${HOST}:FramaTest.tar . || EXIT=1
    check_output FramaTest.tar

    tar -xvf FramaTest.tar
    echo "Removing old file ..."
    rm -f FramaTest.tar

    echo "... running tests on remote host for simulator (linux) done."
    pause
}

function trigger_run_frama_sit() {
    OUTPUT_DIR=$1
    OUTPUT=$2
    SUBPHASE=$3

    SCRIPT=./Software/SIT/TestReport/Frama-c/FramaLinux.sh

    echo "Running tests on remote host for simulator (linux) ..."

    echo "Running the remote script ..."
    remote_call ${SCRIPT} ${OUTPUT_DIR} ${OUTPUT} ${SUBPHASE} || EXIT=1
    check "the remote script"
    
    echo "Getting the test result ..."
    scp -i ${KEY} ${USER}@${HOST}:FramaTest.tar . || EXIT=1
    check_output FramaTest.tar

    tar -xvf FramaTest.tar
    echo "Removing old file ..."
    rm -f FramaTest.tar

    echo "... running tests on remote host for simulator (linux) done."
    pause
}

function run_klee() {
    # this function aims to run klee on SUI sources
    # currently few shortcuts are applied:
    # - all c-files are concatented into one file
    # - al! header files are copied to compilation directory (comipliotion without -I)
    # - macro __EEPROM_DATA has been removed

    OUTPUT_DIR=$1
    OUTPUT=$2
    SUBPHASE=$3

    echo "Running Klee on remote host (linux) ..."

    INPUT_DIR=/home/jenkins/workspace/gripper/Generation/Software/UnitDesignAndImpl/Implementation/Sources/

    create_and_cd_build_dir

    # remove old source file
    MAIN_SOURCE_FILE=ghex.c
    rm -f ${MAIN_SOURCE_FILE}
    check_deleted ${MAIN_SOURCE_FILE}

    # concatenate all source files
    #remove by VB
	#cat $INPUT_DIR/../../Libraries/Cmocka/maintest.c >> ${MAIN_SOURCE_FILE}
    for file in `find $INPUT_DIR -name "*.c"`; do
      cat $file >> ${MAIN_SOURCE_FILE}
    done

    # remove problematic macro
    sed -e "/__EEPROM/ d" -i ${MAIN_SOURCE_FILE}

    # copy all header files
    for file in `find $INPUT_DIR -name "*.h"`; do
      cp $file .
    done
    for file in `find $INPUT_DIR/../../Libraries -name "*.h"`; do
      cp $file .
    done

    # compile source to llvm bytecode
    clang-3.4 -emit-llvm -c -g -I. ${MAIN_SOURCE_FILE} -o ghex.bc

    # run klee on bytecode
    klee -max-time 2 ghex.bc 2>&1 | tee ../../$OUTPUT

    #svn revert --depth=infinity $INPUT_DIR

    # parse output file and generate bamboo Test.xml output
    bamboo_test_xml ${OUTPUT_DIR} ../../${OUTPUT} Klee "warning"

    rm -fr ~/KleeTest.tar
    find . -name "Test.xml" | xargs tar -cvf ~/KleeTest.tar

    echo "... running Klee on remote host (linux) done."
}

function trigger_run_klee() {
    OUTPUT_DIR=$1
    OUTPUT=$2
    SUBPHASE=$3

    SCRIPT=./General/BuildServer/Tools/CommonScripts/KleeLinux.sh

    echo "Running tests on remote host for simulator (linux) ..."

    echo "Running the remote script ..."
    remote_call ${SCRIPT} ${OUTPUT_DIR} ${OUTPUT} ${SUBPHASE} || EXIT=1
    check "the remote script"
    
    echo "Getting the test result ..."
    scp -i ${KEY} ${USER}@${HOST}:KleeTest.tar . || EXIT=1
    check_output KleeTest.tar

    tar -xvf KleeTest.tar
    echo "Removing old file ..."
    rm -f KleeTest.tar

    echo "... running tests on remote host for simulator (linux) done."
    pause
}

function trigger_run_klee_sui() {
    OUTPUT_DIR=$1
    OUTPUT=$2
    SUBPHASE=$3

    SCRIPT=./Software/UnitDesignAndImpl/Analysis/Klee/KleeLinux.sh

    echo "Running Klee unit tests on remote host (linux) ..."

    echo "Running the remote script ..."
    remote_call ${SCRIPT} ${OUTPUT_DIR} ${OUTPUT} ${SUBPHASE} || EXIT=1
    check "the remote script"

    echo "Getting the test result ..."
    scp -i ${KEY} ${USER}@${HOST}:KleeTest.tar . || EXIT=1
    check_output KleeTest.tar

    tar -xvf KleeTest.tar
    echo "Removing old file ..."
    rm -f KleeTest.tar

    echo "... running Klee unit tests on remote host (linux) done."
    pause 
}

function trigger_run_klee_sit() {
    OUTPUT_DIR=$1
    OUTPUT=$2
    SUBPHASE=$3

    SCRIPT=./Software/SIT/TestReport/Klee/KleeLinux.sh

    echo "Running Klee unit tests on remote host (linux) ..."

    echo "Running the remote script ..."
    remote_call ${SCRIPT} ${OUTPUT_DIR} ${OUTPUT} ${SUBPHASE} || EXIT=1
    check "the remote script"
    
    echo "Getting the test result ..."
    scp -i ${KEY} ${USER}@${HOST}:KleeTest.tar . || EXIT=1
    check_output KleeTest.tar

    tar -xvf KleeTest.tar
    echo "Removing old file ..."
    rm -f KleeTest.tar

    echo "... running Klee unit tests on remote host (linux) done."
    pause
}

function generate_binaries() {
    OUTPUT_DIR="../../../WP6-8.5.3_SVR-R/"
    OUTPUT="$1"
    SUBPHASE=$2

    create_and_cd_build_dir

    echo "Running cmake ..."
    cmake -DCMAKE_TOOLCHAIN_FILE=${CYGWIN_TOOLCHAIN_FILE} ${CMAKELISTS_DIR} 2>&1 | tee "${OUTPUT_DIR}/${OUTPUT}" || EXIT=1
    check "cmake"

    echo "Running make ..."
    make 2>&1 | tee -a "${OUTPUT_DIR}/${OUTPUT}" || EXIT=1
    check "make"
}

