#!/bin/bash

OUTPUT_DIR=$1
OUTPUT=$2
SUBPHASE=$3

cd ${OUTPUT_DIR}/RemoteSimulator

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

build_simulator ${OUTPUT_DIR} ${OUTPUT} ${SUBPHASE}

