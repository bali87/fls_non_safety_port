@echo off
set DEBUG_MODE="%1"

echo "Checking for bash.exe ..."
for %%X in (bash.exe) do (set FOUND_BASH=%%~$PATH:X)
if not defined FOUND_BASH (
    echo "ERROR: bash.exe not found, exiting"
    echo "Please install Cygwin (x86 32bit) and ensure that its executables are in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... bash.exe found."

echo "Checking for make.exe ..."
for %%X in (make.exe) do (set FOUND_MAKE=%%~$PATH:X)
if not defined FOUND_MAKE (
    echo "ERROR: make.exe not found, exiting"
    echo "Please install Make (through Cygwin x86 32bit) and ensure that its executable is in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... make.exe found."

echo "Checking for cmake.exe ..."
for %%X in (cmake.exe) do (set FOUND_CMAKE=%%~$PATH:X)
if not defined FOUND_CMAKE (
    echo "ERROR: cmake.exe not found, exiting"
    echo "Please install Cmake (through Cygwin x86 32bit) and ensure that its executable is in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... cmake.exe found."

echo "Checking for gcc.exe ..."
for %%X in (gcc.exe) do (set FOUND_GCC=%%~$PATH:X)
if not defined FOUND_GCC (
    echo "ERROR: gcc.exe not found, exiting"
    echo "Please install GCC-core (through Cygwin x86 32bit) and ensure that its executable is in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... gcc.exe found."

echo "Checking for ssh.exe ..."
for %%X in (ssh.exe) do (set FOUND_SSH=%%~$PATH:X)
if not defined FOUND_SSH (
    echo "ERROR: ssh.exe not found, exiting"
    echo "Please install OpenSSH (through Cygwin x86 32bit) and ensure that its executable is in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... ssh.exe found."

echo "Checking for unzip.exe ..."
for %%X in (unzip.exe) do (set FOUND_UNZIP=%%~$PATH:X)
if not defined FOUND_UNZIP (
    echo "ERROR: unzip.exe not found, exiting"
    echo "Please install unzip (through Cygwin x86 32bit) and ensure that its executable is in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... unzip.exe found."

echo "Checking for xsltproc.exe ..."
for %%X in (xsltproc.exe) do (set FOUND_XSLTPROC=%%~$PATH:X)
if not defined FOUND_XSLTPROC (
    echo "ERROR: xsltproc.exe not found, exiting"
    echo "Please install xsltproc (through Cygwin x86 32bit) and ensure that its executable is in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... xsltproc.exe found."

echo "Checking for cccc.exe ..."
for %%X in (cccc.exe) do (set FOUND_DOT=%%~$PATH:X)
if not defined FOUND_DOT (
    echo "ERROR: cccc.exe not found, exiting"
    echo "Please install cccc and ensure that its executable is in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... cccc.exe found"

echo "Checking for doxygen.exe ..."
for %%X in (doxygen.exe) do (set FOUND_DOXYGEN=%%~$PATH:X)
if not defined FOUND_DOXYGEN (
    echo "ERROR: doxygen.exe not found, exiting"
    echo "Please install Doxygen and ensure that its executable is in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... doxygen.exe found."

echo "Checking for dot.exe ..."
for %%X in (dot.exe) do (set FOUND_DOT=%%~$PATH:X)
if not defined FOUND_DOT (
    echo "ERROR: dot.exe not found, exiting"
    echo "Please install Graphviz and ensure that its executables are in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... dot.exe found."

echo "Checking for hhc.exe ..."
for %%X in (hhc.exe) do (set FOUND_HHC=%%~$PATH:X)
if not defined FOUND_HHC (
    echo "ERROR: hhc.exe not found, exiting"
    echo "Please install HTML Workshop and ensure that its executable is in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... hhc.exe found."

echo "Checking for artt.exe ..."
for %%X in (artt.exe) do (set ARTT_PYTHON=%%~$PATH:X)
if not defined ARTT_PYTHON (
    echo "ERROR: artt.exe not found, exiting"
    echo "Please install artt and ensure that its executable is in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... artt.exe found."

echo "Checking for python.exe ..."
for %%X in (python.exe) do (set FOUND_PYTHON=%%~$PATH:X)
if not defined FOUND_PYTHON (
    echo "ERROR: python.exe not found, exiting"
    echo "Please install Python3 and ensure that its executable is in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... python.exe found."

python VerifySystemSetupPythonPackages.py

rem echo "Checking for excel.exe ..."
rem for %%X in (excel.exe) do (set FOUND_EXCEL=%%~$PATH:X)
rem if not defined FOUND_EXCEL (
rem    echo "ERROR: excel.exe not found, exiting"
rem    echo "Please install Microsoft Office."
rem    pause
rem    exit /b 1
rem )
rem echo "... excel.exe found."

echo "Checking for lint-nt.exe ..."
for %%X in (lint-nt.exe) do (set FOUND_LINTNT=%%~$PATH:X)
if not defined FOUND_LINTNT (
    echo "ERROR: lint-nt.exe not found, exiting"
    echo "Please install PCLint and ensure that its executable is in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... lint-nt.exe found."

echo "Checking for xc8.exe ..."
for %%X in (xc8.exe) do (set FOUND_XC8=%%~$PATH:X)
if not defined FOUND_XC8 (
    echo "ERROR: xc8.exe not found, exiting"
    echo "Please install PCLint and ensure that its executable is in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... xc8.exe found."

echo "Checking for mdb.bat ..."
for %%X in (mdb.bat) do (set FOUND_MDB=%%~$PATH:X)
if not defined FOUND_MDB (
    echo "ERROR: mdb.bat not found, exiting"
    echo "Please install PCLint and ensure that its executable is in PATH."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... xc8.exe found."

echo "Checking for JAVA_HOME environment variable..."
IF NOT DEFINED JAVA_HOME (
    echo "ERROR: JAVA_HOME environment variable not defined, exiting."
    echo "Please install Java JDK and setup JAVA_HOME environment variable properly."
    if %DEBUG_MODE% NEQ "skip" pause
    exit /b 1
)
echo "... JAVA_HOME environment variable set."





echo "Setting files permissions (at cygwin level) ..."
bash.exe -c "chmod 600 RSA_key"
bash.exe -c "mkdir -p /home/$USER"
bash.exe -c "mkdir -p /home/$USER/.ssh"
bash.exe -c "chmod 700 ../../../Software/UnitDesignAndImpl/Implementation/Libraries/Cmocka/*.dll"
echo "... done."

if %DEBUG_MODE% NEQ "skip" pause

