# -*- coding: utf-8 -*-
""" Generate project release with ISO 26262 work products. """

import generator.release as gr
import sys

def main(index_only=None):
    """ Main. """

    rel = gr.Release('Gripper Hex Sensor', ['logo.png', 'logo.jpg', 'exida_logo_small.png'],
                  index_only)

    rel.write('# Phase P6_PDSL - Product development at software level')
    rel.add_dir('P6_PDSL')


    # rel.write('## Subphase SP6-6_SSSR - Specification of software safety requirements')
    #rel.addDir('/P6_PDSL/SP6-6_SSSR')
    #rel.write('## Subphase SP6-7_SAD - Software architectural design>> %README_MD%')
    #rel.addDir('/P6_PDSL/SP6-7_SAD')


    rel.write('## Subphase SP6-8_SUDI - Software unit design and implementation')
    rel.add_dir('P6_PDSL/SUDI')

    rel.write('### Work product WP6-8.5.1_SUDS - Software unit design specification')
    rel.add_dir('P6_PDSL/SUDI/SUDS')

    rel.add_item(
        'Software unit design specification, as a CHM file, generated with '
        'doxygen. The complete unit design is done in doxygen format. It is not '
        'just a mere commenting (documentation) of source code, but a complete '
        'unit design.',
        'P6_PDSL/SUDI/SUDS',
        'SUDS.chm')

    if not index_only:
        rel.write('### Work product WP6-8.5.2_SUI - Software unit implementation')
        rel.add_dir('P6_PDSL/SUDI/SUI')

        rel.add_item(
            'Source code, organized into layers/subsystems/modules/units.',
            'P6_PDSL/SUDI/SUI/Sources')

        rel.add_item(
            'cmake project file.',
            'P6_PDSL/SUDI/SUI',
            'CMakeLists.txt')

    rel.write('### Work product WP6-8.5.3_SVR-R - '
              'Software verification report')
    rel.add_dir('P6_PDSL/SUDI/SVR-R')

    rel.add_item(
        'Software verification report - code metrics.',
        'P6_PDSL/SUDI/SVR-R',
        'Software verification report - code metrics.chm')

    rel.add_item(
        'Software verification report - compiler warnings.',
        'P6_PDSL/SUDI/SVR-R',
        'Software verification report - compiler log.txt')

    rel.add_item(
        'Software verification report - MISRA report, against MISRA-C:2004 rules.',
        'P6_PDSL/SUDI/SVR-R',
        'Software verification report - MISRA report.xlsx')

    rel.write('## Subphase SP6-9_SUT - Software unit testing')
    rel.add_dir('P6_PDSL/SP6-9_SUT')

    if not index_only:
        rel.write('### Work product WP6-9.5.2_SVS - Software verification specification')
        rel.add_dir('P6_PDSL/SUT/SVS')

        rel.add_item(
            'Specification of unit tests (as doxygen) and the implementation of unit tests.',
            'P6_PDSL/SUT/SVS/Sources')

        rel.copy('General/BuildServer/Project/.idea', 'misc.xml')
        rel.copy('General/BuildServer/Project/.idea', 'modules.xml')
        rel.copy('General/BuildServer/Project/.idea', 'vcs.xml')
        rel.copy('General/BuildServer/Project/.idea', 'Project.iml')

        rel.add_item(
            'cmake project file, for tests.',
            'P6_PDSL/SUT/SVS',
            'CMakeLists.txt')

        #rel.add_item(
        #    'CMocka test framework, containing both source and compiled libraries. Run build.sh to compile '
        #    'on your target if needed.',
        #    'P6_PDSL/SP6-9_SUT/WP6-9.5.2_SVS/Cmocka')

        #rel.add_item(
        #    'Cygwin Dynamic Link Libraries. Needed to run compiled test without Cygwin installed.',
        #    'P6_PDSL/SP6-9_SUT/WP6-9.5.2_SVS/Cygwin')

    rel.write('### Work product WP6-9.5.3_SVR-R - Software verification report')
    rel.add_dir('P6_PDSL/SUT/SVR-R')

    #if not index_only:
    #    rel.add_item(
    #        'Windows executable, which executes all unit tests on windows. Ready to '
    #        'run on windows from command line, it displays test results on screen.',
    #        'P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/Test_windows')

    rel.add_item(
        'Software verification report - address sanitizer runtime output.',
        'P6_PDSL/SUT/SVR-R',
        'Software verification report - address sanitizer runtime output (linux gcc 32 bit).txt')

    rel.add_item(
        'Software verification report - coverage report.',
        'P6_PDSL/SUT/SVR-R',
        'Software verification report - code coverage.chm')

    #rel.add_item(
    #    'Software verification report - compiler log.',
    #    'P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R',
    #    '6-9.5.3 Software verification report - compiler log.txt')

    rel.add_item(
        'Software verification report - results of unit testing (which tests passed and which failed, if any).',
        'P6_PDSL/SUT/SVR-R',
        'Software verification report - results of unit testing (linux gcc 32 bit).txt')

    rel.add_item(
        'Software verification report - dr memory runtime output.',
        'P6_PDSL/SUT/SVR-R',
        'Software verification report - dr memory runtime output (linux gcc 32 bit).txt')

    rel.finalize()


if __name__ == '__main__':
    if len(sys.argv) == 2 and sys.argv[1] == 'index':
        main(True)
    else:
        main()

