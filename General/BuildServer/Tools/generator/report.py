# -*- coding: utf-8 -*-
""" Generate all reports. """

import subprocess
import sys
import os
import platform

def pause():
    try:
        input("Press ENTER to start the step.")
    except EOFError:
        pass

class Report(object):
    """ Report. """

    def __init__(self, report_path, report_script):
        """ Constructor.
        :param report_script: Script execution path.
        :param report_script: Script file name.
        """
        self.report_path = report_path
        self.report_script = report_script

    def generate(self):
        """ Generate report. """
        cwd = os.getcwd()
        os.chdir(self.report_path)
        error_code = 1
        try:
            if (platform.system() == "Windows"):
                error_code = subprocess.call(self.report_script)
            else:
                error_code = subprocess.call('./' + self.report_script)
            if error_code:
                print('Generation of %s failed with exit code %d...' % (self.report_script, error_code))
        except OSError:
            print('Generation of %s failed ...' % self.report_script)
        os.chdir(cwd)
        return error_code

    def get_name(self):
        """ Return script file name. """
        return self.report_script


class ReportsGenerator(object):
    """ Report generator. """

    def __init__(self):
        """ Constructor. """
        self.reports = []

    def register(self, report_path, report_script):
        """ Register report.
        :param report_path: Script execution path (relative to ISO root).
        :param report_script: Script file name.
        """
        self.reports.append(Report(report_path, report_script))

    def generate_all(self):
        """ Generate all scripts. """
        for (i, report) in enumerate(self.reports):
            print ('=======================================')
            print ('Step %d / %d - %s....' % (i+1, len(self.reports), report.get_name()))
            pause()
            if report.generate():
                sys.exit(1)
            print ('Step %d / %d finished' % (i+1, len(self.reports)))


def set_executable(filename):
    """ Set executable file used to generate reports """
    with open('executable.txt', 'w') as f:
        f.write(filename)


