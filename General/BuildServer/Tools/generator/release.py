# -*- coding: utf-8 -*-
""" Generate project release with ISO 26262 work products. """

import shutil
import os
import datetime
import markdown
import re
import sys

INDEX_PREFIX = '.'



class MdFileWrap(object):
    """ Markdown file wrapper. """

    def __init__(self, htmlfile, project_name, project_version, project_logos):
        """ Constructor. """
        self.htmlfile = htmlfile
        self.project_name = project_name
        self.project_version = project_version
        self.project_logos = project_logos
        self.content = ''

    def write(self, text):
        """ Write a line. """
        self.content = self.content + text

    def close(self, index_only=None):
        """ Save the as a processed markdown content to a file. """

        html_header = """<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<link href="ReadmeFiles/fsssonts.css"  type="text/css" rel="stylesheet">

<link href="ReadmeFiles/bootstrap.css" rel="stylesheet">
<link href="ReadmeFiles/font-awesome.min.css" rel="stylesheet">
<link href="ReadmeFiles/animation.css" rel='stylesheet' id='theme-css' type='text/css' media='all' />
<link href="ReadmeFiles/custom.css" rel="stylesheet">
<link href="ReadmeFiles/responsive.css" rel="stylesheet">

<script>
    function iealert() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (! (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)))
        {
            document.getElementById('iealert').innerHTML = '<pre>NOTE: This document works better with Internet Explorer.</pre>'
        }
		
		return false;
	}
</script>
"""
        htmltext = markdown.markdown(self.content)
        htmltext = re.sub('<head>',
                          '<head><link rel="stylesheet" href="style.css">',
                          htmltext)
        mdhtml = open(self.htmlfile, "w")
        if index_only:
            html_header = re.sub('ReadmeFiles', 'General/BuildServer/Tools/ReadmeFiles', html_header)
        mdhtml.write(html_header)
        mdhtml.write("<title>%s ver. %s</title></head>" %
                     (self.project_name, self.project_version))
        mdhtml.write("""<body onload="iealert();"><div id="iealert"></div>""")
        for logo in self.project_logos:
            if index_only:
                mdhtml.write('<img src="General/BuildServer/Tools/ReadmeFiles/%s" />' % logo)
            else:
                mdhtml.write('<img src="ReadmeFiles/%s" />' % logo)
        mdhtml.write(htmltext)
        mdhtml.write("</html>")
        mdhtml.close()


def sanitize_version(version):
    """ Strip all characters that are not a dot nor a digit.
    :param version: String to sanitize.
    :return: Sanitized string.
    """
    return ''.join(list(filter(lambda x: x == '.' or x.isdigit(), version)))


class Release(object):
    """ Release generator. """

    def __init__(self, project_name, project_logos, index_only=None):
        """Create a release instance.
        :param project_name: String with project name.
        :param project_logos: List of a images to be included in readme file.
        """
        self.index_only = index_only
        # FIXME: version.txt
        project_version = "0.1" #sanitize_version(open('version.txt', 'r').read())
        self.iso_root = '../../..'
        self.release_id = ('Release_' + re.sub(' ', '_', project_name) +
                           '_' + datetime.datetime.now().date().isoformat())
        print(self.release_id)
        self.release_dir = self.release_id
        print("Generating release %s ..." % self.release_id)
        if os.path.exists(self.release_dir):
            shutil.rmtree(self.release_dir)
        self.release_zip = (self.iso_root + '/General/BuildServer/WP1-23.5.22_R/' +
                            self.release_id)
        self.add_dir()
        self.add_dir(self.release_dir)
        self.release_dir += '/' + self.release_dir
        if self.index_only:
            output = '../../../Index.html'
        else:
            output = self.release_dir + '/Readme.html'
        self.mdfile = MdFileWrap(output, project_name, project_version, project_logos)
        shutil.copytree('ReadmeFiles', self.release_dir + '/ReadmeFiles')
        shutil.copyfile('ShowReadme.bat', self.release_dir + '/ShowReadme.bat')
        self.write('# %s - Project release information' %
                   (project_name))
        self.write('This file lists the work products that are part of this'
                   ' release. The work product are organized according to ISO'
                   ' 26262 work products (grouped in phases, subphases and'
                   ' then work products).')
        self.write("(C) exida, %s" % datetime.datetime.now().year)

    def write(self, line, suffix='\n\n'):
        """ Write line with suffix.
        :param line: Content of line.
        :param suffix: Optional argument. By default two EOLs are used as line suffix.
        :return: None.
        """
        self.mdfile.write(line + suffix)

    def add_dir(self, dirname=''):
        """ Create directory in target release directory.
        :param dirname: Relative path (with respect to ISO root).
        :return: None.
        """
        target = self.release_dir + '/' + dirname
        if not os.path.exists(target) and not self.index_only:
            os.mkdir(target)

    def copy(self, path, filename=None):
        """ Copy file or directory to release directory.
        :param path: Relative path (with respect to ISO root).
        :param filename: Optional argument. If skipped, directory will be
        copied recursively.
        :return: None.
        """
        if filename:
            src = self.iso_root + '/' + path + '/' + filename
            dst_dir = self.release_dir + '/' + path
            dst = dst_dir + '/' + filename
            if not self.index_only:
                if not os.path.exists(dst_dir):
                    os.makedirs(dst_dir)
                shutil.copyfile(src, dst)
        else:
            if not self.index_only:
                shutil.copytree(self.iso_root + '/' + path,
                                self.release_dir + '/' + path)

    def add_item(self, description, item_location, item_name=None):
        """ Add item to release.

        :param md_description: Description used in markdown file.
        :param item_location: Relative path.
        :param item_name: Optional argument. If skipped, directory will be copied recursively.
        :return: None.
        """
        if item_name:
            print("adding %s ..." % item_name)
            self.copy(item_location, item_name)
            item_location = item_location + '/' + item_name
        else:
            print("adding %s ..." % item_location)
            self.copy(item_location)
        if not self.index_only:
            md_description = '[%s](./%s) %s' % (item_location, item_location, description)
        else:
            md_description = '[%s](%s/%s) %s' % (item_location, INDEX_PREFIX, item_location, description)
        self.write(md_description)

    def finalize(self):
        """ Prepare release.

        Invoke markdown processor by closing markdown file and compress the
        release directory.
        :return: None
        """
        self.write("""
## Disclaimer of Warranty

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

## Limitation of Liability

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
""")

        self.mdfile.close(self.index_only)
        if not self.index_only:
            print("compressing %s.zip ..." % self.release_zip)
            shutil.make_archive(self.release_zip, 'zip', self.release_dir + '/../')

