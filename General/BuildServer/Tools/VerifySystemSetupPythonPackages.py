import sys

print("Checking for python markdown ...")
try:
    import markdown
    print("... python markdown found")
except:
    print("ERROR: python markdown not found, exiting")
    print("Please install python markdown package.")
    input('Press Enter to continue ...')
    sys.exit(1)
