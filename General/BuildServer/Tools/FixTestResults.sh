#!/bin/bash

# fix the "." so that it is correctly displayed in bamboo (no dots in the last string) and additionally extend it with NonInstrumented label
#find -name "*.xml" -exec sed -e "s/WP6-10.5.2_SVS-R/WP6-10_5_2_SVS-R_NonInstrumented/g" -i {}  \;
# fix the "." so that it is correctly displayed in bamboo (no dots in the last string) and additionally extend it with NonInstrumented label
#find -name "*.xml" -exec sed -e "s/SP6-9_SUT\/WP6-9.5.2_SVS/SP6-09_SUT\/WP6-09_5_2_SVS_NonInstrumented/g" -i {}  \;


echo "Test.xml file list:"
# which find
# which sed
find ../../.. -name "Test.xml"
echo "starting the conversion step 1 ..."
find ../../.. -name "Test.xml" -exec sed -e "s/WP\([[:digit:]]\)-\([[:digit:]][[:digit:]]\)\.\([[:digit:]]\)\.\([[:digit:]]\)_/WP\1-\2_\3_\4_/g" -i {} \;
echo "... step 1 done, starting step 2 ..."
find ../../.. -name "Test.xml" -exec sed -e "s/WP\([[:digit:]]\)-\([[:digit:]]\)\.\([[:digit:]]\)\.\([[:digit:]]\)_/WP\1-0\2_\3_\4_/g" -i {} \;
echo "... step 2 done, starting step 3 ..."
find ../../.. -name "Test.xml" -exec sed -e "s/SP\([[:digit:]]\)-\([[:digit:]]\)_/SP\1-0\2_/g" -i {} \;
echo "... xml parse finished"
