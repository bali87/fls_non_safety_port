# -*- coding: utf-8 -*-
""" Generate all reports. """

import generator.report as gr

def main():
    """ Main. """
    gr.set_executable('gHex')

    generator = gr.ReportsGenerator()
    # Generate Doxygen
    generator.register('../../../P6_PDSL/SP6-8_SUDI/WP6-8.5.1_SUDS/Doxygen', 'Specification.bat')
    # Compile
    generator.register('../../../P6_PDSL/SP6-8_SUDI/WP6-8.5.2_SUI/Generators', 'GenerateTransformerCode.bat')
    generator.register('../../../P6_PDSL/SP6-8_SUDI/WP6-8.5.2_SUI/Scripts', 'GenerateImplementationBinaries.bat')
    # Generate metrics, reports 
    generator.register('../../../P6_PDSL/SP6-8_SUDI/WP6-8.5.3_SVR-R/CodeMetrics', 'CodeMetrics.bat')
    generator.register('../../../P6_PDSL/SP6-8_SUDI/WP6-8.5.3_SVR-R/MISRA', 'Misra.bat')
    generator.register('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/AddressSanitizer', 'AddressSanitizer.bat')
    generator.register('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/DrMemory', 'DrMemory.bat')
    generator.register('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/LCov', 'LCov.bat')
    generator.register('../../../P6_PDSL/SP6-9_SUT/WP6-9.5.3_SVR-R/ClangInstrumented', 'ClangInstrumented.bat')

    generator.generate_all()


if __name__ == '__main__':
    main()

