/**
 * @file
 *
 * Unit that provides mock objects for the unit testing of Timer2, for the objects in dependent units:
 *  - define-s,
 *  - global variables,
 *  - global functions.
 *
 * 
 * @version $Id: CSUTMockFileForOneTestedUnit.c 102 2017-11-02 16:53:53Z piotr.serwa $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalDefines Mocks for global defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalVariables Mocks for global variables
 *  @{ 
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{ 
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */
volatile uint16 TMR3HLD;
volatile uint16 T2CON;
volatile uint16 PR2;
volatile uint16 TMR2;
volatile uint16 PR3;
volatile uint16 TMR3;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalFunctionsDefinitions Mocks for global functions definitions
 *  @{
 */



/** @} */
