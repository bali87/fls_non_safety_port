/**
 * @file
 *
 * Unit that provides unit tests for the unit Timer2.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "Timer2.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void Timer2SUTGetValue_TestWithNullValues(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest Timer2SUTGetValue_Tests[] = 
{
	cmocka_unit_test(Timer2SUTGetValue_TestWithNullValues)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/**
 * @test Timer2SUTGetValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void Timer2SUTGetValue_TestWithNullValues(void ** state)
{

    /* given */
    uint8_t retVal;
    TMR2 = 0x00U;
    TMR3HLD = 0x00U;
    /* when */
    retVal = Timer2_GetValue();
    /* then */
    assert_int_equal(retVal, 0U);


    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(Timer2SUTGetValue_Tests, NULL, NULL)

/** @} */
