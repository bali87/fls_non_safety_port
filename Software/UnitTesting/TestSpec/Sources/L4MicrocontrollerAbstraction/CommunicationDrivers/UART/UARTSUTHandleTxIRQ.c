/**
 * @file
 *
 * Unit that provides unit tests for the unit UART.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "UART.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void UARTSUTHandleTxIRQ_TestDataRegValue(void ** state);
static void UARTSUTHandleTxIRQ_TestEmptyFIFO(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest UARTSUTHandleTxIRQ_Tests[] = 
{

		cmocka_unit_test(UARTSUTHandleTxIRQ_TestDataRegValue),
		cmocka_unit_test(UARTSUTHandleTxIRQ_TestEmptyFIFO)

};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */

/**
 * Initialization of tested unit.
 */


/**
 * @test UARTSUTHandleTxIRQ_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void UARTSUTHandleTxIRQ_TestDataRegValue(void ** state)
{

    /* given */
    //uint8_t retVal;
	IFS0 = 0xFFFFU;
	UART_TxFIFO.state = FIFO_NOT_EMPTY;
	expect(FIFO_GetData,descrPtr,&UART_TxFIFO);
	will_return(GET_U1STA,(0u << _U1STA_UTXBF_POSITION));
	will_return(FIFO_GetData,0x55);
	will_return(FIFO_GetData,STD_RETURN_OK);
	//will_return(GET_U1STA,(1u << _U1STA_UTXBF_POSITION));
	/* when */
    UART_HandleTxIRQ();
    /* then */
    assert_int_equal(U1TXREG, 0x55U);
    assert_int_equal(IFS0, (0xFFFFU & ~(1U << _IFS0_U1TXIF_POSITION)));

    return;
}
/**
 * @test UARTSUTHandleTxIRQ_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void UARTSUTHandleTxIRQ_TestEmptyFIFO(void ** state)
{

    /* given */
    //uint8_t retVal;
	UART_TxFIFO.state = FIFO_EMPTY;
	IEC0 = 0xFFFFU;
	IFS0 = 0xFFFFU;
	//will_return(GET_U1STA,(1u << _U1STA_UTXBF_POSITION));
	/* when */
    UART_HandleTxIRQ();
    /* then */

    assert_int_equal(IFS0, (0xFFFFU & ~(1U << _IFS0_U1TXIF_POSITION)));

    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(UARTSUTHandleTxIRQ_Tests, NULL, NULL)

/** @} */
