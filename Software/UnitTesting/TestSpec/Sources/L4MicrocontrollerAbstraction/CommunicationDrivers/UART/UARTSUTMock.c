/**
 * @file
 *
 * Unit that provides mock objects for the unit testing of UART, for the objects in dependent units:
 *  - define-s,
 *  - global variables,
 *  - global functions.
 *
 * 
 * @version $Id: CSUTMockFileForOneTestedUnit.c 102 2017-11-02 16:53:53Z piotr.serwa $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"
#include "FIFO.h"
#include "GPIO.h"
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalDefines Mocks for global defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalVariables Mocks for global variables
 *  @{ 
 */
volatile uint16_t  	IEC0;
volatile uint16_t  	IFS0;
volatile uint16_t  	IFS4;
volatile uint16_t  	IPC2;
volatile uint16_t  	IPC3;

volatile uint16_t 	U1TXREG;
volatile uint16_t 	U1RXREG;
volatile uint16_t 	U1ADMD;
volatile uint16_t 	U1BRG;
volatile uint16_t 	U1STA;
volatile uint16_t 	U1MODE;
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{ 
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalFunctionsDefinitions Mocks for global functions definitions
 *  @{
 */

Std_ReturnType FIFO_InitDescriptor(FIFO_DescrType * descrPtr, uint8 * buffPtr, uint16 buffLength)
{
	Std_ReturnType retVal = (Std_ReturnType)mock(FIFO_InitDescriptor);
	return (retVal);
}
Std_ReturnType FIFO_GetData(FIFO_DescrType * descrPtr,uint8 * dataPtr)
{
Std_ReturnType retVal;
	check(FIFO_GetData,descrPtr);
	//check(FIFO_GetData,dataPtr);
	*dataPtr = (uint8)mock(FIFO_GetData);
	retVal = (Std_ReturnType)mock(FIFO_GetData);
return (retVal);
}
Std_ReturnType FIFO_GetFreeSize(FIFO_DescrType * descrPtr,uint16 * sizePtr)
{
Std_ReturnType retVal;
	check(FIFO_GetFreeSize,descrPtr);
	//check(FIFO_GetFreeSize,sizePtr);
	*sizePtr = (uint16_t)mock(FIFO_GetFreeSize);
	retVal = (Std_ReturnType)mock(FIFO_GetFreeSize);
return (retVal);
}
Std_ReturnType FIFO_PutData(FIFO_DescrType * descrPtr,uint8 data)
{
Std_ReturnType retVal;
	check(FIFO_PutData,descrPtr);
	check(FIFO_PutData,data);
	retVal = (Std_ReturnType)mock(FIFO_PutData);
return (retVal);
}
uint16_t GET_U1STA(void)
{
	return ((uint16_t)mock(GET_U1STA));
}
Std_ReturnType FIFO_ClearFIFO( FIFO_DescrType *descrPtr )
{
	Std_ReturnType retVal;
	check(FIFO_ClearFIFO,descrPtr);
	retVal = (Std_ReturnType)mock(FIFO_ClearFIFO);
	return (retVal);
}
Std_ReturnType GPIO_SetPinValue(GPIO_PortType gpioPort,uint8 pin,uint8 value)
{

	return (STD_RETURN_OK);
}
/** @} */
