/**
 * @file
 *
 * Unit that provides unit tests for the unit UART.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "UART.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */



static void UARTSUTPutMessage_TestBuffPtrEqNULL(void ** state);
static void UARTSUTPutMessage_TestFIFOFreeSizeLessThanSize(void ** state);
static void UARTSUTPutMessage_TestFIFOFreeSizeEqSize(void ** state);
static void UARTSUTPutMessage_TestUARTBuffNotFULL(void ** state);
static void UARTSUTPutMessage_FIFOGetError(void ** state);
static void UARTSUTPutMessage_CheckInterruptEnabled(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest UARTSUTPutMessage_Tests[] = 
{
	cmocka_unit_test(UARTSUTPutMessage_TestBuffPtrEqNULL),
	cmocka_unit_test(UARTSUTPutMessage_TestFIFOFreeSizeLessThanSize),
	cmocka_unit_test(UARTSUTPutMessage_TestFIFOFreeSizeEqSize),
	cmocka_unit_test(UARTSUTPutMessage_TestUARTBuffNotFULL),
	cmocka_unit_test(UARTSUTPutMessage_FIFOGetError),
	cmocka_unit_test(UARTSUTPutMessage_CheckInterruptEnabled)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */




/**
 * @test UARTSUTPutMessage_TestLenPtrEqNULL
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void UARTSUTPutMessage_TestBuffPtrEqNULL(void ** state)
{

    /* given */
    uint8_t retVal;
    uint8_t len = 4U;
    /* when */
    retVal = UART_PutMessage(NULL,len);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);

 return;
}
/**
 * @test UARTSUTPutMessage_TestLenPtrEqNULL
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void UARTSUTPutMessage_TestFIFOFreeSizeLessThanSize(void ** state)
{

    /* given */
    uint8_t retVal;

    uint8_t msgBuffer[12];
    expect(FIFO_GetFreeSize,descrPtr,&UART_TxFIFO);
	will_return(FIFO_GetFreeSize,11u);
	will_return(FIFO_GetFreeSize,STD_RETURN_OK);
    /* when */
    retVal = UART_PutMessage(msgBuffer,12u);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);
 return;
}
static void UARTSUTPutMessage_TestFIFOFreeSizeEqSize(void ** state)
{

    /* given */
    uint8_t retVal;

    uint8_t msgBuffer[3];


    expect(FIFO_GetFreeSize,descrPtr,&UART_TxFIFO);
	will_return(FIFO_GetFreeSize,3u);
	will_return(FIFO_GetFreeSize,STD_RETURN_OK);

	expect(FIFO_PutData,descrPtr,&UART_TxFIFO);
	expect(FIFO_PutData,data,msgBuffer[0]);
	will_return(FIFO_PutData,STD_RETURN_OK);

	expect(FIFO_PutData,descrPtr,&UART_TxFIFO);
	expect(FIFO_PutData,data,msgBuffer[1]);
	will_return(FIFO_PutData,STD_RETURN_OK);

	expect(FIFO_PutData,descrPtr,&UART_TxFIFO);
	expect(FIFO_PutData,data,msgBuffer[2]);
	will_return(FIFO_PutData,STD_RETURN_OK);
	will_return(GET_U1STA,(1U << _U1STA_UTXBF_POSITION));

    /* when */
    retVal = UART_PutMessage(msgBuffer,3U);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);
 return;
}
static void UARTSUTPutMessage_TestUARTBuffNotFULL(void ** state)
{

    /* given */
    uint8_t retVal;

    uint8_t msgBuffer[3] = {0x11U,0x22U,0x33U};
    U1STA &= ~(1U << _U1STA_UTXBF_POSITION);
	expect(FIFO_GetFreeSize,descrPtr,&UART_TxFIFO);
	will_return(FIFO_GetFreeSize,3U);
	will_return(FIFO_GetFreeSize,STD_RETURN_OK);

	expect(FIFO_PutData,descrPtr,&UART_TxFIFO);
	expect(FIFO_PutData,data,msgBuffer[0]);
	will_return(FIFO_PutData,STD_RETURN_OK);

	expect(FIFO_PutData,descrPtr,&UART_TxFIFO);
	expect(FIFO_PutData,data,msgBuffer[1]);
	will_return(FIFO_PutData,STD_RETURN_OK);

	expect(FIFO_PutData,descrPtr,&UART_TxFIFO);
	expect(FIFO_PutData,data,msgBuffer[2]);
	will_return(FIFO_PutData,STD_RETURN_OK);

	will_return(GET_U1STA,~(1U <<_U1STA_UTXBF_POSITION));
	expect(FIFO_GetData,descrPtr,&UART_TxFIFO);
	will_return(FIFO_GetData,msgBuffer[0]);
	will_return(FIFO_GetData,STD_RETURN_OK);




    /* when */
    retVal = UART_PutMessage(msgBuffer,3U);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);
    assert_int_equal(U1TXREG, msgBuffer[0]);
 return;
}
static void UARTSUTPutMessage_FIFOGetError(void ** state)
{

    /* given */
    uint8_t retVal;

    uint8_t msgBuffer[3] = {0x11U,0x22U,0x33U};
    U1STA &= ~(1U << _U1STA_UTXBF_POSITION);
    U1TXREG = 0xFFFFU;
	expect(FIFO_GetFreeSize,descrPtr,&UART_TxFIFO);
	will_return(FIFO_GetFreeSize,3U);
	will_return(FIFO_GetFreeSize,STD_RETURN_OK);

	expect(FIFO_PutData,descrPtr,&UART_TxFIFO);
	expect(FIFO_PutData,data,msgBuffer[0]);
	will_return(FIFO_PutData,STD_RETURN_OK);

	expect(FIFO_PutData,descrPtr,&UART_TxFIFO);
	expect(FIFO_PutData,data,msgBuffer[1]);
	will_return(FIFO_PutData,STD_RETURN_OK);

	expect(FIFO_PutData,descrPtr,&UART_TxFIFO);
	expect(FIFO_PutData,data,msgBuffer[2]);
	will_return(FIFO_PutData,STD_RETURN_OK);

	will_return(GET_U1STA,~(1U <<_U1STA_UTXBF_POSITION));
	expect(FIFO_GetData,descrPtr,&UART_TxFIFO);
	will_return(FIFO_GetData,msgBuffer[0]);
	will_return(FIFO_GetData,STD_RETURN_ERROR);
    /* when */
    retVal = UART_PutMessage(msgBuffer,3U);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);
    assert_int_equal(U1TXREG, 0xFFFFU);
 return;
}
static void UARTSUTPutMessage_CheckInterruptEnabled(void ** state)
{

    /* given */
    uint8_t retVal;

    uint8_t msgBuffer[3] = {0x11U,0x22U,0x33U};
    U1STA &= ~(1U << _U1STA_UTXBF_POSITION);
    IEC0 = 0x00U;
	expect(FIFO_GetFreeSize,descrPtr,&UART_TxFIFO);
	will_return(FIFO_GetFreeSize,3U);
	will_return(FIFO_GetFreeSize,STD_RETURN_OK);

	expect(FIFO_PutData,descrPtr,&UART_TxFIFO);
	expect(FIFO_PutData,data,msgBuffer[0]);
	will_return(FIFO_PutData,STD_RETURN_OK);

	expect(FIFO_PutData,descrPtr,&UART_TxFIFO);
	expect(FIFO_PutData,data,msgBuffer[1]);
	will_return(FIFO_PutData,STD_RETURN_OK);

	expect(FIFO_PutData,descrPtr,&UART_TxFIFO);
	expect(FIFO_PutData,data,msgBuffer[2]);
	will_return(FIFO_PutData,STD_RETURN_OK);

	will_return(GET_U1STA,~(1U <<_U1STA_UTXBF_POSITION));
	expect(FIFO_GetData,descrPtr,&UART_TxFIFO);
	will_return(FIFO_GetData,msgBuffer[0]);
	will_return(FIFO_GetData,STD_RETURN_OK);

    /* when */
    retVal = UART_PutMessage(msgBuffer,3U);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);
    assert_int_equal(IEC0, (1U << _IEC0_U1TXIE_POSITION));
 return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(UARTSUTPutMessage_Tests, NULL, NULL)

/** @} */
