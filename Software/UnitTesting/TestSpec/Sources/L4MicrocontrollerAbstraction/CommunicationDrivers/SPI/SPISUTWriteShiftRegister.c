/**
 * @file
 *
 * Unit that provides unit tests for the unit SPI2.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z michal.szczepan $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "SPI.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */

static void SPISUTWriteShiftRegister_NullPointerAs1stParamReturnsError(void ** state);
static void SPISUTWriteShiftRegister_ValidPointerAs1stParamReturnsOK(void **state);
static void SPISUTWriteShiftRegister_GivenValuesAsPar1WriteHWBufferRightSequence(void **state);

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest SPISUTWriteShiftRegister_Tests[] =
{
		cmocka_unit_test(SPISUTWriteShiftRegister_NullPointerAs1stParamReturnsError),
		cmocka_unit_test(SPISUTWriteShiftRegister_ValidPointerAs1stParamReturnsOK),
		cmocka_unit_test(SPISUTWriteShiftRegister_GivenValuesAsPar1WriteHWBufferRightSequence)


};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */

/**
 * @test
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTWriteShiftRegister_NullPointerAs1stParamReturnsError(void **state)
{

    /* given */

    uint8_t retVal = 0U;
    /* when */

    retVal = SPI_WriteShiftRegister(NULL);

    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTWriteShiftRegister_ValidPointerAs1stParamReturnsOK(void **state)
{

    /* given */

    uint8_t retVal = 0U;
    uint32_t dataVal = 0U;
    /* when */

    retVal = SPI_WriteShiftRegister(&dataVal);

    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);


    return;
}
/**
 * @test
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTWriteShiftRegister_GivenValuesAsPar1WriteHWBufferRightSequence(void **state)
{

    /* given */


    uint32_t dataVal = 0xFFFF5555U;
    /* when */

    SPI_WriteShiftRegister(&dataVal);

    /* then */

    assert_int_equal(SPI2BUFL, 0x5555U);
    assert_int_equal(SPI2BUFH, 0xFFFFU);

    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(SPISUTWriteShiftRegister_Tests, NULL, NULL)

/** @} */
