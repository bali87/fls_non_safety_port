/**
 * @file
 *
 * Unit that provides unit tests for the unit SPI.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "SPI.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void SPISUTDisableRxInterrupt_Test0(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest SPISUTDisableRxInterrupt_Tests[] = 
{
	cmocka_unit_test(SPISUTDisableRxInterrupt_Test0)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/**
 * @test SPISUTDisableRxInterrupt_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTDisableRxInterrupt_Test0(void ** state)
{

    /* given */
    uint8_t retVal;
    /* when */
    SPI_DisableRxInterrupt();
    /* then */
    assert_int_equal(retVal, 0U);


    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(SPISUTDisableRxInterrupt_Tests, NULL, NULL)

/** @} */
