/**
 * @file
 *
 * Unit that provides unit tests for the unit SPI2.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z michal.szczepan $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "SPI.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */

static void SPISUTReadShiftRegister_NullPointerAs1stParamReturnsError(void ** state);
static void SPISUTReadShiftRegister_ValidPointerAs1stParamReturnsOK(void **state);
static void SPISUTReadShiftRegister_GivenDataInHWBufferReadInRightSequence(void **state);


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest SPISUTReadShiftRegister_Tests[] = {
		cmocka_unit_test(SPISUTReadShiftRegister_NullPointerAs1stParamReturnsError),
		cmocka_unit_test(SPISUTReadShiftRegister_ValidPointerAs1stParamReturnsOK),
		cmocka_unit_test(SPISUTReadShiftRegister_GivenDataInHWBufferReadInRightSequence)

};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */

/**
 * @test
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTReadShiftRegister_NullPointerAs1stParamReturnsError(void **state)
{

    /* given */

    uint8_t retVal = 0U;
    /* when */

    retVal = SPI_ReadShiftRegister(NULL);

    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTReadShiftRegister_ValidPointerAs1stParamReturnsOK(void **state)
{

    /* given */

    uint8_t retVal = 0U;
    uint32_t dataVal;
    /* when */
    will_return(GET_SPI2BUFL,0x00U);
    will_return(GET_SPI2BUFH,0x00U);
    retVal = SPI_ReadShiftRegister(&dataVal);

    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);


    return;
}
/**
 * @test
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTReadShiftRegister_GivenDataInHWBufferReadInRightSequence(void **state)
{

    /* given */

    uint8_t retVal = 0U;
    uint32_t dataVal;
    /* when */
    will_return(GET_SPI2BUFL,0x5555U);
    will_return(GET_SPI2BUFH,0xFFFFU);
    retVal = SPI_ReadShiftRegister(&dataVal);

    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);
    assert_int_equal(dataVal, 0xFFFF5555U);

    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(SPISUTReadShiftRegister_Tests, NULL, NULL)

/** @} */
