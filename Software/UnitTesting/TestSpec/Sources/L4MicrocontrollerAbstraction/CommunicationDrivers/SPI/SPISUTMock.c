/**
 * @file
 *
 * Unit that provides mock objects for the unit testing of SPI2, for the objects in dependent units:
 *  - define-s,
 *  - global variables,
 *  - global functions.
 *
 * 
 * @version $Id: CSUTMockFileForOneTestedUnit.c 102 2017-11-02 16:53:53Z piotr.serwa $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalDefines Mocks for global defines
 *  @{
 */
 uint16_t GET_SPI2STATL(void)
 {
	 uint16_t retVal;
	 	retVal = (uint16_t)mock(GET_SPI2STATL);
	 	return (retVal);
 }
uint16_t GET_SPI2BUFL(void)
{
	uint16_t retVal;
		retVal = (uint16_t)mock(GET_SPI2BUFL);
		return (retVal);
}
uint16_t GET_SPI2BUFH(void)
{
	uint16_t retVal;
		retVal = (uint16_t)mock(GET_SPI2BUFH);
		return (retVal);
}
uint16_t GET_SPI2CON1L(void)
{
	uint16_t retVal;
		retVal = (uint16_t)mock(GET_SPI2CON1L);
		return (retVal);
}


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalVariables Mocks for global variables
 *  @{ 
 */
volatile uint16_t  IFS3;
volatile uint16_t  IEC3;
volatile uint16_t SPI2BUFL;
volatile uint16_t SPI2BUFH;
volatile uint16_t SPI2URDTL;
volatile uint16_t SPI2URDTH;
volatile uint16_t SPI2IMSKL;
volatile uint16_t SPI2IMSKH;
volatile uint16_t SPI2BRGL;
volatile uint16_t SPI2STATL;
volatile uint16_t SPI2CON1L;
volatile uint16_t SPI2CON2L;
volatile uint16_t SPI2CON1H;
volatile uint16_t SPI2CON2L;

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{ 
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalFunctionsDefinitions Mocks for global functions definitions
 *  @{
 */
void IRQ_NO_PSV _DefaultInterrupt(void)
{

}


/** @} */
