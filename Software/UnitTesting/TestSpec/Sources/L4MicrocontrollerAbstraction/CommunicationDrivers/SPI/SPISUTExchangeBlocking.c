/**
 * @file
 *
 * Unit that provides unit tests for the unit SPI.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "SPI.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void SPISUTExchangeBlocking_NullPointerAs1stParamReturnsError(void ** state);
static void SPISUTExchangeBlocking_NullPointerAs2ndParamReturnsError(void ** state);
static void SPISUTExchangeBlocking_ValidParamsReturnsOK(void ** state);
static void SPISUTExchangeBlocking_GivenDataAsPar1WriteHWBufferRightSequence32BitMode(void ** state);
static void SPISUTExchangeBlocking_GivenDataAsPar1WriteHWBufferRightSequence16BitMode(void ** state);
static void SPISUTExchangeBlocking_GivenDataAsPar1WriteHWBufferRightSequence8BitMode(void ** state);
static void SPISUTExchangeBlocking_GivenDataInHWBufferReadRightSequence32BitMode(void ** state);
static void SPISUTExchangeBlocking_GivenDataInHWBufferReadRightSequence16BitMode(void ** state);
static void SPISUTExchangeBlocking_GivenDataInHWBufferReadRightSequence8BitMode(void ** state);


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest SPISUTExchangeBlocking_Tests[] = 
{
	cmocka_unit_test(SPISUTExchangeBlocking_NullPointerAs1stParamReturnsError),
	cmocka_unit_test(SPISUTExchangeBlocking_NullPointerAs2ndParamReturnsError),
	cmocka_unit_test(SPISUTExchangeBlocking_ValidParamsReturnsOK),
	cmocka_unit_test(SPISUTExchangeBlocking_GivenDataAsPar1WriteHWBufferRightSequence32BitMode),
	cmocka_unit_test(SPISUTExchangeBlocking_GivenDataAsPar1WriteHWBufferRightSequence16BitMode),
	cmocka_unit_test(SPISUTExchangeBlocking_GivenDataAsPar1WriteHWBufferRightSequence8BitMode),
	cmocka_unit_test(SPISUTExchangeBlocking_GivenDataInHWBufferReadRightSequence32BitMode),
	cmocka_unit_test(SPISUTExchangeBlocking_GivenDataInHWBufferReadRightSequence16BitMode),
	cmocka_unit_test(SPISUTExchangeBlocking_GivenDataInHWBufferReadRightSequence8BitMode)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/**
 * @test SPISUTExchangeBlocking_NullPointerAs1stParamReturnsError
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTExchangeBlocking_NullPointerAs1stParamReturnsError(void ** state)
{

    /* given */
    Std_ReturnType retVal;
    uint8 data = 0x00u;
    /* when */
    retVal = SPI_ExchangeBlocking(NULL,&data);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test SPISUTExchangeBlocking_NullPointerAs2ndParamReturnsError
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTExchangeBlocking_NullPointerAs2ndParamReturnsError(void ** state)
{

    /* given */
    Std_ReturnType retVal;
    uint8 data = 0x00u;
    /* when */
    retVal = SPI_ExchangeBlocking(&data,NULL);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test SPISUTExchangeBlocking_ValidParamsReturnsOK
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTExchangeBlocking_ValidParamsReturnsOK(void ** state)
{

    /* given */
    Std_ReturnType retVal;
    uint8 data = 0x00u;

    will_return(GET_SPI2STATL,~_SPI2STATL_SPITBF_MASK);
    will_return(GET_SPI2CON1L,_SPI2CON1L_MODE32_MASK);
    will_return(GET_SPI2STATL,~_SPI2STATL_SPIRBE_MASK);

    will_return(GET_SPI2BUFL,0x00U);
    will_return(GET_SPI2BUFH,0x00U);
    /* when */
    retVal = SPI_ExchangeBlocking(&data,&data);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);


    return;
}
/**
 * @test SPISUTExchangeBlocking_GivenDataAsPar1WriteHWBufferRightSequence32BitMode
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTExchangeBlocking_GivenDataAsPar1WriteHWBufferRightSequence32BitMode(void ** state)
{

    /* given */

    uint32 dataOut = 0x5555FFFFu;
    uint32 dataIn = 0x00u;
    SPI2BUFL = 0u;
    SPI2BUFH = 0u;
    will_return(GET_SPI2STATL,~_SPI2STATL_SPITBF_MASK);
    will_return(GET_SPI2CON1L,_SPI2CON1L_MODE32_MASK);
    will_return(GET_SPI2STATL,~_SPI2STATL_SPIRBE_MASK);

    will_return(GET_SPI2BUFL,0x0000u);
    will_return(GET_SPI2BUFH,0x0000u);
    /* when */
    SPI_ExchangeBlocking((uint8_t *)&dataOut,(uint8_t *)&dataIn);
    /* then */

    assert_int_equal(SPI2BUFH, 0x5555u);
    assert_int_equal(SPI2BUFL, 0xFFFFu);

    return;
}
/**
 * @test SPISUTExchangeBlocking_GivenDataAsPar1WriteHWBufferRightSequence16BitMode
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTExchangeBlocking_GivenDataAsPar1WriteHWBufferRightSequence16BitMode(void ** state)
{

    /* given */

    uint32 dataOut = 0x5555FFFFu;
    uint32 dataIn = 0x00u;
    SPI2BUFL = 0u;
    SPI2BUFH = 0u;

    will_return(GET_SPI2STATL,~_SPI2STATL_SPITBF_MASK);
    will_return(GET_SPI2CON1L,_SPI2CON1L_MODE16_MASK);
    will_return(GET_SPI2STATL,~_SPI2STATL_SPIRBE_MASK);

    will_return(GET_SPI2BUFL,0x0000u);


    /* when */
    SPI_ExchangeBlocking((uint8_t *)&dataOut,(uint8_t *)&dataIn);
    /* then */
    assert_int_equal(SPI2BUFL, 0xFFFFu);
    assert_int_equal(SPI2BUFH, 0x0u);

    return;
}
/**
 * @test SPISUTExchangeBlocking_GivenDataAsPar1WriteHWBufferRightSequence8BitMode
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTExchangeBlocking_GivenDataAsPar1WriteHWBufferRightSequence8BitMode(void ** state)
{

    /* given */

    uint32 dataOut = 0xFFu;
    uint32 dataIn = 0x00u;
    SPI2BUFL = 0u;
    SPI2BUFH = 0u;

    will_return(GET_SPI2STATL,~_SPI2STATL_SPITBF_MASK);
    will_return(GET_SPI2CON1L,0x0U);
    will_return(GET_SPI2STATL,~_SPI2STATL_SPIRBE_MASK);

    will_return(GET_SPI2BUFL,0x00u);

    /* when */
    SPI_ExchangeBlocking((uint8_t *)&dataOut,(uint8_t *)&dataIn);
    /* then */
    assert_int_equal(SPI2BUFL, 0x00FFu);
    assert_int_equal(SPI2BUFH, 0x0u);


    return;
}
/**
 * @test SPISUTExchangeBlocking_GivenDataInHWBufferReadRightSequence32BitMode
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTExchangeBlocking_GivenDataInHWBufferReadRightSequence32BitMode(void ** state)
{

    /* given */

    uint32 dataOut = 0x00u;
    uint32 dataIn = 0x00u;
    SPI2BUFL = 0u;
    SPI2BUFH = 0u;

    will_return(GET_SPI2STATL,~_SPI2STATL_SPITBF_MASK);
    will_return(GET_SPI2CON1L,_SPI2CON1L_MODE32_MASK);
    will_return(GET_SPI2STATL,~_SPI2STATL_SPIRBE_MASK);

    will_return(GET_SPI2BUFH,0x5555u);
    will_return(GET_SPI2BUFL,0xffffu);

    /* when */
    SPI_ExchangeBlocking((uint8_t *)&dataOut,(uint8_t *)&dataIn);
    /* then */

    assert_int_equal(dataIn, 0x5555FFFFu);

    return;
}
/**
 * @test SPISUTExchangeBlocking_GivenDataInHWBufferReadRightSequence16BitMode
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTExchangeBlocking_GivenDataInHWBufferReadRightSequence16BitMode(void ** state)
{

    /* given */

    uint32 dataOut = 0x00u;
    uint32 dataIn = 0x00u;
    SPI2BUFL = 0u;
    SPI2BUFH = 0u;

    will_return(GET_SPI2STATL,~_SPI2STATL_SPITBF_MASK);
    will_return(GET_SPI2CON1L,_SPI2CON1L_MODE16_MASK);
    will_return(GET_SPI2STATL,~_SPI2STATL_SPIRBE_MASK);


    will_return(GET_SPI2BUFL,0xffffu);

    /* when */
    SPI_ExchangeBlocking((uint8_t *)&dataOut,(uint8_t *)&dataIn);
    /* then */

    assert_int_equal(dataIn, 0xFFFFu);

    return;
}
/**
 * @test SPISUTExchangeBlocking_GivenDataInHWBufferReadRightSequence8BitMode
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SPISUTExchangeBlocking_GivenDataInHWBufferReadRightSequence8BitMode(void ** state)
{

    /* given */

    uint32 dataOut = 0x00u;
    uint32 dataIn = 0x00u;
    SPI2BUFL = 0u;
    SPI2BUFH = 0u;

    will_return(GET_SPI2STATL,~_SPI2STATL_SPITBF_MASK);
    will_return(GET_SPI2CON1L,0u);
    will_return(GET_SPI2STATL,~_SPI2STATL_SPIRBE_MASK);


    will_return(GET_SPI2BUFL,0x0ffu);

    /* when */
    SPI_ExchangeBlocking((uint8_t *)&dataOut,(uint8_t *)&dataIn);
    /* then */

    assert_int_equal(dataIn, 0xFFu);

    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(SPISUTExchangeBlocking_Tests, NULL, NULL)

/** @} */
