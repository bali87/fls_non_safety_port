/**
 * @file
 *
 * Unit that provides unit tests for the unit GPIO.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "GPIO.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void GPIO_GetPinValue_NullPointerAs3rdParReturnsError(void ** state);
static void GPIO_GetPinValue_ValidPointerAs3rdParReturnsOK(void ** state);
static void GPIO_GetPinValue_LatchAPinOverBoundReturnError(void ** state);
static void GPIO_GetPinValue_SettedBitInLatchALowestPinReturnRight(void ** state);
static void GPIO_GetPinValue_SettedBitInLatchAHighestPinReturnRight(void ** state);
static void GPIO_GetPinValue_CleanBitInLatchALowestPinReturnRight(void ** state);
static void GPIO_GetPinValue_CleanBitInLatchAHighestPinReturnRight(void ** state);

static void GPIO_GetPinValue_LatchBPinOverBoundReturnError(void ** state);
static void GPIO_GetPinValue_SettedBitInLatchBLowestPinReturnRight(void ** state);
static void GPIO_GetPinValue_SettedBitInLatchBHighestPinReturnRight(void ** state);
static void GPIO_GetPinValue_CleanBitInLatchBLowestPinReturnRight(void ** state);
static void GPIO_GetPinValue_CleanBitInLatchBHighestPinReturnRight(void ** state);
static void GPIO_GetPinValue_BadPortIDReturnError(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest GPIOSUTGetPinValue_Tests[] = 
{
	cmocka_unit_test(GPIO_GetPinValue_NullPointerAs3rdParReturnsError),
	cmocka_unit_test(GPIO_GetPinValue_ValidPointerAs3rdParReturnsOK),
	cmocka_unit_test(GPIO_GetPinValue_LatchAPinOverBoundReturnError),
	cmocka_unit_test(GPIO_GetPinValue_SettedBitInLatchALowestPinReturnRight),
	cmocka_unit_test(GPIO_GetPinValue_SettedBitInLatchAHighestPinReturnRight),
	cmocka_unit_test(GPIO_GetPinValue_CleanBitInLatchALowestPinReturnRight),
	cmocka_unit_test(GPIO_GetPinValue_CleanBitInLatchAHighestPinReturnRight),
	cmocka_unit_test(GPIO_GetPinValue_LatchBPinOverBoundReturnError),
	cmocka_unit_test(GPIO_GetPinValue_SettedBitInLatchBLowestPinReturnRight),
	cmocka_unit_test(GPIO_GetPinValue_SettedBitInLatchBHighestPinReturnRight),
	cmocka_unit_test(GPIO_GetPinValue_CleanBitInLatchBLowestPinReturnRight),
	cmocka_unit_test(GPIO_GetPinValue_CleanBitInLatchBHighestPinReturnRight),
	cmocka_unit_test(GPIO_GetPinValue_BadPortIDReturnError),
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */




/**
 * @test GPIOSUTGetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIO_GetPinValue_NullPointerAs3rdParReturnsError(void ** state)
{

    /* given */
    uint8_t retVal;
    /* when */
    retVal = GPIO_GetPinValue(0u,0u,NULL);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}



/**
 * @test GPIOSUTGetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIO_GetPinValue_ValidPointerAs3rdParReturnsOK(void ** state)
{

    /* given */
    uint8_t retVal;
    uint8 data;
    will_return(GET_LATA,0u);
    /* when */
    retVal = GPIO_GetPinValue(GPIO_PORTA,0u,&data);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);


    return;
}
/**
 * @test GPIOSUTGetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIO_GetPinValue_LatchAPinOverBoundReturnError(void ** state)
{

    /* given */
    uint8_t retVal;
    uint8 data;

    /* when */
    retVal = GPIO_GetPinValue(GPIO_PORTA,PORTA_PIN_NUM,&data);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test GPIOSUTGetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIO_GetPinValue_SettedBitInLatchALowestPinReturnRight(void ** state)
{

    /* given */

    uint8 data;
    will_return(GET_LATA,0x01u);
    /* when */
    GPIO_GetPinValue(GPIO_PORTA,0u,&data);
    /* then */
    assert_int_equal(0x01u, data);


    return;
}
/**
 * @test GPIOSUTGetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIO_GetPinValue_SettedBitInLatchAHighestPinReturnRight(void ** state)
{

    /* given */

    uint8 data;
    will_return(GET_LATA,0x10u);
    /* when */
    GPIO_GetPinValue(GPIO_PORTA,4u,&data);
    /* then */
    assert_int_equal(0x01u, data);


    return;
}
/**
 * @test GPIOSUTGetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIO_GetPinValue_CleanBitInLatchALowestPinReturnRight(void ** state)
{

    /* given */

    uint8 data;
    will_return(GET_LATA,0xFEu);
    /* when */
   GPIO_GetPinValue(GPIO_PORTA,0u,&data);
    /* then */
    assert_int_equal(0x00u, data);


    return;
}
/**
 * @test GPIOSUTGetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */

static void GPIO_GetPinValue_CleanBitInLatchAHighestPinReturnRight(void ** state)
{

    /* given */

    uint8 data;
    will_return(GET_LATA,0xEFu);
    /* when */
   GPIO_GetPinValue(GPIO_PORTA,4u,&data);
    /* then */
    assert_int_equal(0x00u, data);


    return;
}
/**
 * @test GPIOSUTGetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIO_GetPinValue_LatchBPinOverBoundReturnError(void ** state)
{

    /* given */
    uint8_t retVal;
    uint8 data;

    /* when */
    retVal = GPIO_GetPinValue(GPIO_PORTB,PORTB_PIN_NUM,&data);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test GPIOSUTGetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIO_GetPinValue_SettedBitInLatchBLowestPinReturnRight(void ** state)
{

    /* given */

    uint8 data;
    will_return(GET_LATB,0x01u);
    /* when */
  GPIO_GetPinValue(GPIO_PORTB,0u,&data);
    /* then */
    assert_int_equal(0x01u, data);


    return;
}
/**
 * @test GPIOSUTGetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIO_GetPinValue_SettedBitInLatchBHighestPinReturnRight(void ** state)
{

    /* given */

    uint8 data;
    will_return(GET_LATB,0x8000u);
    /* when */
    GPIO_GetPinValue(GPIO_PORTB,15u,&data);
    /* then */
    assert_int_equal(0x01u, data);


    return;
}
/**
 * @test GPIOSUTGetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIO_GetPinValue_CleanBitInLatchBLowestPinReturnRight(void ** state)
{

    /* given */

    uint8 data;
    will_return(GET_LATB,0xFFFEu);
    /* when */
    GPIO_GetPinValue(GPIO_PORTB,0u,&data);
    /* then */
    assert_int_equal(0x00u, data);


    return;
}
/**
 * @test GPIOSUTGetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */

static void GPIO_GetPinValue_CleanBitInLatchBHighestPinReturnRight(void ** state)
{

    /* given */

    uint8 data;
    will_return(GET_LATB,0x7FFFu);
    /* when */
  GPIO_GetPinValue(GPIO_PORTB,15u,&data);
    /* then */
    assert_int_equal(0x00u, data);


    return;
}
/**
 * @test GPIOSUTGetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */

static void GPIO_GetPinValue_BadPortIDReturnError(void ** state)
{

    /* given */
    uint8_t retVal;
    uint8 data;

    /* when */
    retVal = GPIO_GetPinValue(GPIO_PORT_MAX,15u,&data);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(GPIOSUTGetPinValue_Tests, NULL, NULL)

/** @} */
