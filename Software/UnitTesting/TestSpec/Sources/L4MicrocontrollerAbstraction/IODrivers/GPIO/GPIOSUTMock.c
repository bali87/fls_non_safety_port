/**
 * @file
 *
 * Unit that provides mock objects for the unit testing of GPIO, for the objects in dependent units:
 *  - define-s,
 *  - global variables,
 *  - global functions.
 *
 * 
 * @version $Id: CSUTMockFileForOneTestedUnit.c 102 2017-11-02 16:53:53Z piotr.serwa $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalDefines Mocks for global defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalVariables Mocks for global variables
 *  @{ 
 */
volatile uint16_t LATA;
volatile uint16_t LATB;
volatile uint16_t TRISA;
volatile uint16_t TRISB;
volatile uint16_t ODCA;
volatile uint16_t ODCB;
volatile uint16_t ANSA;
volatile uint16_t ANSB;
volatile uint16_t IOCPUB;
volatile uint16_t IOCPUA;
volatile uint16_t IOCPDB;
volatile uint16_t IOCPDA;


volatile uint16_t RPINR0;
volatile uint16_t RPINR1;
volatile uint16_t RPINR2;
volatile uint16_t RPINR3;
volatile uint16_t RPINR5;
volatile uint16_t RPINR6;
volatile uint16_t RPINR7;
volatile uint16_t RPINR8;
volatile uint16_t RPINR11;
volatile uint16_t RPINR12;
volatile uint16_t RPINR18;
volatile uint16_t RPINR19;
volatile uint16_t RPINR20;
volatile uint16_t RPINR21;
volatile uint16_t RPINR22;
volatile uint16_t RPINR23;
volatile uint16_t RPINR25;
volatile uint16_t RPINR28;
volatile uint16_t RPINR29;

volatile uint16_t RPOR0;
volatile uint16_t RPOR1;
volatile uint16_t RPOR2;
volatile uint16_t RPOR3;
volatile uint16_t RPOR4;
volatile uint16_t RPOR5;
volatile uint16_t RPOR6;
volatile uint16_t RPOR7;
volatile uint16_t RPOR13;

volatile uint16_t  INTCON2;

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{ 
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalFunctionsDefinitions Mocks for global functions definitions
 *  @{
 */
void IRQ_NO_PSV _DefaultInterrupt(void)
{

}
uint16_t GET_LATA		(void)
{
	return(mock(GET_LATA));
}
uint16_t GET_LATB		(void)
{
	return(mock(GET_LATB));
}

/** @} */
