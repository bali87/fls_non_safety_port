/**
 * @file
 *
 * Unit that provides unit tests for the unit GPIO.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "GPIO.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void GPIOSUTSetExtIntHandler_NullPointerAs2stParamtereReturnError(void ** state);
static void GPIOSUTSetExtIntHandler_ValidPointerAs2stParamtereReturnOK(void ** state);
static void GPIOSUTSetExtIntHandler_ExtIntIdAs1stParamtereReturnError(void ** state);
static void GPIOSUTSetExtIntHandler_ValidPointerAs2stParamterSetHandlerArrayRight(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest GPIOSUTSetExtIntHandler_Tests[] = 
{
	cmocka_unit_test(GPIOSUTSetExtIntHandler_NullPointerAs2stParamtereReturnError),
	cmocka_unit_test(GPIOSUTSetExtIntHandler_ValidPointerAs2stParamtereReturnOK),
	cmocka_unit_test(GPIOSUTSetExtIntHandler_ExtIntIdAs1stParamtereReturnError),
	cmocka_unit_test(GPIOSUTSetExtIntHandler_ValidPointerAs2stParamterSetHandlerArrayRight)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/**
 * @test GPIOSUTSetExtIntHandler_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIOSUTSetExtIntHandler_NullPointerAs2stParamtereReturnError(void ** state)
{

    /* given */
    uint8_t retVal;
    /* when */
    retVal = GPIO_SetExtIntHandler(0u,NULL);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test GPIOSUTSetExtIntHandler_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIOSUTSetExtIntHandler_ValidPointerAs2stParamtereReturnOK(void ** state)
{

    /* given */
    uint8_t retVal;
    void dummyFunc(void){;}

    /* when */
    retVal = GPIO_SetExtIntHandler(0u,(GPIO_ExtIntFuncPtrType)dummyFunc);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);


    return;
}
/**
 * @test GPIOSUTSetExtIntHandler_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIOSUTSetExtIntHandler_ExtIntIdAs1stParamtereReturnError(void ** state)
{

    /* given */
    uint8_t retVal;
    void dummyFunc(void){;}

    /* when */
    retVal = GPIO_SetExtIntHandler(GPIO_EXT_INT_MAX,(GPIO_ExtIntFuncPtrType)dummyFunc);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test GPIOSUTSetExtIntHandler_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIOSUTSetExtIntHandler_ValidPointerAs2stParamterSetHandlerArrayRight(void ** state)
{

    /* given */
    uint8_t retVal;
    void dummyFunc(void){;}

    /* when */
    retVal = GPIO_SetExtIntHandler(GPIO_EXT_INT1,(GPIO_ExtIntFuncPtrType)dummyFunc);
    /* then */
    assert_int_equal(GPIO_ExtIntHandlerArray[GPIO_EXT_INT1], (GPIO_ExtIntFuncPtrType)dummyFunc);


    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(GPIOSUTSetExtIntHandler_Tests, NULL, NULL)

/** @} */
