/**
 * @file
 *
 * Unit that provides unit tests for the unit GPIO.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "GPIO.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */

static void GPIOSUTSetPinValue_BadPortIDReturnError(void ** state);
static void GPIOSUTSetPinValue_LatchAPinOverBoundReturnError(void ** state);
static void GPIOSUTSetPinValue_LatchBPinOverBoundReturnError(void ** state);
static void GPIOSUTSetPinValue_GivenOneAsLowestPinWriteToLatchARightPosition(void ** state);
static void GPIOSUTSetPinValue_GivenOneAsHighestPinWriteToLatchARightPosition(void ** state);
static void GPIOSUTSetPinValue_GivenOneAsLowestPinWriteToLatchBRightPosition(void ** state);
static void GPIOSUTSetPinValue_GivenOneAsHighestPinWriteToLatchBRightPosition(void ** state);
static void GPIOSUTSetPinValue_GivenZeroAsHighestPinWriteToLatchBRightPosition(void ** state);
static void GPIOSUTSetPinValue_GivenZeroAsLowestPinWriteToLatchBRightPosition(void ** state);
static void GPIOSUTSetPinValue_GivenZeroAsHighestPinWriteToLatchARightPosition(void ** state);
static void GPIOSUTSetPinValue_GivenZeroAsLowestPinWriteToLatchARightPosition(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest GPIOSUTSetPinValue_Tests[] = 
{
	cmocka_unit_test(GPIOSUTSetPinValue_BadPortIDReturnError),
	cmocka_unit_test(GPIOSUTSetPinValue_LatchAPinOverBoundReturnError),
	cmocka_unit_test(GPIOSUTSetPinValue_LatchBPinOverBoundReturnError),
	cmocka_unit_test(GPIOSUTSetPinValue_GivenOneAsLowestPinWriteToLatchARightPosition),
	cmocka_unit_test( GPIOSUTSetPinValue_GivenOneAsHighestPinWriteToLatchARightPosition),
	cmocka_unit_test(GPIOSUTSetPinValue_GivenOneAsLowestPinWriteToLatchBRightPosition),
	cmocka_unit_test( GPIOSUTSetPinValue_GivenOneAsHighestPinWriteToLatchBRightPosition),
	cmocka_unit_test(GPIOSUTSetPinValue_GivenZeroAsHighestPinWriteToLatchBRightPosition ),
	cmocka_unit_test(GPIOSUTSetPinValue_GivenZeroAsLowestPinWriteToLatchBRightPosition),
	cmocka_unit_test( GPIOSUTSetPinValue_GivenZeroAsHighestPinWriteToLatchARightPosition),
	cmocka_unit_test( GPIOSUTSetPinValue_GivenZeroAsLowestPinWriteToLatchARightPosition)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/**
 * @test GPIOSUTSetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIOSUTSetPinValue_BadPortIDReturnError(void ** state)
{

    /* given */
    uint8_t retVal;
    /* when */
    retVal = GPIO_SetPinValue(GPIO_PORT_MAX,0u,0u);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test GPIOSUTSetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIOSUTSetPinValue_LatchAPinOverBoundReturnError(void ** state)
{

    /* given */
    uint8_t retVal;
    /* when */
    retVal = GPIO_SetPinValue(GPIO_PORTA,PORTA_PIN_NUM,0u);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test GPIOSUTSetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIOSUTSetPinValue_LatchBPinOverBoundReturnError(void ** state)
{

    /* given */
    uint8_t retVal;
    /* when */
    retVal = GPIO_SetPinValue(GPIO_PORTB,PORTB_PIN_NUM,0u);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test GPIOSUTSetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIOSUTSetPinValue_GivenOneAsLowestPinWriteToLatchARightPosition(void ** state)
{

    /* given */

    LATA = 0x00U;
    /* when */
 GPIO_SetPinValue(GPIO_PORTA,0u,1u);
    /* then */
    assert_int_equal(LATA, 0x01u);


    return;
}
/**
 * @test GPIOSUTSetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIOSUTSetPinValue_GivenOneAsHighestPinWriteToLatchARightPosition(void ** state)
{

    /* given */

    LATA = 0x00U;
    /* when */
    GPIO_SetPinValue(GPIO_PORTA,4u,1u);
    /* then */
    assert_int_equal(LATA, 0x10u);


    return;
}
/**
 * @test GPIOSUTSetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIOSUTSetPinValue_GivenOneAsLowestPinWriteToLatchBRightPosition(void ** state)
{

    /* given */

    LATB = 0x00U;
    /* when */
   GPIO_SetPinValue(GPIO_PORTB,0u,1u);
    /* then */
    assert_int_equal(LATB, 0x01u);


    return;
}
/**
 * @test GPIOSUTSetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIOSUTSetPinValue_GivenOneAsHighestPinWriteToLatchBRightPosition(void ** state)
{

    /* given */

    LATB = 0x00U;
    /* when */
    GPIO_SetPinValue(GPIO_PORTB,15u,1u);
    /* then */
    assert_int_equal(LATB, 0x8000u);


    return;
}


/**
 * @test GPIOSUTSetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */

static void GPIOSUTSetPinValue_GivenZeroAsLowestPinWriteToLatchARightPosition(void ** state)
{

    /* given */

    LATA = 0xFFU;
    /* when */
  GPIO_SetPinValue(GPIO_PORTA,0u,0u);
    /* then */
    assert_int_equal(LATA, 0xFEu);


    return;
}
/**
 * @test GPIOSUTSetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */


static void GPIOSUTSetPinValue_GivenZeroAsHighestPinWriteToLatchARightPosition(void ** state)
{

    /* given */
       LATA = 0xFFu;
    /* when */
   GPIO_SetPinValue(GPIO_PORTA,4u,0u);
    /* then */
    assert_int_equal(LATA, 0xEFu);


    return;
}
/**
 * @test GPIOSUTSetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */

static void GPIOSUTSetPinValue_GivenZeroAsLowestPinWriteToLatchBRightPosition(void ** state)
{

    /* given */

    LATB = 0xFFFFu;
    /* when */
    GPIO_SetPinValue(GPIO_PORTB,0u,0u);
    /* then */
    assert_int_equal(LATB, 0xFFFEu);


    return;
}
/**
 * @test GPIOSUTSetPinValue_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIOSUTSetPinValue_GivenZeroAsHighestPinWriteToLatchBRightPosition(void ** state)
{

    /* given */

    LATB = 0xFFFFu;
    /* when */
    GPIO_SetPinValue(GPIO_PORTB,15u,0u);
    /* then */
    assert_int_equal(LATB, 0x7FFFu);


    return;
}

/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(GPIOSUTSetPinValue_Tests, NULL, NULL)

/** @} */
