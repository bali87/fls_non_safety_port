/**
 * @file
 *
 * Unit that provides unit tests for the unit GPIO.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "GPIO.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void GPIOSUTSetPinInFunction_Test0(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest GPIOSUTSetPinInFunction_Tests[] = 
{
	cmocka_unit_test(GPIOSUTSetPinInFunction_Test0)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/**
 * @test GPIOSUTSetPinInFunction_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void GPIOSUTSetPinInFunction_Test0(void ** state)
{

    /* given */
    uint8_t retVal;
    /* when */
    retVal = GPIO_SetPinInFunction(GPIO_PORT_MAX,0u,0u);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(GPIOSUTSetPinInFunction_Tests, NULL, NULL)

/** @} */
