/**
 * @file
 *
 * Unit that provides unit tests for the unit SysTimer.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "SysTimer.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void SysTimerSUTSetTimePeriod_TestArrayOut(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest SysTimerSUTSetTimePeriod_Tests[] = 
{
	cmocka_unit_test(SysTimerSUTSetTimePeriod_TestArrayOut)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/**
 * @test SysTimerSUTSetTimePeriod_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void SysTimerSUTSetTimePeriod_TestArrayOut(void ** state)
{

    /* given */
    uint8_t retVal;
    /* when */
    retVal = SysTimer_SetTimePeriod(SYS_TIMER_SLOT_NUMBER,0u);
    /* then */
    assert_int_equal(retVal, 0U);
    assert_int_equal(SysTimer_TimerSlots[0u].timeOutValue,0u);

    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(SysTimerSUTSetTimePeriod_Tests, NULL, NULL)

/** @} */
