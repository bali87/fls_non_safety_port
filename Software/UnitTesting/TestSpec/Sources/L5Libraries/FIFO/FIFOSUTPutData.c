/**
 * @file
 *
 * Unit that provides unit tests for the unit FIFO.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "FIFO.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void FIFOSUTPutData_TestHeadTurnaround(void ** state);
static void FIFOSUTPutData_TestBecomeFull(void ** state);
static void FIFOSUTPutData_TestBecomeNotEmpty(void ** state);

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest FIFOSUTPutData_Tests[] = 
{

	cmocka_unit_test(FIFOSUTPutData_TestHeadTurnaround),
	cmocka_unit_test(FIFOSUTPutData_TestBecomeFull),
	cmocka_unit_test(FIFOSUTPutData_TestBecomeNotEmpty)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



static void FIFOSUTPutData_TestHeadTurnaround(void ** state)
{

    /* given */
    uint8_t retVal;
    uint8_t testBuff[4] = {0U,1U,2U,3U};

    FIFO_DescrType fifoDescr = {0U};
    FIFO_InitDescriptor(&fifoDescr,testBuff,4U);
    fifoDescr.state = FIFO_NOT_EMPTY;
    fifoDescr.head = 3U;
    fifoDescr.tail = 2U;
    /* when */
    retVal = FIFO_PutData(&fifoDescr,4U);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);
    assert_int_equal(fifoDescr.head, 0U);
    assert_int_equal(fifoDescr.buffPtr[3], 4U);
    return;
}
static void FIFOSUTPutData_TestBecomeFull(void ** state)
{

    /* given */
    uint8_t retVal;
    uint8_t testBuff[4] = {0U,1U,2U,3U};

    FIFO_DescrType fifoDescr = {0U};
    FIFO_InitDescriptor(&fifoDescr,testBuff,4U);
    fifoDescr.state = FIFO_NOT_EMPTY;
    fifoDescr.head = 2U;
    fifoDescr.tail = 3U;
    /* when */
    retVal = FIFO_PutData(&fifoDescr,4U);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);
    assert_int_equal(fifoDescr.tail,0U);
    assert_int_equal(fifoDescr.buffPtr[2], 4U);
    assert_int_equal( fifoDescr.state, FIFO_FULL);
    assert_int_equal(fifoDescr.head, 3U);
    return;
}
static void FIFOSUTPutData_TestBecomeNotEmpty(void ** state)
{

    /* given */
    uint8_t retVal;
    uint8_t testBuff[4] = {0U,1U,2U,3U};

    FIFO_DescrType fifoDescr = {0U};
    FIFO_InitDescriptor(&fifoDescr,testBuff,4U);

    /* when */
    retVal = FIFO_PutData(&fifoDescr,4U);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);
    assert_int_equal(fifoDescr.tail, 0U);
    assert_int_equal(fifoDescr.head, 1U);
    assert_int_equal(fifoDescr.buffPtr[0], 4U);
    assert_int_equal( fifoDescr.state, FIFO_NOT_EMPTY);

    return;
}

/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(FIFOSUTPutData_Tests, NULL, NULL)

/** @} */
