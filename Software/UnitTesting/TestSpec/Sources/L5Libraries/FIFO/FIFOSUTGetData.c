/**
 * @file
 *
 * Unit that provides unit tests for the unit FIFO.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "FIFO.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void FIFOSUTGetData_TestInitialisedDescriptor(void ** state);
static void FIFOSUTGetData_TestBecomeEmpty(void ** state);
static void FIFOSUTGetData_TestTailTournaround(void ** state);
static void FIFOSUTGetData_TestBecomeNotEmpty(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest FIFOSUTGetData_Tests[] = 
{

	cmocka_unit_test(FIFOSUTGetData_TestInitialisedDescriptor),
	cmocka_unit_test(FIFOSUTGetData_TestBecomeEmpty),
	cmocka_unit_test(FIFOSUTGetData_TestTailTournaround),
	cmocka_unit_test(FIFOSUTGetData_TestBecomeNotEmpty)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */




/**
 * @test FIFOSUTGetData_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void FIFOSUTGetData_TestInitialisedDescriptor(void ** state)
{

    /* given */
	uint8_t retVal;
	uint8_t data = 0U;
	uint8_t testBuff[8] = {0U};
	uint8_t testBuffLen = 8U;
	FIFO_DescrType fifoDescr = {0U};
    FIFO_InitDescriptor(&fifoDescr,testBuff,testBuffLen);
	/* when */
    retVal = FIFO_GetData(&fifoDescr,&data);
    /* then */
    assert_int_equal(retVal, STD_RETURN_NOT_AVAILABLE);
    assert_int_equal(fifoDescr.state, FIFO_EMPTY);


    return;
}
/**
 * @test FIFOSUTGetData_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void FIFOSUTGetData_TestBecomeEmpty(void ** state)
{

    /* given */
	uint8_t retVal;
	uint8_t data = 0U;
	uint8_t testBuff[4] = {0x11U,0x22U,0x33U,0x44U};
	uint8_t testBuffLen = 4U;
	FIFO_DescrType fifoDescr = {0U};
    FIFO_InitDescriptor(&fifoDescr,testBuff,testBuffLen);
    fifoDescr.state = FIFO_NOT_EMPTY;
    fifoDescr.head = 2U;
    fifoDescr.tail = 1U;

    /* when */
    retVal = FIFO_GetData(&fifoDescr,&data);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);
    assert_int_equal(fifoDescr.state, FIFO_EMPTY);
    assert_int_equal(data,0x22U);

    return;
}
/**
 * @test FIFOSUTGetData_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void FIFOSUTGetData_TestBecomeNotEmpty(void ** state)
{

    /* given */
	uint8_t retVal;
	uint8_t data = 0U;
	uint8_t testBuff[4] = {0x11U,0x22U,0x33U,0x44U};
	uint8_t testBuffLen = 4U;
	FIFO_DescrType fifoDescr = {0U};
    FIFO_InitDescriptor(&fifoDescr,testBuff,testBuffLen);
    fifoDescr.state = FIFO_FULL;
    fifoDescr.head = 1U;
    fifoDescr.tail = 2U;

    /* when */
    retVal = FIFO_GetData(&fifoDescr,&data);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);
    assert_int_equal(fifoDescr.state, FIFO_NOT_EMPTY);
    assert_int_equal(data,0x33U);

    return;
}
/**
 * @test FIFOSUTGetData_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void FIFOSUTGetData_TestTailTournaround(void ** state)
{

    /* given */
	uint8_t retVal;
	uint8_t data = 0U;
	uint8_t testBuff[4] = {0x11U,0x22U,0x33U,0x44U};
	uint8_t testBuffLen = 4U;
	FIFO_DescrType fifoDescr = {0U};
    FIFO_InitDescriptor(&fifoDescr,testBuff,testBuffLen);
    fifoDescr.state = FIFO_NOT_EMPTY;
    fifoDescr.head = 2U;
    fifoDescr.tail = 3U;

    /* when */
    retVal = FIFO_GetData(&fifoDescr,&data);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);
    assert_int_equal(fifoDescr.state, FIFO_NOT_EMPTY);
    assert_int_equal(fifoDescr.tail, 0U);
    assert_int_equal(data,0x44U);

    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(FIFOSUTGetData_Tests, NULL, NULL)

/** @} */
