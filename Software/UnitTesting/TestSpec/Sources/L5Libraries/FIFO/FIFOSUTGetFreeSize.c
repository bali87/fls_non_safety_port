/**
 * @file
 *
 * Unit that provides unit tests for the unit FIFO.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "FIFO.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void FIFOSUTGetFreeSize_TestNullDescrPtr(void ** state);
static void FIFOSUTGetFreeSize_TestNullSizePtr(void ** state);
static void FIFOSUTGetFreeSize_TestEmpty(void ** state);
static void FIFOSUTGetFreeSize_TestHeadGreaterThanTail(void ** state);
static void FIFOSUTGetFreeSize_TestTailGreaterThanHead(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest FIFOSUTGetFreeSize_Tests[] = 
{
	cmocka_unit_test(FIFOSUTGetFreeSize_TestNullDescrPtr),
	cmocka_unit_test(FIFOSUTGetFreeSize_TestNullSizePtr),
	cmocka_unit_test(FIFOSUTGetFreeSize_TestEmpty),
	cmocka_unit_test(FIFOSUTGetFreeSize_TestHeadGreaterThanTail),
	cmocka_unit_test(FIFOSUTGetFreeSize_TestTailGreaterThanHead)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/**
 * @test FIFOSUTGetFreeSize_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void FIFOSUTGetFreeSize_TestNullDescrPtr(void ** state)
{

    /* given */
    uint8_t retVal;
    uint16 size;
    /* when */
    retVal = FIFO_GetFreeSize(NULL,&size);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test FIFOSUTGetFreeSize_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void FIFOSUTGetFreeSize_TestNullSizePtr(void ** state)

{

    /* given */
    uint8_t retVal;

    FIFO_DescrType fifoDescr;

    /* when */
    retVal = FIFO_GetFreeSize(&fifoDescr,NULL);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test FIFOSUTGetFreeSize_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void FIFOSUTGetFreeSize_TestEmpty(void ** state)
{

    /* given */
    uint8_t retVal;
    uint16 size;
    uint8_t buffer[5];
    FIFO_DescrType fifoDescr;
    FIFO_InitDescriptor(&fifoDescr,buffer,5U);
    /* when */
    retVal = FIFO_GetFreeSize(&fifoDescr,&size);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);
    assert_int_equal(size, 5U);

    return;
}
/**
 * @test FIFOSUTGetFreeSize_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void FIFOSUTGetFreeSize_TestHeadGreaterThanTail(void ** state)
{

    /* given */
    uint8_t retVal;
    uint16 size;
    uint8_t buffer[5];
    FIFO_DescrType fifoDescr;
    FIFO_InitDescriptor(&fifoDescr,buffer,5U);
    fifoDescr.head = 3U;
    fifoDescr.tail = 2U;
    /* when */
    retVal = FIFO_GetFreeSize(&fifoDescr,&size);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);
    assert_int_equal(size, 4U);

    return;
}
/**
 * @test FIFOSUTGetFreeSize_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void FIFOSUTGetFreeSize_TestTailGreaterThanHead(void ** state)
{

    /* given */
    uint8_t retVal;
    uint16 size;
    uint8_t buffer[5];
    FIFO_DescrType fifoDescr;
    FIFO_InitDescriptor(&fifoDescr,buffer,5U);
    fifoDescr.head = 1U;
    fifoDescr.tail = 3U;
    /* when */
    retVal = FIFO_GetFreeSize(&fifoDescr,&size);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);
    assert_int_equal(size, 2U);

    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(FIFOSUTGetFreeSize_Tests, NULL, NULL)

/** @} */
