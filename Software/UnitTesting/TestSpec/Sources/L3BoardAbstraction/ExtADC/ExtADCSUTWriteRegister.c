/**
 * @file
 *
 * Unit that provides unit tests for the unit ExtADC.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "ExtADC.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void ExtADCSUTWriteRegister_NullPointerAs2ndParReturnsError(void ** state);
static void ExtADCSUTWriteRegister_AddressAs1stParOutOfRangeReturnsError(void ** state);
static void ExtADCSUTWriteRegister_ValidPointerAs2ndParReturnsOK(void ** state);
static void ExtADCSUTWriteRegister_AddressAs1stParInRangeBoundReturnsOK(void ** state);
static void ExtADCSUTWriteRegister_ReadBackValueDontMatchWrittenReturnError(void ** state);
static void ExtADCSUTWriteRegister_ReadBackValueMatchWrittenReturnOK(void ** state);
static void ExtADCSUTWriteRegister_GivenValueWrittenToSPICorrect(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest ExtADCSUTWriteRegister_Tests[] = 
{
	cmocka_unit_test(ExtADCSUTWriteRegister_NullPointerAs2ndParReturnsError),
	cmocka_unit_test(ExtADCSUTWriteRegister_AddressAs1stParOutOfRangeReturnsError),
	cmocka_unit_test(ExtADCSUTWriteRegister_ValidPointerAs2ndParReturnsOK),
	cmocka_unit_test(ExtADCSUTWriteRegister_AddressAs1stParInRangeBoundReturnsOK),
	cmocka_unit_test(ExtADCSUTWriteRegister_ReadBackValueDontMatchWrittenReturnError),
	cmocka_unit_test(ExtADCSUTWriteRegister_ReadBackValueMatchWrittenReturnOK),
	cmocka_unit_test(ExtADCSUTWriteRegister_GivenValueWrittenToSPICorrect)

};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/**
 * @test ExtADCSUTWriteRegister_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTWriteRegister_NullPointerAs2ndParReturnsError(void ** state)
{

    /* given */
    uint8_t retVal;
    /* when */
    retVal = ExtADC_WriteRegister(0u,NULL);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test ExtADCSUTWriteRegister_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTWriteRegister_AddressAs1stParOutOfRangeReturnsError(void ** state)
{

    /* given */
    uint8_t retVal;
    /* when */
    retVal = ExtADC_WriteRegister((EXTADC_REG_ID+1),NULL);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test ExtADCSUTWriteRegister_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTWriteRegister_ValidPointerAs2ndParReturnsOK(void ** state)
{

    /* given */
    uint8_t retVal;
    uint8_t dat=0u;

    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_16BIT);
    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,LOW);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);

    expect(SPI_Exchange16bit,data,((uint16_t) (EXTADC_CMD_REGISTER_WRITE | (uint8_t) 0u)) << 8);
    will_return(SPI_Exchange16bit,0u);

    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,HIGH);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);


    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_16BIT);
    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,LOW);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);

    expect(SPI_Exchange16bit,data,((uint16_t) (EXTADC_CMD_REGISTER_READ | (uint8_t) 0u)) << 8);
    will_return(SPI_Exchange16bit,0u);

    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_32BIT);
    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,HIGH);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);
    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_32BIT);

    /* when */
    retVal = ExtADC_WriteRegister(0u,&dat);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);


    return;
}
/**
 * @test ExtADCSUTWriteRegister_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTWriteRegister_AddressAs1stParInRangeBoundReturnsOK(void ** state)
{

    /* given */
    uint8_t retVal;
    uint8_t dat;
    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_16BIT);
    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,LOW);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);

    expect(SPI_Exchange16bit,data,((uint16_t) (EXTADC_CMD_REGISTER_WRITE | (uint8_t) EXTADC_REG_ID)) << 8);
    will_return(SPI_Exchange16bit,0u);

    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,HIGH);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);


    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_16BIT);
    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,LOW);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);
    expect(SPI_Exchange16bit,data,((uint16_t) (EXTADC_CMD_REGISTER_READ | (uint8_t) EXTADC_REG_ID)) << 8);
    will_return(SPI_Exchange16bit,0u);

    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_32BIT);
    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,HIGH);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);
    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_32BIT);

    /* when */
    retVal = ExtADC_WriteRegister(EXTADC_REG_ID,&dat);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);


    return;
}
/**
 * @test ExtADCSUTWriteRegister_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTWriteRegister_ReadBackValueDontMatchWrittenReturnError(void ** state)
{

    /* given */
    uint8_t retVal;
    uint8_t dat;
    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_16BIT);
    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,LOW);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);

    expect(SPI_Exchange16bit,data,((uint16_t) (EXTADC_CMD_REGISTER_WRITE | (uint8_t) EXTADC_REG_ID)) << 8);
    will_return(SPI_Exchange16bit,0u);

    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,HIGH);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);


    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_16BIT);
    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,LOW);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);

    expect(SPI_Exchange16bit,data,((uint16_t) (EXTADC_CMD_REGISTER_READ | (uint8_t) EXTADC_REG_ID)) << 8);
    will_return(SPI_Exchange16bit,0xFFu);

    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_32BIT);
    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,HIGH);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);
    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_32BIT);

    /* when */
    retVal = ExtADC_WriteRegister(EXTADC_REG_ID,&dat);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test ExtADCSUTWriteRegister_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTWriteRegister_ReadBackValueMatchWrittenReturnOK(void ** state)
{

    /* given */
    uint8_t retVal;
    uint8_t dat = 0x00u;
    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_16BIT);
    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,LOW);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);

    expect(SPI_Exchange16bit,data,(((uint16_t) (EXTADC_CMD_REGISTER_WRITE | (uint8_t) EXTADC_REG_ID)) << 8));
    will_return(SPI_Exchange16bit,0u);

    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,HIGH);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);


    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_16BIT);
    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,LOW);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);

    expect(SPI_Exchange16bit,data,(((uint16_t) (EXTADC_CMD_REGISTER_READ | (uint8_t) EXTADC_REG_ID)) << 8));
    will_return(SPI_Exchange16bit,0x00u);

    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_32BIT);
    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,HIGH);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);
    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_32BIT);

    /* when */
    retVal = ExtADC_WriteRegister(EXTADC_REG_ID,&dat);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);


    return;
}
/**
 * @test ExtADCSUTWriteRegister_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTWriteRegister_GivenValueWrittenToSPICorrect(void ** state)
{

    /* given */
    uint8_t retVal;
    uint8_t dat = 0x5Fu;
    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_16BIT);
    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,LOW);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);

    expect(SPI_Exchange16bit,data,(((uint16_t) (EXTADC_CMD_REGISTER_WRITE | (uint8_t) EXTADC_REG_ID)) << 8)|dat);
    will_return(SPI_Exchange16bit,0u);

    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,HIGH);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);


    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_16BIT);
    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,LOW);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);

    expect(SPI_Exchange16bit,data,(((uint16_t) (EXTADC_CMD_REGISTER_READ | (uint8_t) EXTADC_REG_ID)) << 8));
    will_return(SPI_Exchange16bit,0x5fu);

    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_32BIT);
    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,HIGH);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);
    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_32BIT);

    /* when */
    retVal = ExtADC_WriteRegister(EXTADC_REG_ID,&dat);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);


    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(ExtADCSUTWriteRegister_Tests, NULL, NULL)

/** @} */
