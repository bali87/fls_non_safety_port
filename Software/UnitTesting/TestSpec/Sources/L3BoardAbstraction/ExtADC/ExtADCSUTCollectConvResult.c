/**
 * @file
 *
 * Unit that provides unit tests for the unit ExtADC.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "ExtADC.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void ExtADCSUTCollectConvResult_PulseCounterIncrementAfterCall(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest ExtADCSUTCollectConvResult_Tests[] = 
{
	cmocka_unit_test(ExtADCSUTCollectConvResult_PulseCounterIncrementAfterCall)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */

/**
 * Initialization of tested unit.
 */


/**
 * @test ExtADCSUTCollectConvResult_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTCollectConvResult_PulseCounterIncrementAfterCall(void ** state)
{

    /* given */

    ExtADC_DataRegisterType convData;

    convData.chID = 29U;
    pulseCount = 0u;
    expect(SPI_ReadShiftRegister,dataPtr,(uint32_t *) & ExtADC_DataRegVal.rawData);
    will_return(SPI_ReadShiftRegister,&convData.rawData);
    will_return(SPI_ReadShiftRegister,STD_RETURN_OK);
    /* when */
    ExtADC_CollectConvResult();
    /* then */
    assert_int_equal(pulseCount, 1U);


    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(ExtADCSUTCollectConvResult_Tests, NULL, NULL)

/** @} */
