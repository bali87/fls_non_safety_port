/**
 * @file
 *
 * Unit that provides unit tests for the unit ADS.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z michal.szczepan $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

//#include "Std.h"

#include "CmockaFramework.h"

#include "ExtADC.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void ExtADCSUTGetChannelValue_TestIndexOver(void ** state);
static void ExtADCSUTGetChannelValue_TestIndexCH8(void **state);
static void ExtADCSUTGetChannelValue_TestIndexCH24(void **state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest ExtADCSUTGetChannelValue_Tests[] =
{
		cmocka_unit_test(ExtADCSUTGetChannelValue_TestIndexOver),
		cmocka_unit_test(ExtADCSUTGetChannelValue_TestIndexCH8),
		cmocka_unit_test(ExtADCSUTGetChannelValue_TestIndexCH24),
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */

/**
 * @test
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTGetChannelValue_TestIndexOver(void **state)
{

    /* given */

    uint32_t retVal = 0U;
    /* when */


    retVal = ExtADC_GetChannelValue(EXTADC_NUMBEROFCHANNELS);
    /* then */
    assert_int_equal(retVal, 0U);


    return;
}
/**
 * @test
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTGetChannelValue_TestIndexCH8(void **state)
{

    /* given */
	ExtADC_ConvDataArray[8] = 0x55ffU;
	uint32_t retVal = 0U;
    /* when */


    retVal = ExtADC_GetChannelValue(8U);
    /* then */
    assert_int_equal(retVal, 0x55ffU);


    return;
}
static void ExtADCSUTGetChannelValue_TestIndexCH24(void **state)
{

    /* given */
	ExtADC_ConvDataArray[24] = 0x55ffU;
	uint32_t retVal = 0U;
    /* when */


    retVal = ExtADC_GetChannelValue(24U);
    /* then */
    assert_int_equal(retVal, 0x55ffU);


    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(ExtADCSUTGetChannelValue_Tests, NULL, NULL)

/** @} */
