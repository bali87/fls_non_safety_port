/**
 * @file
 *
 * Unit that provides unit tests for the unit ExtADC.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "ExtADC.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void ExtADCSUTConvReadyInterruptHandler_InterruptFlagClearedAfterCall(void ** state);
static void ExtADCSUTConvReadyInterruptHandler_PulseCountBiggerThanExpectedOccursTimeout(void ** state);
static void ExtADCSUTConvReadyInterruptHandler_PulseCountLessThanExpectedSPIHwRegShiftedOut(void ** state);

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest ExtADCSUTConvReadyInterruptHandler_Tests[] = 
{
	cmocka_unit_test(ExtADCSUTConvReadyInterruptHandler_InterruptFlagClearedAfterCall),
	cmocka_unit_test(ExtADCSUTConvReadyInterruptHandler_PulseCountBiggerThanExpectedOccursTimeout),
	cmocka_unit_test(ExtADCSUTConvReadyInterruptHandler_PulseCountLessThanExpectedSPIHwRegShiftedOut)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/**
 * @test ExtADCSUTConvReadyInterruptHandler_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTConvReadyInterruptHandler_InterruptFlagClearedAfterCall(void ** state)
{

    /* given */
	ADS_initialized = 0u;
	IFS1 = 0xFFFFu;
	/* when */
    ExtADC_ConvReadyInterruptHandler();
    /* then */
    assert_int_equal(IFS1, (0xFFFFu & ~(1 <<_IFS1_INT1IF_POSITION)));


    return;
}
/**
 * @test ExtADCSUTConvReadyInterruptHandler_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTConvReadyInterruptHandler_PulseCountBiggerThanExpectedOccursTimeout(void ** state)
{

    /* given */
	ADS_initialized = 1u;
	ExtADCConvStatus = EXTADC_PENDING;
	pulseCount = 8u;
	pulseExpected = 7u;
	/* when */
    ExtADC_ConvReadyInterruptHandler();
    /* then */
    assert_int_equal(ExtADCConvStatus,EXTADC_TIMEOUT);


    return;
}
/**
 * @test ExtADCSUTConvReadyInterruptHandler_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTConvReadyInterruptHandler_PulseCountLessThanExpectedSPIHwRegShiftedOut(void ** state)
{

    /* given */
	ADS_initialized = 1u;
	ExtADCConvStatus = EXTADC_PENDING;
	pulseCount = 6u;
	pulseExpected = 8u;

//	expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
	//expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
	//expect(GPIO_SetPinValue,value,LOW);
	//will_return(GPIO_SetPinValue,0u);

	will_return(SPI_EnableRxInterrupt,STD_RETURN_OK);

	expect(SPI_WriteShiftRegister,dataPtr,&data);
	will_return(SPI_WriteShiftRegister,0u);

	/* when */
    ExtADC_ConvReadyInterruptHandler();
    /* then */



    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(ExtADCSUTConvReadyInterruptHandler_Tests, NULL, NULL)

/** @} */
