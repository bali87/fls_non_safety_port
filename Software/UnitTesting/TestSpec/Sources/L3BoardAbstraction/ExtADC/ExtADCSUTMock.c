/**
 * @file
 *
 * Unit that provides mock objects for the unit testing of ADS, for the objects in dependent units:
 *  - define-s,
 *  - global variables,
 *  - global functions.
 *
 * 
 * @version $Id: CSUTMockFileForOneTestedUnit.c 102 2017-11-02 16:53:53Z piotr.serwa $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"
#include "SPI.h"
//#include "MCU.h"
#include "GPIO.h"
#include "SysTimer.h"
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalDefines Mocks for global defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalVariables Mocks for global variables
 *  @{ 
 */
volatile uint16_t IFS1;
volatile uint16_t LATA;
volatile uint16_t LATB ;
volatile uint16_t IEC1;
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{ 
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalFunctionsDefinitions Mocks for global functions definitions
 *  @{
 */
void SysDelay_Ms(uint64_t ms)
{
	check(SysDelay_Ms,ms);

}
void SPI_SetRXINTCallback (SPI_IntFuncPtrType fPtr)
{
	check(SPI_SetRXINTCallback,fPtr);
}
void SPI_SetTransferMode (SPI_TransferModeType mode)
{
	check(SPI_SetTransferMode,mode);
}
uint16_t SPI_Exchange16bit (uint16_t data)
{
	uint16_t retVal;
	check(SPI_Exchange16bit,data);
	retVal = (uint16_t)mock(SPI_Exchange16bit);
	return (retVal);
}
Std_ReturnType SPI_ReadShiftRegister (uint32_t * dataPtr)
{
	uint8_t retVal;
	check(SPI_ReadShiftRegister,dataPtr);
	dataPtr = ( uint32_t *)mock(SPI_ReadShiftRegister);
	retVal = (Std_ReturnType)mock(SPI_ReadShiftRegister);
	return (retVal);
}
Std_ReturnType SPI_WriteShiftRegister (uint32_t * dataPtr)
{
	uint8_t retVal;
	check(SPI_WriteShiftRegister,dataPtr);
	retVal = (Std_ReturnType)mock(SPI_WriteShiftRegister);
	return (retVal);
}
Std_ReturnType SPI_IsInitialised(void)
{
	Std_ReturnType retVal;
	retVal = (Std_ReturnType)mock(SPI_IsInitialised);
	return(retVal);
}
void SPI_EnableRxInterrupt(void)
{
	mock(SPI_EnableRxInterrupt);
}
Std_ReturnType GPIO_SetPinValue(GPIO_PortType gpioPort,uint8 pin,uint8 value)
{
	Std_ReturnType retVal;
	check(GPIO_SetPinValue,gpioPort);
	check(GPIO_SetPinValue,pin);
	check(GPIO_SetPinValue,value);
	retVal = (Std_ReturnType)mock(GPIO_SetPinValue);
	return(retVal);
}
Std_ReturnType GPIO_SetExtIntHandler(uint8 extIntID,GPIO_ExtIntFuncPtrType fPtr)
{
	Std_ReturnType retVal;
	check(GPIO_SetExtIntHandler,extIntID);
	check(GPIO_SetExtIntHandler,fPtr);
	retVal = (Std_ReturnType)mock(GPIO_SetExtIntHandler);
	return(retVal);
}
Std_ReturnType SysTimer_SetTimePeriod( uint16 timerSlot, uint16 timerPeriod )
{
	Std_ReturnType retVal;
	retVal = (Std_ReturnType)mock(SysTimer_SetTimePeriod);
	return(retVal);
}
Std_ReturnType SysTimer_StartTimer( uint16 timerSlot, SysTimer_ModeType timerMode )
{
	Std_ReturnType retVal;
	retVal = (Std_ReturnType)mock(SysTimer_StartTimer);
	return(retVal);
}
SysTimer_ReturnType SysTimer_GetTimerStatus( uint16 timerSlot )
{
	Std_ReturnType retVal;
	retVal = (Std_ReturnType)mock(SysTimer_GetTimerStatus);
	return(retVal);
}
/** @} */
