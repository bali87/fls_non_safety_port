/**
 * @file
 *
 * Unit that provides unit tests for the unit ExtADC.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "ExtADC.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void ExtADCSUTReadRegister_NullPointerAs2ndParReturnError(void ** state);
static void ExtADCSUTReadRegister_ValidPointerAs2ndParReturnOK(void ** state);
static void ExtADCSUTReadRegister_GivenDataInSPIReadCorrect(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest ExtADCSUTReadRegister_Tests[] = 
{
	cmocka_unit_test(ExtADCSUTReadRegister_NullPointerAs2ndParReturnError),
	cmocka_unit_test(ExtADCSUTReadRegister_ValidPointerAs2ndParReturnOK),
	cmocka_unit_test(ExtADCSUTReadRegister_GivenDataInSPIReadCorrect)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/**
 * @test ExtADCSUTReadRegister_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTReadRegister_NullPointerAs2ndParReturnError(void ** state)
{

    /* given */
    uint8_t retVal;
    /* when */
    retVal = ExtADC_ReadRegister(0u,NULL);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * @test ExtADCSUTReadRegister_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTReadRegister_ValidPointerAs2ndParReturnOK(void ** state)
{

    /* given */
    uint8_t retVal;
    uint8_t dat;
    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_16BIT);

    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,LOW);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);

    expect(SPI_Exchange16bit,data,((uint16_t) (EXTADC_CMD_REGISTER_READ | (uint8_t) 0u)) << 8);
    will_return(SPI_Exchange16bit,0u);

    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,HIGH);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);

    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_32BIT);
    /* when */
    retVal = ExtADC_ReadRegister(0u,&dat);
    /* then */
    assert_int_equal(retVal, STD_RETURN_OK);


    return;
}
/**
 * @test ExtADCSUTReadRegister_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void ExtADCSUTReadRegister_GivenDataInSPIReadCorrect(void ** state)
{

    /* given */

    uint8_t dat;
    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_16BIT);

    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,LOW);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);

    expect(SPI_Exchange16bit,data,((uint16_t) (EXTADC_CMD_REGISTER_READ | (uint8_t) 0u)) << 8);
    will_return(SPI_Exchange16bit,0x55u);

    expect(GPIO_SetPinValue,gpioPort,BOARD_EXTADC_NCS_PORT);
    expect(GPIO_SetPinValue,pin,BOARD_EXTADC_NCS_PIN);
    expect(GPIO_SetPinValue,value,HIGH);
    will_return(GPIO_SetPinValue,STD_RETURN_OK);

    expect(SPI_SetTransferMode,mode,SPI_TRANSFER_MODE_32BIT);
    /* when */
    ExtADC_ReadRegister(0u,&dat);
    /* then */
    assert_int_equal(dat, 0x55u);


    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(ExtADCSUTReadRegister_Tests, NULL, NULL)

/** @} */
