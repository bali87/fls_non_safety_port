/**
 * @file
 *
 * Unit that provides mock objects for the unit testing of Board, for the objects in dependent units:
 *  - define-s,
 *  - global variables,
 *  - global functions.
 *
 * 
 * @version $Id: CSUTMockFileForOneTestedUnit.c 102 2017-11-02 16:53:53Z piotr.serwa $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"
#include "GPIO.h"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalDefines Mocks for global defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalVariables Mocks for global variables
 *  @{ 
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{ 
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalFunctionsDefinitions Mocks for global functions definitions
 *  @{
 */
void GPIO_Init(void)
{

}
Std_ReturnType GPIO_SetupPin(GPIO_PortType gpioPort, uint8 pin,GPIO_DirectionType direction,uint8 od)
{
	return((Std_ReturnType)mock(GPIO_SetupPin));
}
Std_ReturnType GPIO_SetPinInFunction(GPIO_PortType gpioPort, uint8 pin,GPIO_RPInFunType func)
{
	return((Std_ReturnType)mock(GPIO_SetPinInFunction));
}
Std_ReturnType GPIO_SetPinOutputFunction(GPIO_PortType gpioPort, uint8 pin,GPIO_RPOutFunType func)
{
	return((Std_ReturnType)mock(GPIO_SetPinOutputFunction));
}
Std_ReturnType UART_Init(void)
{
	return((Std_ReturnType)mock(UART_Init));
}
Std_ReturnType SysTimer_Init( void )
{
	return((Std_ReturnType)mock(SysTimer_Init));
}
Std_ReturnType ExtADC_Init(uint32 convChMask)
{
	return((Std_ReturnType)mock(ExtADC_Init));
}
uint32_t ExtADC_GetChannelValue(uint8_t chId)
{
	return((uint32_t)mock(ExtADC_GetChannelValue));
}
void SPI_Init (void)
{

}
void MCUConfig_Init(void)
{

}
Std_ReturnType I2C_Init( void )
{
	return(STD_RETURN_OK);
}
/** @} */
