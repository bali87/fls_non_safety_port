/**
 * @file
 *
 * Unit that provides unit tests for the unit Board.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "Board.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */

static void BoardSUTCollectSignals_Test0(void ** state);
static void BoardSUTCollectSignals_Test1(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest BoardSUTCollectSignals_Tests[] = 
{
	cmocka_unit_test(BoardSUTCollectSignals_Test0),
	cmocka_unit_test(BoardSUTCollectSignals_Test1)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/**
 * @test BoardSUTCollectSignals_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void BoardSUTCollectSignals_Test0(void ** state)
{

    /* given */
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0xFFFFFFu);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
    /* when */
    Board_CollectSignals();
    /* then */
    assert_int_equal(  Board_SignalGroup[BOARD_SG1].pdArray[0], (uint16)(0xFFFFFFu >> 7));


    return;
}
/**
 * @test BoardSUTCollectSignals_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void BoardSUTCollectSignals_Test1(void ** state)
{

    /* given */
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0xFFFFFFu);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
	will_return(ExtADC_GetChannelValue,0u);
    /* when */
    Board_CollectSignals();
    /* then */
    assert_int_equal(  Board_SignalGroup[BOARD_SG1].pdArray[0], 0xFFFFu);


    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(BoardSUTCollectSignals_Tests, NULL, NULL)

/** @} */
