/**
 * @file
 *
 * Unit that provides unit tests for the unit Board.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z balazs.vincze $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"

#include "Board.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void BoardSUTGetSignalGroup_NULLAs2ndParReturnError(void ** state);
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest BoardSUTGetSignalGroup_Tests[] = 
{
	cmocka_unit_test(BoardSUTGetSignalGroup_NULLAs2ndParReturnError)
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */

/**
 * Initialization of tested unit.
 */


/**
 * @test BoardSUTGetSignalGroup_Test0
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void BoardSUTGetSignalGroup_NULLAs2ndParReturnError(void ** state)
{

    /* given */
    uint8_t retVal;
    /* when */
    retVal = Board_GetSignalGroup(0u,NULL);
    /* then */
    assert_int_equal(retVal, STD_RETURN_ERROR);


    return;
}
/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(BoardSUTGetSignalGroup_Tests, NULL, NULL)

/** @} */
