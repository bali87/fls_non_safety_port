#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

echo "Running SUT tests on Host (cygwin) ..."

create_and_cd_build_dir

# set relative paths with respect to build dir
OUTPUT_TXT=../../TestReport_HostOutput.txt

echo "Removing old output ..."
rm -fr ${OUTPUT_TXT}

echo "Running ctest ..."
ctest -VV -T Test 2>&1 | tee ${OUTPUT_TXT}
check "ctest"

find . -name "Test.xml" -exec sed -e "s/<Name>/&Host_/g" -i {} \;
find . -name "Test.xml" -exec sed -e "s/_SVS\//&Host_/g" -i {} \;

echo "... running SUT tests on Host (cygwin) done."
pause

