#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

echo "Running SUT compilation on Host (cygwin) ..."

create_and_cd_build_dir

# set relative paths with respect to build dir
OUTPUT_TXT=../../TestReport_HostBuild.txt

echo "Removing old files ..."
rm -fr ${OUTPUT_TXT} *.exe *.dll

echo "Running cmake ..."
cmake -DCMAKE_TOOLCHAIN_FILE=${CYGWIN_TOOLCHAIN_FILE} -DSUBPHASE=SUT ${CMAKELISTS_DIR} 2>&1 | tee ${OUTPUT_TXT} || EXIT=1
#-DCMAKE_VERBOSE_MAKEFILE=1 -
check "cmake"

echo "Running make ..."
make -j6 2>&1 | tee -a ${OUTPUT_TXT} || EXIT=1
check "make"

check_output ${OUTPUT_TXT}

#cp ./SVS/*.exe ../
#cp ./SVS/*.dll ../

echo "... running SUT compilation on Host (cygwin) done."

echo "Running SUT tests on Host (cygwin) ..."

# set relative paths with respect to build dir
OUTPUT_TXT=../../TestReport_HostOutput.txt

echo "Removing old output ..."
rm -fr ${OUTPUT_TXT}
echo "Running ctest ..."
ctest -VV -T Test 2>&1 | tee ${OUTPUT_TXT}
check "ctest"

find . -name "Test.xml" -exec sed -e "s/<Name>/&Host_/g" -i {} \;
find . -name "Test.xml" -exec sed -e "s/_SVS\//&Host_/g" -i {} \;

echo "... running SUT tests on Host (cygwin) done."
pause

