#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1


OUTPUT_TXT="../../Software verification report - address sanitizer runtime output (linux gcc 32 bit).txt"
SUBPHASE=SUT
 	
echo "Running Address Sanitizer on remote host (linux) ..."
create_and_cd_build_dir

echo "Running cmake ..."
cmake -DCMAKE_TOOLCHAIN_FILE=${LINUX_ASAN_TOOLCHAIN_FILE} \
-DSUBPHASE=${SUBPHASE} ${CMAKELISTS_DIR} || EXIT=1
check "cmake"

echo "Running make ..."
make || EXIT=1
check "make"


echo "Removing old files ..."
rm -f "${OUTPUT_TXT}"
find . -name "Test.xml" -exec rm {} \;
check_deleted "${OUTPUT_TXT}"

ctest -VV -T Test >> "${OUTPUT_TXT}"

find . -name "Test.xml" -exec sed -e "s/<Name>/&AddressSanitizer_/g" -i {} \;
find . -name "Test.xml" -exec sed -e "s/_SVS\//&AddressSanitizer_/g" -i {} \;
find . -name "Test.xml" -exec sed -e "s/_SVS-R\//&AddressSanitizer_/g" -i {} \;
find . -name "Test.xml" -exec sed -e "s/_ITS\//&AddressSanitizer_/g" -i {} \;

 pause

