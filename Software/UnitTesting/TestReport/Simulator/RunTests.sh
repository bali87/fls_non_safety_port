#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

echo "Running SUT tests on Simulator (cygwin) ..."

create_and_cd_build_dir

# set relative paths with respect to build dir
SIMULATION_DEBUG=../../TestReport_SimulatorDebug.txt
SIMULATION_OUTPUT=../../TestReport_SimulatorOutput.txt

echo "Removing old files ..."
rm -fr ${SIMULATION_OUTPUT} ${SIMULATION_DEBUG}

echo "Running all simulations ..."
for HEX in `find . -name "*.hex"`; do
    run_simulation ${HEX} ${SIMULATION_OUTPUT} ${SIMULATION_DEBUG}
done
check "all simulations"

check_output ${SIMULATION_OUTPUT}
check_output ${SIMULATION_DEBUG}

bamboo_test_xml Software/UnitTesting/TestReport ../../TestReport_SimulatorOutput.txt Simulator FAILED

echo "... running SUT tests on Simulator (cygwin) done."
pause

