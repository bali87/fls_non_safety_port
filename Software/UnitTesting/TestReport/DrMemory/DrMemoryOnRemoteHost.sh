#!/bin/bash


. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

OUTPUT_DIR=Software/UnitTesting/TestReport
OUTPUT="Software verification report - dr memory runtime output (linux gcc 32 bit).txt"
SUBPHASE=SUT

trigger_dr_memory "${OUTPUT_DIR}" "${OUTPUT}" ${SUBPHASE}

