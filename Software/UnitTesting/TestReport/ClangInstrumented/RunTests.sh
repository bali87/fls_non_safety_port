. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh


OUTPUT_TXT="../../Software verification report - results of unit testing (clang instrumented).txt"
SUBPHASE=SUT

echo "Running Clang Instrumented compilation on remote host (linux) ..."

create_and_cd_build_dir

echo "Running cmake ..."
cmake -DCMAKE_TOOLCHAIN_FILE=${LINUX_CLANG_TOOLCHAIN_FILE} \
          -DSUBPHASE=${SUBPHASE} ${CMAKELISTS_DIR} || EXIT=1
check "cmake"

echo "Running make ..."
make || EXIT=1
check "make"

echo "Removing old files ..."
rm -f "${OUTPUT_TXT}"
find . -name "Test.xml" -exec rm {} \;
check_deleted "${OUTPUT_TXT}"

ctest -VV -T Test >> "${OUTPUT_TXT}"

find . -name "Test.xml" -exec sed -e "s/<Name>/&ClangInstrumented_/g" -i {} \;
find . -name "Test.xml" -exec sed -e "s/_SVS\//&ClangInstrumented_/g" -i {} \;
find . -name "Test.xml" -exec sed -e "s/_SVS-R\//&ClangInstrumented_/g" -i {} \;
find . -name "Test.xml" -exec sed -e "s/_ITS\//&ClangInstrumented_/g" -i {} \;
  

