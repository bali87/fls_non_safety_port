. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

OUTPUT_DIR=./Software/UnitTesting/TestReport/
OUTPUT_TESTING="Software verification report - results of unit testing (linux gcc 32 bit).txt"
SUBPHASE=SUT

trigger_clang_instrumented "${OUTPUT_DIR}" "${OUTPUT_TESTING}" "${SUBPHASE}"
