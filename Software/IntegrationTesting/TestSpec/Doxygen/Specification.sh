#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

echo "Running Doxygen generation ..."

# set relative paths with respect to build dir
OUTPUT_CHM=../TestSpec.chm

echo "Removing old files ..."
rm -fr ${OUTPUT_CHM} warnings.log
check_deleted ${OUTPUT_CHM}

# use patched doxygen with java wrapper
export PATH=../../../../General/BuildServer/Production/doxygen:$PATH

echo "Running doxygen ..."
doxygen SUDS.dox || EXIT=1
check "doxygen"

check_output ${OUTPUT_CHM}

echo "... running Doxygen generation done."
pause

