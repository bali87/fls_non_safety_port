/**
 * @file
 *
 * Unit that provides integration for FUN01 Data acqusition.
 * 
 * @version $Id: CSUTTestFileForOneFunction.c 139 2017-11-06 15:39:26Z michal.szczepan $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"
#include "SPI.c"
#include "ExtADC.c"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */


static void FUN01SITStartConversion_TestAnything(void ** state);

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest FUN01SITStartConversion_Tests[] = {
		cmocka_unit_test(FUN01SITStartConversion_TestAnything),
		
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */


/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */

/**
 * @test
 * - @given
 * - @when
 * - @then
 * - @method
 * - @verifies
 */
static void FUN01SITStartConversion_TestAnything(void **state)
{

    /* given */

    uint8_t retVal = 0U;
    /* when */


    ExtADC_StartConversion();
	ExtADC_ConvReadyInterruptHandler();
	SPI_RxInterruptHandler();
	retVal = ExtADC_IsConvFinished();
    /* then */
    assert_int_equal(retVal, 0U);


    return;
}

/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(FUN01SITStartConversion_Tests, NULL, NULL)

/** @} */
