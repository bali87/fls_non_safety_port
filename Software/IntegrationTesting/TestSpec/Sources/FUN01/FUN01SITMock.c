/**
 * @file
 *
 * Unit that provides mock objects for the unit testing of ADS, for the objects in dependent units:
 *  - define-s,
 *  - global variables,
 *  - global functions.
 *
 * 
 * @version $Id: CSUTMockFileForOneTestedUnit.c 102 2017-11-02 16:53:53Z piotr.serwa $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 *  @{
 */

#include "Std.h"

#include "CmockaFramework.h"
#include "SPI.h"
#include "MCU.h"
#include "GPIO.h"
#include "SysTimer.h"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalDefines Mocks for global defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalVariables Mocks for global variables
 *  @{ 
 */
volatile uint16_t IEC3;
volatile uint16_t IFS3;
volatile uint16_t IEC1;
volatile uint16_t IFS1;
volatile uint16_t SPI2BUFH;
volatile uint16_t SPI2BUFL;
volatile uint16_t SPI2URDTH;
volatile uint16_t SPI2URDTL;
volatile uint16_t SPI2IMSKH;
volatile uint16_t SPI2IMSKL;
volatile uint16_t SPI2BRGL;
volatile uint16_t SPI2STATL;
volatile uint16_t SPI2CON2L;
volatile uint16_t SPI2CON1H;
volatile uint16_t SPI2CON1L;
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 *  @{ 
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalFunctionsDefinitions Mocks for global functions definitions
 *  @{
 */
uint16_t GET_U1STA(void)
{
	return((uint16)mock(GET_U1STA));
}
uint16_t GET_SPI2BUFH(void)
{
	return((uint16)mock(GET_SPI2BUFH));
}
uint16_t GET_SPI2BUFL(void)
{
	return((uint16)mock(GET_SPI2BUFL));
}
uint16_t GET_SPI2CON1L(void)
{
	return((uint16)mock(GET_SPI2CON1L));
}
uint16_t GET_SPI2STATL(void)
{
	return((uint16)mock(GET_SPI2STATL));
}
void GPIO_SetPinValue(GPIO_PortType gpioPort,uint8 pin,uint8 value)
{

}
Std_ReturnType SysTimer_SetTimePeriod( uint16 timerSlot, uint16 timerPeriod )
{
	return((Std_ReturnType)mock(SysTimer_SetTimePeriod));
}
Std_ReturnType SysTimer_StartTimer( uint16 timerSlot, SysTimer_ModeType timerMode )
{
	return((Std_ReturnType)mock(SysTimer_StartTimer));
}
SysTimer_ReturnType SysTimer_GetTimerStatus( uint16 timerSlot )
{
	return((SysTimer_ReturnType)mock(SysTimer_GetTimerStatus));
}


/** @} */
