#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

echo "Running Integration Tests compilation on Host (cygwin) ..."

create_and_cd_build_dir

# set relative paths with respect to build dir
OUTPUT_TXT=../../TestReport_HostBuild.txt

echo "Removing old files ..."
rm -fr ${OUTPUT_TXT} *.exe *.dll

echo "Running cmake ..."
cmake -DCMAKE_TOOLCHAIN_FILE=${CYGWIN_TOOLCHAIN_FILE} -DSUBPHASE=SIT ${CMAKELISTS_DIR} 2>&1 | tee ${OUTPUT_TXT} || EXIT=1
#-DCMAKE_VERBOSE_MAKEFILE=1 -
check "cmake"

echo "Running make ..."
make -j6 2>&1 | tee -a ${OUTPUT_TXT} || EXIT=1
check "make"

check_output ${OUTPUT_TXT}

#cp ./SVS/*.exe ../
#cp ./SVS/*.dll ../

echo "... running Integration Tests compilation on Host (cygwin) done."
pause

