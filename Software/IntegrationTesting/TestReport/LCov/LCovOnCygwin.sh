#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

REPORT="Software verification report - code coverage.chm"
enable_interactive_mode $1

echo "Running coverage report generation on remote host (linux) ..."

    create_and_cd_build_dir

    echo "Removing old files ..."
    rm -fr ./coverage-report

    echo "Running cmake ..."
    cmake -DCMAKE_TOOLCHAIN_FILE=${CYGWIN_LCOV_TOOLCHAIN_FILE} \
          -DSUBPHASE=SUT ${CMAKELISTS_DIR} || EXIT=1
    check "cmake"

    echo "Running make ..."
    make || EXIT=1
    check "make"

    echo "Running tests ..."
    ctest -VV -T Test


    #if [[ -z "${INCLUDE_TESTS}" ]]; then
    #    GCOV_ROOT=./Implementation
    #else 
        GCOV_ROOT=.
    #fi

    # suppress cmocka_main
    find ${GCOV_ROOT} -name 'cmocka_main.*.gcda' -exec rm -f {} \;
    find ${GCOV_ROOT} -name 'cmocka_main.*.gcno' -exec rm -f {} \;

    find ${GCOV_ROOT} -name '*.gcda' | xargs gcov
    #lcov --capture --no-list-full-path --directory ${GCOV_ROOT} \
    #     --output-file ./coverage.info
    #genhtml ./coverage.info --output-directory ./coverage-report

    #echo "Generating CHM file ..."
    #hhc ./index.hhp # || EXIT=1
    #echo "... generating CHM file finished."
    #check_output index.chm

    #mv ./index.chm "../${REPORT}"
    #check_output "../${REPORT}"

    #echo "Removing old files ..."
    #rm -fr coverage-report

    echo "... running coverage report generation on remote host done."
    pause


