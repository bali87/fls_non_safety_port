#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

OUTPUT_DIR=Software/UnitTesting/TestReport/LCov
OUTPUT=coverage-report.zip
SUBPHASE=SUT
REPORT="Software verification report - code coverage.chm"

trigger_coverage "${OUTPUT_DIR}" "${OUTPUT}" ${SUBPHASE} "${REPORT}" WITH_TEST_SOURCE

