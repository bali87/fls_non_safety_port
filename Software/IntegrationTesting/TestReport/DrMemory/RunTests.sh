#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1


OUTPUT_TXT="../../Software verification report - dr memory runtime output (linux gcc 32 bit).txt"
SUBPHASE=SUT


  echo "Running DrMemory on remote host (linux) ..."

  create_and_cd_build_dir

  echo "Running cmake ..."
  cmake -DCMAKE_TOOLCHAIN_FILE=${LINUX_DRMEM_TOOLCHAIN_FILE} \
         -DSUBPHASE=${SUBPHASE} ${CMAKELISTS_DIR} || EXIT=1
  check "cmake"

  echo "Running make"
  make || EXIT=1
  check "make"

 

  echo "Removing old files ..."
  rm -fr "${OUTPUT_TXT}" drmemory_logdir
  check_deleted "${REPORT}"

    mkdir -p drmemory_logdir

    for EXEC in `find . -perm /u=x,g=x,o=x -type f`; do
        if [[ ($EXEC == *.sh) || ($EXEC == *.dll) || ($EXEC == *.so) || \
              ($EXEC == *.so.*) || ($EXEC == a.out) || ($EXEC == *.bin) ]]; then
            echo "skipping file ..." > /dev/null
        else
            echo "${EXEC}" >> "${OUTPUT_TXT}"
            echo "=======================" >> "${OUTPUT_TXT}"
            echo "" >> "${OUTPUT_TXT}"
            /opt/DrMemory-Linux-1.8.0-8/bin/drmemory -logdir drmemory_logdir \
                -- ${EXEC} 2>&1 | tee -a "${OUTPUT_TXT}"
            echo "" >> "${OUTPUT_TXT}"
            echo "" >> "${OUTPUT_TXT}"
        fi
    done;

    echo "Removing old file ..."
    rm -fr "~/Test.xml"
    check_deleted "~/Test.xml"

    echo "Fixing xml tags formatting ..."
    sed -e "s/</\\&lt;/g" -e "s/>/\\&gt;/g" -i "${OUTPUT_TXT}"

    echo "Generating ctest file ..."
    bamboo_test_xml "../.." "${OUTPUT_TXT}" DrMemory "[^\\ 0]\\+0* total" 
    check_output "Test.xml"

    mv Test.xml ~/Test.xml

    echo "... running DrMemory on remote host (linux) done."
