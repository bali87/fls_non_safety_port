#!/bin/bash

# generate Test.xml based on cccc_report.xml

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

# input
CCCC_REPORT_XML=cccc_report.xml
# strip information from input
TRANSFORM=BambooCCCCTransform.xsl
BAMBOO_OUTPUT_TXT=BambooCCCCOutput.txt
# output template
OUTPUT_TEMPLATE_XML=BambooCCCCTemplate.txt
# output
OUTPUT_XML=./Test.xml

chmod 777 ${OUTPUT_TEMPLATE_XML}
cp ${OUTPUT_TEMPLATE_XML} ${OUTPUT_XML}

echo "Running sanitize output xml ..."
/usr/bin/xsltproc ${TRANSFORM} ${CCCC_REPORT_XML} > ${BAMBOO_OUTPUT_TXT} || EXIT=1
sed "s/</\&lt;/" -i ${BAMBOO_OUTPUT_TXT} || EXIT=1
sed "s/>/\&gt;/" -i ${BAMBOO_OUTPUT_TXT} || EXIT=1
check "sanitize output xml"

STATUS=passed
STATUS_CAMEL=Passed
ERROR_CODE=0
if grep -q -e "level=\"[^0]\"" ${CCCC_REPORT_XML}; then
    STATUS=failed
    STATUS_CAMEL=Failed
    ERROR_CODE=1
fi

echo "Running sed ..."
sed -e "s/__DATE__/`date`/" -i ${OUTPUT_XML} || EXIT=1
sed -e "s/__TIME__/`date`/" -i ${OUTPUT_XML} || EXIT=1
sed -e "s/__STATUS__/${STATUS}/" -i ${OUTPUT_XML} || EXIT=1
sed -e "s/__STATUS_CAMEL__/${STATUS_CAMEL}/" -i ${OUTPUT_XML} || EXIT=1
sed -e "s/__ERROR_CODE__/${ERROR_CODE}/" -i ${OUTPUT_XML} || EXIT=1
sed -f BambooCCCCReadOutput.sed -i ${OUTPUT_XML} || EXIT=1
check "sed"

echo "Removing temporary files ..."
rm -fr ${BAMBOO_OUTPUT_TXT}

