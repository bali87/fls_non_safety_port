#!/bin/bash

# get input filename
file_name=$1

if [[ $file_name = *.c ]]; then
    # metrics on c-file
    base_name=`basename $file_name .c`
else 
    if [[ $file_name = *.h ]]; then
        # triggered on header file - use c-file instead
        base_name=`basename $file_name .h`
    else
        # not matching c-file or header file - compute metrics in folder
        COPY_FOLDER=TRUE
    fi
fi

# get input folder name
dir_name=`pwd`/`dirname ${file_name}`

BROWSER=chrome.exe

INPUT_TXT=input_files.txt
REPORT_XML_OUTPUT=cccc_report.xml
REPORT_HTML_OUTPUT=cccc_report.html

echo "Creating temp directory ..."
tmp=`mktemp -d`

echo "Using default configuration ..."
cp cccc.opt $tmp
cp cccc.db $tmp
cd $tmp

# collect files to generate metrics
if [[ $COPY_FOLDER = "TRUE" ]]; then
    cp $dir_name/* .
    ls *.c *.h >> ${INPUT_TXT}
else
    for file in `find $dir_name -name "${base_name}*" -type f`; do
      cp $file .
      echo `basename $file` >> ${INPUT_TXT}
    done
fi

# run metrics generation and display the result in a browser
cat ${INPUT_TXT} | cccc --lang=c.ansi --opt_infile=./cccc.opt --outdir="." --xml_outfile=${REPORT_XML_OUTPUT} --html_outfile=${REPORT_HTML_OUTPUT} - && ${BROWSER} "file://c:/cygwin64${tmp}/anonymous.html"

