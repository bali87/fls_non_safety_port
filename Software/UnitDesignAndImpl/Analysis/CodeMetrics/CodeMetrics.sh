#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

echo "Running Code Metrics report generation ..."

INPUT_TXT=input_files.txt
DEFAULT_CHM_OUTPUT=index.chm
REPORT_CHM_OUTPUT="../Software verification report - code metrics.chm"
REPORT_XML_OUTPUT=cccc_report.xml
REPORT_HTML_OUTPUT=cccc_report.html
TEST_XML_OUTPUT=Test.xml

echo "Removing old files ..."
rm -fr ${INPUT_TXT} *.db *.chm *.html *.xml

echo "Collecting input source files ..."
find ../../Implementation/Sources -name "*.c" > ${INPUT_TXT}
find ../../Implementation/Sources -name "*.h" >> ${INPUT_TXT}
check_output ${INPUT_TXT}

echo "Running cccc ..."
cccc --lang=c.ansi --opt_infile=./cccc.opt --outdir="." --xml_outfile=${REPORT_XML_OUTPUT} --html_outfile=${REPORT_HTML_OUTPUT} `cat ${INPUT_TXT}` || EXIT=1
check "cccc"
check_output ${REPORT_XML_OUTPUT}
check_output ${REPORT_HTML_OUTPUT}

echo "Running ctest file generation ..."
./BambooCCCCGenerate.sh $1 || EXIT=1
check "ctest file generation"
check_output ${TEST_XML_OUTPUT}

echo "Running CHM generation ..."
hhc index.hhp # || EXIT=1
# SUPPRESS HHC ERROR CODE
#check "CHM generation"
echo "... running CHM generation finished."
pause 

check_output ${DEFAULT_CHM_OUTPUT}
mv ${DEFAULT_CHM_OUTPUT} "${REPORT_CHM_OUTPUT}"
check_output "${REPORT_CHM_OUTPUT}"

echo "Removing temporary files ..."
rm -fr *.db *.html anonymous.xml cccc_src.xml ${REPORT_XML_OUTPUT} ${INPUT_TXT}

echo "... running Code Metrics Report generation done."
pause

