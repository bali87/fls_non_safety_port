#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

STATUS=passed
STATUS_CAMEL=Passed
ERROR_CODE=0

MISRA_XML=Lint_Violations.xml

OUTPUT_TEMPLATE_XML=BambooMISRATemplate.xml
OUTPUT_XML=./Test.xml

chmod 777 ${OUTPUT_TEMPLATE_XML}

cp ${OUTPUT_TEMPLATE_XML} ${OUTPUT_XML}
check_output ${OUTPUT_XML}

function update() {
  REPLACE=$1
  COUNT_PATTERN=$2
  COUNT=`grep "<type>${COUNT_PATTERN}</type>" ${MISRA_XML} | wc -l | cut -d" " -f 1`
  sed -e "s/${REPLACE}/$COUNT/" -i ${OUTPUT_XML}
  eval "${COUNT_PATTERN}=${COUNT}"
  #Test should not fail because of notes, infos or warnings
  #if [ ! "${COUNT}" == "0" ]; then
  #  STATUS=failed
  #  STATUS_CAMEL=Failed
  #  ERROR_CODE=1
  #fi
}

function updateErrors() {
    REPLACE="__ERROR__"
    COUNT_PATTERN="Error"
    COUNT=`grep "<type>${COUNT_PATTERN}</type>" ${MISRA_XML} | wc -l | cut -d" " -f 1`
    sed -e "s/${REPLACE}/${COUNT}/" -i ${OUTPUT_XML}
    eval "${COUNT_PATTERN}=${COUNT}"
    if [ ! "${COUNT}" == "0" ]; then
        STATUS=failed
        STATUS_CAMEL=Failed
        ERROR_CODE=1
    fi
}

echo "Running sed ..."
update __WARNING__ Warning
update __INFO__ Info
update __NOTE__ Note
updateErrors
sed -e "s/__DATE__/`date`/" -i ${OUTPUT_XML} || EXIT=1
sed -e "s/__TIME__/`date`/" -i ${OUTPUT_XML} || EXIT=1
sed -e "s/__STATUS__/${STATUS}/" -i ${OUTPUT_XML} || EXIT=1
sed -e "s/__STATUS_CAMEL__/${STATUS_CAMEL}/" -i ${OUTPUT_XML} || EXIT=1
sed -e "s/__ERROR_CODE__/${ERROR_CODE}/" -i ${OUTPUT_XML} || EXIT=1
sed -e "s/__OUTPUT__/MISRA report has been generated./" -i ${OUTPUT_XML} || EXIT=1
check "sed"

