#!/bin/bash


. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

echo "Running MISRA report generation ..."
echo "Removing temporary files ..."
rm -fr Lint_Violations.xlsx Lint_Violations.xls Lint_Violations.xml LNT_ProjectFiles.lnt LNT_ProjectFolders.lnt LNT_ProjectLibraryFolders.lnt Test.xml Bamboo.lnt
check_deleted Lint_Violations.xlsx
check_deleted Lint_Violations.xls
check_deleted Lint_Violations.xml
check_deleted LNT_ProjectFiles.lnt
check_deleted LNT_ProjectFolders.lnt
check_deleted LNT_ProjectLibraryFolders.lnt
check_deleted Test.xml
echo "... removing temporary files done."
#pause

echo "Generating folder list ..."
for folder in `find ../../Implementation/Sources -type d`; do
    echo "-i\"`cygpath -w ${folder}`\"" >> LNT_ProjectFolders.lnt
    echo "-libdir(`cygpath -w ${folder}`)" >> LNT_ProjectFolders.lnt
done
check_output LNT_ProjectFolders.lnt

echo "-i\"misra_include\"" >> LNT_ProjectLibraryFolders.lnt
echo "+libdir(misra_include)" >> LNT_ProjectLibraryFolders.lnt

echo "Generating library folder list ..."
for folder in `find ../../Implementation/Libraries -type d`; do
    echo "-i\"`cygpath -w ${folder}`\"" >> LNT_ProjectLibraryFolders.lnt
    echo "+libdir(`cygpath -w ${folder}`)" >> LNT_ProjectLibraryFolders.lnt
done


#FIXME: MISRA finds a lot of errors in external, 3rd party libraries, so it fails
# for folder in `find /usr/include -type d`; do
   # echo "-i\"`cygpath -w ${folder}`\"" >> LNT_ProjectLibraryFolders.lnt
   # echo "+libdir(`cygpath -w ${folder}`)" >> LNT_ProjectLibraryFolders.lnt
# done



MCC_INCLUDE="C:\\Program Files (x86)\\Microchip\\xc16\\v1.34\\include"
echo "-i\"${MCC_INCLUDE}\"" >> LNT_ProjectLibraryFolders.lnt
echo "+libdir(${MCC_INCLUDE})" >> LNT_ProjectLibraryFolders.lnt
check_output LNT_ProjectLibraryFolders.lnt

echo "Generating file list ..."
for file in `find ../../Implementation/Sources -name "*.c"`; do
    echo "`cygpath -w ${file}`" >> LNT_ProjectFiles.lnt
done
check_output LNT_ProjectFiles.lnt

lint-nt.exe +v  -i"."  std.lnt -os\(.\\Lint_Violations.xml\) \
    env-xml.lnt LNT_ProjectFiles.lnt +libclass\(ansi\) # || EXIT=1
# SUPPRESS lint error
check_output Lint_Violations.xml

echo "Running ctest file generation ..."
./BambooMISRAGenerate.sh $1 || EXIT=1
check_output Test.xml

echo "Running convertion from XML to XLSX ..."
cscript XML2XLSX.vbs || EXIT=1
check "conversion from XML to XLSX"
check_output "Lint_Violations.xlsx"

OUTPUT="Software verification report - MISRA report.xlsx"
mv ./Lint_Violations.xlsx ../"${OUTPUT}"
check_output ../"${OUTPUT}"

echo "Removing temporary files ..."
rm -fr Lint_Violations.xlsx Lint_Violations.xls Lint_Violations.xml LNT_ProjectFiles.lnt LNT_ProjectFolders.lnt LNT_ProjectLibraryFolders.lnt Bamboo.lnt
check_deleted Lint_Violations.xlsx
check_deleted Lint_Violations.xls
check_deleted Lint_Violations.xml
check_deleted LNT_ProjectFiles.lnt
check_deleted LNT_ProjectFolders.lnt
check_deleted LNT_ProjectLibraryFolders.lnt
echo "... removing temporary files done."
#pause

echo "... MISRA report generation done."
#pause
