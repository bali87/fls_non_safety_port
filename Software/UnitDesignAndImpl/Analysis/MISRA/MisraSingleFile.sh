#!/bin/bash

echo "Running MISRA report generation ..."
echo "Removing temporary files ..."
rm -fr Lint_Violations.xlsx Lint_Violations.xls Lint_Violations.xml LNT_ProjectFiles.lnt LNT_ProjectFolders.lnt LNT_ProjectLibraryFolders.lnt Test.xml Bamboo.lnt
echo "... removing temporary files done."

echo "Generating folder list ..."
for folder in `find ../../Implementation/Sources -type d`; do
    echo "-i\"`cygpath -w ${folder}`\"" >> LNT_ProjectFolders.lnt
    echo "-libdir(`cygpath -w ${folder}`)" >> LNT_ProjectFolders.lnt
done

echo "Generating library folder list ..."
for folder in `find ../../Implementation/Libraries -type d`; do
    echo "-i\"`cygpath -w ${folder}`\"" >> LNT_ProjectLibraryFolders.lnt
    echo "+libdir(`cygpath -w ${folder}`)" >> LNT_ProjectLibraryFolders.lnt
done




MCC_INCLUDE="C:\\Program Files (x86)\\Microchip\\xc8\\v1.34\\include"
echo "-i\"${MCC_INCLUDE}\"" >> LNT_ProjectLibraryFolders.lnt
echo "+libdir(${MCC_INCLUDE})" >> LNT_ProjectLibraryFolders.lnt

echo "Generating file list ..."
echo "`cygpath -w ${1}`" >> LNT_ProjectFiles.lnt


lint-nt.exe +v  -i"."  std.lnt -os\(.\\Lint_Violations.xml\) \
    env-xml.lnt LNT_ProjectFiles.lnt +libclass\(ansi\) # || EXIT=1


echo "Running convertion from XML to XLSX ..."
cscript XML2XLSX.vbs || EXIT=1

excel Lint_Violations.xlsx

