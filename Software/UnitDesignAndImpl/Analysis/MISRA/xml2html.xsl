<?xml version="1.0"?>
<!-- greeting.xsl -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<xsl:text disable-output-escaping="yes">
			&lt;html&gt;
			&lt;head&gt;
			&lt;link rel="stylesheet" href="lint.css" type="text/css" /&gt;
			&lt;/head&gt;
			&lt;body&gt;
			&lt;table&gt;

			&lt;tr class="header"&gt;
			&lt;th&gt;
			file
			&lt;/th&gt;
			&lt;th&gt;
			line
			&lt;/th&gt;
			&lt;th&gt;
			type
			&lt;/th&gt;
			&lt;th&gt;
			code
			&lt;/th&gt;
			&lt;th&gt;
			desc
			&lt;/th&gt;
			&lt;/tr&gt;
		</xsl:text>

		<xsl:apply-templates select="/doc/message"/>
		<xsl:text disable-output-escaping="yes">
			&lt;/table&gt;
			&lt;/body&gt;
			&lt;/html&gt;
		</xsl:text>

	</xsl:template>
	<xsl:template match="/doc/message/">
		<xsl:apply-templates select="message"/>
	</xsl:template>

	<xsl:template match="message">
		<xsl:text disable-output-escaping="yes">&lt;tr class="</xsl:text><xsl:value-of select="./type"/><xsl:text disable-output-escaping="yes">"&gt;</xsl:text>
		<xsl:text disable-output-escaping="yes">&lt;td&gt;</xsl:text>

		<xsl:value-of select="./file"/>
		<xsl:text disable-output-escaping="yes">
			&lt;/td&gt;
			&lt;td&gt;
		</xsl:text>
		<xsl:value-of select="./line"/>
		<xsl:text disable-output-escaping="yes">  
			&lt;/td&gt;
			&lt;td&gt;
		</xsl:text>
		<xsl:value-of select="./type"/>

		<xsl:text disable-output-escaping="yes">
			&lt;/td&gt;
			&lt;td&gt;
		</xsl:text>
		<xsl:value-of select="./code"/>

		<xsl:text disable-output-escaping="yes">
			&lt;/td&gt;
			&lt;td&gt;
		</xsl:text>      
		<xsl:value-of select="./desc"/>

		<xsl:text disable-output-escaping="yes">
			&lt;/td&gt;
			&lt;/tr&gt;
		</xsl:text>
	</xsl:template>

</xsl:stylesheet>

