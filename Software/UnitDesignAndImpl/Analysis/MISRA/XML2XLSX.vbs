Option Explicit

rem On Error Resume Next

ExcelMacroExample

Sub ExcelMacroExample() 

  Dim xlApp 
  Dim xlBook

  WScript.Echo "Starting the excel application  ..."  

  Set xlApp = CreateObject("Excel.Application") 
  
  WScript.Echo "... excel created"  
  
  rem open the macro excel file    
  Set xlBook = xlApp.Workbooks.Open(Replace(WScript.ScriptFullName, WScript.ScriptName, "XML2XLSX.xlsm"), 0, True) 
  
  WScript.Echo "Converting to excel format  ..."
  xlApp.Run "Convert"
  
  WScript.Echo "... conversion done"
    
  xlApp.Quit 
  
  WScript.Echo "... excel closed"

  Set xlBook = Nothing 
  Set xlApp = Nothing 

End Sub 