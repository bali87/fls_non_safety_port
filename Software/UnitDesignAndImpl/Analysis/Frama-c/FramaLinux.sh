#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

OUTPUT_DIR=./Software/UnitDesignAndImpl/Analysis/
OUTPUT=SVR-R_Frama.txt
SUBPHASE=SUI

run_frama ${OUTPUT_DIR} ${OUTPUT} ${SUBPHASE}

