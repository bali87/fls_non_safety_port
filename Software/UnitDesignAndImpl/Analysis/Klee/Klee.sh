#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

OUTPUT_DIR=./Software/UnitDesignAndImpl/Analysis/
OUTPUT=SVR-R_Klee.txt
SUBPHASE=SUI

trigger_run_klee_sui ${OUTPUT_DIR} ${OUTPUT} ${SUBPHASE}

