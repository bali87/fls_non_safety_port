#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

echo "Running SUI compilation on Target (cygwin) ..."

create_and_cd_build_dir

# set relative paths with respect to build dir
OUTPUT_TXT=../../Implementation_TargetBuild.txt

echo "Removing old files ..."
rm -fr ${OUTPUT_TXT} *.hex

echo "Running cmake ..."
echo ${XC16_TOOLCHAIN_FILE}

cmake -DCMAKE_TOOLCHAIN_FILE=${MCU_TOOLCHAIN_FILE} -DSUBPHASE=SUDI ${CMAKELISTS_DIR} 2>&1 | tee ${OUTPUT_TXT} || EXIT=1
check "cmake"

echo "Running make ..."
make 2>&1 | tee -a ${OUTPUT_TXT} || EXIT=1
check "make"

collect_hex
check_output ${OUTPUT_TXT}

echo "... running SUI compilation on Target (cygwin) done."
pause


