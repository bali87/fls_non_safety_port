@echo off
set DEBUG_MODE="%1"

echo "running program on Target"

set SIMULATION_INPUT=%CD%\RunConfig.txt
SET SIMULATION_MDBLOG=..\SUI_TargetOutput.txt"


echo "deleting old files ..."
del /q %SIMULATION_MDBLOG%
del /q *.xml*
echo "... deleted"

echo "starting the program through microchip debugger ..."
mdb.bat %SIMULATION_INPUT% >> %SIMULATION_MDBLOG%
rem fixme: mdb.bat exits probably the entire script here
echo "... program finished"



rem check if file is generated. If not then exit with error code.
if not exist %SIMULATION_OUTPUT% goto exit

echo "deleting unnecessary xml files..."
del /q *.xml*
echo "... deleted"

rem exit without error
echo "... running program on Target finished successfully"
if %DEBUG_MODE% NEQ "skip" pause
exit /b

rem exit with error
:exit
del /q *.xml*
echo "... running program on Target finished with ERROR"
if %DEBUG_MODE% NEQ "skip" pause
exit /b 1

