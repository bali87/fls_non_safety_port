#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

echo "Running SUDI compilation on Host (cygwin) ..."

create_and_cd_build_dir

# set relative paths with respect to build dir
OUTPUT_TXT=../../Implementation_HostBuild.txt

echo "Removing old files ..."
rm -fr ${OUTPUT_TXT} *.exe *.dll


echo "Running cmake ..."
echo  ${CYGWIN_TOOLCHAIN_FILE}
cmake -DCMAKE_TOOLCHAIN_FILE=${CYGWIN_TOOLCHAIN_FILE} -DSUBPHASE=SUDI ${CMAKELISTS_DIR} 2>&1 | tee ${OUTPUT_TXT} || EXIT=1
check "cmake"

echo "Running make ..."
make 2>&1 | tee -a ${OUTPUT_TXT} || EXIT=1
check "make"

check_output ${OUTPUT_TXT}

echo "... running SUDI compilation on Host (cygwin) done."
pause

