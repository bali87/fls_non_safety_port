/**
 * @file
 *
 * Simple c-file with short main function. Unit may be used to evaluate the size of compiled cmocka code on PIC32.
 * 
 * @version $Id: maintest.c 102 2017-11-02 16:53:53Z piotr.serwa $
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */

//#include "Std.h"

#define CMOCKA_DEBUG

#include "CmockaFramework.h"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForDefines Mocks for defines of dependent units
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalVariables Mocks for global variables of dependent units
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup MocksForGlobalFunctions Mocks for global functions of dependent units
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */

static void Maintest_NormalCall(void **state);

static void Maintest_CheckSingleCall(void **state);

static void Maintest_CheckDoubleCall(void **state);

static void Maintest_ForbiddenCall(void **state);

static void Maintest_ExpectMoreCalls(void **state);

static void Maintest_ExpectManyCalls(void **state);

static void Maintest_ExpectCorrect(void **state);

static void Maintest_ExpectWrong(void **state);

static void Maintest_ExpectNotProvided(void **state);

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */

/**
 * List of the tests provided in this software unit.
 */
static const struct CMUnitTest Tests[] = {
        cmocka_unit_test(Maintest_NormalCall),
        cmocka_unit_test(Maintest_CheckSingleCall),
        cmocka_unit_test(Maintest_CheckDoubleCall),
        cmocka_unit_test(Maintest_ForbiddenCall),
        cmocka_unit_test(Maintest_ExpectMoreCalls),
        cmocka_unit_test(Maintest_ExpectManyCalls),

        cmocka_unit_test(Maintest_ExpectCorrect),
        cmocka_unit_test(Maintest_ExpectWrong),
        cmocka_unit_test(Maintest_ExpectNotProvided),
};

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariablesInitialized Static variables initialized
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariablesNonInitialized Static variables non-initialized
 * @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */

static void checkFunction(void) {

    CMOCKA_CHECKPOINT(checkFunction);
}

static void Maintest_NormalCall(void **state) {

    /* given */

    /* when */
    checkFunction();

    /* then */

    return;
}

static void Maintest_CheckSingleCall(void **state) {

    /* given */
    will_be_called(checkFunction, 1U);

    /* when */
    checkFunction();

    /* then */

    return;
}

static void Maintest_CheckDoubleCall(void **state) {

    /* given */
    will_be_called(checkFunction, 2U);

    /* when */
    checkFunction();
    checkFunction();

    /* then */

    return;
}

static void Maintest_ForbiddenCall(void **state) {

    /* given */
    will_be_called(checkFunction, 0U);

    /* when */
    checkFunction();

    /* then */

    return;
}

static void Maintest_ExpectMoreCalls(void **state) {

    /* given */
    will_be_called(checkFunction, 5U);

    /* when */
    checkFunction();
    checkFunction();

    /* then */

    return;
}

static void Maintest_ExpectManyCalls(void **state) {

    /* given */
    will_be_called(checkFunction, 13U);

    /* when */

    /* then */

    return;
}

static int add(int a, int b) {
    check(add, a);
    check(add, b);
	return (int) mock(add);
}

static void Maintest_ExpectCorrect(void **state) {

    /* given */
    expect(add, a, 3);
    expect(add, b, 5);
    will_return(add, 8);

    /* when */
    int ret = add(3, 5);

    /* then */
    assert_int_equal(ret, 8);

    return;
}

static void Maintest_ExpectWrong(void **state) {

    /* given */
    expect(add, a, 13);
    expect(add, b, 15);
    will_return(add, 8);

    /* when */
    int ret = add(3, 5);

    /* then */
    assert_int_equal(ret, 8);

    return;
}

static void Maintest_ExpectNotProvided(void **state) {

    /* given */
    will_return(add, 8);

    /* when */
    int ret = add(3, 5);

    /* then */
    assert_int_equal(ret, 8);

    return;
}

/** @} */

/**
 * Main function handling tests functions defined in the structure tests.
 */
CMOCKA_MAIN(Tests, NULL, NULL
)

