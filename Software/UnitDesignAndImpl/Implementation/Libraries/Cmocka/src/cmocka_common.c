#include <stdio.h>
#include <stdlib.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>

#ifndef __XC16__
#include "cmocka.h"
#include "cmocka.c"
#else
#include "xc_cmocka.h"
#include "xc_cmocka.c"
#endif

int cmocka_main(int argc, char ** argv,
                const struct CMUnitTest group_tests[],
                CMFixtureFunction group_setup,
                CMFixtureFunction group_teardown,
                int numberOfTests);

int cmocka_main(int argc, char ** argv,
                const struct CMUnitTest group_tests[],
                CMFixtureFunction group_setup, 
                CMFixtureFunction group_teardown,
                int numberOfTests)
{
    printf("Found %d tests.\n", numberOfTests);

    if (argc == 2) {                                                      
        int testNumber = atoi(argv[1]);                                   
                                                                          
        if ((testNumber >= 0) && (testNumber < numberOfTests)) {
            printf("Running test #%d.\n", testNumber);
            #ifndef __XC16__
            return _cmocka_run_group_tests("Running single test", &group_tests[testNumber], 1U, (group_setup),
                                          (group_teardown));
            #else
            return _cmocka_run_tests("Running single test", &group_tests[testNumber], 1U);
            #endif
        }
                                                                          
        else {                                                            
            printf("No #%d test found.\n", testNumber);
            return 1;                                                     
        }                                                                 
    }                                                                     
                                                                          
    else {
#ifndef __XC16__
        printf("Running all tests\n");
        return _cmocka_run_group_tests("All tests", group_tests, numberOfTests, group_setup, group_teardown);
#else
        return _cmocka_run_tests("All tests", group_tests, numberOfTests);
#endif /* !__XC16__ */
    }                                                                     
}

