/*
 * Copyright 2008 Google Inc.
 * Copyright 2014-2015 Andreas Schneider <asn@cryptomilk.org>
 * Copyright 2015      Jakub Hrozek <jakub.hrozek@posteo.se>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_MALLOC_H
#include <malloc.h>
#endif

#ifdef HAVE_INTTYPES_H
#include <inttypes.h>
#endif

#ifdef HAVE_SIGNAL_H
#include <signal.h>
#endif

#include <stdint.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#if 0
#include <xc.h>
#include <stdio.h>
/*****************************
Dependencies:   xc.h, stdio.h
Processor:      PIC18F6722
Complier:       XC8 v1.00 or higher 
Result: Prints to the UART1 Window of the IDE
Debugger: Simulator
*****************************/
void putch(unsigned char byte)
{
            TXSTA=0x26;
            SPEN=1;
            TXREG=byte;            /* transmit a character to Serial IO */
            while(!TXIF)continue;
            TXIF=0;
}
#endif
#ifdef __XC16__
#include <xc.h>
#include <stdio.h>
/*****************************
Dependencies:   xc.h, stdio.h
Processor:      PIC18F6722
Complier:       XC16 v1.00 or higher 
Result: Prints to the UART1 Window of the IDE
Debugger: Simulator
*****************************/
void putch(unsigned char byte)
{
            U1MODE = 0; // Clear UART1 mode register
			U1STA = 0;    // Clear UART1 status register
            U1BRG = 0x0007;// Calculate value of baud rate register U1BRG = 0x0044;// Calculate value of baud rate register
			U1MODEbits.UARTEN = 1;   // Enable UART1 module
            U1STAbits.UTXEN = 1;     // Enable UART1 transmit
			U1MODEbits.UARTEN = 1;  // enabling UART ON bit
			U1STAbits.UTXEN = 1;
			U1TXREG = byte; /* transmit a character to Serial IO */
			while(U1STAbits.UTXBF) continue;
}
#endif

#define MAX_HEAP_SIZE 1024U

unsigned char pic_heap[MAX_HEAP_SIZE];

unsigned int pic_sp = 0;

void pic_heap_usage();

void * pic_malloc(size_t size)
{
    void * ret = (void *) &pic_heap[pic_sp];

    if (pic_sp + size > MAX_HEAP_SIZE) 
	{
		printf("Error ! Heap mock usage: %d / %d (bytes)\n", pic_sp, MAX_HEAP_SIZE);
        ret = NULL;
    }
    else 
	{
        pic_sp += size;
    }
    
   //pic_heap_usage();

    return ret;    
}

void pic_free(void)  {
    pic_sp = 0;
    //pic_heap_usage();
}
void free(void * ptr)
{
    //pic_heap_usage();
}

void pic_heap_usage()
{
    printf("Heap mock usage: %d / %d (bytes)\n", pic_sp, MAX_HEAP_SIZE);
}

void _cleanup_call_checks(void);

/*
 * This allows to add a platform specific header file. Some embedded platforms
 * sometimes miss certain types and definitions.
 *
 * Example:
 *
 * typedef unsigned long int uintptr_t
 * #define _UINTPTR_T 1
 * #define _UINTPTR_T_DEFINED 1
 */
#ifdef CMOCKA_PLATFORM_INCLUDE
# include "cmocka_platform.h"
#endif /* CMOCKA_PLATFORM_INCLUDE */

#include <xc_cmocka.h>
#include <xc_cmocka_private.h>

/* Size of guard bytes around dynamically allocated blocks. */
#define MALLOC_GUARD_SIZE 16
/* Pattern used to initialize guard blocks. */
#define MALLOC_GUARD_PATTERN 0xEF
/* Pattern used to initialize memory allocated with test_malloc(). */
#define MALLOC_ALLOC_PATTERN 0xBA
#define MALLOC_FREE_PATTERN 0xCD
/* Alignment of allocated blocks.  NOTE: This must be base2. */
#define MALLOC_ALIGNMENT sizeof(size_t)

/* Printf formatting for source code locations. */
#define SOURCE_LOCATION_FORMAT "%s:%u"

/*
 * Declare and initialize the pointer member of ValuePointer variable name
 * with ptr.
 */
#define declare_initialize_value_pointer_pointer(name, ptr) \
    ValuePointer name ; \
    name.value = 0; \
    name.x.pointer = (void*)(ptr)

/*
 * Declare and initialize the value member of ValuePointer variable name
 * with val.
 */
#define declare_initialize_value_pointer_value(name, val) \
    ValuePointer name ; \
    name.value = val

/* Cast a LargestIntegralType to pointer_type via a ValuePointer. */
#define cast_largest_integral_type_to_pointer( \
    pointer_type, largest_integral_type) \
    ((pointer_type)((ValuePointer*)&(largest_integral_type))->x.pointer)

/* Used to cast LargetIntegralType to void* and vice versa. */
typedef union ValuePointer {
    LargestIntegralType value;
    struct {
#if defined(WORDS_BIGENDIAN) && (WORDS_SIZEOF_VOID_P == 4)
        unsigned int padding;
#endif
        void *pointer;
    } x;
} ValuePointer;

/* Doubly linked list node. */
typedef struct ListNode {
    const char *key;
    LargestIntegralType value;
    struct ListNode *next;
    struct ListNode *prev;
} ListNode;

/* State of each test. */
typedef struct TestState {
    const ListNode *check_point; /* Check point of the test if there's a */
                                 /* setup function. */
    void *state;                 /* State associated with the test. */
} TestState;

/* Determines whether two values are the same. */
typedef int (*EqualityFunction)(const void *left, const void *right);

/* Value of a symbol and the place it was declared. */
typedef struct SymbolValue {
    SourceLocation location;
    LargestIntegralType value;
} SymbolValue;

/*
 * Contains a list of values for a symbol.
 * NOTE: Each structure referenced by symbol_values_list_head must have a
 * SourceLocation as its' first member.
 */
typedef struct SymbolMapValue {
    const char *symbol_name;
    ListNode symbol_values_list_head;
} SymbolMapValue;

/* Used by list_free() to deallocate values referenced by list nodes. */
typedef void (*CleanupListValue)(const void *value, void *cleanup_value_data);

enum CMUnitTestStatus {
    CM_TEST_NOT_STARTED = 0x00,
    CM_TEST_PASSED = 0x01,
    CM_TEST_FAILED = 0x10,
    CM_TEST_ERROR  = 0x11,
    CM_TEST_SKIPPED = 0x12,
};

enum CMUnitTestStatus last_test_status; /* PASSED, FAILED, ABORT ... */

/* Structure used to check the range of integer types.a */
//typedef struct CheckIntegerRange {
//    CheckParameterEvent event;
//    LargestIntegralType minimum;
//    LargestIntegralType maximum;
//} CheckIntegerRange;

/* Structure used to check whether an integer value is in a set. */
//typedef struct CheckIntegerSet {
//    CheckParameterEvent event;
//    const LargestIntegralType *set;
//    size_t size_of_set;
//} CheckIntegerSet;

/* Used to check whether a parameter matches the area of memory referenced by
 * this structure.  */
//typedef struct CheckMemoryData {
//    CheckParameterEvent event;
//    const void *memory;
//    size_t size;
//} CheckMemoryData;

static ListNode* list_initialize(ListNode * node);
static ListNode* list_add(ListNode * const head, ListNode *new_node);
static ListNode* list_add_value(ListNode * head, const char *key,
                                     const LargestIntegralType value);
static ListNode* list_remove(ListNode * const node);
static void list_remove_free(ListNode * const node);
static int list_empty(const ListNode * const head);
static int list_find(
    ListNode * const head, const void *value,
    const EqualityFunction equal_func, ListNode **output);
static ListNode* list_free(ListNode * head);

static void add_symbol_value(
    ListNode * symbol_map_head, const char * symbol_name,
    const LargestIntegralType value);
static int get_symbol_value(
    ListNode * symbol_map_head, const char * symbol_name,
    LargestIntegralType *outputPtr);
static void free_symbol_map_value(
    const void *value, void *cleanup_value_data);
static void remove_always_return_values(ListNode * const map_head,
                                        const size_t number_of_symbol_names);
static int check_for_leftover_values(
    const ListNode * const map_head, const char * const error_message,
    const size_t number_of_symbol_names);
/*
 * This must be called at the beginning of a test to initialize some data
 * structures.
 */
static void initialize_testing(const char *test_name);

/* This must be called at the end of a test to free() allocated structures. */
static void teardown_testing(const char *test_name);

/*
 * Keeps track of the calling context returned by setenv() so that the fail()
 * method can jump out of a test.
 */
static  jmp_buf global_run_test_env;
static  int global_running_test = 1;

/* Keeps track of the calling context returned by setenv() so that */
/* mock_assert() can optionally jump back to expect_assert_failure(). */
jmp_buf global_expect_assert_env;
int global_expecting_assert = 0;
const char *global_last_failed_assert = NULL;

/* Keeps a map of the values that functions will have to return to provide */
/* mocked interfaces. */
static  ListNode global_function_result_map_head;
/* Location of the last mock value returned was declared. */
static  SourceLocation global_last_mock_value_location;
/* ADDED BY vb */
static ListNode global_function_parameter_map_head;
static SourceLocation global_last_parameter_location;
/* End added by vb */
/* Exit the currently executing test. */
static void exit_test(const int quit_application)
{
    if (global_running_test) {
        longjmp(global_run_test_env, 1);
    } else if (quit_application) {
        exit(-1);
    }
}

/* Initialize a SourceLocation structure. */
static void initialize_source_location(SourceLocation * const location) {
    location->file = NULL;
    location->line = 0;
}


/* Determine whether a source location is currently set. */
static int source_location_is_set(const SourceLocation * const location) {
    return location->file && location->line;
}

/* Create function results and expected parameter lists. */
static void initialize_testing(const char *test_name) {
	(void)test_name;
    list_initialize(&global_function_result_map_head);
    initialize_source_location(&global_last_mock_value_location);
}


static void teardown_testing(const char *test_name) {
    list_free(&global_function_result_map_head);
    initialize_source_location(&global_last_mock_value_location);
}


/* Initialize a list node. */
static ListNode* list_initialize(ListNode * node) {
    node->key = NULL;
    node->value = 0U;
    node->next = node;
    node->prev = node;
    return node;
}


/*
 * Adds a value at the tail of a given list.
 * The node referencing the value is allocated from the heap.
 */
static ListNode* list_add_value(ListNode * head, const char *key,
                                     const LargestIntegralType value) {
    ListNode * new_node = (ListNode *) pic_malloc(sizeof(ListNode));
    new_node->value = value;
    new_node->key = key;
	
    return list_add(head, new_node);
}


/* Add new_node to the end of the list. */
static ListNode* list_add(ListNode * const head, ListNode *new_node) {
    new_node->next = head;
    new_node->prev = head->prev;
    head->prev->next = new_node;
    head->prev = new_node;
    return new_node;
}


/* Remove a node from a list. */
static ListNode* list_remove(ListNode * const node) {
    node->prev->next = node->next;
    node->next->prev = node->prev;
    return node;
}


/* Remove a list node from a list and free the node. */
static void list_remove_free(ListNode * const node) {
    list_remove(node);
}


/*
 * Frees memory kept by a linked list The cleanup_value function is called for
 * every "value" field of nodes in the list, except for the head.  In addition
 * to each list value, cleanup_value_data is passed to each call to
 * cleanup_value.  The head of the list is not deallocated.
 */
static ListNode* list_free(ListNode * head) {
    while (!list_empty(head)) {
        list_remove_free(head->next);
    }
    return head;
}


/* Determine whether a list is empty. */
static int list_empty(const ListNode * const head) {
    return head->next == head;
}


/*
 * Find a value in the list using the equal_func to compare each node with the
 * value.
 */
static int list_find(ListNode * const head, const void *value,
                     const EqualityFunction equal_func, ListNode **output) {
    ListNode *current = head->next;
    while ((int)current != (int)head) {
        if (equal_func(current->key, value)) {
            *output = current;
            return 1;
        }
        current = current->next;
    }
    return 0;
}

/*
 * Determine whether a symbol name referenced by a symbol_map_value matches the
 * specified function name.
 */
static int symbol_names_match(const void *map_value, const void *symbol) {
    return !strcmp((const char*) map_value, (const char*) symbol);
}


/*
 * Adds a value to the queue of values associated with the given hierarchy of
 * symbols.  It's assumed value is allocated from the heap.
 */
static void add_symbol_value(ListNode * symbol_map_head,
                             const char * symbol_name,
                             const LargestIntegralType value
                             ) {
    list_add_value(symbol_map_head, symbol_name, value);
}


/*
 * Gets the next value associated with the given hierarchy of symbols.
 * The value is returned as an output parameter with the function returning the
 * node's old refcount value if a value is found, 0 otherwise.  This means that
 * a return value of 1 indicates the node was just removed from the list.
 */
static int get_symbol_value(
        ListNode * head, const char * symbol_name,
        LargestIntegralType *outputPtr) {
    ListNode *target_node;

    if (list_find(head, symbol_name, symbol_names_match, &target_node)) {
        *outputPtr = target_node->value;
        list_remove_free(target_node);
        return 1;
    } else {
        printf("No entries for symbol %s.\n", symbol_name);
    }
    return 0;
}



/* Get the next return value for the specified mock function. */
LargestIntegralType _mock(const char * const function, const char* const file,
                          const int const line) {
    LargestIntegralType result;
    const int rc = get_symbol_value(&global_function_result_map_head,
                                    function, &result);
    if (rc) {
        return result;
    } else {
        printf(SOURCE_LOCATION_FORMAT ": error: Could not get value "
                       "to mock function %s\n", file, line, function);
        if (source_location_is_set(&global_last_mock_value_location)) {
            printf(SOURCE_LOCATION_FORMAT
                           ": note: Previously returned mock value was declared here\n",
                           global_last_mock_value_location.file,
                           global_last_mock_value_location.line);
        } else {
            printf("There were no previously returned mock values for "
                           "this test.\n");
        }
        exit_test(1);
    }
    return 0;
}

/* Get the next return value for the specified mock function. */
void _void_mock(const char * const function, const char* const file,
                          const int const line) {

//    LargestIntegralType result;
    ListNode *target_node;

    if (list_find(&global_function_result_map_head, function, symbol_names_match, &target_node)) {
        if (target_node->value == 0U) {
            printf(SOURCE_LOCATION_FORMAT ": error: Too many calls "
                           "to function %s\n", file, line, function);
            exit_test(1);
        } else {
            target_node->value = target_node->value - 1U;
        }

    } else {
        /* no action */
    }

    return;
}

static int symbol_starts_with_hash(const void *map_value, const void *symbol) {
    #ifdef CMOCKA_DEBUG
    printf("Function %s is checked.\n", (const char *) map_value);
    #endif
    return ((*((const char *) map_value)) == '#');
}

void _cleanup_call_checks(void) {

//    LargestIntegralType result;
    ListNode *target_node;

    while (list_find(&global_function_result_map_head, NULL, symbol_starts_with_hash, &target_node)) {
        if (target_node->value == 0U) {
            #ifdef CMOCKA_DEBUG
            printf("Function %s was called as many times as expected.\n", target_node->key);
            #endif
            list_remove_free(target_node);
        } else {
            printf("Error: Function %s should be called %llu more time(s).\n",
                   target_node->key, target_node->value);
            exit_test(1);
        }
    }

    return;
}

/* Add a return value for the specified mock function name. */
void _will_return(const char * const function_name, const char * const file, const int line, const LargestIntegralType value) {
    add_symbol_value(&global_function_result_map_head, function_name, value);
}

/* Returns 1 if the specified values are equal.  If the values are not equal
 * an error is displayed and 0 is returned. */
static int values_equal_display_error(const LargestIntegralType left,
                                      const LargestIntegralType right) {
    const int equal = left == right;
    if (!equal) {
        printf(LargestIntegralTypePrintfFormat " != "
                       LargestIntegralTypePrintfFormat "\n", left, right);
    }
    return equal;
}

/*
 * Returns 1 if the specified values are not equal.  If the values are equal
 * an error is displayed and 0 is returned. */
static int values_not_equal_display_error(const LargestIntegralType left,
                                          const LargestIntegralType right) {
    const int not_equal = left != right;
    if (!not_equal) {
        printf(LargestIntegralTypePrintfFormat " == "
                       LargestIntegralTypePrintfFormat "\n", left, right);
    }
    return not_equal;
}

#ifdef USELESS_CODE
/*
 * Determine whether value is contained within check_integer_set.
 * If invert is 0 and the value is in the set 1 is returned, otherwise 0 is
 * returned and an error is displayed.  If invert is 1 and the value is not
 * in the set 1 is returned, otherwise 0 is returned and an error is
 * displayed.
 */
static int value_in_set_display_error(
        const LargestIntegralType value,
        const CheckIntegerSet * const check_integer_set, const int invert) {
    int succeeded = invert;
    {
        const LargestIntegralType * const set = check_integer_set->set;
        const size_t size_of_set = check_integer_set->size_of_set;
        size_t i;
        for (i = 0; i < size_of_set; i++) {
            if (set[i] == value) {
                /* If invert = 0 and item is found, succeeded = 1. */
                /* If invert = 1 and item is found, succeeded = 0. */
                succeeded = !succeeded;
                break;
            }
        }
        if (succeeded) {
            return 1;
        }
        printf("%" PRIu64 " is %sin the set (", value,
                       invert ? "" : "not ");
        for (i = 0; i < size_of_set; i++) {
            printf("%" PRIu64 ", ", set[i]);
        }
        printf(")\n");
    }
    return 0;
}


/*
 * Determine whether a value is within the specified range.  If the value is
 * within the specified range 1 is returned.  If the value isn't within the
 * specified range an error is displayed and 0 is returned.
 */
static int integer_in_range_display_error(
        const LargestIntegralType value, const LargestIntegralType range_min,
        const LargestIntegralType range_max) {
    if (value >= range_min && value <= range_max) {
        return 1;
    }
    printf("%" PRIu64 " is not within the range %" PRIu64 "-%" PRIu64 "\n",
                   value, range_min, range_max);
    return 0;
}


/*
 * Determine whether a value is within the specified range.  If the value
 * is not within the range 1 is returned.  If the value is within the
 * specified range an error is displayed and zero is returned.
 */
static int integer_not_in_range_display_error(
        const LargestIntegralType value, const LargestIntegralType range_min,
        const LargestIntegralType range_max) {
    if (value < range_min || value > range_max) {
        return 1;
    }
    printf("%" PRIu64 " is within the range %" PRIu64 "-%" PRIu64 "\n",
                   value, range_min, range_max);
    return 0;
}


/*
 * Determine whether the specified areas of memory are equal.  If they're equal
 * 1 is returned otherwise an error is displayed and 0 is returned.
 */
static int memory_equal_display_error(const char* const a, const char* const b,
                                      const size_t size) {
    int differences = 0;
    size_t i;
    for (i = 0; i < size; i++) {
        const char l = a[i];
        const char r = b[i];
        if (l != r) {
            printf("difference at offset %d 0x%02x 0x%02x\n",
                           i, l, r);
            differences ++;
        }
    }
    if (differences) {
        printf("%d bytes of %p and %p differ\n", differences,
                    a, b);
        return 0;
    }
    return 1;
}


/*
 * Determine whether the specified areas of memory are not equal.  If they're
 * not equal 1 is returned otherwise an error is displayed and 0 is
 * returned.
 */
static int memory_not_equal_display_error(
        const char* const a, const char* const b, const size_t size) {
    size_t same = 0;
    size_t i;
    for (i = 0; i < size; i++) {
        const char l = a[i];
        const char r = b[i];
        if (l == r) {
            same ++;
        }
    }
    if (same == size) {
        printf("%dbytes of %p and %p the same\n", same,
                    a, b);
        return 0;
    }
    return 1;
}
#endif

/* Replacement for assert. */
void mock_assert(const int result, const char* const expression,
                 const char* const file, const int line) {
    if (!result) {
        if (global_expecting_assert) {
            global_last_failed_assert = expression;
            longjmp(global_expect_assert_env, result);
        } else {
            printf("ASSERT: %s\n", expression);
            _fail(file, line);
        }
    }
}


void _assert_true(const LargestIntegralType result,
                  const char * const expression,
                  const char * const file, const int line) {
    if (!result) {
        printf("%s\n", expression);
        _fail(file, line);
    }
}


void _assert_int_equal(
        const LargestIntegralType a, const LargestIntegralType b,
        const char * const file, const int line) {
    if (!values_equal_display_error(a, b)) {
        _fail(file, line);
    }
}


void _assert_int_not_equal(
        const LargestIntegralType a, const LargestIntegralType b,
        const char * const file, const int line) {
    if (!values_not_equal_display_error(a, b)) {
        _fail(file, line);
    }
}
/* Added by VB */
void _check_expected(
        const char * const function_name, const char * const parameter_name,
        const char* file, const int line, const LargestIntegralType value) {
    void *result;
    const char* symbols[] = {function_name, parameter_name};
    const int rc = get_symbol_value(&global_function_parameter_map_head,
                                    symbols, (long long unsigned int *)&result);
    if (rc) {
        CheckParameterEvent * const check = (CheckParameterEvent*)result;
        int check_succeeded;
        global_last_parameter_location = check->location;
        
		check_succeeded = check->check_value(value, check->check_value_data);
        
		if (rc == 1) {
            //free(check);
        }
        if (!check_succeeded) {
            printf(SOURCE_LOCATION_FORMAT
                           ": error: Check of parameter %s, function %s failed\n"
                           SOURCE_LOCATION_FORMAT
                           ": note: Expected parameter declared here\n",
                           file, line,
                           parameter_name, function_name,
                           global_last_parameter_location.file,
                           global_last_parameter_location.line);
            _fail(file, line);
        }
    } else {
        printf(SOURCE_LOCATION_FORMAT ": error: Could not get value "
                    "to check parameter %s of function %s\n", file, line,
                    parameter_name, function_name);
        if (source_location_is_set(&global_last_parameter_location)) {
            printf(SOURCE_LOCATION_FORMAT
                        ": note: Previously declared parameter value was declared here\n",
                        global_last_parameter_location.file,
                        global_last_parameter_location.line);
        } else {
            printf("There were no previously declared parameter values "
                        "for this test.\n");
        }
        exit_test(1);
    }
}
/* End Added by VB */



#ifdef USELESS_CODE
void _assert_memory_equal(const void * const a, const void * const b,
                          const size_t size, const char* const file,
                          const int line) {
    if (!memory_equal_display_error((const char*)a, (const char*)b, size)) {
        _fail(file, line);
    }
}


void _assert_memory_not_equal(const void * const a, const void * const b,
                              const size_t size, const char* const file,
                              const int line) {
    if (!memory_not_equal_display_error((const char*)a, (const char*)b,
                                        size)) {
        _fail(file, line);
    }
}


void _assert_in_range(
        const LargestIntegralType value, const LargestIntegralType minimum,
        const LargestIntegralType maximum, const char* const file,
        const int line) {
    if (!integer_in_range_display_error(value, minimum, maximum)) {
        _fail(file, line);
    }
}

void _assert_not_in_range(
        const LargestIntegralType value, const LargestIntegralType minimum,
        const LargestIntegralType maximum, const char* const file,
        const int line) {
    if (!integer_not_in_range_display_error(value, minimum, maximum)) {
        _fail(file, line);
    }
}

void _assert_in_set(const LargestIntegralType value,
                    const LargestIntegralType values[],
                    const size_t number_of_values, const char* const file,
                    const int line) {
    CheckIntegerSet check_integer_set;
    check_integer_set.set = values;
    check_integer_set.size_of_set = number_of_values;
    if (!value_in_set_display_error(value, &check_integer_set, 0)) {
        _fail(file, line);
    }
}

void _assert_not_in_set(const LargestIntegralType value,
                        const LargestIntegralType values[],
                        const size_t number_of_values, const char* const file,
                        const int line) {
    CheckIntegerSet check_integer_set;
    check_integer_set.set = values;
    check_integer_set.size_of_set = number_of_values;
    if (!value_in_set_display_error(value, &check_integer_set, 1)) {
        _fail(file, line);
    }
}
#endif

void _fail(const char * const file, const int line) {
    printf(SOURCE_LOCATION_FORMAT ": error: Failure!\n", file, line);
    exit_test(0);
}

enum cm_printf_type {
    PRINTF_TEST_START,
    PRINTF_TEST_SUCCESS,
    PRINTF_TEST_FAILURE,
    PRINTF_TEST_ERROR,
};

static void cmprintf_group_start_standard(const size_t num_tests)
{
    printf("[==========] Running %u test(s).\n",
                  (unsigned)num_tests);
}

static void cmprintf_group_finish_standard(size_t total_executed,
                                           size_t total_passed,
                                           size_t total_failed,
                                           size_t total_errors,
                                           const struct CMUnitTest * const cm_tests)
{
//    size_t i;

    printf("[==========] %u test(s) run.\n", (unsigned)total_executed);
    printf("[  PASSED  ] %u test(s).\n",
                (unsigned)(total_passed));

    if (total_failed) {
        printf("[  FAILED  ] %d test(s)\n", total_failed);
        /*
        for (i = 0; i < total_executed; i++) {
            struct CMUnitTest *cmtest = &cm_tests[i];

            if (last_test_status == CM_TEST_FAILED) {
                printf("[  FAILED  ] %s\n", cmtest->name);
            }
        }
        printf("\n %u FAILED TEST(S)\n",
                    (unsigned)(total_failed + total_errors));*/
    }
}

static void cmprintf(enum cm_printf_type type,
                              const char *test_name,
                              const char *error_message)
{
    switch (type) {
    case PRINTF_TEST_START:
        printf("[ RUN      ] %s\n", test_name);
        break;
    case PRINTF_TEST_SUCCESS:
        printf("[       OK ] %s\n", test_name);
        break;
    case PRINTF_TEST_FAILURE:
        if (error_message != NULL) {
            printf("%s\n", error_message);
        }
        printf("[  FAILED  ] %s\n", test_name);
        break;
    case PRINTF_TEST_ERROR:
        if (error_message != NULL) {
            printf("%s\n", error_message);
        }
        printf("[  ERROR   ] %s\n", test_name);
        break;
    }
}

/****************************************************************************
 * CMOCKA TEST RUNNER
 ****************************************************************************/
static int cmocka_run_one_test_or_fixture(const char *function_name,
                                          CMUnitTestFunction test_func,
                                          CMFixtureFunction setup_func,
                                          CMFixtureFunction teardown_func,
                                          void ** volatile state,
                                          const void * heap_check_point)
{
    void *current_state = NULL;
    int rc = 0;

    /* FIXME check only one test or fixture is set */

    /* Init the test structure */
    initialize_testing(function_name);

    if (setjmp(global_run_test_env) == 0) {
        if (test_func != NULL) {
            (test_func) (state != NULL ? state : &current_state);
            _cleanup_call_checks();
            rc = 0;
        } else if (setup_func != NULL) {
            rc = (setup_func) (state != NULL ? state : &current_state);
        } else if (teardown_func != NULL) {
            rc = (teardown_func) (state != NULL ? state : &current_state);
        } else {
            /* ERROR */
        }
    } else {
        /* TEST FAILED */
        rc = -1;
    }
    teardown_testing(function_name);

    return rc;
}

static int cmocka_run_one_tests(const struct CMUnitTest *test)
{
    int rc = 0;

    /* Run setup */
    if (test->setup_func != NULL) {
        rc = cmocka_run_one_test_or_fixture(test->name,
                                            NULL,
                                            test->setup_func,
                                            NULL,
                                            NULL,
                                            NULL);
        if (rc != 0) {
            last_test_status = CM_TEST_ERROR;
            printf("Test setup failed");
        }
    }

    if (rc == 0) {
        rc = cmocka_run_one_test_or_fixture(test->name,
                                            test->test_func,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL);

        if (rc == 0) {
            last_test_status = CM_TEST_PASSED;
        } else {
            last_test_status = CM_TEST_FAILED;
        }

        rc = 0;
    }

    /* Run teardown */
    if ((rc == 0) && (test->teardown_func != NULL)) {
        rc = cmocka_run_one_test_or_fixture(test->name,
                                            NULL,
                                            NULL,
                                            test->teardown_func,
                                            NULL,
                                            NULL);
        if (rc != 0) {
            last_test_status = CM_TEST_ERROR;
            printf("Test teardown failed");
        }
    }

    return rc;
}


int _cmocka_run_tests(const char *group_name,
                            const struct CMUnitTest * const cm_tests,
                            const size_t num_tests)
{
    size_t total_failed = 0;
    size_t total_passed = 0;
    size_t total_executed = 0;
    size_t total_errors = 0;
    size_t total_skipped = 0;
    size_t i;
    int rc;

    /* Make sure LargestIntegralType is at least the size of a pointer. */
    assert_true(sizeof(LargestIntegralType) >= sizeof(void*));

    cmprintf_group_start_standard(num_tests);

        /* Execute tests */
        for (i = 0; i < num_tests; i++) {
            const struct CMUnitTest *cmtest = &cm_tests[i];
            size_t test_number = i + 1;

            last_test_status = CM_TEST_NOT_STARTED;

            cmprintf(PRINTF_TEST_START, cmtest->name, NULL);

            rc = cmocka_run_one_tests(cmtest);

            total_executed++;
            if (rc == 0) {
                switch (last_test_status) {
                    case CM_TEST_PASSED:
                        cmprintf(PRINTF_TEST_SUCCESS,
                                 cmtest->name,
                                 NULL);
                        total_passed++;
                        break;
                    case CM_TEST_FAILED:
                        cmprintf(PRINTF_TEST_FAILURE,
                                 cmtest->name,
                                 NULL);
                        total_failed++;
                        break;
                    default:
                        cmprintf(PRINTF_TEST_ERROR,
                                 cmtest->name,
                                 "Internal cmocka error");
                        total_errors++;
                        break;
                }
            } else {
                cmprintf(PRINTF_TEST_ERROR,
                         cmtest->name,
                         "Could not run the test - check test fixtures");
                total_errors++;
            }

            pic_free();
        }

    cmprintf_group_finish_standard(total_executed,
                                   total_passed,
                                   total_failed,
                                   total_errors,
                                   cm_tests);

#ifdef __XC16__
    __builtin_software_breakpoint();
#endif
    return total_failed + total_errors;
}



