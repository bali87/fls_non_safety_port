#ifndef CMOCKA_MAIN_H
#define CMOCKA_MAIN_H

#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>

#ifndef __XC16__
#include "cmocka.h"
#else
#include "xc_cmocka.h"
#endif

int cmocka_main(int argc, char ** argv,
                const struct CMUnitTest group_tests[],
                CMFixtureFunction group_setup, 
                CMFixtureFunction group_teardown,
                int numberOfTests);

#ifndef __XC16__
#define CMOCKA_MAIN(cm_tests, cm_setup, cm_teardown)                           \
    int main(int argc, char ** argv) {                                         \
        return cmocka_main(argc, argv, (cm_tests), (cm_setup), (cm_teardown),  \
                (sizeof((cm_tests)) / sizeof(struct CMUnitTest)));             \
    }
#else
#define CMOCKA_MAIN(cm_tests, cm_setup, cm_teardown)                           \
    int main(void) {                                                          \
        cmocka_run_group_tests((cm_tests), (cm_setup), (cm_teardown));         \
    }
#endif

void _void_mock(const char * function, const char* file, const int line);

#ifdef __XC16__

#define will_be_called(function, value) \
    _will_return(("#" #function), __FILE__, __LINE__, \
    cast_to_largest_integral_type(value))
#define void_mock(function) _void_mock("#" #function, __FILE__, __LINE__)

#define expect(function, parameter, value) \
    _will_return(#function #parameter, __FILE__, __LINE__, \
                 cast_to_largest_integral_type(value))
#define check(function, parameter) \
    assert_int_equal(cast_to_largest_integral_type((parameter)), \
    cast_to_largest_integral_type(mock(function ## parameter)))

#else

#define will_be_called(function, value)
#define void_mock(function)

#define expect(function, parameter, value) \
    _will_return((#function #parameter), __FILE__, __LINE__, \
    cast_to_largest_integral_type(value), 1)
#define check(function, parameter) \
    assert_int_equal(cast_to_largest_integral_type((parameter)), \
    cast_to_largest_integral_type(mock(function ## parameter)))

#endif

#define CMOCKA_CHECKPOINT(function) void_mock(function)

#endif

