#include "p24FJ256GA702.h"

/* GPIO */
volatile uint16_t  LATA __attribute__((__sfr__));
volatile uint16_t  LATB;
/* I2C */
uint16_t I2CBRG;
volatile uint16_t  I2C2CONH __attribute__((__sfr__));
volatile uint16_t I2C2CONL __attribute__((__sfr__));
volatile uint16_t I2C2STAT __attribute__((__sfr__));
volatile uint16_t I2C2BRG __attribute__((__sfr__));
volatile uint16_t I2C2TRN __attribute__((__sfr__));
volatile uint16_t I2C2RCV __attribute__((__sfr__));
/* SPI */

volatile uint16_t SPI2CON1H;
volatile uint16_t SPI2CON2L;
volatile uint16_t SPI2STATL;
volatile uint16_t SPI2BRGL;
volatile uint16_t SPI2IMSKL;
volatile uint16_t SPI2IMSKH;
volatile uint16_t SPI2URDTL;
volatile uint16_t SPI2URDTH;
volatile uint16_t SPI2CON1L;
volatile uint16_t SPI2BUFL;
volatile uint16_t SPI2BUFH;


volatile uint16_t  RPINR0;
volatile uint16_t  RPINR1;
volatile uint16_t  RPINR2;
volatile uint16_t  RPINR3;
volatile uint16_t  RPINR5;
volatile uint16_t  RPINR6;
volatile uint16_t  RPINR7;
volatile uint16_t  RPINR8;
volatile uint16_t  RPINR11;
volatile uint16_t  RPINR12;
volatile uint16_t  RPINR18;
volatile uint16_t  RPINR19;
volatile uint16_t  RPINR20;
volatile uint16_t  RPINR21;
volatile uint16_t  RPINR22;
volatile uint16_t  RPINR23;
volatile uint16_t  RPINR25;
volatile uint16_t  RPINR28;
volatile uint16_t  RPINR29;

volatile uint16_t  RPOR0;
volatile uint16_t  RPOR1;
volatile uint16_t  RPOR2;
volatile uint16_t  RPOR3;
volatile uint16_t  RPOR4;
volatile uint16_t  RPOR5;
volatile uint16_t  RPOR6;
volatile uint16_t  RPOR7;
volatile uint16_t  RPOR13;


volatile uint16_t  SPLIM;
/* IEC */
volatile uint16_t  IEC0;
volatile uint16_t  IEC1;
volatile IEC3BITS IEC3bits;
volatile I2C2CONLBITS I2C2CONLbits;
volatile I2C2STATBITS I2C2STATbits;
/* IFS */
volatile uint16_t  IFS0;
volatile uint16_t  IFS1;
volatile IFS3BITS IFS3bits;

/* IPC */
volatile uint16_t  IPC0;
volatile uint16_t  IPC2;
volatile uint16_t  IPC3;
volatile uint16_t  IPC5;
volatile uint16_t  IPC8;
volatile uint16_t  IPC12;
volatile uint16_t  IPC14;
/* Ext int */
volatile uint16_t  INTCON1;
volatile uint16_t  INTCON2;
volatile uint16_t  INTCON4;
/* GPIO */

volatile uint16_t    TRISA;
volatile uint16_t    TRISB;
volatile uint16_t   ANSA;
volatile uint16_t   ANSB;
volatile uint16_t    IOCPDA;
volatile uint16_t    IOCPDB;
volatile uint16_t   IOCPUA;
volatile uint16_t   IOCPUB;

volatile uint16_t   ODCA;
volatile uint16_t   ODCB;
/* NVRAM */
volatile uint16_t NVMADRU;
volatile uint16_t NVMADR;
volatile uint16_t NVMCON;
volatile uint16_t NVMKEY;
volatile uint16_t TBLPAG;
/* */

/* UART */

volatile uint16_t U1MODE;
volatile uint16_t U1STA;
volatile uint16_t U1BRG;
volatile uint16_t U1ADMD;
volatile uint16_t U1RXREG;
volatile uint16_t U1TXREG;
/* Timer 1 */
volatile uint16_t TMR1;
volatile uint16_t PR1;
volatile uint16_t  T1CON;
/* Timer 2 */
volatile uint16_t TMR2;
volatile uint16_t PR2;
volatile uint16_t  T2CON;
/* Timer 3 */
volatile uint16_t TMR3;
volatile uint16_t PR3;
volatile uint16_t  T3CON;
volatile uint16_t TMR3HLD;
/* Oscillator */
volatile uint16_t CLKDIV = 0x3120;
volatile uint16_t  OSCTUN;
volatile uint16_t  REFOCONL;
volatile uint16_t REFOCONH ;
volatile uint16_t REFOTRIML;
volatile uint16_t DCOTUN;
volatile uint16_t DCOCON;
volatile uint16_t OSCDIV;
volatile uint16_t OSCFDIV;
volatile uint16_t  OSCCON;
volatile uint16_t RCON ;

volatile uint16_t  IFS3;
volatile uint16_t  IFS4;
volatile uint16_t  IEC3;
