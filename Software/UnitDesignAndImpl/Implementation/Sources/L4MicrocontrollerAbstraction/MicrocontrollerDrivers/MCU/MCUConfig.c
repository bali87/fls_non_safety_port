/**
  @Generated MPLAB(c) Code Configurator Source File

  @Company:
    Microchip Technology Inc.

  @File Name:
    mcc.c

  @Summary:
    This is the mcc.c file generated using MPLAB(c) Code Configurator

  @Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - pic24-dspic-pic32mm : v1.25
        Device            :  PIC24FJ256GA702
        Driver Version    :  1.02
    The generated drivers are tested against the following:
        Compiler          :  XC16 1.26
        MPLAB             :  MPLAB X 3.45
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/
#include <MCUConfig.h>
#include "Std.h"
#include "MCU.h"
#include "GPIO.h"

// Configuration bits: selected in the GUI
#if ((COMPILE_FOR_TARGET) && (!defined(SUT)))
/*
#define CP //Code protection
// FSEC
#pragma config BWRP = OFF    // Boot Segment Write-Protect bit->Boot Segment may be written
#pragma config BSEN = ON    // Boot Segment Control bit->No Boot Segment
#pragma config GWRP = OFF    // General Segment Write-Protect bit->General Segment may be written
#pragma config CWRP = OFF    // Configuration Segment Write-Protect bit->Configuration Segment may be written
#pragma config AIVTDIS = ON     // Alternate Interrupt Vector Table bit->Enabled AIVT

#pragma config BSLIM = 0x1FF6

#ifdef CP
#pragma config BSS = STNDRD    // Boot Segment Code-Protect Level bits->No Protection (other than BWRP)
#pragma config GSS = STNDRD    // General Segment Code-Protect Level bits->No Protection (other than GWRP)
#pragma config CSS = STNDRD    // Configuration Segment Code-Protect Level bits->No Protection (other than CWRP)
#else
#pragma config BSS = DISABLED    // Boot Segment Code-Protect Level bits->No Protection (other than BWRP)
#pragma config GSS = DISABLED    // General Segment Code-Protect Level bits->No Protection (other than GWRP)
#pragma config CSS = DISABLED    // Configuration Segment Code-Protect Level bits->No Protection (other than CWRP)
#endif

// FOSCSEL
#pragma config FNOSC = FRCPLL    // Oscillator Source Selection->Fast RC Oscillator with divide-by-N with PLL module (FRCPLL)
#pragma config PLLMODE = PLL96DIV2    // PLL Mode Selection->96 MHz PLL. Oscillator input is divided by 2 (8 MHz input)
#pragma config IESO = ON    // Two-speed Oscillator Start-up Enable bit->Start up device with FRC, then switch to user-selected oscillator source

// FOSC
#pragma config POSCMD = NONE    // Primary Oscillator Mode Select bits->Primary Oscillator disabled
#pragma config OSCIOFCN = ON    // OSC2 Pin Function bit->OSC2 is general purpose digital I/O pin
#pragma config SOSCSEL = OFF    // SOSC Power Selection Configuration bits->Digital (SCLKI) mode
#pragma config PLLSS = PLL_FRC    // PLL Secondary Selection Configuration bit->PLL is fed by the on-chip Fast RC (FRC) oscillator
//#pragma config IOL1WAY = ON    // Peripheral pin select configuration bit->Allow only one reconfiguration
#pragma config IOL1WAY = OFF    // Peripheral pin select configuration bit->Allow multiple reconfigurations

#pragma config FCKSM = CSDCMD    // Clock Switching Mode bits->Both Clock switching and Fail-safe Clock Monitor are disabled
// FWDT
#pragma config WDTPS = PS32768    // Watchdog Timer Postscaler bits->1:32768
#pragma config FWPSA = PR128    // Watchdog Timer Prescaler bit->1:128
#pragma config FWDTEN = OFF    // Watchdog Timer Enable bits->WDT and SWDTEN disabled
#pragma config WINDIS = OFF    // Watchdog Timer Window Enable bit->Watchdog Timer in Non-Window mode
#pragma config WDTWIN = WIN25    // Watchdog Timer Window Select bits->WDT Window is 25% of WDT period
#pragma config WDTCMX = WDTCLK    // WDT MUX Source Select bits->WDT clock source is determined by the WDTCLK Configuration bits
#pragma config WDTCLK = LPRC    // WDT Clock Source Select bits->WDT uses LPRC

// FPOR
#pragma config BOREN = ON    // Brown Out Enable bit->Brown Out Enable Bit
#pragma config LPCFG = OFF    // Low power regulator control->No Retention Sleep
#pragma config DNVPEN = ENABLE    // Downside Voltage Protection Enable bit->Downside protection enabled using ZPBOR when BOR is inactive

// FICD
#pragma config ICS = PGD1    // ICD Communication Channel Select bits->Communicate on PGEC1 and PGED1
#pragma config JTAGEN = OFF    // JTAG Enable bit->JTAG is disabled

// FDEVOPT1
#pragma config ALTCMPI = DISABLE    // Alternate Comparator Input Enable bit->C1INC, C2INC, and C3INC are on their standard pin locations
#pragma config TMPRPIN = OFF    // Tamper Pin Enable bit->TMPRN pin function is disabled
#pragma config SOSCHP = ON    // SOSC High Power Enable bit (valid only when SOSCSEL = 1->Enable SOSC high power mode (default)
#pragma config ALTI2C1 = ALTI2CEN    // Alternate I2C pin Location->SDA1 and SCL1 on RB9 and RB8
*/
#endif


static void MCUConfig_InitIRQPriorities(void);
static void MCUConfig_InitOscillator(void);

void MCUConfig_Init(void)
{

#ifdef AIVT_ENABLED
    SET_MASK(INTCON2,AIVTEN,1u);
#else
    SET_MASK(INTCON2,AIVTEN,0u);
#endif

   // INTCON2bits.AIVTEN=0;
//    SET_MASK(INTCON2,AIVTEN,0u);



    MCUConfig_InitOscillator();
    MCUConfig_InitIRQPriorities();



}
/**
 *
 */
static void MCUConfig_InitOscillator(void)
{

	// CF no clock failure; NOSC FRCPLL; SOSCEN disabled; POSCEN disabled; CLKLOCK unlocked; OSWEN Switch is Complete; IOLOCK not-active;

    __builtin_write_OSCCONL((uint8_t) (0x0100 & 0x00FF));

    // CPDIV 1:1; PLLEN enabled; DOZE 1:8; RCDIV FRCPLL; DOZEN disabled; ROI disabled;
    CLKDIV = 0x3120u;
    // STOR disabled; STORPOL Interrupt when STOR is 1; STSIDL disabled; STLPOL Interrupt when STLOCK is 1; STLOCK disabled; STSRC SOSC; STEN disabled; TUN Center frequency;
    OSCTUN = 0x0000u;
    // ROEN enabled; ROSEL FOSC/2; ROSIDL disabled; ROSWEN disabled; ROOUT enabled; ROSLP disabled;
    REFOCONL = 0x9001u;
    // RODIV 0;
    REFOCONH = 0x0000u;
    // ROTRIM 0;
    REFOTRIML = 0x0000u;
    // DCOTUN 0;
    DCOTUN = 0x0000u;
    // DCOFSEL 8; DCOEN disabled;
    DCOCON = 0x0700u;
    // DIV 0;
    OSCDIV = 0x0000u;
    // TRIM 0;
    OSCFDIV = 0x0000u;
    // WDTO disabled; TRAPR disabled; SLEEP disabled; BOR disabled; DPSLP disabled; CM disabled; SWR disabled; SWDTEN disabled; EXTR disabled; POR disabled; SBOREN disabled; IDLE disabled; IOPUWR disabled; VREGS disabled;
    RCON = 0x0000u;
}

/**
    void INTERRUPT_Initialize (void)
*/
static void MCUConfig_InitIRQPriorities (void)
{
    //    INT1I: INT1 - External Interrupt 1
    //    Priority: 1
    //    IPC5bits.INT1IP = 3;
    SET_MASK(IPC5,INT1IP,3u);
    //    MICI: MI2C2 - I2C2 Master Events
    //    Priority: 1
    //    IPC12bits.MI2C2IP = 1;
    SET_MASK(IPC12,MI2C2IP,1u);
    //    SICI: SI2C2 - I2C2 Slave Events
    //    Priority: 1
    //    IPC12bits.SI2C2IP = 1;
    //    SET_MASK(IPC12,SI2C2IP,1u);
    //    TI: T1 - Timer1
    //    Priority: 1
    //    IPC0bits.T1IP = 2;
    SET_MASK(IPC0,T1IP,2u);
    //    UERI: U1E - UART1 Error
    //    Priority: 1
    //    IPC16bits.U1ERIP = 2;
    //    UTXI: U1TX - UART1 Transmitter
    //    Priority: 1
    //    IPC3bits.U1TXIP = 2;
    SET_MASK(IPC3,U1TXIP,2u);
    //    URXI: U1RX - UART1 Receiver
    //    Priority: 1
    //    IPC2bits.U1RXIP = 2;
    SET_MASK(IPC2,U1RXIP,2u);
    //    IPC8bits.SPI2IP = 3;
    SET_MASK(IPC8,SPI2IP,3u);
    //    IPC8bits.SPI2TXIP = 3;
    SET_MASK(IPC8,SPI2TXIP,3u);
    //    IPC14bits.SPI2RXIP = 3;
    SET_MASK(IPC14,SPI2RXIP,3u);

    SET_MASK(INTCON1,NSTDIS,1u);
    //    INTCON1bits.NSTDIS = 1u;
    //    INTCON1bits.NSTDIS = 0u;
}
/**
 End of File
*/
