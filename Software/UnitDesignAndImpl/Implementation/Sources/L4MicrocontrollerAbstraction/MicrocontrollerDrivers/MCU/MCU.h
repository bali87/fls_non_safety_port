/* 
 * File:   MCU.h
 * Author: Vincze Balazs
 *
 * Created on 2018. m�jus 21., 15:19
 */
/** @cond **/
#ifndef MCU_H
#define	MCU_H
#include "Std.h"


#include <p24FJ256GA702.h>

#if (COMPILE_FOR_TARGET)

#define MCU_ENABLE_INTERRUPTS()  __builtin_disi(0)//( INTCON2bits.GIE = 1u )
#define MCU_DISABLE_INTERRUPTS() __builtin_disi(0x3FFF)//( INTCON2bits.GIE = 0u )

#if ((defined(__DEBUG))  && (COMPILE_FOR_TARGET))
#define BREAKPNT()  __builtin_software_breakpoint()
#else
#define BREAKPNT() __builtin_nop()
#endif

#else

#define MCU_ENABLE_INTERRUPTS()
#define MCU_DISABLE_INTERRUPTS()
#define __builtin_write_NVM()
#define __builtin_tblrdl(_par)	((uint16)_par)
#define __builtin_tblrdh(_par)	((uint16)_par)
#define    __builtin_tblwtl(_par1,_par2)
#define __builtin_tblwth(_par1,_par2)
#define __builtin_write_OSCCONL(_par)	((void)_par)


#endif
/** MCU Register Mocks */
/* Implementation Phase*/
#if ((!defined (SUT))&&((!defined (SIT))))

#define GET_U1STA() 	(U1STA)
#define GET_SPI2BUFH()	(SPI2BUFH)
#define GET_SPI2BUFL()	(SPI2BUFL)
#define GET_SPI2CON1L()	(SPI2CON1L)
#define GET_SPI2STATL()	(SPI2STATL)
#define GET_LATA()		(LATA)
#define GET_LATB()		(LATB)


/* Unit or Integration Testing */
#else

uint16_t GET_U1STA(void);
uint16_t GET_SPI2BUFH	(void);
uint16_t GET_SPI2BUFL	(void);
uint16_t GET_SPI2CON1L	(void);
uint16_t GET_SPI2STATL	(void);
uint16_t GET_LATA		(void);
uint16_t GET_LATB		(void);

#endif
/** @endcond **/
#endif	/* MCU_H */

