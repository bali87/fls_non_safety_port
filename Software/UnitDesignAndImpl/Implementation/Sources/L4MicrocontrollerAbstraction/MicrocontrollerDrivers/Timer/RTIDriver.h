/**********************************************************************
* (C) Copyright 2017, OptoForce Ltd.
* -------------------------------------------------------------------
* \verbatim
*
* OPTOFORCE LIMITED PROPRIETARY INFORMATION
*
* Property of OptoForce ltd, Unauthorized reproduction and/or
* distribution is strictly prohibited. This product is protected
* under copyright law and trade secret law as an unpublished
* work
* (C) Copyright OptoForce. All rights reserved.
* \endverbatim
* -------------------------------------------------------------------
*
* -------------------------------------------------------------------
* FILE DESCRIPTION
* -------------------------------------------------------------------
* File: my_timer.h
* Component: -
* Module: -
* Generator: -
*
* Description: -
*
*---------------------------------------------------------------------
*---------------------------------------------------------------------
* Author: Vincze Balazs
*---------------------------------------------------------------------
* Revision History
*---------------------------------------------------------------------
* Version   Date     Author  Change       ID Description
*---------------------------------------------------------------------
* 00.01.00 2017. szept. 22. Vincze Balazs 0000000000000 Initial Version
*-------------------------------------------------------------------*/
#ifndef RTIDRIVER_H_
#define RTIDRIVER_H_
#include "Std.h"
#include <inttypes.h>

extern volatile uint32_t timeout;
extern volatile uint32_t us100_tick;
/*uint32_t get_ms_tick(void);*/
uint32_t get_100us_tick(void);
void RTIDriver_Init(void);
#endif /* RTIDRIVER_H_ */
