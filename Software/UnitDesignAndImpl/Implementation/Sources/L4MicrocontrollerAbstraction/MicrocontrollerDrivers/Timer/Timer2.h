#ifndef TIMER2_H
#define TIMER2_H

/**
 * @file
 * 
 * Header file of the unit ${FILENAME}. //fixme remove .h
 *
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */
 



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */
/** @fn Std_ReturnType Timer2_Init( void )
* @brief Timer2 init function, 1s turn-around time, 0.5us resolution, no interrupt is enabled.
* @param none
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType Timer2_Init( void );
/** @fn uint16 Timer2_GetValue( void )
* @brief Returns the actual value of the free-running Timer2 peripheral.
* @return Actual value of Timer2 in us.
*/
uint16 Timer2_GetValue( void );
/** @fn void Timer2_Reset( void )
* @brief Sets the actual value of the free-running Timer2 peripheral to 0.
* @return nothing.
*/
void Timer2_Reset(void );

/** @} */

#endif /* TIMER2_H */
