
/**
 * @file
 *
 * Unit that manages the computation of the system delay /Unit that provides library functions for computation of delay.
 * fixme update the comment.
 * 
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */

#include "Std.h"
#include "MCU.h"
#include "SysTimer.h"
#include "Timer1.h"




/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticDefines Static defines
 */


 
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



 /** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */



/** @} */
/* ================================================================================================================== */
 
/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */ 



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */ 



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */ 


Std_ReturnType Timer1_Init( void )
{
    //TMR1 0; 
    TMR1 = 0x0000u;
    //Period = 0.001 s; Frequency = 16000000 Hz; PR1 16000; 
    PR1 = 0x3E80u;
    //TCKPS 1:1; TON enabled; TSIDL disabled; TCS FOSC/2; TECS SOSC; TSYNC disabled; TGATE disabled; 
    //T1CON = 0x8000;
    /* Clear T1CON register */
    T1CON = 0x0000u;

    /* Clear interrupt flag */    
    //IFS0bits.T1IF = FALSE;
    CLR_BIT( IFS0, _IFS0_T1IF_POSITION );

    /*Enable the interrupt*/
    //IEC0bits.T1IE = TRUE;
    SET_BIT( IEC0, _IEC0_T1IE_POSITION );

    /* Start the Timer */
    //T1CONbits.TON = 1;
    SET_BIT( T1CON, _T1CON_TON_POSITION );
    
    return 0;
};


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */ 
/**
 *
 */
void __attribute__ ( ( interrupt, no_auto_psv ) ) InterruptVector(T1) (  )
{
    MCU_DISABLE_INTERRUPTS();

    SysTimer_MainCounter++;

    //IFS0bits.T1IF = FALSE;
    CLR_BIT( IFS0, _IFS0_T1IF_POSITION );
    
    MCU_ENABLE_INTERRUPTS();
}
