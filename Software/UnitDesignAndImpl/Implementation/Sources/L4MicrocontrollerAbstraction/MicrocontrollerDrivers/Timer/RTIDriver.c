/**********************************************************************
* (C) Copyright 2017, OptoForce Ltd.
* -------------------------------------------------------------------
* \verbatim
*
* OPTOFORCE LIMITED PROPRIETARY INFORMATION
*
* Property of OptoForce ltd, Unauthorized reproduction and/or
* distribution is strictly prohibited. This product is protected
* under copyright law and trade secret law as an unpublished
* work
* (C) Copyright OptoForce. All rights reserved.
* \endverbatim
* -------------------------------------------------------------------
*
* -------------------------------------------------------------------
* FILE DESCRIPTION
* -------------------------------------------------------------------
* File: my_timer.c
* Component: -
* Module: -
* Generator: -
*
* Description: -
*
*---------------------------------------------------------------------
*---------------------------------------------------------------------
* Author: Vincze Balazs
*---------------------------------------------------------------------
* Revision History
*---------------------------------------------------------------------
* Version   Date     Author  Change       ID Description
*---------------------------------------------------------------------
* 00.01.00 2017. szept. 22. Vincze Balazs 0000000000000 Initial Version
*-------------------------------------------------------------------*/

#include <RTIDriver.h>
#include "rti.h"
#include "SysTimer.h"
volatile uint32_t timeout=0u;
volatile uint32_t us100_tick=0u;
#if 0
static volatile uint32_t ms_tick=0u;

uint32_t get_ms_tick(void)
{
uint32_t temp = ms_tick;
return temp;
}
#endif
uint32_t get_100us_tick(void)
{
uint32_t temp = us100_tick;
return temp;
}
/**
 * @brief rtiNotification
 * @param notification
 */
void rtiNotification(uint32 notification)
{
static uint32 notSave;
    notSave = notification;
    if(rtiNOTIFICATION_COMPARE0 ==(rtiNOTIFICATION_COMPARE0 & notification))
    {
        SysTimer_MainCounter++;

    }
    if(rtiNOTIFICATION_COMPARE1 ==(rtiNOTIFICATION_COMPARE1 & notification))
    {
      //  SysTimer_MainCounter++;// us100_tick++;
    }

    notSave = 0;

}
void RTIDriver_Init(void)
{
    rtiInit();          /* Timer Init */
    rtiSetPeriod(rtiNOTIFICATION_COMPARE0, 10000u); // 100us
    rtiSetPeriod(rtiNOTIFICATION_COMPARE0, 10000u); // 1ms
   rtiEnableNotification(rtiNOTIFICATION_COMPARE0); // Add systimer ?
   rtiDisableNotification(rtiNOTIFICATION_COMPARE1);
    //rtiEnableNotification(rtiNOTIFICATION_COMPARE1);// Add systimer ?
    rtiStartCounter(rtiCOUNTER_BLOCK0);// Add systimer ?
}
