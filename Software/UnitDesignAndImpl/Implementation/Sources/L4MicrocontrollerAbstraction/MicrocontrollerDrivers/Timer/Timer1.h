#ifndef TIMER1_H
#define TIMER1_H

/**
 * @file
 * 
 * Header file of the unit ${FILENAME}. //fixme remove .h
 *
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */
 



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */
/** @fn Std_ReturnType Timer1_Init( void )
* @brief Timer1 init function, 1ms turn-around time, timer 1 inerrupt is enabled.
* @param none
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType Timer1_Init( void );

/** @} */

#endif /* TIMER1_H */
