/**
 * @file
 *
 * Unit that manages the computation of the system delay /Unit that provides library functions for computation of delay.
 * fixme update the comment.
 * 
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */

#include "Std.h"
#include "MCU.h"
#include "Timer2.h"




/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticDefines Static defines
 */


 
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



 /** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */



/** @} */
/* ================================================================================================================== */
 
/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */ 



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */ 



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */ 


Std_ReturnType Timer2_Init( void )
{
    //TMR3 0; 
    TMR3 = 0x0000u;
    //PR3 30; 
    PR3 = 0x001Eu;
    //TMR2 0; 
    TMR2 = 0x0000u;
    //Period = 1 s; Frequency = 16000000 Hz; PR2 33920; 
    PR2 = 0x8480u;
    //TCKPS 1:8; T32 32 Bit; TON enabled; TSIDL disabled; TCS FOSC/2; TECS SOSC; TGATE disabled; 
    //T2CON = 0x8018;
    /**Clear T2CON register */
    T2CON = 0x0000u;
    
    /* Configure 32-bit mode */
    SET_BIT( T2CON, _T2CON_T32_POSITION );
    
    /* Set input clock prescaler to 1:8 */
    SET_BIT( T2CON, _T1CON_TCKPS0_POSITION );
    CLR_BIT( T2CON, _T1CON_TCKPS1_POSITION );

    /* Start timer */
    //T2CONbits.TON = 1;
    SET_BIT( T2CON, _T2CON_TON_POSITION );
    return 0;
};


uint16 Timer2_GetValue( void )
{
    return (TMR2>>1);

}


void Timer2_Reset( void )
{
    TMR2 = 0x0000u;
    return;
}

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */ 



///**
//  TMR2 Generated Driver API Source File 
//
//  @Company
//    Microchip Technology Inc.
//
//  @File Name
//    tmr2.c
//
//  @Summary
//    This is the generated source file for the TMR2 driver using PIC24 / dsPIC33 / PIC32MM MCUs
//
//  @Description
//    This source file provides APIs for driver for TMR2. 
//    Generation Information : 
//        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - pic24-dspic-pic32mm : 1.55
//        Device            :  PIC24FJ256GA702
//    The generated drivers are tested against the following:
//        Compiler          :  XC16 v1.34
//        MPLAB 	          :  MPLAB X v4.15
//*/
//
///*
//    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
//    software and any derivatives exclusively with Microchip products.
//
//    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
//    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
//    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
//    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
//    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.
//
//    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
//    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
//    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
//    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
//    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
//    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
//    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
//
//    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
//    TERMS.
//*/
//
///**
//  Section: Included Files
//*/
//
//#include <xc.h>
//#include "tmr2.h"
//
///**
//  Section: Data Type Definitions
//*/
//
///** TMR Driver Hardware Instance Object
//
//  @Summary
//    Defines the object required for the maintainence of the hardware instance.
//
//  @Description
//    This defines the object required for the maintainence of the hardware
//    instance. This object exists once per hardware instance of the peripheral.
//
//  Remarks:
//    None.
//*/
//
//typedef struct _TMR_OBJ_STRUCT
//{
//    /* Timer Elapsed */
//    bool                                                    timerElapsed;
//    /*Software Counter value*/
//    uint8_t                                                 count;
//
//} TMR_OBJ;
//
//static TMR_OBJ tmr2_obj;
//
///**
//  Section: Driver Interface
//*/
//
//
//void TMR2_Initialize (void)
//{
//    //TMR3 0; 
//    TMR3 = 0x0000;
//    //PR3 30517; 
//    PR3 = 0x7735;
//    //TMR2 0; 
//    TMR2 = 0x0000;
//    //Period = 125 s; Frequency = 16000000 Hz; PR2 37888; 
//    PR2 = 0x9400;
//    //TCKPS 1:1; T32 32 Bit; TON enabled; TSIDL disabled; TCS FOSC/2; TECS SOSC; TGATE disabled; 
//    T2CON = 0x8008;
//
//    
//	
//    tmr2_obj.timerElapsed = false;
//
//}
//
//
//void TMR2_Tasks_32BitOperation( void )
//{
//    /* Check if the Timer Interrupt/Status is set */
//    if(IFS0bits.T3IF)
//    {
//        tmr2_obj.count++;
//        tmr2_obj.timerElapsed = true;
//        IFS0bits.T3IF = false;
//    }
//}
//
//
//
//
//void TMR2_Period32BitSet( uint32_t value )
//{
//    /* Update the counter values */
//    PR2 = (value & 0x0000FFFF);
//    PR3 = ((value & 0xFFFF0000)>>16);
//}
//
//uint32_t TMR2_Period32BitGet( void )
//{
//    uint32_t periodVal = 0xFFFFFFFF;
//
//    /* get the timer period value and return it */
//    periodVal = (((uint32_t)PR3 <<16) | PR2);
//
//    return( periodVal );
//
//}
//
//void TMR2_Counter32BitSet( uint32_t value )
//{
//    /* Update the counter values */
//   TMR3HLD = ((value & 0xFFFF0000)>>16);
//   TMR2 = (value & 0x0000FFFF);
//
//}
//
//uint32_t TMR2_Counter32BitGet( void )
//{
//    uint32_t countVal = 0xFFFFFFFF;
//    uint16_t countValUpper;
//    uint16_t countValLower;
//
//    countValLower = TMR2;
//    countValUpper = TMR3HLD;
//
//    /* get the current counter value and return it */
//    countVal = (((uint32_t)countValUpper<<16)| countValLower );
//
//    return( countVal );
//
//}
//
//
//
//void TMR2_Start( void )
//{
//    /* Reset the status information */
//    tmr2_obj.timerElapsed = false;
//
//
//    /* Start the Timer */
//    T2CONbits.TON = 1;
//}
//
//void TMR2_Stop( void )
//{
//    /* Stop the Timer */
//    T2CONbits.TON = false;
//
//}
//
//bool TMR2_GetElapsedThenClear(void)
//{
//    bool status;
//    
//    status = tmr2_obj.timerElapsed;
//
//    if(status == true)
//    {
//        tmr2_obj.timerElapsed = false;
//    }
//    return status;
//}
//
//int TMR2_SoftwareCounterGet(void)
//{
//    return tmr2_obj.count;
//}
//
//void TMR2_SoftwareCounterClear(void)
//{
//    tmr2_obj.count = 0; 
//}
//
///**
// End of File
//*/
