#include "../NvMem/NvMem.h"

#include "MCU.h"

//-----------------------------------------------------------

#define NVMemIsError()    (NVMCON & (_NVMCON_WRERR_MASK))

//-----------------------------------------------------------

int NV_Mem_Erase_Page(uint32_t address)
{
    //Set up pointer to the first memory location to be written
    NVMADRU = address >> 16;   // Initialize PM Page Boundary SFR
    NVMADR = address & 0xFFFF; // Initialize lower word of address
    NVMCON = NVMOP_PAGE_ERASE; // Initialize NVMCON
    NVMKEY=0x55;
    NVMKEY=0xAA;
    asm("DISI #5"); // Block all interrupts with priority <7
    // for next 5 instructions
    __builtin_write_NVM(); // check function to perform
    
    while(NVMCON & _NVMCON_WR_MASK);
    
    return NVMemIsError();
}

//uint16_t NV_Mem_Write_DWord(uint32_t address, uint32_t data)
//{
//    uint32_t progAddr = address; // Address of word to program
//    uint32_t progData1L = data1 >> 0; // Data to program lower word of word 1
//    uint8_t progData1H = data1 >> 16; // Data to program upper byte of word 1
//    uint32_t progData2L = data2 >> 0; // Data to program lower word of word 2
//    uint8_t progData2H = data2 >> 16; // Data to program upper byte of word 2
//    //Set up NVMCON for word programming
//    NVMCON = NVMOP_WORD_DW_PGM; // Initialize NVMCON
//    TBLPAG = 0xFA; // Point TBLPAG to the write latches
//    //Set up pointer to the first memory location to be written
//    NVMADRU = progAddr>>16; // Initialize PM Page Boundary SFR
//    NVMADR = progAddr & 0xFFFF; // Initialize lower word of address
//    //Perform TBLWT instructions to write latches
//    __builtin_tblwtl(0, progData1L); // Write word 1 to address low word
//    __builtin_tblwth(0, progData1H); // Write word 1 to upper byte
//    __builtin_tblwtl(2, progData2L); // Write word 2 to address low word
//    __builtin_tblwth(2, progData2H); // Write word 2 to upper byte
//    asm("DISI #5"); // Block interrupts with priority <7 for next 5
//    // instructions
//    __builtin_write_NVM(); // XC16 function to perform unlock sequence and set WR
//    
//    while(NVMCON & _NVMCON_WR_MASK);
//    
//    return NVMemIsError();
//}
//
//uint32_t NV_Mem_Read_DWord(uint32_t address)
//{ 
//    uint32_t addrOffset, readDataL, readDataH, readData;
//    
//	// Create 24 bit EA for read by loading TBLPAG
//    TBLPAG = (address >> 16) & 0xFF; // Load upper address into TBLPAG register
//    addrOffset = (address >> 0) & 0xFFFF;
//    
//	// Read data from program memory
//    readDataL = __builtin_tblrdl(addrOffset); // readDataL contains 16 bit variable data
//    readDataH = __builtin_tblrdh(addrOffset); // readDataH contains 16 bit variable data
//    readData = ((readDataL << 0)  | (readDataH << 16)) & 0x00FFFFFF;
//    return readData;
//}

int NV_Mem_Read_DWord_To_Buffer(uint32_t address,uint8_t* data,int length)
{ 
    uint32_t wordAddress,byteOffset, addrOffset, readDataL, readDataH;
	
    wordAddress=((address>>1)<<1);	//convert byte to word
	byteOffset=address-wordAddress;
	
	
	// Create 24 bit EA for read by loading TBLPAG
    TBLPAG = (wordAddress >> 16) & 0xFF; // Load upper address into TBLPAG register
    addrOffset = (wordAddress & 0xFFFF);
    
	// Read data from program memory
    readDataL = __builtin_tblrdl(addrOffset); // readDataL contains 16 bit variable data
    readDataH = __builtin_tblrdh(addrOffset); // readDataH contains 16 bit variable data
	uint8_t tmp[3]={0};
	uint8_t len=3-byteOffset;
	if (length<len)
		len=length;
	
	tmp[0]=readDataL & 0xFF;
	tmp[1]=readDataL>>8;
	tmp[2]=readDataH & 0xFF;
	
	int i=0;
	for (i=0;i<len;i++)
		data[i]=tmp[i+byteOffset];
//	memcpy(data,(void*)tmp+byteOffset,len);
    return (len);
}

int NV_Mem_Write_Row(uint32_t address, uint8_t* data)
{
    uint16_t i;
    
	//Set up NVMCON for word programming
    NVMCON = NVMOP_ROW_PGM; // Initialize NVMCON
    TBLPAG = 0xFA; // Point TBLPAG to the write latches
    
	//Set up pointer to the first memory location to be written
    NVMADRU = address>>16; // Initialize PM Page Boundary SFR
    NVMADR = address & 0xFFFF; // Initialize lower word of address
    
	//Perform TBLWT instructions to write latches
    for (i = 0; i < ROW_SIZE; i++) {
        __builtin_tblwtl(i*2, (data[i*DATA_SIZE+1] << 8) | (data[i*DATA_SIZE])); // Write word 1 to address low word
        __builtin_tblwth(i*2, (data[i*DATA_SIZE+2])); // Write word 1 to upper byte
    }
    asm("DISI #5"); // Block interrupts with priority <7 for next 5 instructions
    __builtin_write_NVM(); // XC16 function to perform unlock sequence and set WR
    while(NVMCON & _NVMCON_WR_MASK);
    return NVMemIsError();
}

int NV_Mem_Read_Row(uint32_t address, uint8_t* data)
{
    uint16_t offset, readDataL, readDataH, i;
    uint8_t page,lastpage=-1;
    page=(address >> 16) & 0xFF;
   
    for (i = 0; i < ROW_SIZE; i++){
        page = (address >> 16) & 0xFF;
        offset = address & 0xFFFF;
        if (lastpage!=page) {
            TBLPAG = page;
            lastpage=page;
        }
        readDataL = __builtin_tblrdl(offset); // readDataL contains 16 bit variable data
        readDataH = __builtin_tblrdh(offset);
        data[i*DATA_SIZE+0]=readDataL;
        data[i*DATA_SIZE+1]=readDataL>>8;
        data[i*DATA_SIZE+2]=readDataH;
        address+=(ADDRESS_SIZE/2);
    }
    return 0;
}

int NV_Mem_Clear(uint32_t start, uint32_t end)
{
    uint32_t i;
    uint16_t result=0;
    for( i = start; i < end; i += BLOCK_SIZE_ADDR ) 
		result = NV_Mem_Erase_Page( i );
    return result;
}
