// Copyright (c) 2002-2010,  Microchip Technology Inc.
//
// Microchip licenses this software to you solely for use with Microchip
// products.  The software is owned by Microchip and its licensors, and
// is protected under applicable copyright laws.  All rights reserved.
//
// SOFTWARE IS PROVIDED "AS IS."  MICROCHIP EXPRESSLY DISCLAIMS ANY
// WARRANTY OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING BUT
// NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  IN NO EVENT SHALL
// MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
// CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, HARM TO YOUR
// EQUIPMENT, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY
// OR SERVICES, ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED
// TO ANY DEFENSE THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION,
// OR OTHER SIMILAR COSTS.
//
// To the fullest extent allowed by law, Microchip and its licensors
// liability shall not exceed the amount of fees, if any, that you
// have paid directly to Microchip to use this software.
//
// MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
// OF THESE TERMS.
#ifndef __NVMEM_H__
#define __NVMEM_H__

#include "MCU.h"

#define SYS_FREQ  (80000000L)

#if defined(__PIC24FJ256GA702__)
    #define NVMOP_NOP               0x4000      // NOP operation
    #define NVMOP_WORD_DW_PGM       0x4001      // Word program operation
    #define NVMOP_ROW_PGM           0x4002      // Row program operation
    #define NVMOP_PAGE_ERASE        0x4003      // Page erase operation

    #define ROW_SIZE                128            // # of 32-bit Instructions per Row
    #define NUM_ROWS_PER_BLOCK      8              //Number of Rows per Page

    #define DATA_SIZE               3
    #define ADDRESS_SIZE            4
    #define ROW_SIZE_ADDR           (ADDRESS_SIZE * ROW_SIZE)
    #define ROW_SIZE_DATA           (DATA_SIZE * ROW_SIZE)// Page size in Bytes
    #define BLOCK_SIZE_ADDR          (ADDRESS_SIZE * ROW_SIZE * NUM_ROWS_PER_BLOCK)
    #define BLOCK_SIZE_DATA          (DATA_SIZE * ROW_SIZE * NUM_ROWS_PER_BLOCK)

#else   //PIC32
    #include <stdint.h>
    #define NVMOP_NOP               0x4000      // NOP operation
    #define NVMOP_WORD_PGM          0x4001      // Word program operation
    #define NVMOP_ROW_PGM           0x4003      // Row program operation
    #define NVMOP_PAGE_ERASE        0x4004      // Page erase operation

    #define PAGE_SIZE               1024        // # of 32-bit Instructions per Page
    #define BYTE_PAGE_SIZE          (4 * PAGE_SIZE) // Page size in Bytes
    #define ROW_SIZE                128         // # of 32-bit Instructions per Row
    #define BYTE_ROW_SIZE           (4 * ROW_SIZE) // # Row size in Bytes
    #define NUM_ROWS_PER_BLOCK           8              //Number of Rows per Page 

#endif
/**
 *
 * @param address
 * @param data
 * @return
 */
int NV_Mem_Write_Word(uint32_t address, uint32_t data);
/**
 *
 * @param address
 * @return
 */
int NV_Mem_Erase_Page(uint32_t address);
/**
 *
 * @param address
 * @param data
 * @return
 */
int NV_Mem_Read_Row(uint32_t address, uint8_t* data);
/**
 *
 * @param address
 * @param data
 * @return
 */
int NV_Mem_Write_Row(uint32_t address, uint8_t* data);
/**
 *
 * @param address
 * @return
 */
uint32_t NV_Mem_Read_DWord(uint32_t address);
/**
 *
 * @param address
 * @param data
 * @param length
 * @return
 */
int NV_Mem_Read_DWord_To_Buffer(uint32_t address,uint8_t* data,int length);
/**
 *
 * @return
 */
int NV_Mem_Clear_Error(void);
/**
 *
 * @param start
 * @param end
 * @return
 */
int NV_Mem_Clear(uint32_t start, uint32_t end);



#endif
