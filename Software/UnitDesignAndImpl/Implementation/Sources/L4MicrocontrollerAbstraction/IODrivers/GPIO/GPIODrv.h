#ifndef DRIVERS_GPIO_GPIODRV_H_
#define DRIVERS_GPIO_GPIODRV_H_

/**
* @file GPIO.h
* @author Vincze Balazs
* @date 2017. nov. 29.
* @brief Implementation file of module XXXX
* @version V 1.0
*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */
#include <inttypes.h>
#include "het.h"
#include "gio.h"

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */

/**
 * Short description.
 *
 * Detailed description for Doxygen, possibly several lines.
 * And several sentences.
 */

typedef enum
{
    GPIO_IN  =  0u,
    GPIO_OUT = 1u

}GPIODrv_DirectionType;



/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */

/**
 * Short description.
 *
 * Detailed description for Doxygen, possibly several lines.
 * And several sentences.
 */
#if 0
typedef struct
{

    /**
     * Short description Doxygen.
     *
     * Detailed description for Doxygen, possibly several lines.
     * And several sentences.
     */
    uint8 bar;

} Template_FooT;
#endif
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */

/**
 * Description.
 */ 
/* const uint8 Template_gVal = 0U; */

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */

int16_t GPIO_SetPinODR(gioPORT_t *portPtr, uint32 bit, uint32 enable);
int16_t GPIO_SetPinPUPD(gioPORT_t *portPtr, uint32 bit, uint32 disable, uint32_t puNPD);
int16_t GPIO_SetPinValue(gioPORT_t *portPtr, uint32 bit, uint32 value);
int16_t GPIO_SetPinDirection(gioPORT_t *portPtr, uint32 bit, GPIODrv_DirectionType direction);
void GPIO_Init(void);
int16_t GPIO_SetPinValue(gioPORT_t *portPtr, uint32 bit, uint32 value);
uint32_t GPIO_GetPinValue(gioPORT_t *portPtr, uint32 bit);

/*
extern void Template_HandleSchedule (void);

extern uint8 Template_GlobalFoo (int8 templateFoo);

*/
/**@}*/
#endif /* DRIVERS_GPIO_GPIODRV_H_ */
