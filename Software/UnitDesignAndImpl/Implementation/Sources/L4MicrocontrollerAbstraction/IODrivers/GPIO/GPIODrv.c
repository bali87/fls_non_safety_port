/**
* @file GPIO.c
* @author Vincze Balazs
* @date 2017. nov. 29.
* @brief Implementation file of module XXXX
* @version V 1.0
*/
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */
//#include "Std.h"
#include <GPIODrv.h>
#include "gio.h"
#include "het.h"
#include <inttypes.h>
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 */

/**
 * Description.
 */ 
/*#define TEMPLATE_DEF (20U) */

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */

/**
 * Short description for EA and Doxygen.
 *
 * Detailed description for Doxygen, possibly several lines.
 * And several sentences.
 */
#if 0
typedef struct _Template_BarT;
{

    /**
     * Short description Doxygen.
     *
     * Detailed description for Doxygen, possibly several lines.
     * And several sentences.
     */
    uint8 bar;

} Template_BarT;
#endif
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */

/**
 * Description.
 */ 
/* const uint8 Template_val = 0U; */

/**@}*/
/* ================================================================================================================== */
 
/**
 * @addtogroup StaticVariablesInitialized Static variables initialized
 *  @{
 */ 


/**
 * Short description for EA and Doxygen.
 *
 * Detailed description for Doxygen, possibly several lines.
 * And several sentences.
 */
/*static int8 Template_StaticData;*/

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariablesNonInitialized Static variables non-initialized
 * @{
 */ 

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */ 

/* static uint8 GPIO_StaticFoo (int8 templateFoo); */

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */ 
/**
 * Short description of the global variable for EA and Doxygen.
 *
 * Detailed description for Doxygen, possibly several lines.
 * And several sentences.
 *
 *  @startuml
 *    Sender->Receiver  : Command()
 *    Sender<--Receiver : Ack()
 *  @enduml 
 *
 * @param[in] templateFoo Description for Doxygen.
 * @return Description for Doxygen.
 *
 */
void GPIO_Init(void)
{
    //gioInit();      /* TODO: Implement Own driver */
    /* Initialise perhperals only whose don't have driver liek NXHET */
    /* Bring HET2 out of reset */
    hetREG2->GCR = 0U;
    hetREG2->GCR = 1U;
    /* Bring GIO out of reset */
    gioREG->GCR0 = 0U;
    gioREG->GCR0 = 1U;





}

int16_t GPIO_SetPinODR(gioPORT_t *portPtr, uint32 bit, uint32 enable)
{
int16_t retVal = 0;
/** - Port A open drain enable */
    if(0x00U == enable)
    {
        portPtr->PDR  &= (uint32)~((uint32)0U << bit); /* Clear Bit */
    }
    else
    {
        portPtr->PDR  |= (uint32)((uint32)1U << bit); /* Set bit */
    }
return (retVal);
}
int16_t GPIO_SetPinPUPD(gioPORT_t *portPtr, uint32 bit, uint32 disable, uint32_t puNPD)
{
int16_t retVal = 0;
/** - Port A open drain enable */
    if(0x00U == disable)
    {
        portPtr->PULDIS  |= (uint32)((uint32)1U << bit); /* Set bit */
    }
    else
    {
        portPtr->PULDIS  &= (uint32)~((uint32)0U << bit); /* Clear Bit */

    }
    if(0x00U == puNPD)
    {
         portPtr->PSL  &= (uint32)~((uint32)0U << bit); /* Clear Bit */
    }
    else
    {
         portPtr->PSL  |= (uint32)((uint32)1U << bit); /* Set bit */
    }
return (retVal);
}

int16_t GPIO_SetPinValue(gioPORT_t *portPtr, uint32 bit, uint32 value)
{
int16_t retVal = 0;
    if (value != 0U)
    {
        portPtr->DSET = (uint32)1U << bit;
    }
    else
    {
        portPtr->DCLR = (uint32)1U << bit;
    }

    return (retVal);
}
int16_t GPIO_SetPinDirection(gioPORT_t *portPtr, uint32 bit, GPIODrv_DirectionType direction)
{
int16_t retVal = 0;
    if(0x00U == direction)
    {
        portPtr->DIR  &= (uint32)~((uint32)0U << bit); /* Clear Bit */
    }
    else
    {
        portPtr->DIR  |= (uint32)((uint32)1U << bit); /* Set bit */
    }
return (retVal);
}
uint32_t GPIO_GetPinValue(gioPORT_t *portPtr, uint32 bit)
{

return ((portPtr->DIN >> bit) & 1U);

}
/**@}*/

