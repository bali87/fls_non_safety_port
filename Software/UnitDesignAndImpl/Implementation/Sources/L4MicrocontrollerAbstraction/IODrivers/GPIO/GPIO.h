#ifndef Optoforce_SHexHost_GPIO_H
#define Optoforce_SHexHost_GPIO_H

/**
 * @file
 *
 * Unit that manages:
 * - .....
 * - .........
 *
 * @version $Id$
 */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */
#include "Std.h"
#if 0
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */
#define USE_EXT_INT1

#if COMPILE_FOR_TARGET && !defined (SUT)
#define GPIO_ExtInt1Handler	__attribute__ ( ( interrupt,auto_psv ) ) InterruptVector(INT1)
#endif


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */
/** @enum GPIO_PortType
*	@brief GPIO's port ID's
**/
typedef enum
{
    /** PORTA **/
	GPIO_PORTA,
	/** PORTB **/
	GPIO_PORTB,
	/** MAX_PORT for checking **/
	GPIO_PORT_MAX
}GPIO_PortType;
typedef enum
{
    GPIO_DIRECTION_IN   = 1u,
    GPIO_DIRECTION_OUT  = 0u
}GPIO_DirectionType;
/** @enum GPIO_ExtIntIDType
*	@brief GPIO's External Interrupt ID's
**/
typedef enum
{
#ifdef USE_EXT_INT0
	/** EXTINT0 **/
	GPIO_EXT_INT0,
#endif
#ifdef USE_EXT_INT1
	/** EXTINT1 **/
	GPIO_EXT_INT1,
#endif
#ifdef USE_EXT_INT2
	/** EXTINT2 **/
	GPIO_EXT_INT2,
#endif
#ifdef USE_EXT_INT3
	/** EXTINT3 **/
	GPIO_EXT_INT3,
#endif
#ifdef USE_EXT_INT4
	/** EXTINT4 **/
	GPIO_EXT_INT4,
#endif
	GPIO_EXT_INT_MAX
}GPIO_ExtIntIDType;
typedef void (*GPIO_ExtIntFuncPtrType)(void);
/** @enum GPIO_RPType
*	@brief GPIO's pin's ID for remapping
**/
typedef enum
{
    /** RP0 -> RB0 **/
	GPIO_RP0 = 0u,
	/** RP1 -> RB1 **/
	GPIO_RP1 = 1u,
	/** RP2 -> RB2 **/
	GPIO_RP2 = 2u,
	/** RP3 -> RB3 **/
	GPIO_RP3 = 3u,
	/** RP4 -> RB4 **/
	GPIO_RP4 = 4u,
	/** RP5 -> RB5 **/
	GPIO_RP5 = 5u,
	/** RP6 -> RB6 **/
	GPIO_RP6 = 6u,
	/** RP7 -> RB7 **/
	GPIO_RP7 = 7u,
	/** RP8 -> RB8 **/
	GPIO_RP8 = 8u,
	/** RP9 -> RB9 **/
	GPIO_RP9 = 9u,
	/** RP10 -> RB10 **/
	GPIO_RP10 = 0x0Au,
	/** RP11 -> RB11 **/
	GPIO_RP11 = 0x0Bu,
	/** RP12 -> RB12 **/
	GPIO_RP12 = 0x0Cu,
	/** RP13 -> RB13 **/
	GPIO_RP13 = 0x0Du,
	/** RP14 -> RB14 **/
	GPIO_RP14 = 0x0Eu,
	/** RP15 -> RB15 **/
	GPIO_RP15 = 0x0Fu,
	/** RP26 -> RA0 **/
	GPIO_RP26 = 0x1Au,
	/** RP27 -> RA1 **/
	GPIO_RP27 = 0x1Bu
}GPIO_RPType;
/** @enum GPIO_RPOutFunType
*	@brief GPIO's pin alternate output function
**/
typedef enum
{
	/** None (Pin Disabled) **/
    RP_OUT_FUN_NONE			= 0x00u,
	/** Comparator 1 Output **/
	RP_OUT_FUN_C1OUT		= 0x01u,
	/** Comparator 2 Output **/
	RP_OUT_FUN_C2OUT		= 0x02u,
	/** UART 1 Transmit **/
	RP_OUT_FUN_U1TX 		= 0x03u,
	/** UART1 Request-to-Send */
	RP_OUT_FUN_U1RTS		= 0x04u,
	/** UART 2 Transmit **/
	RP_OUT_FUN_U2TX			= 0x05u,
	/** UART2 Request-to-Send */
	RP_OUT_FUN_U2RTS		= 0x06u,
	/** SPI1 Data Output */
	RP_OUT_FUN_SDO1 		= 0x07u,
	/** SPI1 Clock Output **/
	RP_OUT_FUN_SCK1OUT 		= 0x08u,
	/** SPI1 Slave Select Output **/
	RP_OUT_FUN_SS1OUT 		= 0x09u,
	/** SPI2 Data Output */
	RP_OUT_FUN_SDO2 		= 0x0Au,
	/** SPI2 Clock Output **/
	RP_OUT_FUN_SCK2OUT 		= 0x0Bu,
	/** SPI2 Slave Select Output **/
	RP_OUT_FUN_SS2OUT 		= 0x0Cu,
	/** Output Compare 1 **/
	RP_OUT_FUN_OC1 			= 0x0Du,
	/** Output Compare 2 **/
	RP_OUT_FUN_OC2 			= 0x0Eu,
	/** Output Compare 3 **/
	RP_OUT_FUN_OC3 			= 0x0Fu,
	/** CCP2A Output Compare **/
	RP_OUT_FUN_OCM2A 		= 0x10u,
	/** CCP2B Output Compare **/
	RP_OUT_FUN_OCM2B 		= 0x11u,
	/** CCP3A Output Compare **/
	RP_OUT_FUN_OCM3A 		= 0x12u,
	/** CCP3B Output Compare **/
	RP_OUT_FUN_OCM3B 		= 0x13u,
	/** CCP4A Output Compare **/
	RP_OUT_FUN_OCM4A 		= 0x14u,
	/** CCP4B Output Compare **/
	RP_OUT_FUN_OCM4B 		= 0x15u,
	/** Reserved **/
	RP_OUT_FUN_Reserved 	= 0x16u,
	/** SPI3 Data Output **/
	RP_OUT_FUN_SDO3 		= 0x17u,
	/** SPI3 Clock Output **/
	RP_OUT_FUN_SCK3OUT 		= 0x18u,
	/** SPI3 Slave Select Output **/
	RP_OUT_FUN_SS3OUT 		= 0x19u,
	/** Comparator 3 Output **/
	RP_OUT_FUN_C3OUT 		= 0x1Au,
	/** RTCC Power Control **/
	RP_OUT_FUN_PWRGT 		= 0x1Bu,
	/** Reference Clock Output **/
	RP_OUT_FUN_REFO 		= 0x1Cu,
	/** CLC1 Output **/
	RP_OUT_FUN_CLC1OUT 		= 0x1Du,
	/** CLC2 Output **/
	RP_OUT_FUN_CLC2OUT 		= 0x1Eu,
	/** RTCC Clock Output **/
	RP_OUT_FUN_RTCC 		= 0x1Fu
}GPIO_RPOutFunType;
/** @enum GPIO_RPInFunType
*	@brief GPIO's pin alternate input function
**/
typedef enum
{
	/**	Output Compare Trigger 1 **/
	RP_IN_FUN_OCTRIG1,
	/**	External Interrupt 1 **/
	RP_IN_FUN_INT1,
	/**	External Interrupt 2 **/
	RP_IN_FUN_INT2,
	/**	External Interrupt 3 **/
	RP_IN_FUN_INT3,
	/**	External Interrupt 4 **/
	RP_IN_FUN_INT4,
	/**	Output Compare Trigger 2 **/
	RP_IN_FUN_OCTRIG2,
	/**	Timer2 External Clock **/
	RP_IN_FUN_T2CK,
	/**	Timer3 External Clock **/
	RP_IN_FUN_T3CK,
	/**	Input Capture 1 **/
	RP_IN_FUN_ICM1,
	/**	Input Capture 2 **/
	RP_IN_FUN_ICM2,
	/**	Input Capture 3 **/
	RP_IN_FUN_ICM3,
	/**	Input Capture 4 **/
	RP_IN_FUN_ICM4,
	/**	Input Capture 1 **/
	RP_IN_FUN_IC1,
	/**	Input Capture 2 **/
	RP_IN_FUN_IC2,
	/**	Input Capture 3 **/
	RP_IN_FUN_IC3,
	/**	Output Compare Fault A **/
	RP_IN_FUN_OCFA,
	/**	Output Compare Fault B **/
	RP_IN_FUN_OCFB,
	/**	CCP Clock Input A **/
	RP_IN_FUN_TCKIA,
	/**	CCP Clock Input B **/
	RP_IN_FUN_TCKIB,
	/**	UART1 Receive **/
	RP_IN_FUN_U1RX,
	/**	UART1 Clear-to-Send **/
	RP_IN_FUN_U1CTS,
	/**	UART2 Receive **/
	RP_IN_FUN_U2RX,
	/**	UART2 Clear-to-Send **/
	RP_IN_FUN_U2CTS,
	/**	SPI1 Data Input **/
	RP_IN_FUN_SDI1,
	/**	SPI1 Clock Input **/
	RP_IN_FUN_SCK1IN,
	/**	SPI1 Slave Select Input **/
	RP_IN_FUN_SS1IN,
	/**	SPI2 Data Input **/
	RP_IN_FUN_SDI2,
	/**	SPI2 Clock Input **/
	RP_IN_FUN_SCK2IN,
	/**	SPI2 Slave Select Input **/
	RP_IN_FUN_SS2IN,
	/**	Generic Timer External Clock **/
	RP_IN_FUN_TxCK,
	/**	CLC Input A **/
	RP_IN_FUN_CLCINA,
	/**	CLC Input B **/
	RP_IN_FUN_CLCINB,
	/**	SPI3 Data Input **/
	RP_IN_FUN_SDI3,
	/**	SPI3 Clock Input **/
	RP_IN_FUN_SCK3IN,
	/**	SPI3 Slave Select Input **/
	RP_IN_FUN_SS3IN
}GPIO_RPInFunType;
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */
/** @fn void GPIO_Init(void)
*/
void GPIO_Init(void);

/**@fn Std_ReturnType GPIO_SetupPin(GPIO_PortType gpioPort, uint8 pin,uint8 direction,uint8 pUp,uint8 pDown,uint8 od)
 *
 * @param[in] gpioPort: 	GPIO Port ID
 * @param[in] pin: 			GPIO Pin ID
 * @param[in] direction: 	Pin direction
 * @param[in] od:			Open Drain Configuration
 * @return		ERROR Status
 */
Std_ReturnType GPIO_SetupPin(GPIO_PortType gpioPort, uint8 pin,GPIO_DirectionType direction,uint8 od);
/** @fn Std_ReturnType GPIO_SetPinInFunction(GPIO_PortType gpioPort, uint8 pin,GPIO_RPInFunType func)
 *
 * @param[in] gpioPort
 * @param[in] pin
 * @param[in] func
 * @return
 */
Std_ReturnType GPIO_SetPinInFunction(GPIO_PortType gpioPort, uint8 pin,GPIO_RPInFunType func);
/** @fn Std_ReturnType GPIO_SetPinOutputFunction(GPIO_PortType gpioPort, uint8 pin,GPIO_RPOutFunType func)
 *
 * @param[in] gpioPort
 * @param[in] pin
 * @param[in] func
 * @return
 */
Std_ReturnType GPIO_SetPinOutputFunction(GPIO_PortType gpioPort, uint8 pin,GPIO_RPOutFunType func);
/**@fn uint8 GPIO_GetPinValue(GPIO_PortType gpioPort,uint8 pin)
 *
 * @param[in] gpioPort
 * @param[in] pin
 * @param[out] dataPtr
 * @return
 */
Std_ReturnType GPIO_GetPinValue(GPIO_PortType gpioPort,uint8 pin,uint8 * dataPtr);
/**@fn Std_ReturnType GPIO_SetPinValue(GPIO_PortType gpioPort,uint8 pin,uint8 value)
 *
 * @param[in] gpioPort
 * @param[in] pin
 * @param[in] value
 */
Std_ReturnType GPIO_SetPinValue(GPIO_PortType gpioPort,uint8 pin,uint8 value);
/**@fn Std_ReturnType GPIO_SetExtIntHandler(uint8 extIntID,GPIO_ExtIntFuncPtrType fPtr)
 *
 * @param[in] extIntID
 * @param[in] fPtr
 * @return
 */
Std_ReturnType GPIO_SetExtIntHandler(uint8 extIntID,GPIO_ExtIntFuncPtrType fPtr);
#if COMPILE_FOR_HOST
/**
 *
 */
void GPIO_ExtInt1Handler(void);
#endif
/** @} */
#endif
#endif /* Optoforce_SHexHost_GPIO_H */
