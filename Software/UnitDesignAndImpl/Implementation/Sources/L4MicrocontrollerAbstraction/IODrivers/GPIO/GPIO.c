/**
 * @file
 *
 * Unit that manages:
 * - .....
 * - .........
 *
 * @version $Id$
 */
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */
#include "Std.h"
#if 0
#include "GPIO.h"
#include "MCU.h"

#include "ErrorHandler.h"

/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticDefines Static defines
 */
/* */
/** Only works multiple calling if #pragma config IOL1WAY = OFF  */
#if COMPILE_FOR_TARGET
#define GPIO_UNLOCK_PPS()    __builtin_write_OSCCONL(OSCCON & 0xbfU)
#define GPIO_LOCK_PPS()      __builtin_write_OSCCONL(OSCCON | 0x40)
#else
#define GPIO_UNLOCK_PPS()
#define GPIO_LOCK_PPS()
#endif



#define PORTA_PIN_NUM	5u
#define PORTB_PIN_NUM	16u

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */
/** @enum GPIO_StateType
*	@brief Enum State of module initialization.
*/
typedef enum
{
    GPIO_STATE_UNINITIALISED = 0u,
    GPIO_STATE_INITIALISED = 1u
}GPIO_StateType;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */
/** @var static GPIO_StateType GPIO_State
	@brief State of module initialization.
*/
static GPIO_StateType GPIO_State = GPIO_STATE_UNINITIALISED;
void (*GPIO_ExtIntHandlerArray[GPIO_EXT_INT_MAX])(void);

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */

void GPIO_Init(void)
{
uint8 i;

	for(i=0u;i<GPIO_EXT_INT_MAX;i++)
	{
		GPIO_ExtIntHandlerArray[i] = &_DefaultInterrupt;
	}
	/****************************************************************************
       * Setting the Output Latch SFR(s)
       ***************************************************************************/
    LATA = 0x0001;
    LATB = 0x0010;

    /****************************************************************************
     * Setting the GPIO Direction SFR(s)
     ***************************************************************************/
    TRISA = 0x000AU;
    TRISB = 0x04DFU;

    /****************************************************************************
     * Setting the Weak Pull Up and Weak Pull Down SFR(s)
     ***************************************************************************/
    IOCPDA = 0x0000U;
    IOCPDB = 0x0000U;
    IOCPUA = 0x0000U;
    IOCPUB = 0x0000U;

    /****************************************************************************
     * Setting the Open Drain SFR(s)
     ***************************************************************************/
    ODCA = 0x0000U;
    ODCB = 0x0000U;

    /****************************************************************************
     * Setting the Analog/Digital Configuration SFR(s)
     ***************************************************************************/
    ANSA = 0x0000U;
    ANSB = 0x0000U;

    /****************************************************************************
     * Set the PPS
     ***************************************************************************/
    /****************************************************************************
     * Set the PPS
     ***************************************************************************/

    GPIO_State = GPIO_STATE_INITIALISED;
    return;
}

Std_ReturnType GPIO_SetPinValue(GPIO_PortType gpioPort,uint8 pin,uint8 value)
{
	Std_ReturnType retVal;
	switch(gpioPort)
    {
    case GPIO_PORTA:
    	if(pin < PORTA_PIN_NUM)
    	{
    		LATA = (LATA & ~(1u << pin))|(((uint16)value & 0x01u) << pin);
    	retVal = STD_RETURN_OK;
    	}
    	else
    	{
    		 retVal = STD_RETURN_ERROR;
    	}
    	break;
    case GPIO_PORTB:
    	if(pin < PORTB_PIN_NUM)
    	{
    		LATB = (LATB & ~(1u << pin))|(((uint16)value & 0x01u) << pin);
    		retVal = STD_RETURN_OK;
    	}
    	else
    	{
    		 retVal = STD_RETURN_ERROR;
    	}
        break;
    default:
    	 retVal = STD_RETURN_ERROR;
        break;
    }
	return(retVal);
}

Std_ReturnType GPIO_GetPinValue(GPIO_PortType gpioPort,uint8 pin,uint8 * dataPtr)
{
	Std_ReturnType retVal;
    if(NULL == dataPtr)
    {
    	retVal = STD_RETURN_ERROR;
    }
    else
    {
		switch(gpioPort)
		{
		case GPIO_PORTA:
			if(pin < PORTA_PIN_NUM)
			{
				(*dataPtr) = ((GET_LATA() >> pin)&0x01U);
				retVal = STD_RETURN_OK;
			}
			else
			{
				retVal = STD_RETURN_ERROR;
			}
			break;
		case GPIO_PORTB:
			if(pin < PORTB_PIN_NUM)
			{
				(*dataPtr) = ((GET_LATB() >> pin)&0x01U);
				retVal = STD_RETURN_OK;
			}
			else
			{
				retVal = STD_RETURN_ERROR;
			}
			break;
		default:
			retVal = STD_RETURN_ERROR;
			break;
		}

    }
    return (retVal);
}

Std_ReturnType GPIO_SetupPin(GPIO_PortType gpioPort, uint8 pin,GPIO_DirectionType direction,uint8 od)
{
    Std_ReturnType retVal;

        switch(gpioPort)
        {
        case GPIO_PORTA:
        	if(pin < PORTA_PIN_NUM)
        	{
        		/* Set Direction */
        		TRISA = (TRISA & ~(1u << pin))|(((uint16)direction & 0x01u) << pin);
        		/* Set OpenDrain */
        		ODCA = (ODCA & ~(1u << pin))|(((uint16)od & 0x01u) << pin);
        		retVal = STD_RETURN_OK;
        	}
        	else
        	{
        		retVal = STD_RETURN_ERROR;
        	}
            break;
        case GPIO_PORTB:
        	if(pin < PORTB_PIN_NUM)
        	{
        		/* Set Direction */
        		TRISB = (TRISB & ~(1u << pin))|(((uint16)direction & 0x01u) << pin);
        		/* Set OpenDrain */
        		ODCB = (ODCB & ~(1u << pin))|(((uint16)od & 0x01u) << pin);
        		retVal = STD_RETURN_OK;
        	}
        	else
        	{
        		retVal = STD_RETURN_ERROR;
        	}
            break;
        default:
        	retVal = STD_RETURN_ERROR;
            break;
        }
    return (retVal);
}

Std_ReturnType GPIO_SetPinOutputFunction(GPIO_PortType gpioPort, uint8 pin,GPIO_RPOutFunType func)
{
    Std_ReturnType retVal;
    if(gpioPort < GPIO_PORT_MAX)
    {
        if(GPIO_PORTA == gpioPort)
        {

            GPIO_UNLOCK_PPS();

            retVal = STD_RETURN_OK;
            switch(pin)
            {
            case 0u:
                SET_MASK(RPOR13,RP26R,(func&0x3F));
                break;
            case 1u:
                SET_MASK(RPOR13,RP27R,func);
                break;
            default:
                retVal = STD_RETURN_ERROR;
                break;

            }
             GPIO_LOCK_PPS();

        }
        else
        {


            GPIO_UNLOCK_PPS();


            retVal = STD_RETURN_OK;
            switch(pin)
            {
            case 0u:
                SET_MASK(RPOR0,RP0R,func);
                break;
            case 1u:
                SET_MASK(RPOR0,RP1R,func);
                break;
            case 2u:
                SET_MASK(RPOR1,RP2R,func);
                break;
            case 3u:
                SET_MASK(RPOR1,RP3R,func);
                break;
            case 4u:
                SET_MASK(RPOR2,RP4R,func);
                break;
            case 5u:
                SET_MASK(RPOR2,RP5R,func);
                break;
            case 6u:
                SET_MASK(RPOR3,RP6R,func);
                break;
            case 7u:
                SET_MASK(RPOR3,RP7R,func);
                break;
            case 8u:
                SET_MASK(RPOR4,RP8R,func);
                break;
            case 9u:
                SET_MASK(RPOR4,RP9R,func);
                break;
            case 10u:
                SET_MASK(RPOR5,RP10R,func);
                break;
            case 11u:
                SET_MASK(RPOR5,RP11R,func);
                break;
            case 12u:
                SET_MASK(RPOR6,RP12R,func);
                break;
            case 13u:
                SET_MASK(RPOR6,RP13R,func);
                break;
            case 14u:
                SET_MASK(RPOR7,RP14R,func);
                break;
            case 15u:
                SET_MASK(RPOR7,RP15R,func);
                break;
            default:
                retVal = STD_RETURN_ERROR;
                break;
            }
            GPIO_LOCK_PPS();

        }
    }
    else
    {
        retVal= STD_RETURN_ERROR;
    }
    return (retVal);
}

Std_ReturnType GPIO_SetPinInFunction(GPIO_PortType gpioPort, uint8 pin,GPIO_RPInFunType func)
{
    uint8 pinRD;
    Std_ReturnType retVal;
    do
    {
        if((GPIO_PORTA == gpioPort)&&(pin < PORTA_PIN_NUM))
        {
            pinRD = (GPIO_RP26 + pin);
        }
        else if((GPIO_PORTB == gpioPort)&&(pin < PORTB_PIN_NUM))
        {
            pinRD = (GPIO_RP0 + pin);
        }
        else
        {
            retVal = STD_RETURN_ERROR;
            break;
        }
        retVal = STD_RETURN_OK;
        GPIO_UNLOCK_PPS();

        switch(func)
        {
        case RP_IN_FUN_OCTRIG1:
            SET_MASK(RPINR0,OCTRIG1R,pinRD);
            break;
        case RP_IN_FUN_INT1:
            SET_MASK(RPINR0,INT1R,pinRD);
            break;
        case RP_IN_FUN_INT2:
            SET_MASK(RPINR1,INT2R,pinRD);
            break;
        case RP_IN_FUN_INT3:
            SET_MASK(RPINR1,INT3R,pinRD);
            break;
        case RP_IN_FUN_INT4:
            SET_MASK(RPINR2,INT4R,pinRD);
            break;
        case RP_IN_FUN_OCTRIG2:
            SET_MASK(RPINR2,OCTRIG2R,pinRD);
            break;
        case RP_IN_FUN_T2CK:
            SET_MASK(RPINR3,T2CKR,pinRD);
            break;
        case RP_IN_FUN_T3CK:
            SET_MASK(RPINR3,T3CKR,pinRD);
            break;
        case RP_IN_FUN_ICM1:
            SET_MASK(RPINR5,ICM1R,pinRD);
            break;
        case RP_IN_FUN_ICM2:
            SET_MASK(RPINR5,ICM2R,pinRD);
            break;
        case RP_IN_FUN_ICM3:
            SET_MASK(RPINR6,ICM3R,pinRD);
            break;
        case RP_IN_FUN_ICM4:
            SET_MASK(RPINR6,ICM4R,pinRD);
            break;
        case RP_IN_FUN_IC1:
            SET_MASK(RPINR7,IC1R,pinRD);
            break;
        case RP_IN_FUN_IC2:
            SET_MASK(RPINR7,IC2R,pinRD);
            break;
        case RP_IN_FUN_IC3:
            SET_MASK(RPINR8,IC3R,pinRD);
            break;
        case RP_IN_FUN_OCFA:
            SET_MASK(RPINR11,OCFAR,pinRD);
            break;
        case RP_IN_FUN_OCFB:
            SET_MASK(RPINR11,OCFBR,pinRD);
            break;
        case RP_IN_FUN_TCKIA:
            SET_MASK(RPINR12,TCKIAR,pinRD);
            break;
        case RP_IN_FUN_TCKIB:
            SET_MASK(RPINR12,TCKIBR,pinRD);
            break;
        case RP_IN_FUN_U1RX:
            SET_MASK(RPINR18, U1RXR,pinRD);
            break;
        case RP_IN_FUN_U1CTS:
            SET_MASK(RPINR18,U1CTSR,pinRD);
            break;
        case RP_IN_FUN_U2RX:
            SET_MASK(RPINR19,U2RXR,pinRD);
            break;
        case RP_IN_FUN_U2CTS:
            SET_MASK(RPINR19,U2CTSR,pinRD);
            break;
        case RP_IN_FUN_SDI1:
            SET_MASK(RPINR20,SDI1R,pinRD);
            break;
        case RP_IN_FUN_SCK1IN:
            SET_MASK(RPINR20,SCK1R,pinRD);
            break;
        case RP_IN_FUN_SS1IN:
            SET_MASK(RPINR21,SS1R,pinRD);
            break;
        case RP_IN_FUN_SDI2:
            SET_MASK(RPINR22,SDI2R,pinRD);
            break;
        case RP_IN_FUN_SCK2IN:
            SET_MASK(RPINR22,SCK2R,pinRD);
            break;
        case RP_IN_FUN_SS2IN:
            SET_MASK(RPINR23,SS2R,pinRD);
            break;
        case RP_IN_FUN_TxCK:
            SET_MASK(RPINR23,TXCKR,pinRD);
            break;
        case RP_IN_FUN_CLCINA:
            SET_MASK(RPINR25,CLCINAR,pinRD);
            break;
        case RP_IN_FUN_CLCINB:
            SET_MASK(RPINR25,CLCINBR,pinRD);
            break;
        case RP_IN_FUN_SDI3:
            SET_MASK(RPINR28,SDI3R,pinRD);
            break;
        case RP_IN_FUN_SCK3IN:
            SET_MASK(RPINR28,SCK3R,pinRD);
            break;
        case RP_IN_FUN_SS3IN:
            SET_MASK(RPINR29,SS3R,pinRD);
            break;
        default:
            retVal = STD_RETURN_ERROR;
            break;

        }
        GPIO_LOCK_PPS();

    }
    while(0);
    return (retVal);
}

Std_ReturnType GPIO_SetExtIntHandler(uint8 extIntID,GPIO_ExtIntFuncPtrType fPtr)
{
	Std_ReturnType retVal;
	if((extIntID >= GPIO_EXT_INT_MAX)||(NULL == fPtr))
	{
		retVal = STD_RETURN_ERROR;
	}
	else
	{
		//INTCON2bits.INT1EP = 1;
		SET_BIT(INTCON2,_INTCON2_INT1EP_POSITION);
                GPIO_ExtIntHandlerArray[extIntID] = fPtr;
		retVal = STD_RETURN_OK;
	}
	return(retVal);
}

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */
/** @fn void GPIO_ExtInt1Handler(void)
 *
 */
void GPIO_ExtInt1Handler(void)
{
	(*GPIO_ExtIntHandlerArray[GPIO_EXT_INT1])();
}

#endif
/** @} */


