/**
* @file SPIDriver.c
* @author Vincze Balazs
* @date 2017. okt. 26.
* @brief Implementation file of module XXXX
* @version V 1.0
*/
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */
#include <Debug.h>
#include "SPIDriver.h"
#include "spi_.h"
#include "GPIO.h"
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticDefines Static defines
 */

/**
 * Description.
 */
/*#define TEMPLATE_DEF (20U) */
#define SPIDRIVER_TXINTFLG        (0x200U) /* Transmitter-empty interrupt flag. */
#define SPIDRIVER_RXINTFLG        (0x100U) /* Receiver-full interrupt flag. */
#define SPIDRIVER_RXOVRNINTFLG    (0x040U) /* Receiver overrun flag. */
#define SPIDRIVER_BITERRFLG       (0x010U) /* Mismatch of internal transmit data and transmitted data. */
#define SPIDRIVER_DESYNCFLG       (0x008U) /* Desynchronization of slave device. */
#define SPIDRIVER_PARITYERRFLG    (0x004U) /* Calculated parity differs from received parity bit. */
#define SPIDRIVER_TIMEOUTFLG      (0x002U) /* Time-out caused by nonactivation of ENA signal */
#define SPIDRIVER_DLENERRFLG      (0x001U) /* Data-length error flag */
#define SPIDRIVER_ERRORFLGMASK    (0x0FFU) /* Mask for all error flags */
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */

/**
 * Short description for EA and Doxygen.
 *
 * Detailed description for Doxygen, possibly several lines.
 * And several sentences.
 */

typedef struct _SPIDriver_SPIDescr_t
{
    spiBASE_t * spiReg;
    gioPORT_t * spiPort;
    uint8_t     masterMode;
    uint8_t     instanceInitialised;
}
SPIDriver_SPIDescr_t;
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */

/**
 * Description.
 */
/* const uint8 Template_val = 0U; */

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariablesInitialized Static variables initialized
 *  @{
 */
SPIDriver_SPIDescr_t spiDescriptors[SPI_BUS_ID_MAX] = {0U};
uint8_t DriverInitialised = 0x00U;
/**
 * Short description for EA and Doxygen.
 *
 * Detailed description for Doxygen, possibly several lines.
 * And several sentences.
 */
/*static int8 Template_StaticData;*/

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariablesNonInitialized Static variables non-initialized
 * @{
 */

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */

/* static uint8 SPIDriver_StaticFoo (int8 templateFoo); */

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */

int16_t SPIDriver_WriteReadArray(SPIDriver_BusID_t busID,uint8_t * pData,uint8_t bytesNumber)
{
uint16 Tx_Data;
uint32_t i=0U;
    SPIDriver_SetCS0Pin(busID,0x00U);
    while(bytesNumber != 0U)
    {
        if((spiDescriptors[busID].spiReg->FLG & SPIDRIVER_ERRORFLGMASK) !=0U)
        {
            ERROR_HALT();   /* TODO: Error handling for SPI peripheral hardware error in SPIDriver_WriteReadArray()! */
        }
        Tx_Data = *pData;

        /*SAFETYMCUSW 51 S MR:12.3 <APPROVED> "Needs shifting for 32-bit value" */
        spiDescriptors[busID].spiReg->DAT1 = ((uint32)SPI_FMT_0 << 24U) |                /* Dataformat */
                ((uint32)0u << 16U)        |                /* CSNR num IF we used in future this will different in SPI's */
                (0x04000000U)                       |                /* Wdelay */
                (0x10000000U)              |                /* CS Hold */
                (uint32)Tx_Data;

        /*SAFETYMCUSW 28 D MR:NA <APPROVED> "Hardware status bit read check" */
        while((spiDescriptors[busID].spiReg->FLG & SPIDRIVER_RXINTFLG) != SPIDRIVER_RXINTFLG)
        {
            i++;
        } /* Wait */
        /*SAFETYMCUSW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
        *pData = (uint16_t)spiDescriptors[busID].spiReg->BUF;
        /*SAFETYMCUSW 567 S MR:17.1,17.4 <APPROVED> "Pointer increment needed" */
        pData++;
        bytesNumber--;
    }
    SPIDriver_SetCS0Pin(busID,0x01U);
    return (bytesNumber);
}
int16_t SPIDriver_WriteReadArray16(SPIDriver_BusID_t busID,uint8_t * pData,uint8_t bytesNumber)
{
uint16 val16;
uint32_t i=0U;
    SPIDriver_SetCS0Pin(busID,0x00U);
    while(bytesNumber != 0U)
    {
        if((spiDescriptors[busID].spiReg->FLG & SPIDRIVER_ERRORFLGMASK) !=0U)
        {
            ERROR_HALT();   /* TODO: Error handling for SPI peripheral hardware error in SPIDriver_WriteReadArray()! */
        }
        val16 = ((pData[0] << 8)|(pData[1]));

        /*SAFETYMCUSW 51 S MR:12.3 <APPROVED> "Needs shifting for 32-bit value" */
        spiDescriptors[busID].spiReg->DAT1 = ((uint32)SPI_FMT_2 << 24U) |                /* Dataformat */
                ((uint32)0u << 16U)        |                /* CSNR num IF we used in future this will different in SPI's */
                (0x04000000U)                       |                /* Wdelay */
                (0x10000000U)              |                /* CS Hold */
                (uint32)val16;

        /*SAFETYMCUSW 28 D MR:NA <APPROVED> "Hardware status bit read check" */
        while((spiDescriptors[busID].spiReg->FLG & SPIDRIVER_RXINTFLG) != SPIDRIVER_RXINTFLG)
        {
            i++;
        } /* Wait */
        /*SAFETYMCUSW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
        //*pData = (uint16_t)spiDescriptors[busID].spiReg->BUF;
        val16 = (uint16_t)spiDescriptors[busID].spiReg->BUF;
        pData[0] = ((val16 >> 8)&0xff);
        pData[1] = ((val16)&0xff);
        /*SAFETYMCUSW 567 S MR:17.1,17.4 <APPROVED> "Pointer increment needed" */
        pData+=2;
        bytesNumber-=2;
    }
    SPIDriver_SetCS0Pin(busID,0x01U);
    return (bytesNumber);
}

int16_t SPIDriver_WriteArray(SPIDriver_BusID_t busID,uint8_t * pData,uint8_t bytesNumber)
{
volatile uint32 SpiBuf;
uint16 Tx_Data;
    SPIDriver_SetCS0Pin(busID,0x00U);
    while(bytesNumber != 0U)
    {
        if((spiDescriptors[busID].spiReg->FLG & SPIDRIVER_ERRORFLGMASK) !=0U)
        {
            ERROR_HALT();
        }
        /*SAFETYMCUSW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
        Tx_Data = *pData;
        spiDescriptors[busID].spiReg->DAT1 = ((uint32)SPI_FMT_0 << 24U) |                /* Dataformat */
                ((uint32)0u << 16U)        |                /* CSNR num IF we used in future this will different in SPI's */
                (0x04000000U)                       |                /* Wdelay */
                (0x10000000U)              |                /* CS Hold */
                (uint32)Tx_Data;
        /*SAFETYMCUSW 567 S MR:17.1,17.4 <APPROVED> "Pointer increment needed" */
        pData++;
        /*SAFETYMCUSW 28 D MR:NA <APPROVED> "Hardware status bit read check" */
        while((spiDescriptors[busID].spiReg->FLG & SPIDRIVER_RXINTFLG) != SPIDRIVER_RXINTFLG)
        {} /* Wait */
        SpiBuf = spiDescriptors[busID].spiReg->BUF;

        bytesNumber--;
    }
    SPIDriver_SetCS0Pin(busID,0x01U);
    return (bytesNumber);
}
int16_t SPIDriver_WriteDataRegister(SPIDriver_BusID_t busID,uint8_t dat)
{
int16_t retVal = -5;

    if(busID >= SPI_BUS_ID_MAX)
    {
        PUTS("// Error 1 retVal = -1;");
        retVal = -1;
    }
    else if(NULL == spiDescriptors[busID].spiReg)
    {
        PUTS("// Error 2 retVal = -2;");
        retVal = -2;
    }
    else if(NULL == spiDescriptors[busID].spiPort)
    {
        PUTS("// Error 3 retVal = -3;");
        retVal = -3;
    }
    else if((spiDescriptors[busID].spiReg->FLG & SPIDRIVER_ERRORFLGMASK) != 0U)
    {
        PUTS("// Error 4 retVal = -4;");
        retVal = -4;
    }
    /* TODO: Check IF TXBUFF is busy!! */
    else
    {
        spiDescriptors[busID].spiReg->DAT1 = ((uint32)SPI_FMT_0 << 24U) |                /* Dataformat */
                ((uint32)0u << 16U)        |                /* CSNR num IF we used in future this will different in SPI's */
                (0x04000000U)              |                /* Wdelay */
                (0x10000000U)              |                /* CS Hold */
                (uint32)(dat);                              /* Shift 0x00 */
        retVal = 0;
    }

    return (retVal);
}
int16_t SPIDriver_ShiftDataRegister(SPIDriver_BusID_t busID)
{
int16_t retVal = -5;

    if(busID >= SPI_BUS_ID_MAX)
    {
        PUTS("// Error 1 retVal = -1;");
        retVal = -1;
    }
    else if(NULL == spiDescriptors[busID].spiReg)
    {
        PUTS("// Error 2 retVal = -2;");
        retVal = -2;
    }
    else if(NULL == spiDescriptors[busID].spiPort)
    {
        PUTS("// Error 3 retVal = -3;");
        retVal = -3;
    }
    else if((spiDescriptors[busID].spiReg->FLG & SPIDRIVER_ERRORFLGMASK) != 0U)
    {
        PUTS("// Error 4 retVal = -4;");
        retVal = -4;
    }
    /* TODO: Check IF TXBUFF is busy!! */
    else
    {
        spiDescriptors[busID].spiReg->DAT1 = ((uint32)SPI_FMT_0 << 24U) |                /* Dataformat */
                ((uint32)0u << 16U)        |                /* CSNR num IF we used in future this will different in SPI's */
                (0x04000000U)              |                /* Wdelay */
                (0x10000000U)              |                /* CS Hold */
                (0x00000000U);                              /* Shift 0x00 */
        retVal = 0;
    }

    return (retVal);
}
int16_t SPIDriver_GetDataRegister(SPIDriver_BusID_t busID,uint8_t * dataRegVal)
{
    int16_t retVal = -10;
    if(busID >= SPI_BUS_ID_MAX)
    {
        PUTS("// Error 1 retVal = -1;");
        retVal = -1;
    }
    else if(NULL ==  spiDescriptors[busID].spiReg)
    {
        PUTS("// Error 2 retVal = -2;");
        retVal = -2;
    }
    else if(NULL == spiDescriptors[busID].spiPort)
    {
        PUTS("// Error 3 retVal = -3;");
        retVal = -3;
    }
    else if(NULL == dataRegVal)
    {
        PUTS("// Error 5 retVal = -5;");
        retVal = -5;
    }
    /* Hardware Error */
    else if((spiDescriptors[busID].spiReg->FLG & SPIDRIVER_ERRORFLGMASK) != 0U)
    {
        PUTS("// Error 4 retVal = -4;");
        retVal = -4;
    }
    /* No New Data on SPI Register */
    else if((spiDescriptors[busID].spiReg->FLG & SPIDRIVER_RXINTFLG) != SPIDRIVER_RXINTFLG)
    {
        retVal = 0;
    }
    else
    {
        *dataRegVal = (uint8_t)spiDescriptors[busID].spiReg->BUF;
        retVal = 1;
    }
    return (retVal);
}
int16_t SPIDriver_IsENAPinLow(SPIDriver_BusID_t busID)
{
    int16_t retVal = -4;
    if(busID >= SPI_BUS_ID_MAX)
    {
        PUTS("// Error 1 retVal = -1;");
        retVal = -1;
    }
    else if(NULL ==  spiDescriptors[busID].spiReg)
    {
        PUTS("// Error 2 retVal = -2;");
        retVal = -2;
    }
    else if(NULL == spiDescriptors[busID].spiPort)
    {
        PUTS("// Error 3 retVal = -3;");
        retVal = -3;
    }
    else
    {
        /* Pin IS LOW */
        if(((spiDescriptors[busID].spiPort->DIN >> SPI_PIN_ENA)&0x01U) == 0x00U)
        {
          //  Debug_Putchar('L');
            retVal = 1;
        }
        /* Pin IS HIGH */
        else
        {
        //    Debug_Putchar('H');
            retVal = 0;
        }
    }
    return (retVal);
}
int16_t SPIDriver_SetCS0Pin(SPIDriver_BusID_t busID,uint32_t value)
{
    int16_t retVal = -4;
    if(busID >= SPI_BUS_ID_MAX)
    {
        PUTS("// Error 1 retVal = -1;");
        retVal = -1;
    }
    else if(NULL ==  spiDescriptors[busID].spiReg)
    {
        PUTS("// Error 2 retVal = -2;");
        retVal = -2;
    }
    else if(NULL == spiDescriptors[busID].spiPort)
    {
        PUTS("// Error 3 retVal = -3;");
        retVal = -3;
    }
    else
    {
        spiDescriptors[busID].spiPort->DOUT &= ~(1U << SPI_PIN_CS0);
        spiDescriptors[busID].spiPort->DOUT |= (value << SPI_PIN_CS0);
        retVal = 1;
    }
    return (retVal);
}
int16_t SPIDriver_InitInstance(SPIDriver_BusID_t busID,uint8_t masterMode)
{
    int16_t retVal = 0;
    /** Check for Bad InstaceID */
    if(busID >= SPI_BUS_ID_MAX)
    {
        retVal = -1;
        PUTS("// Error 1 retVal = -1;");
    }
    /** Check for NULL SPI register */
    else if(NULL ==  spiDescriptors[busID].spiReg)
    {
        retVal = -2;
        PUTS("// Error 2 retVal = -2;");
    }
    /** Check for NULL Port Register */
    else if(NULL ==  spiDescriptors[busID].spiPort)
    {
        retVal = -3;
        PUTS("// Error 3 retVal = -3;");
    }
    /** Check for bad Master Mode */
    else if((masterMode >= SPI_MASTER_MODE_MAX)||(masterMode == SPI_MASTER_MODE_UNKNOWN))
    {
        retVal = -4;
        PUTS("// Error 4 retVal = -4;");
    }
    /** If No error setup registers */
    else
    {
        retVal = 0;
        /** @b initialize @b SPI */
        /** bring SPI out of reset */
        spiDescriptors[busID].spiReg->GCR0 = 0U;
        spiDescriptors[busID].spiReg->GCR0 = 1U;
        spiDescriptors[busID].masterMode = masterMode;
        /** Configure SPI peripheral for Master Mode */
        if(SPI_MASTER_MODE_MASTER == spiDescriptors[busID].masterMode)
        {
            spiDescriptors[busID].spiReg->GCR1 = (spiDescriptors[busID].spiReg->GCR1 & 0xFFFFFFFCU)
                    | ((uint32)((uint32)1U << 1U)  /* CLOKMOD 1=internal 0=external source*/
                       | 1U);  /* 1=MASTER 0=SLAVE*/
        }
        /** Configure SPI peripheral for Slave Mode */
        else
        {
            spiDescriptors[busID].spiReg->GCR1 = (spiDescriptors[busID].spiReg->GCR1 & 0xFFFFFFFCU)
                    | ((uint32)((uint32)0U << 1U)  /* CLOKMOD 1=internal 0=external source*/
                       | 0U);  /* 1=MASTER 0=SLAVE*/
        }
        /** SPI enable pin configuration */
        spiDescriptors[busID].spiReg->INT0 = (spiDescriptors[busID].spiReg->INT0 & 0xFEFFFFFFU)| (uint32)((uint32)0U << 24U);  /* ENABLE HIGHZ */

        /** - Delays */
        spiDescriptors[busID].spiReg->DELAY = (uint32)((uint32)0U << 24U)  /* C2TDELAY */
                | (uint32)((uint32)0U << 16U)  /* T2CDELAY */
                | (uint32)((uint32)0U << 8U)   /* T2EDELAY */
                | (uint32)((uint32)0U << 0U);  /* C2EDELAY */
        /** - Data Format 0 */
        spiDescriptors[busID].spiReg->FMT0 = (uint32)((uint32)1U << 24U)  /* wdelay */
                | (uint32)((uint32)0U << 23U)  /* parity Polarity */
                | (uint32)((uint32)0U << 22U)  /* parity enable */
                | (uint32)((uint32)0U << 21U)  /* wait on enable */
                | (uint32)((uint32)0U << 20U)  /* shift direction */
                | (uint32)((uint32)1U << 17U)  /* clock polarity */
                | (uint32)((uint32)0U << 16U)  /* clock phase */
                | (uint32)((uint32)43U << 8U) /* baudrate prescale */
                | (uint32)((uint32)8U << 0U);  /* data word length */

        /** - Data Format 1 */
        spiDescriptors[busID].spiReg->FMT1 = (uint32)((uint32)1U << 24U)  /* wdelay */
                | (uint32)((uint32)0U << 23U)  /* parity Polarity */
                | (uint32)((uint32)0U << 22U)  /* parity enable */
                | (uint32)((uint32)0U << 21U)  /* wait on enable */
                | (uint32)((uint32)0U << 20U)  /* shift direction */
                | (uint32)((uint32)1U << 17U)  /* clock polarity */
                | (uint32)((uint32)0U << 16U)  /* clock phase */
                | (uint32)((uint32)219U << 8U) /* baudrate prescale */
                | (uint32)((uint32)8U << 0U);  /* data word length */

        /** - Data Format 2 */
        spiDescriptors[busID].spiReg->FMT2 = (uint32)((uint32)0U << 24U)  /* wdelay */
                | (uint32)((uint32)0U << 23U)  /* parity Polarity */
                | (uint32)((uint32)0U << 22U)  /* parity enable */
                | (uint32)((uint32)0U << 21U)  /* wait on enable */
                | (uint32)((uint32)0U << 20U)  /* shift direction */
                | (uint32)((uint32)1U << 17U)  /* clock polarity */
                | (uint32)((uint32)0U << 16U)  /* clock phase */
                | (uint32)((uint32)219U << 8U) /* baudrate prescale */
                | (uint32)((uint32)16U << 0U);  /* data word length */

        /** - Data Format 3 */
        spiDescriptors[busID].spiReg->FMT3 = (uint32)((uint32)0U << 24U)  /* wdelay */
                | (uint32)((uint32)0U << 23U)  /* parity Polarity */
                | (uint32)((uint32)0U << 22U)  /* parity enable */
                | (uint32)((uint32)0U << 21U)  /* wait on enable */
                | (uint32)((uint32)0U << 20U)  /* shift direction */
                | (uint32)((uint32)0U << 17U)  /* clock polarity */
                | (uint32)((uint32)0U << 16U)  /* clock phase */
                | (uint32)((uint32)109U << 8U) /* baudrate prescale */
                | (uint32)((uint32)16U << 0U);  /* data word length */

        /** - set interrupt levels */
        spiDescriptors[busID].spiReg->LVL = (uint32)((uint32)0U << 9U)  /* TXINT */
                | (uint32)((uint32)0U << 8U)  /* RXINT */
                | (uint32)((uint32)0U << 6U)  /* OVRNINT */
                | (uint32)((uint32)0U << 4U)  /* BITERR */
                | (uint32)((uint32)0U << 3U)  /* DESYNC */
                | (uint32)((uint32)0U << 2U)  /* PARERR */
                | (uint32)((uint32)0U << 1U) /* TIMEOUT */
                | (uint32)((uint32)0U << 0U);  /* DLENERR */

        /** - clear any pending interrupts */
        spiDescriptors[busID].spiReg->FLG |= 0xFFFFU;

        /** - enable interrupts */
        spiDescriptors[busID].spiReg->INT0 = ( spiDescriptors[busID].spiReg->INT0 & 0xFFFF0000U)
                | (uint32)((uint32)0U << 9U)  /* TXINT */
                | (uint32)((uint32)0U << 8U)  /* RXINT */
                | (uint32)((uint32)0U << 6U)  /* OVRNINT */
                | (uint32)((uint32)0U << 4U)  /* BITERR */
                | (uint32)((uint32)0U << 3U)  /* DESYNC */
                | (uint32)((uint32)0U << 2U)  /* PARERR */
                | (uint32)((uint32)0U << 1U) /* TIMEOUT */
                | (uint32)((uint32)0U << 0U);  /* DLENERR */

        /** @b initialize @b SPI1 @b Port */

        /** - SPI Port output values */
        spiDescriptors[busID].spiReg->PC3 =    (uint32)((uint32)0U << SPI_PIN_CS0)  /* SCS[0] */
                | (uint32)((uint32)1U << SPI_PIN_CS1)  /* SCS[1] */
                | (uint32)((uint32)1U << SPI_PIN_CS2)  /* SCS[2] */
                | (uint32)((uint32)1U << SPI_PIN_CS3)  /* SCS[3] */
                | (uint32)((uint32)1U << SPI_PIN_CS4)  /* SCS[4] */
                | (uint32)((uint32)1U << SPI_PIN_CS5)  /* SCS[5] */
                | (uint32)((uint32)0U << SPI_PIN_ENA)  /* ENA */
                | (uint32)((uint32)0U << SPI_PIN_CLK)  /* CLK */
                | (uint32)((uint32)0U << SPI_PIN_SIMO)  /* SIMO[0] */
                | (uint32)((uint32)0U << SPI_PIN_SOMI)  /* SOMI[0] */
                | (uint32)((uint32)0U << 17U)  /* SIMO[1] */
                | (uint32)((uint32)0U << 25U); /* SOMI[1] */

        /** - SPI Port direction */
        spiDescriptors[busID].spiReg->PC1  =   (uint32)((uint32)1U << SPI_PIN_CS0)  /* SCS[0] */
                | (uint32)((uint32)1U << 1U)  /* SCS[1] */
                | (uint32)((uint32)1U << 2U)  /* SCS[2] */
                | (uint32)((uint32)1U << 3U)  /* SCS[3] */
                | (uint32)((uint32)1U << 4U)  /* SCS[4] */
                | (uint32)((uint32)1U << 5U)  /* SCS[5] */
                | (uint32)((uint32)0U << SPI_PIN_ENA)  /* ENA */
                | (uint32)((uint32)1U << 9U)  /* CLK */
                | (uint32)((uint32)0U << 10U)  /* SIMO[0] */
                | (uint32)((uint32)0U << 11U)  /* SOMI[0] */
                | (uint32)((uint32)1U << 17U)  /* SIMO[1] */
                | (uint32)((uint32)0U << 25U); /* SOMI[1] */

        /** - SPI Port open drain enable */
        spiDescriptors[busID].spiReg->PC6  =   (uint32)((uint32)0U << SPI_PIN_CS0)  /* SCS[0] */
                | (uint32)((uint32)0U << 1U)  /* SCS[1] */
                | (uint32)((uint32)0U << 2U)  /* SCS[2] */
                | (uint32)((uint32)0U << 3U)  /* SCS[3] */
                | (uint32)((uint32)0U << 4U)  /* SCS[4] */
                | (uint32)((uint32)0U << 5U)  /* SCS[5] */
                | (uint32)((uint32)0U << SPI_PIN_ENA)  /* ENA */
                | (uint32)((uint32)0U << 9U)  /* CLK */
                | (uint32)((uint32)0U << 10U)  /* SIMO[0] */
                | (uint32)((uint32)0U << 11U)  /* SOMI[0] */
                | (uint32)((uint32)0U << 17U)  /* SIMO[1] */
                | (uint32)((uint32)0U << 25U); /* SOMI[1] */

        /** - SPI Port pullup / pulldown selection */
        spiDescriptors[busID].spiReg->PC8  =   (uint32)((uint32)1U << SPI_PIN_CS0)  /* SCS[0] */
                | (uint32)((uint32)1U << 1U)  /* SCS[1] */
                | (uint32)((uint32)1U << 2U)  /* SCS[2] */
                | (uint32)((uint32)1U << 3U)  /* SCS[3] */
                | (uint32)((uint32)1U << 4U)  /* SCS[4] */
                | (uint32)((uint32)1U << 5U)  /* SCS[5] */
                | (uint32)((uint32)1U << SPI_PIN_ENA)  /* ENA */
                | (uint32)((uint32)1U << 9U)  /* CLK */
                | (uint32)((uint32)1U << 10U)  /* SIMO[0] */
                | (uint32)((uint32)1U << 11U)  /* SOMI[0] */
                | (uint32)((uint32)1U << 17U)  /* SIMO[1] */
                | (uint32)((uint32)1U << 25U); /* SOMI[1] */

        /** - SPI Port pullup / pulldown enable*/
        spiDescriptors[busID].spiReg->PC7  =   (uint32)((uint32)0U << SPI_PIN_CS0)  /* SCS[0] */
                | (uint32)((uint32)1U << 1U)  /* SCS[1] */
                | (uint32)((uint32)1U << 2U)  /* SCS[2] */
                | (uint32)((uint32)1U << 3U)  /* SCS[3] */
                | (uint32)((uint32)1U << 4U)  /* SCS[4] */
                | (uint32)((uint32)1U << 5U)  /* SCS[5] */
                | (uint32)((uint32)0U << SPI_PIN_ENA)  /* ENA */
                | (uint32)((uint32)1U << 9U)  /* CLK */
                | (uint32)((uint32)1U << 10U)  /* SIMO[0] */
                | (uint32)((uint32)1U << 11U)  /* SOMI[0] */
                | (uint32)((uint32)1U << 17U)  /* SIMO[1] */
                | (uint32)((uint32)0U << 25U); /* SOMI[1] */

        /** - SPI set all pins to functional */
        spiDescriptors[busID].spiReg->PC0  =   (uint32)((uint32)0U << SPI_PIN_CS0)  /* SCS[0] */
                | (uint32)((uint32)0U << 1U)  /* SCS[1] */
                | (uint32)((uint32)0U << 2U)  /* SCS[2] */
                | (uint32)((uint32)0U << 3U)  /* SCS[3] */
                | (uint32)((uint32)0U << 4U)  /* SCS[4] */
                | (uint32)((uint32)0U << 5U)  /* SCS[5] */
                | (uint32)((uint32)0U << SPI_PIN_ENA)  /* ENA */
                | (uint32)((uint32)1U << 9U)  /* CLK */
                | (uint32)((uint32)1U << 10U)  /* SIMO[0] */
                | (uint32)((uint32)1U << 11U)  /* SOMI[0] */
                | (uint32)((uint32)0U << 17U)  /* SIMO[1] */
                | (uint32)((uint32)0U << 25U); /* SOMI[1] */
        /** - Finally start SPI */
        spiDescriptors[busID].spiReg->GCR1 = (spiDescriptors[busID].spiReg->GCR1 & 0xFEFFFFFFU) | 0x01000000U;

        GPIO_SetPinODR(    spiDescriptors[busID].spiPort, SPI_PIN_ENA, 0X01u);
        GPIO_SetPinDirection(  spiDescriptors[busID].spiPort, SPI_PIN_ENA, 0X00U);
        GPIO_SetPinPUPD(  spiDescriptors[busID].spiPort, SPI_PIN_ENA, 0x01U, 0x01U);
        GPIO_SetPinValue(  spiDescriptors[busID].spiPort, SPI_PIN_ENA, 0x01U);


        GPIO_SetPinODR(    spiDescriptors[busID].spiPort, SPI_PIN_CS0, 0X00u);
        GPIO_SetPinDirection(  spiDescriptors[busID].spiPort, SPI_PIN_CS0, 0X01U);
        GPIO_SetPinPUPD(  spiDescriptors[busID].spiPort, SPI_PIN_CS0, 0x01U, 0x01U);
        GPIO_SetPinValue(  spiDescriptors[busID].spiPort, SPI_PIN_CS0, 0x00U);
        //PUTS("SPI INIT OK retVal = 1;");
    }
    return (retVal);
}
int16_t SPIDriver_Init(void)
{
#if 0
    spiDescriptors[SPI_INS_ID_1].spiPort    = spiPORT3;
    spiDescriptors[SPI_INS_ID_1].spiReg     = spiREG3;
    spiDescriptors[SPI_INS_ID_1].masterMode = SPI_MASTER_MODE_UNKNOWN;
#else

    spiDescriptors[SPI_INS_ID_1].spiPort    = spiPORT1;
    spiDescriptors[SPI_INS_ID_1].spiReg     = spiREG1;
    spiDescriptors[SPI_INS_ID_1].masterMode = SPI_MASTER_MODE_UNKNOWN;
    spiDescriptors[SPI_INS_ID_1].instanceInitialised = 0x00U;
    spiDescriptors[SPI_INS_ID_2].spiPort    = spiPORT2;
    spiDescriptors[SPI_INS_ID_2].spiReg     = spiREG2;
    spiDescriptors[SPI_INS_ID_2].masterMode = SPI_MASTER_MODE_UNKNOWN;
    spiDescriptors[SPI_INS_ID_2].instanceInitialised = 0x00U;
    spiDescriptors[SPI_INS_ID_3].spiPort    = spiPORT3;
    spiDescriptors[SPI_INS_ID_3].spiReg     = spiREG3;
    spiDescriptors[SPI_INS_ID_3].masterMode = SPI_MASTER_MODE_UNKNOWN;
    spiDescriptors[SPI_INS_ID_3].instanceInitialised = 0x00U;
    spiDescriptors[SPI_INS_ID_4].spiPort    = spiPORT4;
    spiDescriptors[SPI_INS_ID_4].spiReg     = spiREG4;
    spiDescriptors[SPI_INS_ID_4].masterMode = SPI_MASTER_MODE_UNKNOWN;
    spiDescriptors[SPI_INS_ID_4].instanceInitialised = 0x00U;
#endif

    DriverInitialised = 0x01;
    return (0);
}
/**@}*/

