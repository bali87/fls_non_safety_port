#ifndef DRIVERS_SPI_SPIDRIVER_H_
#define DRIVERS_SPI_SPIDRIVER_H_

/**
* @file SPIDriver.h
* @author Vincze Balazs
* @date 2017. okt. 26.
* @brief Implementation file of module XXXX
* @version V 1.0
*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */
#include <stdint.h>
#include "spi_.h"
#include "board.h"
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */

/**
 * Short description.
 *
 * Detailed description for Doxygen, possibly several lines.
 * And several sentences.
 */
/* #define TEMPLATE_PI 3.14 */

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */
typedef enum
{
#if BOARD_TYPE == BOARD_FLS
    SPI_INS_ID_1 =      0X00U,
    SPI_INS_ID_2 =      0x01U,
    SPI_INS_ID_3 =      0x02U,
    SPI_INS_ID_4 =      0x03U,
    SPI_BUS_ID_MAX =    0x04U
#else
    SPI_INS_ID_1 =      0X00U,
    SPI_BUS_ID_MAX =    0x01U
#endif
}SPIDriver_BusID_t;
typedef enum
{
    SPI_MASTER_MODE_UNKNOWN =      0X00U,
    SPI_MASTER_MODE_MASTER  =      0x01U,
    SPI_MASTER_MODE_SLAVE   =      0x02U,
    SPI_MASTER_MODE_MAX     =      0x03U
}SPIDriver_MasterMode_t;
/**
 * Short description.
 *
 * Detailed description for Doxygen, possibly several lines.
 * And several sentences.
 */

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */

/**
 * Description.
 */ 


/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */
extern int16_t SPIDriver_InitInstance(SPIDriver_BusID_t spiInstanceID,uint8_t masterMode);
extern int16_t SPIDriver_WriteArray(SPIDriver_BusID_t spiInstanceID,uint8_t * data,uint8_t bytesNumber);
extern int16_t SPIDriver_WriteReadArray(SPIDriver_BusID_t spiInstanceID,uint8_t * data,uint8_t bytesNumber);
int16_t SPIDriver_WriteReadArray16(SPIDriver_BusID_t busID,uint8_t * pData,uint8_t bytesNumber);
extern int16_t SPIDriver_IsENAPinLow(SPIDriver_BusID_t spiInstanceID);
extern int16_t SPIDriver_SetCS0Pin(SPIDriver_BusID_t spiInstanceID,uint32_t value);
extern int16_t SPIDriver_Init(void);
extern int16_t SPIDriver_ShiftDataRegister(SPIDriver_BusID_t spiInstanceID);
extern int16_t SPIDriver_GetDataRegister(SPIDriver_BusID_t spiInstanceID,uint8_t * dataBuffVal);
int16_t SPIDriver_WriteDataRegister(SPIDriver_BusID_t busID,uint8_t dat);
/**@}*/
#endif /* DRIVERS_SPI_SPIDRIVER_H_ */
