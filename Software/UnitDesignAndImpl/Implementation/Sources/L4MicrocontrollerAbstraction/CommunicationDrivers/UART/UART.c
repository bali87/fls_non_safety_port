/**
 * @file
 *
 * Unit that manages:
 * - Interrupt Driven UART Communication with FIFO's
 *
 * @version $Id$
 */
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */
#include "Std.h"
#include "MCU.h"
#include "Board.h"
#include "FIFO.h"
#include "UART.h"
#include "GPIO.h"

/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticDefines Static defines
 */
/**@def UART_TX_BUFFER_SIZE
 * @brief UART's Transmit Buffer Size
 */

#define UART_TX_BUFFER_SIZE	256u
#define UART_RX_BUFFER_SIZE 512u




/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */

/** @enum UART_StateType
 *	@brief Enum State of module initialization.
 */
typedef enum
{
    UART_STATE_UNINITIALIZED = 0u,
    UART_STATE_INITIALIZED = 1u
} UART_StateType;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */
/** @var static UART_StateType UART_State
        @brief State of module initialization.
 */
static UART_StateType UART_State = UART_STATE_UNINITIALIZED;
/** @var  static FIFO_DescrType UART_TxFIFO;
 *  @brief UART's Transmit FIFO Descriptor
 */
volatile FIFO_DescrType UART_TxFIFO;
/** @var  static FIFO_DescrType UART_RxFIFO;
 *  @brief UART's Recieve FIFO Descriptor
 */
volatile FIFO_DescrType UART_RxFIFO;
/** @var static uint8 UART_TxBuffer[UART_BUFFER_SIZE];
 *  @brief UART's Receive FIFO's data buffer
 */
volatile uint8 UART_TxBuffer[UART_TX_BUFFER_SIZE];
/** @var static uint8 UART_RxBuffer[UART_BUFFER_SIZE];
 *  @brief UART's Recieve FIFO's data buffer
 */
volatile uint8 UART_RxBuffer[UART_RX_BUFFER_SIZE];
/** @var static uint8_t UART_BuffByte=0U;
 *  @brief UART's One byte data buffer for interrupt
 */
static volatile uint8 txByte;
static volatile uint8 rxByte;
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */

Std_ReturnType UART_Init (void)
{
    Std_ReturnType retVal = STD_RETURN_ERROR;
    /* Disable UART */
    CLR_BIT (U1MODE, _U1MODE_UARTEN_POSITION);

    /* Init UART Tx FIFO with own Tx data buffer */
    retVal = FIFO_InitDescriptor ((FIFO_DescrType *)&UART_TxFIFO, (uint8 *) UART_TxBuffer, UART_TX_BUFFER_SIZE);

    if (STD_RETURN_OK == retVal)
    {
        /* Init UART Rx FIFO with own Rx data buffer */
        retVal = FIFO_InitDescriptor ((FIFO_DescrType*)&UART_RxFIFO, (uint8 *) UART_RxBuffer, UART_RX_BUFFER_SIZE);
    }

    if (STD_RETURN_OK == retVal)
    {
        /* Clear U1MODE register */
        U1MODE = 0x0000u;

        /* Clear Status and Control Register */
        U1STA = 0x0000u;

        /* Reset Address detect and match register */
        U1ADMD = 0x0000u;

        /* Set High Speed Mode */
        SET_BIT (U1MODE, _U1MODE_BRGH_POSITION);

        /* Set Baud Rate: 250kbaud; Frequency = 16 MHz; U1BRG:=15 */
        U1BRG = 0x000Fu;
        //	 	U1BRG = 0x0003u;

        /* Interrupt configuration */

        /* Set TX interrupt mode 0: Interrupt when a character is transferred to the Transmit Shift Register
         * (this implies there is at least one character open in the transmit buffer) */
        CLR_BIT (U1STA, _U1STA_UTXISEL0_POSITION);
        CLR_BIT (U1STA, _U1STA_UTXISEL1_POSITION);

        /* Set RX interrupt mode 0: Interrupt is set when any character is received and transferred from the RSR to the receive buffer;
         * receive buffer has one or more characters */
        CLR_BIT (U1STA, _U1STA_URXISEL0_POSITION);
        CLR_BIT (U1STA, _U1STA_URXISEL1_POSITION);

        /* Clear interrupt flags */
        CLR_BIT (IFS0, _IFS0_U1TXIF_POSITION);
        CLR_BIT (IFS0, _IFS0_U1RXIF_POSITION);
        CLR_BIT (IFS4, _IFS4_U1ERIF_POSITION);
        CLR_BIT (U1STA, _U1STA_OERR_POSITION);

        /* Set TX interrupt priority to 1 */
        SET_BIT (IPC3, _IPC3_U1TXIP0_POSITION);
        CLR_BIT (IPC3, _IPC3_U1TXIP1_POSITION);
        CLR_BIT (IPC3, _IPC3_U1TXIP2_POSITION);

        /* Enable TX interrupt */
        SET_BIT (IEC0, _IEC0_U1TXIE_POSITION);

        /* Set RX interrupt priority to 2 */
        CLR_BIT (IPC2, _IPC2_U1RXIP0_POSITION);
        SET_BIT (IPC2, _IPC2_U1RXIP1_POSITION);
        CLR_BIT (IPC2, _IPC2_U1RXIP2_POSITION);

        /* Enable RX interrupt */
        SET_BIT (IEC0, _IEC0_U1RXIE_POSITION);

        /* Enable UART peripheral */
        SET_BIT (U1MODE, _U1MODE_UARTEN_POSITION);

        /* Enable UART transmit */
        SET_BIT (U1STA, _U1STA_UTXEN_POSITION);

        /* Enable UART receive */
        SET_BIT (U1STA, _U1STA_URXEN_POSITION);

        UART_State = UART_STATE_INITIALIZED;
    }
    else
    {
        /* Error occured, disable UART */
        CLR_BIT (U1MODE, _U1MODE_UARTEN_POSITION);
        UART_State = UART_STATE_UNINITIALIZED;
    }

    return retVal;
}

Std_ReturnType UART_PutMessage (uint8 *msgPtr, uint16 msgLen)
{
    Std_ReturnType retVal = STD_RETURN_ERROR;
    /* Temporary Variable */
    static uint8 dataVal;
    /* Free Size Variable */
    static uint16 freeSize;
    /* Message Iterator */
    uint16 iterator;
    /* Check Message Pointer not NULL */
    if (msgPtr == NULL)
    {
        retVal = STD_RETURN_ERROR;
    }
    else
    {
        /* Check Free FIFO Size */
        CLR_BIT (IEC0, _IEC0_U1TXIE_POSITION);
        retVal = FIFO_GetFreeSize ((FIFO_DescrType *)&UART_TxFIFO, &freeSize);
        SET_BIT (IEC0, _IEC0_U1TXIE_POSITION);
        if ((STD_RETURN_OK == retVal)&&(freeSize >= msgLen))
        {
            /* Iterate over data bytes */
            for (iterator = 0U; iterator < msgLen; iterator++)
            {
                /* Protect FIFO modification by Tx Interrupt */
                /* Disable Tx Interrupt */
                CLR_BIT (IEC0, _IEC0_U1TXIE_POSITION);
                FIFO_PutData ((FIFO_DescrType*)&UART_TxFIFO, msgPtr[iterator]);
                /* Enable Tx Interrupt*/
                SET_BIT (IEC0, _IEC0_U1TXIE_POSITION);
            }
            /* Sign that no error in operation */
            retVal = STD_RETURN_OK;
            /* Disable Tx Interrupt */
            CLR_BIT (IEC0, _IEC0_U1TXIE_POSITION);
            if (0u == (GET_U1STA () & _U1STA_UTXBF_MASK))
            {
                retVal = FIFO_GetData ((FIFO_DescrType*)&UART_TxFIFO, &dataVal);
                if (STD_RETURN_OK == retVal)
                {
                    /* Put Data to UART Hw FIFO */
                    U1TXREG = dataVal;
                }
                else
                {
                    /* If error occurred break and return */
                    ;
                }
            }
            /* Enable Tx Interrupt*/
            SET_BIT (IEC0, _IEC0_U1TXIE_POSITION);
        }
        else
        {
            retVal = STD_RETURN_ERROR;
        }
    }
    return retVal;
}
#if 0

#else

Std_ReturnType UART_GetMessage (uint8 *msgPtr, uint16 msgLen)
{


    static uint16 freeSize;
    static uint16 avail;
    Std_ReturnType retVal = STD_RETURN_ERROR;
    uint8 i;
    //MCU_DISABLE_INTERRUPTS();

    CLR_BIT (IEC0, _IEC0_U1RXIE_POSITION);
    FIFO_GetFreeSize ((FIFO_DescrType*)&UART_RxFIFO, &freeSize);
    SET_BIT (IEC0, _IEC0_U1RXIE_POSITION);
    //MCU_ENABLE_INTERRUPTS();
    avail = (UART_RX_BUFFER_SIZE - freeSize);
    if (avail >= (msgLen))
    {
        retVal = STD_RETURN_OK;

        for (i = 0u; i < (msgLen); i++)
        {
            //MCU_DISABLE_INTERRUPTS();
            CLR_BIT (IEC0, _IEC0_U1RXIE_POSITION);
            FIFO_GetData ((FIFO_DescrType*)&UART_RxFIFO, &(msgPtr[i]));
            SET_BIT (IEC0, _IEC0_U1RXIE_POSITION);
            //MCU_ENABLE_INTERRUPTS();
        }

    }
    else
    {
        retVal = STD_RETURN_NOT_AVAILABLE;
    }
    return retVal;
}
#endif

Std_ReturnType UART_ClearBuffers (void)
{
    CLR_BIT (IEC0, _IEC0_U1RXIE_POSITION);
    CLR_BIT (IEC0, _IEC0_U1TXIE_POSITION);
    FIFO_ClearFIFO ((FIFO_DescrType*)&UART_RxFIFO);
    SET_BIT (IEC0, _IEC0_U1RXIE_POSITION);
    SET_BIT (IEC0, _IEC0_U1TXIE_POSITION);

    return STD_RETURN_OK;
}
void UART_HandleTxIRQ (void)
{
    static uint8 transmitByte;

    if (!FIFO_IS_EMPTY (UART_TxFIFO))
    {
        if (0u == (GET_U1STA () & (1u << _U1STA_UTXBF_POSITION)))
        {
            FIFO_GetData ((FIFO_DescrType*)&UART_TxFIFO, &transmitByte);
            U1TXREG = transmitByte;
        }
    }
    CLR_BIT (IFS0, _IFS0_U1TXIF_POSITION);
}
void UART_HandleRxIRQ (void)
{
    do
    {

        if (_U1STA_OERR_MASK == (GET_U1STA () & (_U1STA_OERR_MASK)))
        {
            rxByte = U1RXREG;
            /*Reset UART RX Hw Buffer */
            U1STA &= ~(_U1STA_OERR_MASK); //U1STAbits.OERR = 0u;

            GPIO_SetPinValue (BOARD_LED_BLUE_PORT, BOARD_LED_BLUE_PIN, LOW);
            break;
        }
        else if (_U1STA_FERR_MASK == (GET_U1STA () & (_U1STA_FERR_MASK)))
        {
            rxByte = U1RXREG;

        }
        else
        {
            rxByte = U1RXREG;
            FIFO_PutData ((FIFO_DescrType*)&UART_RxFIFO, rxByte);
        }
    } while (_U1STA_URXDA_MASK == (GET_U1STA () & (_U1STA_URXDA_MASK)));
    CLR_BIT (IFS0, _IFS0_U1RXIF_POSITION);
}

void UART_HandleErrIRQ (void)
{

    if (_U1STA_OERR_MASK == (GET_U1STA () & (_U1STA_OERR_MASK)))
    {
        GPIO_SetPinValue (BOARD_LED_BLUE_PORT, BOARD_LED_BLUE_PIN, LOW);
        CLR_BIT (IFS0, _U1STA_OERR_POSITION);
    }
    if (_U1STA_FERR_MASK == (GET_U1STA () & (_U1STA_FERR_MASK)))
    {

    }

    CLR_BIT (IFS4, _IFS4_U1ERIF_POSITION);

}
# if 0 /* Functions for logging */
void UART_Log (const uint8_t c)
{
    /* Put the first byte into the u. tx reg. */
    /* Check UART TX HwBuff is full */
    while ((GET_U1STA () & (1 << _U1STA_UTXBF_POSITION)))
    {
        /* Wait */
        ;
    }
    U1TXREG = c;
}
void UART_LogVoltageAINCH (uint32_t integer)
{
    uint64_t voltage = (((4096UL * (uint64_t) integer) / (7864320UL)));
    uint8_t str[8];
    // str[0] = (voltage % 100000U)/10000U + '0';
    str[0] = (voltage % 10000UL) / 1000UL + '0';
    str[1] = ',';
    str[2] = (voltage % 1000UL) / 100UL + '0';
    str[3] = (voltage % 100UL) / 10UL + '0';
    str[4] = (voltage % 10UL) + '0';
    str[5] = 'V';
    str[6] = '\0';
    UART_LogString (str);
}
#if ((!(defined(SUT)))&&(!(defined(SIT))))
void UART_LogString (const uint8_t * strPtr)
{
    uint8_t i = 0;
    while (strPtr[i] != '\0')
    {
        UART_Log (strPtr[i]);
        i++;
    }
}
#endif

void UART_LogHex (uint32_t integer)
{

    uint8_t LUT[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    uint8_t str[8] = {0U};
    str[0] = LUT[((integer >> 24)& 0x0FUL)];
    str[1] = LUT[((integer >> 20)& 0x0FUL)];
    str[2] = LUT[((integer >> 16)& 0x0FUL)];
    str[3] = LUT[((integer >> 12)& 0x0FUL)];
    str[4] = LUT[((integer >> 8)& 0x0FUL)];
    str[5] = LUT[((integer >> 4)& 0x0FUL)];
    str[6] = LUT[((integer)& 0x0FU)];
    str[7] = '\0';
    UART_LogString (str);
}
void UART_LogInt (uint32_t integer)
{
    uint8_t str[8];
    str[0] = (integer % 10000000UL) / 1000000UL + '0';
    str[1] = (integer % 1000000UL) / 100000UL + '0';
    str[2] = (integer % 100000UL) / 10000UL + '0';
    str[3] = (integer % 10000UL) / 1000UL + '0';
    str[4] = (integer % 1000UL) / 100UL + '0';
    str[5] = (integer % 100UL) / 10UL + '0';
    str[6] = (integer % 10UL) + '0';
    str[7] = '\0';
    UART_LogString (str);
}
#endif
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/** @} */


