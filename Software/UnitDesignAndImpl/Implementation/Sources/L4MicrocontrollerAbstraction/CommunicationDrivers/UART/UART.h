#ifndef UART_H
#define UART_H

/**
 * @file
 *
 * Unit that implements:
 * - UART Driver Interface
 *
 * @version $Id$
 */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */
#include "Std.h"
#include "FIFO.h"
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */

#if COMPILE_FOR_TARGET && !defined SUT
#define UART_HandleTxIRQ IRQ_NO_PSV  InterruptVector(U1TX)
#define UART_HandleRxIRQ IRQ_NO_PSV  InterruptVector(U1RX)
#define UART_HandleErrIRQ IRQ_NO_PSV InterruptVector(U1Err)
#endif


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */
extern volatile FIFO_DescrType UART_RxFIFO;
extern volatile FIFO_DescrType UART_TxFIFO;

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */
/** @fn void UART_Init(void)
 *  @brief UART Driver Initialization with Ring buffered Tx
 *  @retval STD_RETURN_OK: Success
 *  @retval STD_RETURN_ERROR: Error In FIFO Initialization.
 */
Std_ReturnType UART_Init(void);
/** @fn Std_ReturnType UART_PutMessage(uint8 * msgPtr,uint8 msgLen)
 *  @brief Non Blocking UART Write function
 *  @param[in] msgPtr:	Pointer to message cannnot be NULL
 *  @param[in] msgLen:	Message Length
 *  @retval STD_RETURN_OK:
 *  @retval STD_RETURN_ERROR: msgPtr = NULL or not enough space in FIFO or Error in FIFO
 */
Std_ReturnType UART_PutMessage(uint8 *msgPtr, uint16 msgLen);
/** @fn Std_ReturnType UART_GetMessage(uint8 *msgPtr, uint8 *msgLenPtr)
 *  @brief Non Blocking UART read function
 *  @param[in] msgPtr:	Pointer to message cannnot be NULL
 *  @param[in] msgLen:	Message Length
 *  @retval STD_RETURN_NOT_AVAILABLE: No data in SW FIFO
 *  @retval STD_RETURN_OK: Data successfully moved to SW FIFO
 */
Std_ReturnType UART_GetMessage(uint8 *msgPtr, uint16 msgLen);
/** @fn Std_ReturnType UART_ClearBuffers( void )
 *  @brief Clear UART SW FIFO's
 *  @return Always STD_RETURN_OK
 */
Std_ReturnType UART_ClearBuffers( void );
#if COMPILE_FOR_HOST
/** @fn void UART_HandleTxIRQ (void)
 *  @brief UART TX Interrupt Handler
 */
void UART_HandleTxIRQ (void);
/** @fn void UART_HandleRxIRQ (void)
 *  @brief UART RX Interrupt Handler
 *
 */
void UART_HandleRxIRQ (void);
/** @fn void UART_HandleErrIRQ (void)
 *  @brief UART Error Interrupt Handler
 */
void UART_HandleErrIRQ (void);
#endif

# if 0 /* Functions for logging */
/** @fn void UART_Log(const uint8_t c)
 *  @brief UART Blocking 1 byte write function for Logging
 *  @param[in] c:	Byte to send over UART
 */
void UART_Log(const uint8_t c) ;
void UART_LogInt(uint32_t integer);
void UART_LogHex(uint32_t);
void UART_LogVoltageAINCH(uint32_t integer);

#if ((!(defined(SUT)))&&(!(defined(SIT))))
void UART_LogString(const uint8_t * strPtr) ;
#else
#define UART_LogString(_par) (void)(_par)
#endif
#endif

/** @} */

#endif /* UART_H */
