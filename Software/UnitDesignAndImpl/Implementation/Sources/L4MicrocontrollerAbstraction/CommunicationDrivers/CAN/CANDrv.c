/**********************************************************************
* (C) Copyright 2017, OptoForce Ltd.
* -------------------------------------------------------------------
* OPTOFORCE LIMITED PROPRIETARY INFORMATION
*
* Property of OptoForce ltd, Unauthorized reproduction and/or
* distribution is strictly prohibited. This product is protected
* under copyright law and trade secret law as an unpublished
* work
* (C) Copyright OptoForce. All rights reserved.
***********************************************************************/

/**
* @file opto_can.c
* @author Vincze Balazs
* @date 2017. okt. 12.
* @brief
*/

#include <CANDrv.h>
#include <can.h>
#include <Debug.h>
#include <RTIDriver.h>
#include "rti.h"
#include <FIFO.h>
#include "Debug.h"
#define CAN_RX_BUFFER_SIZE (1024u)



static uint32_t CANDrv_TxId = 0u;
static uint32_t CANDrv_RxId = 0u;
FIFO_DescrType CAN_RxRemainderFIFO;
volatile uint8 CAN_RxRemainderBuffer[CAN_RX_BUFFER_SIZE];

typedef struct
{
    uint32_t rxMSGBoxStart;
    uint32_t rxMSGBoxNum;
    uint32_t txMSGBoxStart;
    uint32_t txMSGBoxNum;
    uint32_t txID;
    uint32_t rxID;

    canBASE_t * canRegPtr;
}CANDrv_InstanceType;

CANDrv_InstanceType CANDrv_Handlers[1];


void CANDrv_InitInstance(CANDrv_InstanceID insId,uint32_t CAN_TxId,uint32_t CAN_RxId,uint32_t rxMSGBoxStart,uint32_t rxMSGBoxNum,uint32_t txMSGBoxStart,uint32_t txMSGBoxNum)
{
    CANDrv_InstanceType * insPtr = NULL;
    if(CAN_INS1 == insId)
    {
        CANDrv_TxId = CAN_TxId;
        CANDrv_RxId = CAN_RxId;
        insPtr = &CANDrv_Handlers[CAN_INS1];
        insPtr->canRegPtr = canREG1;
        insPtr->rxMSGBoxStart = rxMSGBoxStart;
        insPtr->rxMSGBoxNum = rxMSGBoxNum;
        insPtr->txMSGBoxStart = txMSGBoxStart;
        insPtr->txMSGBoxNum = txMSGBoxNum;
        insPtr->txID = CAN_TxId;
        insPtr->rxID = CAN_RxId;
    }
    if(NULL != insPtr)
    {

/* USER CODE BEGIN (4) */
/* USER CODE END */
    /** @b Initialize @b CAN1: */

    /** - Setup control register
    *     - Disable automatic wakeup on bus activity
    *     - Local power down mode disabled
    *     - Disable DMA request lines
    *     - Enable global Interrupt Line 0 and 1
    *     - Disable debug mode
    *     - Release from software reset
    *     - Enable/Disable parity or ECC
    *     - Enable/Disable auto bus on timer
    *     - Setup message completion before entering debug state
    *     - Setup normal operation mode
    *     - Request write access to the configuration registers
    *     - Setup automatic retransmission of messages
    *     - Disable error interrupts
    *     - Disable status interrupts
    *     - Enter initialization mode
    */
        insPtr->canRegPtr->CTL = (uint32)0x00000000U
                 | (uint32)0x00000000U
                 | (uint32)((uint32)0x00000005U  << 10U)
                 | (uint32)0x00020043U;

    /** - Clear all pending error flags and reset current status */
        insPtr->canRegPtr->ES |= 0xFFFFFFFFU;

    /** - Assign interrupt level for messages */
        insPtr->canRegPtr->INTMUXx[0U] = (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U;

        insPtr->canRegPtr->INTMUXx[1U] = (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U
                         | (uint32)0x00000000U;

    /** - Setup auto bus on timer period */
        insPtr->canRegPtr->ABOTR = (uint32)0U;

    /** - Initialize message 1
    *     - Wait until IF1 is ready for use
    *     - Set message mask
    *     - Set message control word
    *     - Set message arbitration
    *     - Set IF1 control byte
    *     - Set IF1 message number
    */
    uint32_t i;

    for(i=0u;i<insPtr->rxMSGBoxNum;i++)
    {
     //   print_byte_dec( (insPtr->rxMSGBoxStart + i));
        if(0u == (i%2))
        {
            /*SAFETYMCUSW 28 D MR:NA <APPROVED> "Potentially infinite loop found - Hardware Status check for execution sequence" */
               while ((insPtr->canRegPtr->IF1STAT & 0x80U) ==0x80U)
               {
               } /* Wait */


               insPtr->canRegPtr->IF1MSK  = 0xC0000000U | (uint32)((uint32)((uint32)0x000007FFU & (uint32)0x000007FFU) << (uint32)18U);
               insPtr->canRegPtr->IF1ARB  = (uint32)0x80000000U | (uint32)0x00000000U | (uint32)0x00000000U | (uint32)((uint32)((uint32)insPtr->rxID & (uint32)0x000007FFU) << (uint32)18U);
               insPtr->canRegPtr->IF1MCTL = 0x00001000U | (uint32)0x00000400U | (uint32)0x00000000U | (uint32)0x00000000U | (uint32)8U;
               insPtr->canRegPtr->IF1CMD  = (uint8) 0xF8U;
               insPtr->canRegPtr->IF1NO   = (insPtr->rxMSGBoxStart + i);
        }
        else
        {
            /*SAFETYMCUSW 28 D MR:NA <APPROVED> "Potentially infinite loop found - Hardware Status check for execution sequence" */
              while ((insPtr->canRegPtr->IF2STAT & 0x80U) ==0x80U)
              {
              } /* Wait */

              insPtr->canRegPtr->IF2MSK  = 0xC0000000U | (uint32)((uint32)((uint32)0x000007FFU & (uint32)0x000007FFU) << (uint32)18U);
              insPtr->canRegPtr->IF2ARB  = (uint32)0x80000000U | (uint32)0x00000000U | (uint32)0x00000000U | (uint32)((uint32)((uint32)insPtr->rxID & (uint32)0x000007FFU) << (uint32)18U);
              insPtr->canRegPtr->IF2MCTL = 0x00001000U | (uint32)0x00000400U | (uint32)0x00000000U | (uint32)0x00000000U | (uint32)8U;
              insPtr->canRegPtr->IF2CMD  = (uint8) 0xF8U;
              insPtr->canRegPtr->IF2NO   = (insPtr->rxMSGBoxStart + i);
        }
    }
    for(i=0u;i<insPtr->txMSGBoxNum;i++)
      {
        //print_byte_dec( (insPtr->txMSGBoxStart + i));

          if(0u == (i%2))
          {

              /*SAFETYMCUSW 28 D MR:NA <APPROVED> "Potentially infinite loop found - Hardware Status check for execution sequence" */
              while ((insPtr->canRegPtr->IF1STAT & 0x80U) ==0x80U)
              {
              } /* Wait */

              insPtr->canRegPtr->IF1MSK  = 0xC0000000U | (uint32)((uint32)((uint32)0x000007FFU & (uint32)0x000007FFU) << (uint32)18U);
              insPtr->canRegPtr->IF1ARB  = (uint32)0x80000000U | (uint32)0x00000000U | (uint32)0x20000000U | (uint32)((uint32)((uint32)insPtr->txID & (uint32)0x000007FFU) << (uint32)18U);
              insPtr->canRegPtr->IF1MCTL = 0x00001000U | (uint32)0x00000000U | (uint32)0x00000000U | (uint32)0x00000000U | (uint32)8U;
              insPtr->canRegPtr->IF1CMD  = (uint8) 0xF8U;
              insPtr->canRegPtr->IF1NO   = (insPtr->txMSGBoxStart + i);
          }
          else
          {
              /*SAFETYMCUSW 28 D MR:NA <APPROVED> "Potentially infinite loop found - Hardware Status check for execution sequence" */
                 while ((insPtr->canRegPtr->IF2STAT & 0x80U) ==0x80U)
                 {
                 } /* Wait */

                 insPtr->canRegPtr->IF2MSK  = 0xC0000000U | (uint32)((uint32)((uint32)0x000007FFU & (uint32)0x000007FFU) << (uint32)18U);
                 insPtr->canRegPtr->IF2ARB  = (uint32)0x80000000U | (uint32)0x00000000U | (uint32)0x20000000U | (uint32)((uint32)((uint32)insPtr->txID & (uint32)0x000007FFU) << (uint32)18U);
                 insPtr->canRegPtr->IF2MCTL = 0x00001000U | (uint32)0x00000000U | (uint32)0x00000000U | (uint32)0x00000000U | (uint32)8U;
                 insPtr->canRegPtr->IF2CMD  = (uint8) 0xF8U;
                 insPtr->canRegPtr->IF2NO   = (insPtr->txMSGBoxStart + i);
          }
      }





    /** - Setup IF1 for data transmission
    *     - Wait until IF1 is ready for use
    *     - Set IF1 control byte
    */
    /*SAFETYMCUSW 28 D MR:NA <APPROVED> "Potentially infinite loop found - Hardware Status check for execution sequence" */
    while ((insPtr->canRegPtr->IF1STAT & 0x80U) ==0x80U)
    {
    } /* Wait */
    insPtr->canRegPtr->IF1CMD  = 0x87U;

    /** - Setup IF2 for reading data
    *     - Wait until IF1 is ready for use
    *     - Set IF1 control byte
    */
    /*SAFETYMCUSW 28 D MR:NA <APPROVED> "Potentially infinite loop found - Hardware Status check for execution sequence" */
    while ((insPtr->canRegPtr->IF2STAT & 0x80U) ==0x80U)
    {
    } /* Wait */
    insPtr->canRegPtr->IF2CMD = 0x17U;

    /** - Setup bit timing
    *     - Setup baud rate prescaler extension
    *     - Setup TSeg2
    *     - Setup TSeg1
    *     - Setup sample jump width
    *     - Setup baud rate prescaler
    */
    insPtr->canRegPtr->BTR = (uint32)((uint32)0U << 16U) |
                   (uint32)((uint32)(1U - 1U) << 12U) |
                   (uint32)((uint32)((7U + 1U) - 1U) << 8U) |
                   (uint32)((uint32)(1U - 1U) << 6U) |
                   (uint32)10U;


     /** - CAN1 Port output values */
    insPtr->canRegPtr->TIOC =  (uint32)((uint32)1U  << 18U )
                   | (uint32)((uint32)0U  << 17U )
                   | (uint32)((uint32)0U  << 16U )
                   | (uint32)((uint32)1U  << 3U )
                   | (uint32)((uint32)1U  << 2U )
                   | (uint32)((uint32)1U << 1U );

    insPtr->canRegPtr->RIOC =  (uint32)((uint32)1U  << 18U )
                   | (uint32)((uint32)0U  << 17U )
                   | (uint32)((uint32)0U  << 16U )
                   | (uint32)((uint32)1U  << 3U )
                   | (uint32)((uint32)0U  << 2U )
                   | (uint32)((uint32)0U <<1U );

    /** - Leave configuration and initialization mode  */
    insPtr->canRegPtr->CTL &= ~(uint32)(0x00000041U);

    }
    FIFO_InitDescriptor( &CAN_RxRemainderFIFO,( uint8 *) CAN_RxRemainderBuffer, CAN_RX_BUFFER_SIZE );


}

void CANDriver_Init(uint32_t txId,uint32_t rxId,uint32_t SpeedInBps)
{

/* TODO: Generate Own Can initialisation function */
    CANDrv_TxId = txId;
    CANDrv_RxId = rxId;
    canInit();
    FIFO_InitDescriptor( &CAN_RxRemainderFIFO,( uint8 *) CAN_RxRemainderBuffer, CAN_RX_BUFFER_SIZE );
    //CANDrv_InitInstance();
}

#define CAN_TXRQ_MASK   ((1 << MAX_TX_FRAME)-1)
#define CAN_TXRQ_START  33//32?
uint16_t CANDriver_GetTxFreesize(CANDrv_InstanceID insId)
{


uint8_t msg_iterator=0u;

uint64_t block_flag_curr=0u;



uint64_t       regMaskTx = (uint64_t)((((uint64_t)1ul<<(CANDrv_Handlers[insId].txMSGBoxNum+CANDrv_Handlers[insId].txMSGBoxStart-1))-1ul)&~(uint64_t)(((uint64_t)1ul<<(CANDrv_Handlers[insId].txMSGBoxStart-1))-1ul));
uint64_t        freeSlotCount = 0u;
uint64_t txReq=0ul;


                /* Search Last free messagebox from top id to bottom */
                msg_iterator = CANDrv_Handlers[insId].txMSGBoxStart + CANDrv_Handlers[insId].txMSGBoxNum;  /* Start from MSGBOX 49, second integer(uint32_t) of TXRQ */
                //49 .. 33
                while(msg_iterator > CANDrv_Handlers[insId].txMSGBoxStart)//> 0u)
                {
                    txReq = (uint64_t)((((uint64_t)(CANDrv_Handlers[insId].canRegPtr->TXRQx[1])<<32)|(uint64_t)(CANDrv_Handlers[insId].canRegPtr->TXRQx[0]))&regMaskTx);
                    if(0u == txReq)
                    {
                        freeSlotCount = CANDrv_Handlers[insId].txMSGBoxNum;
                    }
                    block_flag_curr = (uint64_t)((uint64_t)txReq & (uint64_t)((uint64_t)1ul << (msg_iterator - 2u)));   /* Todo generate mask like TXRQ_MASK = ((1 << MAX_TX_FRAME)-1) = 8 bit -> 0x100 - 1 = 0xFF*/

                    if(block_flag_curr) /* If found break with last free */
                    {

                        freeSlotCount = (CANDrv_Handlers[insId].txMSGBoxNum - (msg_iterator -  CANDrv_Handlers[insId].txMSGBoxStart) );
                       break;
                    }
                    msg_iterator--;
                }
                if((block_flag_curr)&&(CANDrv_Handlers[insId].txMSGBoxStart == msg_iterator))
                {
                    //msg_iterator = 1;
                }




     return (freeSlotCount*8);


}
void CANDriver_PutMessage(CANDrv_InstanceID insId,unsigned char* Buffer,uint16_t data_length)
{

uint16_t tx_index = 0u;
uint16_t tx_DLC=0u;
uint8_t msg_iterator=0u;
static uint8_t cnt=0u;
uint64_t block_flag_curr=0u;
uint32_t i=0u;
uint16_t start_len = data_length;

uint64_t       regMaskTx = (uint64_t)((((uint64_t)1ul<<(CANDrv_Handlers[insId].txMSGBoxNum+CANDrv_Handlers[insId].txMSGBoxStart-1))-1ul)&~(uint64_t)(((uint64_t)1ul<<(CANDrv_Handlers[insId].txMSGBoxStart-1))-1ul));

uint64_t txReq=0ul;
    //uint16_t testVal = CANDriver_GetTxFreesize(insId);
    if ( (data_length > 0u) && (data_length<=(CANDrv_Handlers[insId].txMSGBoxNum *8)) )        /* data_length = 1..256*/
    {
        while (data_length>0u)
        {
                /* Search Last free messagebox from top id to bottom */
                msg_iterator = CANDrv_Handlers[insId].txMSGBoxStart + CANDrv_Handlers[insId].txMSGBoxNum;  /* Start from MSGBOX 49, second integer(uint32_t) of TXRQ */
                //49 .. 33
                while(msg_iterator >= CANDrv_Handlers[insId].txMSGBoxStart)//> 0u)
                {
                    txReq = (uint64_t)((((uint64_t)(CANDrv_Handlers[insId].canRegPtr->TXRQx[1])<<32)|(uint64_t)(CANDrv_Handlers[insId].canRegPtr->TXRQx[0]))&regMaskTx);
                    block_flag_curr = (uint64_t)((uint64_t)txReq & (uint64_t)((uint64_t)1ul << (msg_iterator - 2u)));   /* Todo generate mask like TXRQ_MASK = ((1 << MAX_TX_FRAME)-1) = 8 bit -> 0x100 - 1 = 0xFF*/

                    if(block_flag_curr) /* If found break with last free */
                    {

                       break;
                    }
                    msg_iterator--;
                }
                if((block_flag_curr)&&(CANDrv_Handlers[insId].txMSGBoxStart == msg_iterator))
                {
                    //msg_iterator = 1;
                }

                /* Fill Free area from bottom to top */
                while(msg_iterator<=(CANDrv_Handlers[insId].txMSGBoxStart + CANDrv_Handlers[insId].rxMSGBoxNum))
                {
                    if (data_length>=8) {tx_DLC = 8;} else {tx_DLC = data_length;}
                    while ((CANDrv_Handlers[insId].canRegPtr->IF1STAT & 0x80U) ==0x80U)
                    {
                    } /* Wait */
                    CANDrv_Handlers[insId].canRegPtr->IF1ARB  = (uint32)0x80000000U | (uint32)0x00000000U | (uint32)0x20000000U | (uint32)((uint32)((uint32)(CANDrv_TxId) & (uint32)0x000007FFU) << (uint32)18U);
                    CANDrv_Handlers[insId].canRegPtr->IF1MCTL = 0x00001000U | (uint32)0x00000000U | (uint32)0x00000000U | (uint32)0x00000000U | (uint32)tx_DLC;
                    CANDrv_Handlers[insId].canRegPtr->IF1CMD  = (uint8) 0xF8U;
                    CANDrv_Handlers[insId].canRegPtr->IF1NO   = (msg_iterator);

                    while ((CANDrv_Handlers[insId].canRegPtr->IF1STAT & 0x80U) ==0x80U)
                      {
                      } /* Wait */

                      /** - Configure IF1 for
                      *     - Message direction - Write
                      *     - Data Update
                      *     - Start Transmission
                      */
                    CANDrv_Handlers[insId].canRegPtr->IF1CMD = 0x87U;

                      /** - Copy TX data into IF1 */
                      for (i = 0U; i < tx_DLC; i++)
                      {
                          /*SAFETYMCUSW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
                          CANDrv_Handlers[insId].canRegPtr->IF1DATx[i] = Buffer[tx_index];
                          /*SAFETYMCUSW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
                          /*SAFETYMCUSW 567 S MR:17.1,17.4 <APPROVED> "Pointer increment needed" */
                          tx_index++;
                      }
                      /** - Copy TX data into message box */
                      /*SAFETYMCUSW 93 S MR: 6.1,6.2,10.1,10.2,10.3,10.4 <APPROVED> "LDRA Tool issue" */
                      CANDrv_Handlers[insId].canRegPtr->IF1NO = (uint8) (msg_iterator);
                      //tx_index+=tx_DLC;
                      data_length-=tx_DLC;
                      if(data_length==0u)break;
                      msg_iterator++;
                }



        }
        if(data_length > 0u)
       {
           PUTS("Nem fert el!");
           NEWLINE();
           PUTS("FLG=");
           print_word_hex(  block_flag_curr);
           NEWLINE();
           PUTS("CNT=");

           print_byte_hex(cnt);
           PUTS("LEN=");
           print_num_dec(data_length);
           NEWLINE();

           PUTS("START_LEN=");
           print_num_dec(start_len);
           NEWLINE();
           PUTS("TX_INDX=");
           print_num_dec(tx_index);

           while(1);
       }
    }
    else
    {
        PUTS("BIGGER!!");
        while(1);
    }



    cnt++;
}
Std_ReturnType CANDriver_GetMessage(CANDrv_InstanceID insId,uint8_t * dataPtr,uint16_t dataLen)
{
    uint16_t bufDatIndx =0u;
    static uint16_t freeSize =0u;
    Std_ReturnType retVal   =   E_NOT_OK;

    static uint8_t tempData[16*8] = {0u};
    uint16_t avail = 0u;
    uint16_t index;

    index = 0u;
    avail = CANDrv_ReadFullBuffer(CAN_INS1,tempData);
  /*  PUTS("Buffer:");
    print_num_dec(avail);
    PUTCHAR('\r');

    PUTCHAR('\n');*/
    while(index < avail)
    {
        FIFO_PutData(&CAN_RxRemainderFIFO, tempData[index]);
        index++;
    }

    FIFO_GetFreeSize( &CAN_RxRemainderFIFO, &freeSize );
 /*   PUTS("Free:");
    print_num_dec(freeSize);
     PUTCHAR('\r');

     PUTCHAR('\n');*/
    if ( (CAN_RX_BUFFER_SIZE - freeSize)>= dataLen)
    {
        retVal = E_OK;

        for(bufDatIndx=0u;bufDatIndx<dataLen;bufDatIndx++)
        {

            FIFO_GetData(&CAN_RxRemainderFIFO,&dataPtr[bufDatIndx]);

        }
    }
    else
    {
        retVal = E_NOT_OK;
    }
    return (retVal);
}
/** @fn int CAN_Read_Buffer(unsigned char* Buffer)
 * @brief Function to recive bytes from CAN
 * @param Buffer Receive Buffer
 * @return Number of recived bytes
 */
int CANDrv_ReadFullBuffer(CANDrv_InstanceID insId,unsigned char* Buffer)
{
uint32_t buffer_len = 0u;

/* Declarations from TI Driver */
uint32       i;
uint32       j;
uint32_t        k=0;
uint64_t        n=0;
uint32       size=0;


uint64_t       regMask = (uint64_t)((((uint64_t)1ul<<(CANDrv_Handlers[insId].rxMSGBoxNum+CANDrv_Handlers[insId].rxMSGBoxStart-1))-1ul)&~(uint64_t)(((uint64_t)1ul<<(CANDrv_Handlers[insId].rxMSGBoxStart-1))-1ul));

uint64_t       nwDat = 0x00ul;
    for(i=1u;i<(CANDrv_Handlers[insId].rxMSGBoxNum+1u);i++)        /* Check number of valid frames */
    {

        nwDat = ((((uint64_t)(CANDrv_Handlers[insId].canRegPtr->NWDATx[1])<<32)|(uint64_t)(CANDrv_Handlers[insId].canRegPtr->NWDATx[0]))&regMask);

        if((k=((nwDat)&(1ul << i-1))))
        {
            if(i==1)
            {
                nwDat = ((((uint64_t)(CANDrv_Handlers[insId].canRegPtr->NWDATx[1])<<32)|(uint64_t)(CANDrv_Handlers[insId].canRegPtr->NWDATx[0]))&regMask);
                n=nwDat;
            }
            /** - Wait until IF2 is ready for use */
                         /*SAFETYMCUSW 28 D MR:NA <APPROVED> "Potentially infinite loop found - Hardware Status check for execution sequence" */
                         while ((CANDrv_Handlers[insId].canRegPtr->IF2STAT & 0x80U) ==0x80U)
                          {
                          } /* Wait */

                            /** - Configure IF2 for
                            *     - Message direction - Read
                            *     - Data Read
                            *     - Clears NewDat bit in the message object.
                            */
                         CANDrv_Handlers[insId].canRegPtr->IF2CMD = 0x17U;

                            /** - Copy data into IF2 */
                            /*SAFETYMCUSW 93 S MR: 6.1,6.2,10.1,10.2,10.3,10.4 <APPROVED> "LDRA Tool issue" */
                         CANDrv_Handlers[insId].canRegPtr->IF2NO = (uint8) i;

                            /** - Wait until data are copied into IF2 */
                            /*SAFETYMCUSW 28 D MR:NA <APPROVED> "Potentially infinite loop found - Hardware Status check for execution sequence" */
                            while ((CANDrv_Handlers[insId].canRegPtr->IF2STAT & 0x80U) ==0x80U)
                            {
                            } /* Wait */

                            /** - Get number of received bytes
                            *   - Value from 0x8 to 0xF equals length 8.
                            */
                            size = CANDrv_Handlers[insId].canRegPtr->IF2MCTL & 0xFU;
                            if(size > 0x8U)
                            {
                                size = 0x8U;
                            }

                            /** - Copy RX data into destination buffer */
                            for (j = 0U; j < size; j++)
                            {

                                /*SAFETYMCUSW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
                                Buffer[ buffer_len] = CANDrv_Handlers[insId].canRegPtr->IF2DATx[j];
                                /*SAFETYMCUSW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
                                /*SAFETYMCUSW 567 S MR:17.1,17.4 <APPROVED> "Pointer increment needed" */
                                buffer_len++;
                            }

                        /** - Check if data have been lost:
                        *     - no data lost, return 1
                        *     - data lost, return 3
                        */
                        if ((CANDrv_Handlers[insId].canRegPtr->IF2MCTL & 0x4000U) == 0x4000U)
                        {
                            PUTS("\r\nERROR DATA LOST!!!!\r\n");
                            while(1)
                            {

                            }
                        }

           // frame_num++;


       // PUTCHAR('+');
      //  print_byte_dec(frame_num);
     }/*  END for(i=1;i<(MAX_FRAME_NUM+1);i++)  */
     else
     {
         break;
     }

           /*  if( ((i<(MAX_FRAME_NUM+1u)) && (canIsRxMessageArrived(canREG1, (i +1)))))
             {
                 PUTS("\r\nERROR MEGSZALADT A FIFO!!!!!!!!!!\r\n");
                 PUTS("MSGBOXID=");
                 print_byte_dec(i);
                 PUTS("NEXT MSGBOXID=");
                                 print_byte_dec(i+1);
                 while(1)
                 {
                 }
             }*/

     }/* END     for(i=0;i<frame_num;i++) */
    nwDat = ((((uint64_t)(CANDrv_Handlers[insId].canRegPtr->NWDATx[1])<<32)|(uint64_t)(CANDrv_Handlers[insId].canRegPtr->NWDATx[0]))&regMask);
   if((nwDat>n)&&(size>0))
   {
       PUTS("Error RX DATA LOST");
       while(1);
   }
return (buffer_len);
}
void canMessageNotification(canBASE_t *node, uint32 messageBox) /* TODO: This is only for test frame check remove future! */
{
  /*  PUTCHAR('\t');
    PUTCHAR('*');
    sciSendByte(scilinREG, ((uint8_t)messageBox +'0'));
    sciSendByte(scilinREG, '\r');
    sciSendByte(scilinREG, '\n');*/
#ifdef TEST_CAN
    obj_id[cnt]=(uint8_t)messageBox;
    cnt++;
#endif
}

