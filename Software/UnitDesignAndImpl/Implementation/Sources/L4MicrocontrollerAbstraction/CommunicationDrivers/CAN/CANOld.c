/**
 * @file
 *
 * Unit that manages:
 * - .....
 * - .........
 *
 * @version $Id$
 */
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */
#include "CAN.h"

#include "Std.h"
#include "FIFO.h"



/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticDefines Static defines
 */

#define CAN_RX_BUFFER_SIZE (1024u)

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */
/** @enum CANDrv_StateType
*	@brief Enum State of module initialization.
*/
typedef enum
{
    CAN_STATE_UNINITIALISED 	= 0u,
    CANDRV_STATE_INITIALISED 	= 1u
}CAN_StateType;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */
/** @var static CANDrv_StateType CANDrv_State
	@brief State of module initialization.
*/
static CAN_StateType CAN_State = CAN_STATE_UNINITIALISED;
static uint8_t CAN1MessageFifoArea[2 * 32 * 16];
static uint32_t CAN_TxId;
static uint32_t CAN_RxId;
FIFO_DescrType CAN_RxRemainderFIFO;
volatile uint8 CAN_RxRemainderBuffer[CAN_RX_BUFFER_SIZE];
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */
/** @fn void CANDrv_Init(void)
*/

/** @fn void CAN_Init(uint32_t rxId,uint32_t SpeedInHz)
 *
 * 	@param rxId
 * 	@param SpeedInHz
 */
void CAN_Init(uint32_t txId,uint32_t rxId,uint32_t SpeedInBps)
{
    static CAN_BIT_CONFIG canBitConfig;
    /* Save TX and RX ID to local variables */
    CAN_TxId = txId;
    CAN_RxId = rxId;
    /* Enable CAN Transciever */
    CAN_Stdby_Off();
    /* Enable Can peripheral */
    CANEnableModule(CAN1,TRUE);
    /* Enter Configuration Mode */
    CANSetOperatingMode(CAN1, CAN_CONFIGURATION);
    while(CAN_CONFIGURATION != CANGetOperatingMode(CAN1))
    {
        ;/* Wait */
    }

    canBitConfig.phaseSeg2Tq            = CAN_BIT_3TQ;
    canBitConfig.phaseSeg1Tq            = CAN_BIT_3TQ;
    canBitConfig.propagationSegTq       = CAN_BIT_3TQ;
    canBitConfig.phaseSeg2TimeSelect    = TRUE;
    canBitConfig.sample3Time            = TRUE;
    canBitConfig.syncJumpWidth          = CAN_BIT_2TQ;

    CANSetSpeed(CAN1, &canBitConfig, SYS_FREQ, SpeedInBps);

    CANAssignMemoryBuffer(CAN1, CAN1MessageFifoArea, (2 * 32 * 16));

    CANConfigureChannelForTx(CAN1, CAN_CHANNEL0, 32, CAN_TX_RTR_DISABLED, CAN_LOW_MEDIUM_PRIORITY);
    CANConfigureChannelForRx(CAN1, CAN_CHANNEL1, 32, CAN_RX_FULL_RECEIVE);

    CANConfigureFilter      (CAN1, CAN_FILTER0, CAN_RxId, CAN_SID);
    CANConfigureFilterMask  (CAN1, CAN_FILTER_MASK0, 0x7FF, CAN_SID, CAN_FILTER_MASK_IDE_TYPE);
    CANLinkFilterToChannel  (CAN1, CAN_FILTER0, CAN_FILTER_MASK0, CAN_CHANNEL1);
    CANEnableFilter         (CAN1, CAN_FILTER0, TRUE);
    /* Return from Configuration Mode */
    CANSetOperatingMode(CAN1, CAN_NORMAL_OPERATION);
    while(CANGetOperatingMode(CAN1) != CAN_NORMAL_OPERATION)
    {
        ;/* Wait */
    }
    FIFO_InitDescriptor( &CAN_RxRemainderFIFO,( uint8 *) CAN_RxRemainderBuffer, CAN_RX_BUFFER_SIZE );
    CAN_State = CANDRV_STATE_INITIALISED;
    return;

}
/** @fn Std_ReturnType CAN_SendFrame(uint8_t * dataPtr)
 *
 *
 * @param dataPtr
 * @param dataLen
 * @return
 */
Std_ReturnType CAN_SendFrame(uint8_t * dataPtr)
{
    Std_ReturnType retVal = STD_RETURN_ERROR;
    uint8_t i;
    static CANTxMessageBuffer * messagePtr;
    messagePtr = CANGetTxMessageBuffer(CAN1, CAN_CHANNEL0);
    if((NULL != messagePtr)&&(NULL != dataPtr))
    {
        /* Clear Message Descriptor */
        messagePtr->messageWord[0] = 0u;
        messagePtr->messageWord[1] = 0u;
        messagePtr->messageWord[2] = 0u;
        messagePtr->messageWord[3] = 0u;
        /* Setup Frame */
        messagePtr->msgSID.SID 	= CAN_TxId;
        messagePtr->msgEID.IDE 	= 0u;
        messagePtr->msgEID.DLC 	= 8u;
        /* Copy data */
        for(i=0u;i<8u;i++)
        {
            messagePtr->data[i] = dataPtr[i];
        }

        retVal = STD_RETURN_OK;
    }

    CANUpdateChannel(CAN1,CAN_CHANNEL0);
    CANFlushTxChannel(CAN1,CAN_CHANNEL0);
    return (retVal);
}
/** @fn Std_ReturnType CAN_PutMessage(uint8_t * dataPtr,uint16_t dataLen)
 *
 * @param dataPtr
 * @param dataLen
 */
Std_ReturnType CAN_PutMessage(uint8_t * dataPtr,uint16_t dataLen)
{
    Std_ReturnType retVal= STD_RETURN_ERROR;
    static CANTxMessageBuffer * messagePtr;
    static uint8_t txDLC;
    static uint16_t bufDatIndx;
    static uint8_t frmDatIndx;
    uint8_t iterator=0u;
    bufDatIndx = 0u;
    if ((NULL != dataPtr)&& (dataLen > 0u) && (dataLen<=256u) ) // data_length = 1..256
    {
        while( iterator<32u)// (dataLen>0u)
        {
            /* Truncate DLC to 8 */
            txDLC = (dataLen>=8u) ? 8u: dataLen;
            messagePtr = CANGetTxMessageBuffer(CAN1,CAN_CHANNEL0);
            iterator++;
            if(NULL != messagePtr)
            {
                /* Setup Frame */
                messagePtr->msgSID.SID 	= CAN_TxId;
                messagePtr->msgEID.IDE 	= 0u;
                messagePtr->msgEID.DLC 	= txDLC;
                frmDatIndx = 0u;
                /* Copy data to frame from dataPtr's current position */
                while(frmDatIndx<txDLC)
                {
                    messagePtr->data[frmDatIndx] = dataPtr  [bufDatIndx];
                    frmDatIndx++;
                    bufDatIndx++;
                }
                   dataLen-=txDLC;
                  
            }
          
            CANUpdateChannel(CAN1,CAN_CHANNEL0);
             if(0u == dataLen)
             {
                 retVal = STD_RETURN_OK;      
                 break;
                       
             }
         
        }
        CANFlushTxChannel(CAN1,CAN_CHANNEL0);
        
    }
    return (retVal);
}
/** @fn uint16_t CAN_GetMessage(uint8_t * dataPtr,uint16_t dataLen)
 *
 * @param dataPtr
 * @param dataLen
 * @return
 */
Std_ReturnType CAN_GetMessage(uint8_t * dataPtr,uint16_t dataLen)
{
    uint16_t bufDatIndx =0u;
    uint16_t freeSize =0u;
    Std_ReturnType retVal	=	STD_RETURN_ERROR;
    uint8_t rxDLC = 0u;
    static CANRxMessageBuffer * messagePtr = NULL;
    uint8_t frmDatIndx;
    uint8_t iterator=0u;
    /* First Get Remainder bytes from last transmission from SW FIFO */
    /*  while((0u == (FIFO_IS_EMPTY(CANDrv_RxRemainderFIFO))))
      {
      	FIFO_GetData(&CANDrv_RxRemainderFIFO,&dataPtr[bufDatIndx]);
      	bufDatIndx++;
      	if(( bufDatIndx>=dataLen))
      	{
      		retVal = STD_RETURN_OK;
      	}
      }*/
    while ( iterator<32u)
    {
        messagePtr = CANGetRxMessage(CAN1,CAN_CHANNEL1);
        if(NULL != messagePtr)
        {
            rxDLC = messagePtr->msgEID.DLC;
            if (rxDLC>0u)
            {
                retVal = STD_RETURN_OK;
                frmDatIndx = 0;
                while(frmDatIndx<rxDLC)
                {

                
                    
                        /* Save it for next call */
                        retVal = FIFO_PutData(&CAN_RxRemainderFIFO,messagePtr->data[frmDatIndx]);
                        if(STD_RETURN_ERROR == retVal)
                        {
                            retVal = STD_RETURN_ERROR;
                        }
                    
                    frmDatIndx++;
                    bufDatIndx++;

                }
            }

            CANUpdateChannel(CAN1, CAN_CHANNEL1);
        }
        else
        {
        
            break;
        }
        iterator++;
    }
    FIFO_GetFreeSize( &CAN_RxRemainderFIFO, &freeSize );
    if ( (1024u - freeSize)>= dataLen)
    {
        retVal = STD_RETURN_OK;

        for(bufDatIndx=0u;bufDatIndx<dataLen;bufDatIndx++)
        {

            FIFO_GetData(&CAN_RxRemainderFIFO,&dataPtr[bufDatIndx]);

        }
    }
    else
    {
        retVal = STD_RETURN_NOT_AVAILABLE;
    }
    return (retVal);
}
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/** @} */


