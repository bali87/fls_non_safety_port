#ifndef Onrobot_RG2FTBackpack_Host_CANDrv_H
#define Onrobot_RG2FTBackpack_Host_CANDrv_H
#if 0
/**
 * @file
 *
 * Unit that manages:
 * - .....
 * - .........
 *
 * @version $Id$
 */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */
#include "Std.h"
#include "MCU.h"

/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */
#define CAN_Stdby_On()    LATESET=1
#define CAN_Stdby_Off()   LATECLR=1


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */

void CAN_Init(uint32_t txId,uint32_t rxId,uint32_t SpeedInHz);
Std_ReturnType CAN_SendFrame(uint8_t * dataPtr);
Std_ReturnType CAN_PutMessage(uint8_t * dataPtr,uint16_t dataLen);
Std_ReturnType CAN_GetMessage(uint8_t * dataPtr,uint16_t dataLen);
/** @} */
#endif
#endif /* Onrobot_RG2FTBackpack_Host_CANDrv_H */
