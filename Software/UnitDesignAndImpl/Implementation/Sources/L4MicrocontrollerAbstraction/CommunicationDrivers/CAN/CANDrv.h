/* 
 * File:   can.h
 * Author: Pinkoczi Gaspar
 *
 * Created on 2017. janu�r 25., 12:02
 */

#ifndef CAN_H
#define	CAN_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "Std.h"
#include <inttypes.h>

#ifdef ORIGINAL_FW
#define CAN_Stdby_On    LATESET=1;
#define CAN_Stdby_Off   LATECLR=1;
#define CAN1_BRPVAL     0x1

#define LO_Low(param)  ((char *)&param)[0]
#define LO_High(param) ((char *)&param)[1]
#define HI_Low(param)  ((char *)&param)[2]
#define HI_High(param) ((char *)&param)[3]   
#endif




typedef enum
{
    CAN_INS1,
    CAN_INS_CNT
}CANDrv_InstanceID;



#define CAN_MAX_RX_FRAME 16u
#define CAN_MAX_TX_FRAME 16u

extern unsigned int NODE_ID_RX;
extern unsigned int NODE_ID_TX; 

/**
 *
 * @param RX_ID
 * @param SpeedInHz
 */
void CANDriver_Init(uint32_t txId,uint32_t rxId,uint32_t SpeedInBps);


/**
 * 
 * @param tx_ID
 * @param Buffer
 * @param data_length
 */
void CANDriver_PutMessage(CANDrv_InstanceID insId,unsigned char* Buffer,uint16_t data_length);

Std_ReturnType CANDriver_GetMessage(CANDrv_InstanceID insId,uint8_t * dataPtr,uint16_t dataLen);
/**
 * 
 * @param Buffer
 * @return
 */
int CANDrv_ReadFullBuffer(CANDrv_InstanceID insId,unsigned char* Buffer);

void CANDrv_InitInstance(CANDrv_InstanceID insId,uint32_t CAN_TxId,uint32_t CAN_RxId,uint32_t rxMSGBoxStart,uint32_t rxMSGBoxNum,uint32_t txMSGBoxStart,uint32_t txMSGBoxNum);

#ifdef	__cplusplus
}
#endif

#endif	/* CAN_H */

