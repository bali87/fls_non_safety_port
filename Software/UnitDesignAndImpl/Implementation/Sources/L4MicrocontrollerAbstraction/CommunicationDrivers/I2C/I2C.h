#ifndef _I2C_H_
#define _I2C_H_

/**
 * @file
 * 
 * Header file of the unit ${FILENAME}. //fixme remove .h
 *
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */
#if COMPILE_FOR_TARGET && !defined SUT
#define I2C_HandleIRQ IRQ_NO_PSV  InterruptVector(MI2C2)
#endif

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */

typedef enum I2C_MessageStatusType_tag
{
    I2C_MESSAGE_FAIL,
    I2C_MESSAGE_PENDING,
    I2C_MESSAGE_COMPLETE,
    I2C_STUCK_START,
    I2C_MESSAGE_ADDRESS_NO_ACK,
    I2C_DATA_NO_ACK,
    I2C_LOST_STATE
} I2C_MessageStatusType;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */
 


/**
 * @addtogroup GlobalVaribles Global variables
 */
 


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */
/** @fn Std_ReturnType I2C_Init( void )
* @brief
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType I2C_Init( void );
/** @fn Std_ReturnType I2C_MasterWrite( uint8 *dataPtr, uint8 dataLen, uint16 address, I2C_MessageStatusType *statusPtr )
 *
 * @param dataPtr
 * @param dataLen
 * @param address
 * @param statusPtr
 * @return In case of any any error, it returns the error code (not implemented yet)
 */
Std_ReturnType I2C_MasterWrite( uint8 *dataPtr, uint8 dataLen, uint16 address, I2C_MessageStatusType *statusPtr );
/** @fn Std_ReturnType I2C_MasterRead( uint8 *dataPtr, uint8 dataLen, uint16 address, I2C_MessageStatusType *statusPtr )
 *
 * @param dataPtr
 * @param dataLen
 * @param address
 * @param statusPtr
 * @return In case of any any error, it returns the error code (not implemented yet)
 */
Std_ReturnType I2C_MasterRead( uint8 *dataPtr, uint8 dataLen, uint16 address, I2C_MessageStatusType *statusPtr );
/** @fn Std_BoolType I2C_MasterQueueIsEmpty( void )
* @brief
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_BoolType I2C_MasterQueueIsEmpty( void );
/** @fn Std_BoolType I2C_MasterQueueIsFull( void )
* @brief
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_BoolType I2C_MasterQueueIsFull( void );
/** @fn uint8 I2C2_ErrorCountGet( void )
* @brief
* @return In case of any any error, it returns the error code (not implemented yet)
*/
uint8 I2C_ErrorCountGet( void );

#if COMPILE_FOR_HOST
/**
 *
 */
void I2C_HandleIRQ ( void );
#endif

/** @} */

#endif /* I2C_H */
