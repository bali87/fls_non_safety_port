
/**
 * @file
 *
 * Unit that manages the computation of the system delay /Unit that provides library functions for computation of delay.
 * fixme update the comment.
 *
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */

#include "Std.h"
#include "MCU.h"
#include "SysTimer.h"
#include "I2C.h"




/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticDefines Static defines
 */

#define I2C_CONFIG_TR_QUEUE_LENGTH             1u

#define I2C_TRANSMIT_REG                       I2C2TRN                 // Defines the transmit register used to send data.
#define I2C_RECEIVE_REG                        I2C2RCV                 // Defines the receive register used to receive data.

// The following control bits are used in the I2C state machine to manage
// the I2C module and determine next states.
#define I2C_WRITE_COLLISION_STATUS_BIT         I2C2STATbits.IWCOL       // Defines the write collision status bit.
#define I2C_ACKNOWLEDGE_STATUS_BIT             I2C2STATbits.ACKSTAT     // I2C ACK status bit.

#define I2C_START_CONDITION_ENABLE_BIT         I2C2CONLbits.SEN         // I2C START control bit.
#define I2C_REPEAT_START_CONDITION_ENABLE_BIT  I2C2CONLbits.RSEN        // I2C Repeated START control bit.
#define I2C_RECEIVE_ENABLE_BIT                 I2C2CONLbits.RCEN        // I2C Receive enable control bit.
#define I2C_STOP_CONDITION_ENABLE_BIT          I2C2CONLbits.PEN         // I2C STOP control bit.
#define I2C_ACKNOWLEDGE_ENABLE_BIT             I2C2CONLbits.ACKEN       // I2C ACK start control bit.
#define I2C_ACKNOWLEDGE_DATA_BIT               I2C2CONLbits.ACKDT       // I2C ACK data control bit.

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */

typedef struct I2C_TransactionRequestBlockType_tag
{
    uint16  address;    /* Bits <10:1> are the 10 bit address. */
    /* Bits <7:1> are the 7 bit address */
    /* Bit 0 is R/W (1 for read) */
    uint8   length;     /* the # of bytes in the buffer */
    uint8  *pbuffer;    /* a pointer to a buffer of length bytes */
}
I2C_TransactionRequestBlockType;

typedef union I2C_TransactionQueueStatusType_tag
{
    struct
    {
uint8 full:
        1;
uint8 empty:
        1;
uint8 reserved:
        6;
    }
    statusBitField;
    uint8 statusUint8;
} I2C_TransactionQueueStatusType;

typedef struct I2C_TransactionQueueEntryType_tag
{
    uint8                               count;        /* a count of trb's in the trb list */
    I2C_TransactionRequestBlockType    *ptrb_list;    /* pointer to the trb list */
    I2C_MessageStatusType              *pTrFlag;      /* set with the error of the last trb sent. */
    /* if all trb's are sent successfully, */
    /* then this is I2C_MESSAGE_COMPLETE */
}
I2C_TransactionQueueEntryType;

typedef struct I2C_ObjectType_tag
{
    I2C_TransactionQueueEntryType      *pTrTail;      /* tail of the queue */
    I2C_TransactionQueueEntryType      *pTrHead;      /* head of the queue */
    I2C_TransactionQueueStatusType      trStatus;     /* status of the last transaction */
    Std_BoolType                        i2cDoneFlag;  /* flag to indicate the current transaction is done */
    uint8                               i2cErrors;    /* keeps track of errors */
}
I2C_ObjectType;

typedef enum I2C_MasterStateType_tag
{
    I2C_MASTER_STATE_IDLE,
    I2C_MASTER_STATE_RESTART,
    I2C_MASTER_STATE_SEND_ADDR,
    I2C_MASTER_STATE_SEND_DATA,
    I2C_MASTER_STATE_SEND_STOP,
    I2C_MASTER_STATE_ACK_ADDR,
    I2C_MASTER_STATE_RCV_DATA,
    I2C_MASTER_STATE_RCV_STOP,
    I2C_MASTER_STATE_ACK_RCV_DATA,
    I2C_MASTER_STATE_NOACK_STOP,
    I2C_MASTER_STATE_SEND_ADDR_10BIT_LSB,
    I2C_MASTER_STATE_10BIT_RESTART,

} I2C_MasterStateType;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */

static I2C_TransactionQueueEntryType    i2c2_tr_queue[ I2C_CONFIG_TR_QUEUE_LENGTH ];
static I2C_ObjectType                   i2c2_object;
//static I2C_MasterStateType              i2c2_state = I2C_MASTER_STATE_IDLE;
I2C_MasterStateType              i2c2_state = I2C_MASTER_STATE_IDLE;
static uint8                            i2c2_trb_count;
static I2C_TransactionRequestBlockType *p_i2c2_trb_current;
static I2C_TransactionQueueEntryType   *p_i2c2_current = NULL;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */
/**
 *
 * @param trbCount
 * @param trbListPtr
 * @param flagPtr
 * @return
 */
static Std_ReturnType I2C_MasterTRBInsert( uint8 trbCount, I2C_TransactionRequestBlockType *trbListPtr, I2C_MessageStatusType *flagPtr );
/**
 *
 * @param trbPtr
 * @param dataPtr
 * @param dataLen
 * @param address
 * @return
 */
static Std_ReturnType I2C_MasterReadTRBBuild( I2C_TransactionRequestBlockType *trbPtr, uint8 *dataPtr, uint8 dataLen, uint16 address );
/**
 *
 * @param trbPtr
 * @param DataPtr
 * @param dataLen
 * @param address
 * @return
 */
static Std_ReturnType I2C_MasterWriteTRBBuild( I2C_TransactionRequestBlockType *trbPtr, uint8 *DataPtr, uint8 dataLen, uint16 address );
/**
 *
 * @return
 */
static Std_ReturnType I2C_FunctionComplete( void );
/**
 *
 * @param completionCode
 * @return
 */
static Std_ReturnType I2C_Stop( I2C_MessageStatusType completionCode );


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */


Std_ReturnType I2C_Init( void )
{
    Std_ReturnType retVal = STD_RETURN_ERROR;

    i2c2_object.pTrHead = i2c2_tr_queue;
    i2c2_object.pTrTail = i2c2_tr_queue;
    i2c2_object.trStatus.statusBitField.empty = STD_BOOL_TRUE;
    i2c2_object.trStatus.statusBitField.full = STD_BOOL_FALSE;

    i2c2_object.i2cErrors = 0;

    // initialize the hardware
    // Baud Rate Generator Value: I2CBRG 78d, 4Eh - 100kHz;
    // Baud Rate Generator Value: I2CBRG 18d, 12h - 400kHz;
    //    I2C2BRG = 0x004e;
    I2C2BRG = 0x0012;
    // ACKEN disabled; STRICT disabled; STREN disabled; GCEN disabled; SMEN disabled; DISSLW disabled; I2CSIDL disabled; ACKDT Sends ACK; SCLREL Holds; RSEN disabled; A10M 7 Bit; PEN disabled; RCEN disabled; SEN disabled; I2CEN enabled;
    //    I2C2CONL = 0x8200;
    // ACKEN disabled; STRICT disabled; STREN disabled; GCEN disabled; SMEN disabled; DISSLW enabled; I2CSIDL disabled; ACKDT Sends ACK; SCLREL Holds; RSEN disabled; A10M 7 Bit; PEN disabled; RCEN disabled; SEN disabled; I2CEN enabled;
    I2C2CONL = 0x8000;
    // BCL disabled; D_nA disabled; R_nW disabled; P disabled; S disabled; I2COV disabled; IWCOL disabled;
    // SDA hold time = 300ns;
    I2C2CONH = 0x0008;
    I2C2STAT = 0x0000;

    /* MI2C2 - I2C2 Master Events */
    // clear the master interrupt flag
    IFS3bits.MI2C2IF = 0;
    // enable the master interrupt
    IEC3bits.MI2C2IE = 1;

    retVal = STD_RETURN_OK;
    return retVal;
}


uint8 I2C_ErrorCountGet( void )
{
    uint8 retVal;

    retVal = i2c2_object.i2cErrors;
    return retVal;
}


Std_ReturnType I2C_MasterWrite( uint8 *dataPtr, uint8 dataLen, uint16 address, I2C_MessageStatusType *statusPtr )
{
    Std_ReturnType retVal = STD_RETURN_ERROR;
    static I2C_TransactionRequestBlockType I2C_transactionRequestBlock;

    // check if there is space in the queue
    if ( i2c2_object.trStatus.statusBitField.full != STD_BOOL_TRUE )
    {
        I2C_MasterWriteTRBBuild( &I2C_transactionRequestBlock, dataPtr, dataLen, address );
        I2C_MasterTRBInsert( 1u, &I2C_transactionRequestBlock, statusPtr );
    }
    else
    {
        *statusPtr = I2C_MESSAGE_FAIL;
    }

    //    SysTimer_StartTimer( SYS_TIMER_TEST_DELAY, SYS_TIMER_ONE_SHOT );
    //    while ( SYS_TIMER_IS_EXPIRED != SysTimer_GetTimerStatus( SYS_TIMER_TEST_DELAY ))
    //    {
    //        ;
    //    }

    retVal = STD_RETURN_OK;
    return retVal;
}


Std_ReturnType I2C_MasterRead( uint8 *dataPtr, uint8 dataLen, uint16 address, I2C_MessageStatusType *statusPtr )
{
    Std_ReturnType retVal = STD_RETURN_ERROR;
    static I2C_TransactionRequestBlockType I2C_transactionRequestBlock;

    // check if there is space in the queue
    if ( i2c2_object.trStatus.statusBitField.full != STD_BOOL_TRUE )
    {
        I2C_MasterReadTRBBuild( &I2C_transactionRequestBlock, dataPtr, dataLen, address);
        I2C_MasterTRBInsert( 1u, &I2C_transactionRequestBlock, statusPtr);
    }
    else
    {
        *statusPtr = I2C_MESSAGE_FAIL;
    }

    //    SysTimer_StartTimer( SYS_TIMER_TEST_DELAY, SYS_TIMER_ONE_SHOT );
    //    while ( SYS_TIMER_IS_EXPIRED != SysTimer_GetTimerStatus( SYS_TIMER_TEST_DELAY ))
    //    {
    //        ;
    //    }

    retVal = STD_RETURN_OK;
    return retVal;
}


Std_BoolType I2C_MasterQueueIsEmpty( void )
{
    return (( Std_BoolType )i2c2_object.trStatus.statusBitField.empty);
}


Std_BoolType I2C_MasterQueueIsFull( void )
{
    return (( Std_BoolType )i2c2_object.trStatus.statusBitField.full);
}


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */
/** @fn static Std_ReturnType I2C_MasterTRBInsert( uint8 trbCount, I2C_TransactionRequestBlockType *trbListPtr, I2C_MessageStatusType *flagPtr )
 *
 * @param trbCount
 * @param trbListPtr
 * @param flagPtr
 * @return
* @return In case of any any error, it returns the error code (not implemented yet)
*/
static Std_ReturnType I2C_MasterTRBInsert( uint8 trbCount, I2C_TransactionRequestBlockType *trbListPtr, I2C_MessageStatusType *flagPtr )
{
    Std_ReturnType retVal = STD_RETURN_ERROR;

    // check if there is space in the queue
    if ( i2c2_object.trStatus.statusBitField.full != STD_BOOL_TRUE )
    {
        *flagPtr = I2C_MESSAGE_PENDING;

        i2c2_object.pTrTail->ptrb_list = trbListPtr;
        i2c2_object.pTrTail->count     = trbCount;
        i2c2_object.pTrTail->pTrFlag   = flagPtr;
        i2c2_object.pTrTail++;

        // check if the end of the array is reached
        if ( i2c2_object.pTrTail == ( i2c2_tr_queue + I2C_CONFIG_TR_QUEUE_LENGTH ))
        {
            // adjust to restart at the beginning of the array
            i2c2_object.pTrTail = i2c2_tr_queue;
        }

        // since we added one item to be processed, we know
        // it is not empty, so set the empty status to false
        i2c2_object.trStatus.statusBitField.empty = STD_BOOL_FALSE;

        // check if full
        if ( i2c2_object.pTrHead == i2c2_object.pTrTail )
        {
            // it is full, set the full status to true
            i2c2_object.trStatus.statusBitField.full = STD_BOOL_TRUE;
        }

        // for interrupt based
        if ( i2c2_state == I2C_MASTER_STATE_IDLE )
        {
            // force the task to run since we know that the queue has
            // something that needs to be sent
            IFS3bits.MI2C2IF = 1;
        }

    }
    else
    {
        *flagPtr = I2C_MESSAGE_FAIL;
    }

    retVal = STD_RETURN_OK;
    return retVal;
}
/** @fn static Std_ReturnType I2C_MasterReadTRBBuild( I2C_TransactionRequestBlockType *trbPtr, uint8 *dataPtr, uint8 dataLen, uint16 address )
 * @brief
 * @param trbPtr
 * @param dataPtr
 * @param dataLen
 * @param address
* @return In case of any any error, it returns the error code (not implemented yet)
 */
static Std_ReturnType I2C_MasterReadTRBBuild( I2C_TransactionRequestBlockType *trbPtr, uint8 *dataPtr, uint8 dataLen, uint16 address )
{
    Std_ReturnType retVal = STD_RETURN_ERROR;

    trbPtr->address  = address << 1;
    // make this a read
    trbPtr->address |= 0x01;
    trbPtr->length   = dataLen;
    trbPtr->pbuffer  = dataPtr;

    retVal = STD_RETURN_OK;
    return retVal;
}

/** @fn static Std_ReturnType I2C_MasterWriteTRBBuild( I2C_TransactionRequestBlockType *trbPtr, uint8 *DataPtr, uint8 dataLen, uint16 address )
 * @param trbPtr
 * @param DataPtr
 * @param dataLen
 * @param address
* @return In case of any any error, it returns the error code (not implemented yet)
 */
static Std_ReturnType I2C_MasterWriteTRBBuild( I2C_TransactionRequestBlockType *trbPtr, uint8 *DataPtr, uint8 dataLen, uint16 address )
{
    Std_ReturnType retVal = STD_RETURN_ERROR;

    trbPtr->address = address << 1;
    trbPtr->length  = dataLen;
    trbPtr->pbuffer = DataPtr;

    retVal = STD_RETURN_OK;
    return retVal;
}

/** @fn static Std_ReturnType I2C_FunctionComplete( void )
* @brief
* @return In case of any any error, it returns the error code (not implemented yet)
*/
static Std_ReturnType I2C_FunctionComplete( void )
{

    // update the trb pointer
    p_i2c2_trb_current++;

    // are we done with this string of requests?
    if(--i2c2_trb_count == 0)
    {
        i2c2_state = I2C_MASTER_STATE_SEND_STOP;
    }
    else
    {
        i2c2_state = I2C_MASTER_STATE_RESTART;
    }
    return(STD_RETURN_OK);
}

/** @fn static Std_ReturnType I2C_Stop( I2C_MessageStatusType completionCode)
* @brief
 * @param completionCode
* @return In case of any any error, it returns the error code (not implemented yet)
*/

static Std_ReturnType I2C_Stop( I2C_MessageStatusType completionCode)
{
    // then send a stop
    I2C_STOP_CONDITION_ENABLE_BIT = 1;

    // make sure the flag pointer is not NULL
    if (p_i2c2_current->pTrFlag != NULL)
    {
        // update the flag with the completion code
        *(p_i2c2_current->pTrFlag) = completionCode;
    }

    // Done, back to idle
    i2c2_state = I2C_MASTER_STATE_IDLE;
    return(STD_RETURN_OK);
}
/** @fn void I2C_HandleIRQ ( void )

* @return In case of any any error, it returns the error code (not implemented yet)
*/
void I2C_HandleIRQ ( void )
{

    static uint8  *pi2c_buf_ptr;
    static uint16 i2c_address;
    static uint8  i2c_bytes_left;
    static uint8  i2c_10bit_address_restart = 0;

    IFS3bits.MI2C2IF = 0;

    // Check first if there was a collision.
    // If we have a Write Collision, reset and go to idle state */
    if( I2C_WRITE_COLLISION_STATUS_BIT )
    {
        // clear the Write colision
        I2C_WRITE_COLLISION_STATUS_BIT = 0;
        i2c2_state = I2C_MASTER_STATE_IDLE;
        *( p_i2c2_current->pTrFlag ) = I2C_MESSAGE_FAIL;

        // reset the buffer pointer
        p_i2c2_current = NULL;

        return;
    }

    /* Handle the correct i2c state */
    switch( i2c2_state )
    {
    case I2C_MASTER_STATE_IDLE:    /* In reset state, waiting for data to send */

        if( i2c2_object.trStatus.statusBitField.empty != STD_BOOL_TRUE )
        {
            // grab the item pointed by the head
            p_i2c2_current     = i2c2_object.pTrHead;
            i2c2_trb_count     = i2c2_object.pTrHead->count;
            p_i2c2_trb_current = i2c2_object.pTrHead->ptrb_list;

            i2c2_object.pTrHead++;

            // check if the end of the array is reached
            if( i2c2_object.pTrHead == ( i2c2_tr_queue + I2C_CONFIG_TR_QUEUE_LENGTH ))
            {
                // adjust to restart at the beginning of the array
                i2c2_object.pTrHead = i2c2_tr_queue;
            }

            // since we moved one item to be processed, we know
            // it is not full, so set the full status to false
            i2c2_object.trStatus.statusBitField.full = STD_BOOL_FALSE;

            // check if the queue is empty
            if ( i2c2_object.pTrHead == i2c2_object.pTrTail )
            {
                // it is empty so set the empty status to true
                i2c2_object.trStatus.statusBitField.empty = STD_BOOL_TRUE;
            }

            // send the start condition
            I2C_START_CONDITION_ENABLE_BIT = 1;

            // start the i2c request
            i2c2_state = I2C_MASTER_STATE_SEND_ADDR;
        }

        break;

    case I2C_MASTER_STATE_RESTART:

        /* check for pending i2c Request */

        // ... trigger a REPEATED START
        I2C_REPEAT_START_CONDITION_ENABLE_BIT = 1;

        // start the i2c request
        i2c2_state = I2C_MASTER_STATE_SEND_ADDR;

        break;

    case I2C_MASTER_STATE_SEND_ADDR_10BIT_LSB:

        if ( I2C_ACKNOWLEDGE_STATUS_BIT )
        {
            i2c2_object.i2cErrors++;
            I2C_Stop( I2C_MESSAGE_ADDRESS_NO_ACK );
        }
        else
        {
            // Remove bit 0 as R/W is never sent here
            I2C_TRANSMIT_REG = (i2c_address >> 1) & 0x00FF;

            // determine the next state, check R/W
            if ( i2c_address & 0x01 )
            {
                // if this is a read we must repeat start
                // the bus to perform a read
                i2c2_state = I2C_MASTER_STATE_10BIT_RESTART;
            }
            else
            {
                // this is a write continue writing data
                i2c2_state = I2C_MASTER_STATE_SEND_DATA;
            }
        }

        break;

    case I2C_MASTER_STATE_10BIT_RESTART:

        if ( I2C_ACKNOWLEDGE_STATUS_BIT )
        {
            i2c2_object.i2cErrors++;
            I2C_Stop( I2C_MESSAGE_ADDRESS_NO_ACK );
        }
        else
        {
            // ACK Status is good
            // restart the bus
            I2C_REPEAT_START_CONDITION_ENABLE_BIT = 1;

            // fudge the address so S_MASTER_SEND_ADDR works correctly
            // we only do this on a 10-bit address resend
            i2c_address = 0x00F0 | (( i2c_address >> 8 ) & 0x0006 );

            // set the R/W flag
            i2c_address |= 0x0001;

            // set the address restart flag so we do not change the address
            i2c_10bit_address_restart = 1;

            // Resend the address as a read
            i2c2_state = I2C_MASTER_STATE_SEND_ADDR;
        }

        break;

    case I2C_MASTER_STATE_SEND_ADDR:

        /* Start has been sent, send the address byte */

        /* Note:
            On a 10-bit address resend (done only during a 10-bit
            device read), the original i2c_address was modified in
            S_MASTER_10BIT_RESTART state. So the check if this is
            a 10-bit address will fail and a normal 7-bit address
            is sent with the R/W bit set to read. The flag
            i2c_10bit_address_restart prevents the  address to
            be re-written.
         */
        if ( i2c_10bit_address_restart != 1 )
        {
            // extract the information for this message
            i2c_address    = p_i2c2_trb_current->address;
            pi2c_buf_ptr   = p_i2c2_trb_current->pbuffer;
            i2c_bytes_left = p_i2c2_trb_current->length;
        }
        else
        {
            // reset the flag so the next access is ok
            i2c_10bit_address_restart = 0;
        }

        // check for 10-bit address
        if ( i2c_address > 0x00FF )
        {
            // we have a 10 bit address
            // send bits<9:8>
            // mask bit 0 as this is always a write
            I2C_TRANSMIT_REG = 0xF0 | (( i2c_address >> 8 ) & 0x0006 );
            i2c2_state = I2C_MASTER_STATE_SEND_ADDR_10BIT_LSB;
        }
        else
        {
            // Transmit the address
            I2C_TRANSMIT_REG = i2c_address;
            if ( i2c_address & 0x01 )
            {
                // Next state is to wait for address to be acked
                i2c2_state = I2C_MASTER_STATE_ACK_ADDR;
            }
            else
            {
                // Next state is transmit
                i2c2_state = I2C_MASTER_STATE_SEND_DATA;
            }
        }
        break;

    case I2C_MASTER_STATE_SEND_DATA:

        // Make sure the previous byte was acknowledged
        if ( I2C_ACKNOWLEDGE_STATUS_BIT )
        {
            // Transmission was not acknowledged
            i2c2_object.i2cErrors++;

            // Reset the Ack flag
            I2C_ACKNOWLEDGE_STATUS_BIT = 0;

            // Send a stop flag and go back to idle
            I2C_Stop( I2C_DATA_NO_ACK );

        }
        else
        {
            // Did we send them all ?
            if ( i2c_bytes_left-- == 0U )
            {
                // yup sent them all!

                // update the trb pointer
                p_i2c2_trb_current++;

                // are we done with this string of requests?
                if ( --i2c2_trb_count == 0 )
                {
                    I2C_Stop( I2C_MESSAGE_COMPLETE );
                }
                else
                {
                    // no!, there are more TRB to be sent.
                    //I2C2_START_CONDITION_ENABLE_BIT = 1;

                    // In some cases, the slave may require
                    // a restart instead of a start. So use this one
                    // instead.
                    I2C_REPEAT_START_CONDITION_ENABLE_BIT = 1;

                    // start the i2c request
                    i2c2_state = I2C_MASTER_STATE_SEND_ADDR;

                }
            }
            else
            {
                // Grab the next data to transmit
                I2C_TRANSMIT_REG = *pi2c_buf_ptr++;
            }
        }
        break;

    case I2C_MASTER_STATE_ACK_ADDR:

        /* Make sure the previous byte was acknowledged */
        if ( I2C_ACKNOWLEDGE_STATUS_BIT )
        {

            // Transmission was not acknowledged
            i2c2_object.i2cErrors++;

            // Send a stop flag and go back to idle
            I2C_Stop( I2C_MESSAGE_ADDRESS_NO_ACK );

            // Reset the Ack flag
            I2C_ACKNOWLEDGE_STATUS_BIT = 0;
        }
        else
        {
            I2C_RECEIVE_ENABLE_BIT = 1;
            i2c2_state = I2C_MASTER_STATE_ACK_RCV_DATA;
        }
        break;

    case I2C_MASTER_STATE_RCV_DATA:

        /* Acknowledge is completed.  Time for more data */

        // Next thing is to ack the data
        i2c2_state = I2C_MASTER_STATE_ACK_RCV_DATA;

        // Set up to receive a byte of data
        I2C_RECEIVE_ENABLE_BIT = 1;

        break;

    case I2C_MASTER_STATE_ACK_RCV_DATA:

        // Grab the byte of data received and acknowledge it
        *pi2c_buf_ptr++ = I2C_RECEIVE_REG;

        // Check if we received them all?
        if ( --i2c_bytes_left )
        {

            /* No, there's more to receive */

            // No, bit 7 is clear.  Data is ok
            // Set the flag to acknowledge the data
            I2C_ACKNOWLEDGE_DATA_BIT = 0;

            // Wait for the acknowledge to complete, then get more
            i2c2_state = I2C_MASTER_STATE_RCV_DATA;
        }
        else
        {

            // Yes, it's the last byte.  Don't ack it
            // Flag that we will nak the data
            I2C_ACKNOWLEDGE_DATA_BIT = 1;

            I2C_FunctionComplete();
        }

        // Initiate the acknowledge
        I2C_ACKNOWLEDGE_ENABLE_BIT = 1;
        break;

    case I2C_MASTER_STATE_RCV_STOP:
    case I2C_MASTER_STATE_SEND_STOP:

        // Send the stop flag
        I2C_Stop( I2C_MESSAGE_COMPLETE );
        break;

    default:

        // This case should not happen, if it does then
        // terminate the transfer
        i2c2_object.i2cErrors++;
        I2C_Stop( I2C_LOST_STATE );
        break;

    }
}
