/**********************************************************************
* (C) Copyright 2017, OptoForce Ltd.
* -------------------------------------------------------------------
* OPTOFORCE LIMITED PROPRIETARY INFORMATION
*
* Property of OptoForce ltd, Unauthorized reproduction and/or
* distribution is strictly prohibited. This product is protected
* under copyright law and trade secret law as an unpublished
* work
* (C) Copyright OptoForce. All rights reserved.
***********************************************************************/

/**
* @file adc.c
* @author Vincze Balazs
* @date 2017. okt. 12.
* @brief
*/


#include "ExtADCDrv.h"
#include "Debug.h"
#include "spi_.h"
#include "reg_spi.h"
#include "gio.h"
#include "GPIO.h"
#include "board.h"
#include "Debug.h"
//#include "communication.h"
//#include "my_timer.h"
#include "SPIDriver.h"
#include "ExtADCRegister.h"
typedef enum
{
    EXTADC_ST_CH_WAIT_FOR_READY   = 0u,
    EXTADC_ST_CH_GET_DATA_PRE     = 1u,
    EXTADC_ST_CH_GET_DATA_POST    = 2u,
    ADC_CONV_STATE_CHECK_CRC      = 3u,
    EXTADC_ST_CH_FINISH           = 4u,
    EXTADC_ST_ERROR               = 5u
}adc_conv_state;


#define NREADY_CURRENT  0U
#define NREADY_LAST     01U

/**
 * @brief ADC
 */
typedef struct _ExtADCConvPendingDatType
{

    uint8_t         dataCount;                       /**< Byte counter for received bytes */
    adc_conv_state  chConvState;                     /**< State of conversion for FSM */
    uint16_t        chNewDat;                        /**< Channel finish flag */
    uint16_t        chEnable;                        /**< Channel finish flag */
    uint16_t        chPending;

    int16_t         pin_nrdy[2];                    /**< Array to save last and recent state of NREADY pin*/
    //uint8_t         lastChID;                        /**< Last channel's ID to detect lose of a channel */
}
ExtADC_ConvFSMDatType;


typedef struct _ExtADCConvCHDatType
{
    uint32_t    chData;
    uint8_t     chID;
    uint8_t     chErrFlag;
    uint8_t     chNRDYFlag;
    uint8_t     crc8;
}
ExtADCConvCHDatType;


typedef struct _adc_instance_t
{
    SPIDriver_BusID_t           spiBusID;          /* Connected SPI Driver Instance */
    uint8_t                     masterMode;          /* ADC instance's master mode */

    ExtADCConvCHDatType         convChData[MAX_ADC_CHANNELS];
    uint8_t                     convCHCnt;

    ExtADC_ConvFSMDatType    convPendingDat;

    uint32_t                    registers[0x39];
    uint8_t                     dataBuffer[8];                  /* 8 for reset is the max Max ADC data is 4 byte +CRC is5byte */

    uint8_t                     powerMode;
    uint8_t                     operationMode;
    uint8_t                     dataStatusMode;
    uint8_t                     continousReadMode;
    uint8_t                     enableCRCMode;
    gioPORT_t *                 pNSynchPort;
    uint32_t                    nSynchPin;
    uint32_t                    pulseCNT;
    gioPORT_t *                 nReadyPort;
    uint32_t                    nReadyPin;
}
ExtADC_DeviceDescType;


//ExtADCConvCHDatType adc_ch_dat_arr[USED_ADC_MODULES][15];

static uint32_t ExtADC_ReadRegister(uint8_t deviceID,uint8_t address,uint8_t * pRegValue,uint8_t regSize,uint16_t timeout);
static uint32_t ExtADC_WriteRegister(uint8_t deviceID,uint8_t address,uint8_t * pRegValue,uint8_t regSize,uint16_t timeout);
static int16_t ExtADC_Reset(uint8_t deviceID,uint16_t resTimeout);
static uint32_t ExtADC_ReadErrorRegister(uint8_t deviceID);
int16_t ExtADC_SetChannelRegister(uint8_t deviceID,uint8_t channel,uint8_t enable,uint8_t setup_sel,uint8_t ainp,uint8_t ainm);
int16_t ExtADC_SetContinousReadMode(uint8_t deviceID,uint8_t value);
int16_t ExtADC_SetDataStatus(uint8_t deviceID,uint8_t value);
int16_t ExtADC_SetPowerMode(uint8_t deviceID,uint8_t value);
int16_t ExtADC_SetOperationMode(uint8_t deviceID,uint8_t value);
int16_t ExtADC_SetFilterRegister(uint8_t deviceID,uint8_t filter_id,uint8_t filter_type,uint16_t fs);

int16_t ExtADC_SetConfigRegister(uint8_t deviceID,uint8_t config_id,uint8_t bipolar,uint8_t burnout,uint8_t ref_pbuf,uint8_t ref_mbuf,uint8_t ain_pbuf,uint8_t ain_mbuf,uint8_t ref_sel,uint8_t pga);

int16_t ExtADC_SetRefENFlag(uint8_t deviceID,uint8_t value);
int16_t ExtADC_SetCSENFlag(uint8_t deviceID,uint8_t value);
int16_t ExtADC_ResetConvFSM(uint8_t deviceID);

ExtADC_DeviceDescType  ExtADC_Device[USED_ADC_MODULES];
//ExtADCConvCHDatType adc_ch_dat_arr[USED_ADC_MODULES][15];
uint8_t adc_init_status = 0u;
spiDAT1_t spi_test_cfg;

static uint8_t ExtADC_CalculateCRC(uint8_t aBuf[], uint8_t bufSize);
int16_t ExtADC_SetCRCProtection(uint8_t deviceID,uint8_t mode);

//#pragma DATA_SECTION(varInv,".dataInv")
uint8_t var;

//__attribute__((section(".data"))) uint8_t var;
//__attribute__((section(".dataInv"))) uint8_t varInv;




//VAR_PROTECTED(uint32_t,pulseCnt);

int32_t ret = 0;                                      /* Return value */
int32_t sample;                                       /* Stores raw value read from the ADC */

spiDAT1_t    dataconfig = {0};

/** @fn uint8_t AdcInstanceInit(uint8_t masterMode)
 * @brief
 * @return
 */
uint8_t ExtADC_Init(uint8_t masterMode)
{

    SPIDriver_Init();
    dataconfig.CSNR = SPI_CS_0;
    dataconfig.CS_HOLD = 1u;
    dataconfig.DFSEL = SPI_FMT_0;
    dataconfig.WDEL = 0u;
    /* Instance Inits */


    ExtADC_InitDeviceMaster(ADC_INSTANCE1);
    ExtADC_InitDeviceMaster(ADC_INSTANCE2);
    ExtADC_InitDeviceMaster(ADC_INSTANCE3);
    //ExtADC_InitDeviceMaster(ADC_INSTANCE4); /* Not Tested maybe GPIO or PINMUX init needed */


    return (0U);
}
/** @fn int16_t AdcInstance_InitInstance(uint8_t instanceId)
 *
 * @param instanceId
 * @return
 */
int16_t ExtADC_InitDeviceMaster(uint8_t deviceID)
{

    int16_t retVal = 0u;


   //printf("\r\n\t\tadc_instance_init started\r\nID=%s\r\n",deviceID);


    if(deviceID >= USED_ADC_MODULES)
    {
        printf("BAD INSTANCE ID!!\r\n");
        retVal = -1;
        /* Error 1: Bad Instance Error */
    }
    else
    {
        /* Connect ADC Instance with SPI Instance */
        switch(deviceID)
        {
        case ADC_INSTANCE1:
            ExtADC_Device[deviceID].spiBusID = SPI_INS_ID_1;
            ExtADC_Device[deviceID].nReadyPort = BOARD_EXTADC1_NRDY_PORT;
            ExtADC_Device[deviceID].nReadyPin = BOARD_EXTADC1_NRDY_PIN;
            ExtADC_Device[deviceID].pNSynchPort = BOARD_EXTADC1_SYNC_PORT;
            ExtADC_Device[deviceID].nSynchPin = BOARD_EXTADC1_SYNC_PIN;
            break;


        case ADC_INSTANCE2:
            ExtADC_Device[deviceID].spiBusID = SPI_INS_ID_2;
            ExtADC_Device[deviceID].nReadyPort = BOARD_EXTADC2_NRDY_PORT;
            ExtADC_Device[deviceID].nReadyPin = BOARD_EXTADC2_NRDY_PIN;
            ExtADC_Device[deviceID].pNSynchPort = BOARD_EXTADC2_SYNC_PORT;
            ExtADC_Device[deviceID].nSynchPin = BOARD_EXTADC2_SYNC_PIN;
            break;
        case ADC_INSTANCE3:
            ExtADC_Device[deviceID].spiBusID = SPI_INS_ID_3;
            ExtADC_Device[deviceID].nReadyPort = BOARD_EXTADC3_NRDY_PORT;
            ExtADC_Device[deviceID].nReadyPin = BOARD_EXTADC3_NRDY_PIN;
            ExtADC_Device[deviceID].pNSynchPort = BOARD_EXTADC3_SYNC_PORT;
                       ExtADC_Device[deviceID].nSynchPin = BOARD_EXTADC3_SYNC_PIN;
            break;
        case ADC_INSTANCE4:
            ExtADC_Device[deviceID].spiBusID = SPI_INS_ID_4;
            ExtADC_Device[deviceID].nReadyPort = BOARD_EXTADC4_NRDY_PORT;
            ExtADC_Device[deviceID].nReadyPin = BOARD_EXTADC4_NRDY_PIN;
            ExtADC_Device[deviceID].pNSynchPort = BOARD_EXTADC4_SYNC_PORT;
            ExtADC_Device[deviceID].nSynchPin = BOARD_EXTADC4_SYNC_PIN;
            break;


        default:
            ExtADC_Device[deviceID].spiBusID = SPI_BUS_ID_MAX;    /* Generates Error On low level SPI drivers */
            break;

        }
        /* Set ADC Instance Master Mode */
        ExtADC_Device[deviceID].masterMode = ADC_MASTER_MODE_MASTER;

        ExtADCDisableConversion(0x00U);
        /* Init Low Level SPI Driver */
        retVal = SPIDriver_InitInstance(ExtADC_Device[deviceID].spiBusID,SPI_MASTER_MODE_MASTER);

        if(retVal < 0)
        {
            printf("SPIDriver_InitInstance(): Error %d\r\n",(uint16_t)((retVal)/-1));
            retVal = -2;

            /* Error 2: SPI Init Error */
        }
        else
        {
           // printf("Low Level SPID Driver initialised\r\n");
        }
        SPIDriver_SetCS0Pin(ExtADC_Device[deviceID].spiBusID,0x00U);

        /** Reset conversion FSM values */
        ExtADC_ResetConvFSM(deviceID);

        ExtADC_Device[deviceID].pulseCNT = 0x0U;


        do
        {
            /* Reset ADC with 64 cycle '1'-s and read the status register if OK write the mapped registers*/
            retVal = ExtADC_Reset(deviceID,1024U);
            if(retVal < 0)
            {
                printf("AdcInstance_Reset(): Error\r\n");
                retVal = -3;
                break;
            }
            else
            {
               // printf("Hardware Reset Ok!\r\n");

            }
            ExtADC_SetCRCProtection(deviceID,0U);


            ExtADC_SetRefENFlag(deviceID,0x01);
            if(retVal < 0)
            {
                printf("ExtADC_SetCSENFlag(): Error\r\n");
                retVal = -4;
                break;
            }
            else
            {}
            /* Setup channel registers */
            /* Set ADC to Full Power mode */
            retVal = ExtADC_SetPowerMode(deviceID,POWER_MODE_FULL0);             /* Power Mode behaves the conversion speed! */
            if(retVal < 0)
            {
                printf("AdcInstance_SetPowerMode(): Error\r\n");
                retVal = -4;
                break;
            }
            else
            {
                //printf("AdcInstance_SetPowerMode() Ok!\r\n");
            }

            /* Channel 0 -> PD1 */
            retVal = ExtADC_SetChannelRegister(deviceID,0U,ADC_CH_ENABLE,SETUP_SEL0,EXT_ADC_SEL_PD1,EXT_ADC_SEL_AVSS);
            if(retVal < 0)
            {
                printf("AdcInstance_SetChannelRegister(0): Error\r\n");
                retVal = -5;
                break;
            }
            else
            {
                // //printf("AdcInstance_SetChannelRegister(0) Ok!\r\n");
            }
            /* Channel 1 -> PD2 */
            retVal = ExtADC_SetChannelRegister(deviceID,1U,ADC_CH_ENABLE,SETUP_SEL0,EXT_ADC_SEL_PD2,EXT_ADC_SEL_AVSS);
            if(retVal < 0)
            {
                printf("AdcInstance_SetChannelRegister(1): Error\r\n");
                retVal = -6;
                break;
            }
            else
            {
                //  //printf("AdcInstance_SetChannelRegister(1) Ok!\r\n");
            }
            /* Channel 2 -> PD3 */
            retVal = ExtADC_SetChannelRegister(deviceID,2U,ADC_CH_ENABLE,SETUP_SEL0,EXT_ADC_SEL_PD3,EXT_ADC_SEL_AVSS);
            if(retVal < 0)
            {
                printf("AdcInstance_SetChannelRegister(2): Error\r\n");
                retVal = -7;
                break;
                /* Error 4: Reset Error */
            }
            else
            {
                //    //printf("AdcInstance_SetChannelRegister(2) Ok!\r\n");
            }
            /* Channel 3 -> PD4 * */
            retVal = ExtADC_SetChannelRegister(deviceID,3U,ADC_CH_ENABLE,SETUP_SEL0,EXT_ADC_SEL_PD4,EXT_ADC_SEL_AVSS);
            if(retVal < 0)
            {
                printf("AdcInstance_SetChannelRegister(3): Error\r\n");
                retVal = -8;
                break;

            }
            else
            {
                //         //printf("AdcInstance_SetChannelRegister(3) Ok!\r\n");
            }
            /* Channel 4 -> TEMP_MEAS */

            ExtADC_SetChannelRegister(deviceID,4U,ADC_CH_ENABLE,SETUP_SEL2,EXT_ADC_SEL_TEMP_MEAS,EXT_ADC_SEL_AVSS);
            if(retVal < 0)
            {
                printf("AdcInstance_SetChannelRegister(4): Error\r\n");
                retVal = -9;
                break;

            }
            else
            {
                //         //printf("AdcInstance_SetChannelRegister(4) Ok!\r\n");
            }
            /* Channel 5 -> Led current */
            retVal = ExtADC_SetChannelRegister(deviceID,5U,ADC_CH_ENABLE,SETUP_SEL1,EXT_ADC_SEL_LED_I,EXT_ADC_SEL_AVSS);
            if(retVal < 0)
            {
                printf("AdcInstance_SetChannelRegister(5): Error\r\n");
                retVal = -10;
                break;
            }
            else
            {
                //        //printf("AdcInstance_SetChannelRegister(5) Ok!\r\n");
            }
            /* Channel 6 -> Refout */
            ExtADC_SetChannelRegister(deviceID,6U,ADC_CH_ENABLE,SETUP_SEL1,EXT_ADC_SEL_LED_VFMEAS,EXT_ADC_SEL_AVSS);
            if(retVal < 0)
            {
                printf("AdcInstance_SetChannelRegister(6): Error\r\n");
                retVal = -11;
                break;
            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            /* Channel 7 -> Vfmeas */
            retVal = ExtADC_SetChannelRegister(deviceID,7U,ADC_CH_ENABLE,SETUP_SEL1,EXT_ADC_SEL_REFOUT,EXT_ADC_SEL_AVSS);
            if(retVal < 0)
            {
                printf("AdcInstance_SetChannelRegister(7): Error\r\n");
                retVal = -12;
                break;

            }
            else
            {
                //         //printf("AdcInstance_SetChannelRegister(7) Ok!\r\n");
            }
            /* Channel 8 -> Adc Internal temperature */
            retVal = ExtADC_SetChannelRegister(deviceID,8U,ADC_CH_DISABLE,SETUP_SEL1,EXT_ADC_SEL_LED_VFMEAS,EXT_ADC_SEL_AVSS);
            if(retVal < 0)
            {
                printf("AdcInstance_SetChannelRegister(8): Error\r\n");
                retVal = -13;
                break;

            }
            else
            {
                //        //printf("AdcInstance_SetChannelRegister(8) Ok!\r\n");
            }



            retVal = ExtADC_SetConfigRegister(deviceID, 0, UNIPOLAR, 0, 1, 1, 1, 1, 2, 0);  // Refsel
            if(retVal < 0)
            {
                printf("AdcInstance_SetConfigRegister(0): Error\r\n");
                retVal = -14;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            retVal = ExtADC_SetConfigRegister(deviceID, 1, UNIPOLAR, 0, 1, 1, 1, 1, 2, 0);
            if(retVal < 0)
            {
                printf("AdcInstance_SetConfigRegister(1): Error\r\n");
                retVal = -15;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            retVal = ExtADC_SetConfigRegister(deviceID, 2, BIPOLAR, 0, 1, 1, 1, 1, 2, 0);
            if(retVal < 0)
            {
                printf("AdcInstance_SetConfigRegister(2): Error\r\n");
                retVal = -16;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            retVal = ExtADC_SetConfigRegister(deviceID, 3, UNIPOLAR, 0, 0, 0, 1, 1, 0, 0);
            if(retVal < 0)
            {
                printf("AdcInstance_SetConfigRegister(3): Error\r\n");
                retVal = -17;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            retVal = ExtADC_SetConfigRegister(deviceID, 4, UNIPOLAR, 0, 0, 0, 1, 1, 0, 0);
            if(retVal < 0)
            {
                printf("AdcInstance_SetConfigRegister(4): Error\r\n");
                retVal = -18;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            retVal = ExtADC_SetConfigRegister(deviceID, 5, UNIPOLAR, 0, 0, 0, 1, 1, 0, 0);
            if(retVal < 0)
            {
                printf("AdcInstance_SetConfigRegister(5): Error\r\n");
                retVal = -19;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            retVal = ExtADC_SetConfigRegister(deviceID, 6, UNIPOLAR, 0, 0, 0, 1, 1, 0, 0);
            if(retVal < 0)
            {
                printf("AdcInstance_SetConfigRegister(6): Error\r\n");
                retVal = -20;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            retVal = ExtADC_SetConfigRegister(deviceID, 7, UNIPOLAR, 0, 0, 0, 1, 1, 0, 0);
            if(retVal < 0)
            {
                printf("AdcInstance_SetConfigRegister(7): Error\r\n");
                retVal = -21;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }

            retVal = ExtADC_SetFilterRegister(deviceID,0,FILTER_TYPE_SYNC4,1U);
            if(retVal < 0)
            {
                printf("AdcInstance_SetFilterRegister(0): Error\r\n");
                retVal = -23;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            retVal = ExtADC_SetFilterRegister(deviceID,1,FILTER_TYPE_SYNC4,1U);
            if(retVal < 0)
            {
                printf("AdcInstance_SetFilterRegister(1): Error\r\n");
                retVal = -24;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            retVal = ExtADC_SetFilterRegister(deviceID,2,FILTER_TYPE_SYNC4,1U);
            if(retVal < 0)
            {
                printf("AdcInstance_SetFilterRegister(2): Error\r\n");
                retVal = -25;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            retVal = ExtADC_SetFilterRegister(deviceID,3,FILTER_TYPE_SYNC3,1);
            if(retVal < 0)
            {
                printf("AdcInstance_SetFilterRegister(3): Error\r\n");
                retVal = -26;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            retVal = ExtADC_SetFilterRegister(deviceID,4,FILTER_TYPE_SYNC3,1);
            if(retVal < 0)
            {
                printf("AdcInstance_SetFilterRegister(4): Error\r\n");
                retVal = -27;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            retVal = ExtADC_SetFilterRegister(deviceID,5,FILTER_TYPE_SYNC3,1);
            if(retVal < 0)
            {
                printf("AdcInstance_SetFilterRegister(5): Error\r\n");
                retVal = -28;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            retVal = ExtADC_SetFilterRegister(deviceID,6,FILTER_TYPE_SYNC3,1);
            if(retVal < 0)
            {
                printf("AdcInstance_SetFilterRegister(6): Error\r\n");
                retVal = -29;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            retVal = ExtADC_SetFilterRegister(deviceID,7,FILTER_TYPE_SYNC3,1);
            if(retVal < 0)
            {
                printf("AdcInstance_SetFilterRegister(7): Error\r\n");
                retVal = -30;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
            /* Add Data status to 24 bit data (dafault data is 3 byte long without status byte)*/
            retVal = ExtADC_SetDataStatus(deviceID,1);
            if(retVal < 0)
            {
                printf("ExtADC_SetDataStatus(): Error\r\n");
                retVal = -23;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }

            ExtADC_SetCSENFlag(deviceID,0x00U);

            /* Enable Continous Conversion Mode */
            retVal = ExtADC_SetOperationMode(deviceID,OP_MODE_CONT_CONV);
            if(retVal < 0)
            {
                printf("ExtADC_SetOperationMode(): Error\r\n");
                retVal = -24;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }

            /* Enable Continous Read Mode */
            retVal = ExtADC_SetContinousReadMode(deviceID,0);
            if(retVal < 0)
            {
                printf("AdcInstance_SetContinousReadMode(): Error\r\n");
                retVal = -25;
                break;

            }
            else
            {
                //       //printf("AdcInstance_SetChannelRegister(6) Ok!\r\n");
            }
        }
        while(0);

        if(retVal<0)
        {
            printf("Communication Error!!\r\n");
            while(1)
                ;

        }
       // printf("ADC[%d] initialised!\r\n",deviceID);

    }
    // _debug_tmp = (uint8_t)(retVal/-1);Debug_WriteCAN(&_debug_tmp,1U);
    return (retVal);
}



/**
 *
 */
int16_t ExtADC_GetFinishedChannels(uint8_t deviceID,adc_ch_dat_t * p_dat)
{
    int16_t retVal =0;
    uint8_t i=0u;
    if(deviceID >= USED_ADC_MODULES)
    {
        retVal = -1;
    }
    else if (NULL == p_dat)
    {
        retVal = -2;
    }
    else
    {
        for(i=0;i<ExtADC_Device[deviceID].convCHCnt;i++)
        {
            if(1)
            {
                p_dat[i].ch_num = ExtADC_Device[deviceID].convChData[i].chID;
                p_dat[i].ch_data = ExtADC_Device[deviceID].convChData[i].chData;
            }
            else
            {
                retVal = -3;
                break;
            }
            retVal = (i + 1);
        }
    }
    return retVal;
}
uint32_t ExtADC_GetCHData(uint8_t deviceID,uint8_t chID)
{
    uint32_t chData = {0U};

    if(deviceID >= USED_ADC_MODULES)
    {
        //retVal = -1;
    }
    else if(chID >= 16U)
    {
        //retVal = -2;
    }
    else
    {


        chData = ExtADC_Device[deviceID].convChData[chID].chData;

    }
    return chData;
}

int16_t ExtADC_SetChannelRegister(uint8_t deviceID,uint8_t channel,uint8_t enable,uint8_t setup_sel,uint8_t ainp,uint8_t ainm)
{
    int16_t retVal = 0;
    uint32_t testVal = 0x00U;
    /** Check Instance ID range */
    if(deviceID >= USED_ADC_MODULES)
    {
        retVal = -1;
    }
    /** Check Chanel Range */
    else if(channel >= MAX_ADC_CHANNELS)
    {
        retVal = -2;
    }
    else
    {
        ExtADC_Device[deviceID].registers[(ADDRESS_CH0_MAP_REG + channel)] = ((( enable & CH_EN_FLAG_MASK) << CH_EN_FLAG_SHIFT)      |
                (( setup_sel & CH_SETUP_SEL_MASK) << CH_SETUP_SEL_SHIFT)|
                (( ainp & CH_AINP_SEL_MASK) << CH_AINP_SEL_SHIFT)  |
                (( ainm & CH_AINM_SEL_MASK) << CH_AINM_SEL_SHIFT)  );
        ExtADC_WriteRegister(deviceID,(ADDRESS_CH0_MAP_REG + channel),(uint8_t *)&ExtADC_Device[deviceID].registers[(ADDRESS_CH0_MAP_REG + channel)],SIZE_CH_MAP_REG,1024U);
        testVal = ExtADC_Device[deviceID].registers[(ADDRESS_CH0_MAP_REG + channel)];
        ExtADC_ReadRegister(deviceID,(ADDRESS_CH0_MAP_REG + channel),(uint8_t *)&ExtADC_Device[deviceID].registers[(ADDRESS_CH0_MAP_REG + channel)],SIZE_CH_MAP_REG,1024U);
        if(testVal != ExtADC_Device[deviceID].registers[(ADDRESS_CH0_MAP_REG + channel)])
        {
            retVal = -2;
            printf("Write and Read value not the same in: %s()\r\n",__FUNCTION__);
        }
        else
        {
            retVal = 1;
        }
        /* Set Conv need flag what is needed for FSM and check ready check */
        if(ADC_CH_ENABLE == enable)
        {
            ExtADC_Device[deviceID].convPendingDat.chEnable |= (1U << channel);
        }
        else
        {
            ExtADC_Device[deviceID].convPendingDat.chEnable &= ~(1U << channel);
        }

    }
    return (retVal);
}
int16_t ExtADC_SetConfigRegister(uint8_t deviceID,uint8_t config_id,uint8_t bipolar,uint8_t burnout,uint8_t ref_pbuf,uint8_t ref_mbuf,uint8_t ain_pbuf,uint8_t ain_mbuf,uint8_t ref_sel,uint8_t pga)
{
    uint32_t testVal = 0x00U;
    int8_t retVal = 0x00U;
    if(deviceID >= USED_ADC_MODULES)
    {
        retVal=-1;
    }
    else if(config_id >= 8U)
    {
        retVal=-2;
    }
    else
    {
        ExtADC_Device[deviceID].registers[(ADDRESS_CFG0_REG + config_id)] = ((( bipolar & CONF_BIPOLAR_FLAG_MASK) << CONF_BIPOLAR_FLAG_SHIFT)      |
                (( burnout & CONF_BURNOUT_MASK) << CONF_BURNOUT_SHIFT)|
                (( ref_pbuf & CONF_REFBUFP_FLAG_MASK) << CONF_REFBUFP_FLAG_SHIFT)  |
                (( ref_mbuf & CONF_REFBUFM_FLAG_MASK) << CONF_REFBUFM_FLAG_SHIFT)  |
                (( ain_pbuf & CONF_AINBUFP_FLAG_MASK) << CONF_AINBUFP_FLAG_SHIFT)  |
                (( ain_mbuf & CONF_AINBUFM_FLAG_MASK) << CONF_AINBUFM_FLAG_SHIFT)  |
                (( ref_sel & CONF_REFSEL_MASK) << CONF_REFSEL_SHIFT)  |
                (( pga & CONF_PGA_MASK) << CONF_PGA_SHIFT)  );
        ExtADC_WriteRegister(deviceID,(ADDRESS_CFG0_REG + config_id),(uint8_t *)&ExtADC_Device[deviceID].registers[(ADDRESS_CFG0_REG + config_id)],SIZE_CFG_REG,1024U);
        testVal = ExtADC_Device[deviceID].registers[(ADDRESS_CFG0_REG + config_id)];
        ExtADC_ReadRegister(deviceID,(ADDRESS_CFG0_REG + config_id),(uint8_t *)&ExtADC_Device[deviceID].registers[(ADDRESS_CFG0_REG + config_id)],SIZE_CFG_REG,1024U);
        if(testVal != ExtADC_Device[deviceID].registers[(ADDRESS_CFG0_REG + config_id)])
        {
            retVal = -3;
            printf("Write and Read value not the same in: %s()\r\n",__FUNCTION__);
        }
        else
        {
            retVal = 1;
        }
    }
    return (retVal);
}
int16_t ExtADC_SetCRCProtection(uint8_t deviceID,uint8_t mode)
{
    int16_t retVal = 0;
       uint32_t testVal = 0x00U;
       if(deviceID >= USED_ADC_MODULES)
       {
           retVal = -1;
       }
       else
       {
           ExtADC_ReadRegister(deviceID,ADDRESS_ERREN_REG,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ERREN_REG],SIZE_ERREN_REG,1024U);
           if(0x00U == mode)
           {
               ExtADC_Device[deviceID].registers[ADDRESS_ERREN_REG] &= ~((1 & SPI_CRC_ERR_EN_MASK) << SPI_CRC_ERR_EN_SHIFT);

           }
           else
           {
               ExtADC_Device[deviceID].registers[ADDRESS_ERREN_REG] |= ((1 & SPI_CRC_ERR_EN_MASK) << SPI_CRC_ERR_EN_SHIFT);

           }
           ExtADC_WriteRegister(deviceID,ADDRESS_ERREN_REG,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ERREN_REG],SIZE_ERREN_REG,1024U);
           /* After that CRC Mode changed */
           ExtADC_Device[deviceID].enableCRCMode = mode;
           testVal = ExtADC_Device[deviceID].registers[ADDRESS_ERREN_REG];
           ExtADC_ReadRegister(deviceID,ADDRESS_ERREN_REG,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ERREN_REG],SIZE_ERREN_REG,1024U);
           if(testVal != ExtADC_Device[deviceID].registers[ADDRESS_ERREN_REG])
           {
               retVal = -2;
               printf("Write and Read value not the same in: %s()\r\n",__FUNCTION__);
           }
           else
           {
               retVal = 1;
           }
       }


       return (retVal);


}
int16_t ExtADC_SetFilterRegister(uint8_t deviceID,uint8_t filter_id,uint8_t filter_type,uint16_t fs)
{
    int16_t retVal=0;
    uint32_t testVal = 0x00U;
    if(deviceID >= USED_ADC_MODULES)
    {
        retVal=-1;
    }
    else if(filter_id >= 8U)
    {
        retVal=-2;
    }
    else
    {
        ExtADC_Device[deviceID].registers[(ADDRESS_FILT0_REG + filter_id)] =  ((filter_type & FILTER_TYPE_SEL_MASK) << FILTER_TYPE_SEL_SHIFT)     |
                ((0 & FILTER_REJ60_FLAG_MASK) << FILTER_REJ60_FLAG_SHIFT) |
                ((2 & FILTER_POST_FILTER_MASK) << FILTER_POST_FILTER_SHIFT)|
                ((0 & FILTER_SCYCLE_MASK) << FILTER_SCYCLE_SHIFT)         |
                ((fs & FILTER_FRATE_MASK) << FILTER_FRATE_SHIFT);

        ExtADC_WriteRegister(deviceID,(ADDRESS_FILT0_REG + filter_id),(uint8_t *)&ExtADC_Device[deviceID].registers[(ADDRESS_FILT0_REG + filter_id)],SIZE_FILT_REG,1024U);
        testVal = ExtADC_Device[deviceID].registers[(ADDRESS_FILT0_REG + filter_id)];
        ExtADC_ReadRegister(deviceID,(ADDRESS_FILT0_REG + filter_id),(uint8_t *)&ExtADC_Device[deviceID].registers[(ADDRESS_FILT0_REG + filter_id)],SIZE_FILT_REG,1024U);
        if(testVal != ExtADC_Device[deviceID].registers[(ADDRESS_FILT0_REG + filter_id)])
        {
            retVal = -3;
            printf("Write and Read value not the same in: %s()\r\n",__FUNCTION__);
        }
        else
        {
            retVal = 1;
        }
    }
    return (retVal);
}
int16_t ExtADC_GetOffsetRegister(uint8_t deviceID,uint8_t offset_id,uint32_t *offsetPtr)
{
    int16_t retVal=0;
    uint32_t testVal = 0x00U;
    if(deviceID >= USED_ADC_MODULES)
    {
        retVal=-1;
    }
    else if(offset_id >= 8U)
    {
        retVal=-2;
    }
    else if(offsetPtr == NULL)
    {
        retVal=-3;
    }
    else
    {
        ExtADC_ReadRegister(deviceID,(ADDRESS_OFFS0_REG + offset_id),(uint8_t *)&ExtADC_Device[deviceID].registers[(ADDRESS_OFFS0_REG + offset_id)],SIZE_OFFS_REG,1024U);
        *offsetPtr = ExtADC_Device[deviceID].registers[(ADDRESS_OFFS0_REG + offset_id)];
            retVal = 1;
    }
    return (retVal);
}
int16_t ExtADC_SetDataStatus(uint8_t deviceID,uint8_t value)
{
    int16_t retVal = 0;
    uint32_t testVal = 0x00U;
    if(deviceID >= USED_ADC_MODULES)
    {
        retVal = -1;
    }
    else
    {
        ExtADC_Device[deviceID].dataStatusMode = value;
        if(0x00U == value)
        {
            ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG] &= ~((1 & AD_CTRL_DATSTAT_FLAG_MASK) << AD_CTRL_DATSTAT_FLAG_SHIFT);

        }
        else
        {
            ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG] |= ((1 & AD_CTRL_DATSTAT_FLAG_MASK) << AD_CTRL_DATSTAT_FLAG_SHIFT);

        }
        ExtADC_WriteRegister(deviceID,ADDRESS_ADC_CTRL_REG,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG],SIZE_ADC_CTRL_REG,1024U);
        testVal = ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG];
        ExtADC_ReadRegister(deviceID,ADDRESS_ADC_CTRL_REG,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG],SIZE_ADC_CTRL_REG,1024U);
        if(testVal != ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG])
        {
            retVal = -2;
            printf("Write and Read value not the same in: %s()\r\n",__FUNCTION__);
        }
        else
        {
            retVal = 1;
        }
    }
    return (retVal);
}
int16_t ExtADC_SetCSENFlag(uint8_t deviceID,uint8_t value)
{
    int16_t retVal = 0;
    uint32_t testVal = 0x00U;
    if(deviceID >= USED_ADC_MODULES)
    {
        retVal = -1;
    }
    else
    {

        ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG] &= ~((AD_CTRL_CSEN_FLAG_MASK) << AD_CTRL_CSEN_FLAG_SHIFT);
        ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG] |= ((value & AD_CTRL_CSEN_FLAG_MASK) << AD_CTRL_CSEN_FLAG_SHIFT);
        ExtADC_WriteRegister(deviceID,ADDRESS_ADC_CTRL_REG,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG],SIZE_ADC_CTRL_REG,1024U);
        testVal = ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG];
        ExtADC_ReadRegister(deviceID,ADDRESS_ADC_CTRL_REG,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG],SIZE_ADC_CTRL_REG,1024U);
        if(testVal != ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG])
        {
            retVal = -2;
            printf("Write and Read value not the same in: %s()\r\n",__FUNCTION__);
        }
        else
        {
            retVal = 1;
        }

    }
    return (retVal);
}
int16_t ExtADC_SetRefENFlag(uint8_t deviceID,uint8_t value)
{
    int16_t retVal = 0;
    uint32_t testVal = 0x00U;
    if(deviceID >= USED_ADC_MODULES)
    {
        retVal = -1;
    }
    else
    {

        ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG] &= ~((AD_CTRL_REFEN_FLAG_MASK) << AD_CTRL_REFEN_FLAG_SHIFT);
        ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG] |= ((value & AD_CTRL_REFEN_FLAG_MASK) << AD_CTRL_REFEN_FLAG_SHIFT);
        ExtADC_WriteRegister(deviceID,ADDRESS_ADC_CTRL_REG,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG],SIZE_ADC_CTRL_REG,1024U);
        testVal = ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG];
        ExtADC_ReadRegister(deviceID,ADDRESS_ADC_CTRL_REG,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG],SIZE_ADC_CTRL_REG,1024U);
        if(testVal != ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG])
        {
            retVal = -2;
            printf("Write and Read value not the same in: %s()\r\n",__FUNCTION__);
        }
        else
        {
            retVal = 1;
        }

    }
    return (retVal);
}
int16_t ExtADC_SetPowerMode(uint8_t deviceID,uint8_t value)
{
    int16_t retVal = 0;
    uint32_t testVal = 0x00U;
    if(deviceID >= USED_ADC_MODULES)
    {
        retVal = -1;
    }
    else
    {
        ExtADC_Device[deviceID].powerMode = value;
        ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG] &= ~((AD_CTRL_POWMODE_MASK) << AD_CTRL_POWMODE_SHIFT);
        ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG] |= ((value & AD_CTRL_POWMODE_MASK) << AD_CTRL_POWMODE_SHIFT);
        ExtADC_WriteRegister(deviceID,ADDRESS_ADC_CTRL_REG,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG],SIZE_ADC_CTRL_REG,1024U);
        testVal = ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG];
        ExtADC_ReadRegister(deviceID,ADDRESS_ADC_CTRL_REG,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG],SIZE_ADC_CTRL_REG,1024U);
        if(testVal != ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG])
        {
            retVal = -2;
            printf("Write and Read value not the same in: %s()\r\n",__FUNCTION__);
        }
        else
        {
            retVal = 1;
        }

    }
    return (retVal);
}
int16_t ExtADC_SetOperationMode(uint8_t deviceID,uint8_t value)
{
    int16_t retVal = 0;
    uint32_t testVal = 0x00U;
    if(deviceID >= USED_ADC_MODULES)
    {
        retVal = -1;
    }
    else
    {
        ExtADC_Device[deviceID].operationMode = value;
        ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG] &= ~((AD_CTRL_MODE_MASK) << AD_CTRL_MODE_SHIFT);
        ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG] |= ((value & AD_CTRL_MODE_MASK) << AD_CTRL_MODE_SHIFT);
        ExtADC_WriteRegister(deviceID,ADDRESS_ADC_CTRL_REG,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG],SIZE_ADC_CTRL_REG,1024U);
        //was:  testVal = adc_instance[instanceId].registers[SIZE_ADC_CTRL_REG];
        testVal = ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG];

        ExtADC_ReadRegister(deviceID,ADDRESS_ADC_CTRL_REG,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG],SIZE_ADC_CTRL_REG,1024U);
        if(testVal != ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG])
        {
            retVal = -2;

            printf("Write and Read value not the same in: %s()\r\n",__FUNCTION__);
        }
        else
        {
            retVal = 1;
        }
    }
    return (retVal);
}
int16_t ExtADC_SetContinousReadMode(uint8_t deviceID,uint8_t value)
{
    int16_t retVal = 0;
    //   uint32_t testVal = 0x00U;
    if(deviceID >= USED_ADC_MODULES)
    {
        retVal = -1;
    }
    else
    {
        ExtADC_Device[deviceID].continousReadMode = value;
        if(0x00U == value)
        {
            ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG] &= ~((1 & AD_CTRL_CONTREAD_FLAG_MASK) << AD_CTRL_CONTREAD_FLAG_SHIFT);
        }
        else
        {
            ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG] |= ((1 & AD_CTRL_CONTREAD_FLAG_MASK) << AD_CTRL_CONTREAD_FLAG_SHIFT);
        }
        ExtADC_WriteRegister(deviceID,ADDRESS_ADC_CTRL_REG,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG],SIZE_ADC_CTRL_REG,1024U);

        /* According to AD7124-8 datasheet (page 51 paragraph 2): "To exit continuous read mode, write a read data command (0x42) while the DOUT/RDY pin is low."
         * So write read test exits from continuous read mode!
         * */
#if 0

        testVal = ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG];
        ExtADC_ReadRegister(deviceID,ADDRESS_ADC_CTRL_REG,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG],SIZE_ADC_CTRL_REG,1024U);
        if(testVal != ExtADC_Device[deviceID].registers[ADDRESS_ADC_CTRL_REG])
        {
            retVal = -2;
            printf("Write and Read value not the same!");
            printf((char *)__FUNCTION__);
            printf("()\r\n");
        }
        else
        {
            retVal = 1;
        }
#endif

    }
    return (retVal);
}
/** @fn static uint32_t AdcInstance_ReadErrorRegister(uint8_t instance)
 *
 * @param instance
 * @return
 */
static uint32_t ExtADC_ReadErrorRegister(uint8_t deviceID)
{
    // uint32_t error_reg=0x00U;
    uint8_t datCRC=0x0U;
#if 1

    /* Command out 0 in */
    ExtADC_Device[deviceID].dataBuffer[0] = (0x46U);      /* NWrite enable(~0x80) + Next Read(0x40) + (Error Reg. Address)(0x06) */
    /* 0 out byte 2 in */
    ExtADC_Device[deviceID].dataBuffer[1] = (0x00U);
    /* 0 out byte 1 in */
    ExtADC_Device[deviceID].dataBuffer[2] = (0x00U);
    /* 0 out byte 0 in */
    ExtADC_Device[deviceID].dataBuffer[3] = (0x00U);
    if( ExtADC_Device[deviceID].enableCRCMode == 0x01U)
    {
        ExtADC_Device[deviceID].dataBuffer[4] = (0x00U);
        /* Write 4 byte get 4 byte */
        SPIDriver_WriteReadArray(ExtADC_Device[deviceID].spiBusID,ExtADC_Device[deviceID].dataBuffer, 5U);
        ExtADC_Device[deviceID].dataBuffer[0] = (0x46U);
        datCRC = ExtADC_CalculateCRC(&ExtADC_Device[deviceID].dataBuffer[0],5U);
        if(datCRC !=  0)
        {
          // printf("DRV_CRC_ERROR!! in %s\r\nGiven:%02x Calculated: %02x",__FUNCTION__,ExtADC_Device[deviceID].dataBuffer[4],datCRC);
        }
    }
    else
    {
        /* Write 4 byte get 4 byte */
        SPIDriver_WriteReadArray(ExtADC_Device[deviceID].spiBusID,ExtADC_Device[deviceID].dataBuffer, 4U);
    }

    /* byte 3 is for shifting the command */
    ExtADC_Device[deviceID].registers[ADDRESS_ERR_REG] = (((uint32_t)ExtADC_Device[deviceID].dataBuffer[1] << 16)|((uint32_t)ExtADC_Device[deviceID].dataBuffer[2] << 8)|((uint32_t)ExtADC_Device[deviceID].dataBuffer[3] << 0));

#else

    ExtADC_Device[deviceID].registers[ADDRESS_ERR_REG] = (uint8_t)(0x46U); /* NWrite enable(~0x80) + Next Read(0x40) + (Error Reg. Address)(0x06) */
    SPIDriver_WriteReadArray(ExtADC_Device[deviceID].spiBusID,(uint8_t *)&ExtADC_Device[deviceID].registers[ADDRESS_ERR_REG], 4U);
    error_reg = ExtADC_Device[deviceID].registers[ADDRESS_ERR_REG];
#endif

    return (ExtADC_Device[deviceID].registers[ADDRESS_ERR_REG]);
}
/** @fn static uint32_t AdcInstance_ReadRegister(uint8_t instance,uint8_t address,uint8_t * pRegValue,uint8_t len,uint16_t timeout)
 *
 * @param instance
 * @param address
 * @param len
 * @param timeout
 * @return
 */
static uint32_t ExtADC_ReadRegister(uint8_t deviceID,uint8_t address,uint8_t * pRegValue,uint8_t regSize,uint16_t timeout)
{
    uint32_t error_reg = 0U;
    uint8_t i=0U;
    uint8_t dataArray[8] = {0U};
    uint8_t datCRC = 0x00;
    /* TODO: instance check
     * TODO: len check (max is 4 byte)
     * TODO: adress check?
     */

    while(timeout)
    {

        /* Get Error Register to check SPI ignore error flag */
        error_reg = ExtADC_ReadErrorRegister(deviceID);
        /*if(error_reg )
        {
           printf("%02x CError_In: %s()\r\n",error_reg,__FUNCTION__);
        }*/
        /* Is SPI ready? */
        if(0x00U == ((error_reg >> ERR_SPI_IGN_FLAG_SHIFT) & ERR_SPI_IGN_FLAG_MASK))
        {

            /* Set Command */
            dataArray[0] =  (uint8_t)(0x40U | (address & COMMS_REG_SEL_MASK));
            /* Fill TX dataBuffer with len * zeros for read len data*/
            for(i = 1U;i<= regSize;i++)
            {
                dataArray[i] = 0x00U;
            }
            if(0x01 == ExtADC_Device[deviceID].enableCRCMode)
            {

                SPIDriver_WriteReadArray(ExtADC_Device[deviceID].spiBusID,dataArray, (regSize+2U));
                dataArray[0] =  (uint8_t)(0x40U |                            /* Read */
                                       (address & COMMS_REG_SEL_MASK));
                datCRC = ExtADC_CalculateCRC(&dataArray[0], (regSize + 2U));
                if(datCRC != 0U)
                {

                     printf("DRV_CRC_ERROR!! in %s\r\nGiven:%d Calculated: %d",__FUNCTION__,dataArray[regSize + 1] ,datCRC);
                }
            }
            else
            {

                SPIDriver_WriteReadArray(ExtADC_Device[deviceID].spiBusID,dataArray, (regSize+1U));
            }
            ExtADC_Device[deviceID].registers[address] = 0x00U;
            for(i = 1; i <= regSize; i++)
            {
                ExtADC_Device[deviceID].registers[address] = (ExtADC_Device[deviceID].registers[address] << 8);
                ExtADC_Device[deviceID].registers[address] |= (uint32_t)dataArray[i];
            }
            break;

        }
        /* If not ready decrement timeout */
        else
        {


            timeout--;
        }
    }
    if(0x00U == timeout)
    {
        printf("Timeout_In: %s()\r\n",__FUNCTION__);
    }
    return (timeout);
}
static uint32_t ExtADC_ReadRegisterNoErrorCheck(uint8_t deviceID,uint8_t address,uint8_t * pRegValue,uint8_t regSize,uint16_t timeout)
{
    uint32_t error_reg = 0U;
    uint8_t i=0U;
    uint8_t dataArray[8] = {0U};
    uint8_t datCRC = 0x00;
    /* TODO: instance check
     * TODO: len check (max is 4 byte)
     * TODO: adress check?
     */

    while(timeout)
    {
#if 0
        /* Get Error Register to check SPI ignore error flag */
        error_reg = ExtADC_ReadErrorRegister(deviceID);
        /*if(error_reg )
        {
           printf("%02x CError_In: %s()\r\n",error_reg,__FUNCTION__);
        }*/
        /* Is SPI ready? */
        if(0x00U == ((error_reg >> ERR_SPI_IGN_FLAG_SHIFT) & ERR_SPI_IGN_FLAG_MASK))

#endif
            if(1)
        {

            /* Set Command */
            dataArray[0] =  (uint8_t)(0x40U | (address & COMMS_REG_SEL_MASK));
            /* Fill TX dataBuffer with len * zeros for read len data*/
            for(i = 1U;i<= regSize;i++)
            {
                dataArray[i] = 0x00U;
            }
            if(0x01 == ExtADC_Device[deviceID].enableCRCMode)
            {

                SPIDriver_WriteReadArray(ExtADC_Device[deviceID].spiBusID,dataArray, (regSize+2U));
                dataArray[0] =  (uint8_t)(0x40U |                            /* Read */
                                       (address & COMMS_REG_SEL_MASK));
                datCRC = ExtADC_CalculateCRC(&dataArray[0], (regSize + 2U));
                if(datCRC != 0U)
                {
                  // printf("DRV_CRC_ERROR!! in %s\r\nGiven:%02x Calculated: %02x",__FUNCTION__,dataArray[regSize + 1] ,datCRC);
                }
            }
            else
            {

                SPIDriver_WriteReadArray(ExtADC_Device[deviceID].spiBusID,dataArray, (regSize+1U));
            }
            ExtADC_Device[deviceID].registers[address] = 0x00U;
            for(i = 1; i <= regSize; i++)
            {
                ExtADC_Device[deviceID].registers[address] = (ExtADC_Device[deviceID].registers[address] << 8);
                ExtADC_Device[deviceID].registers[address] |= (uint32_t)dataArray[i];
            }
            break;

        }
        /* If not ready decrement timeout */
        else
        {


            timeout--;
        }
    }
    if(0x00U == timeout)
    {
        printf("Timeout_In: %s()\r\n",__FUNCTION__);
    }
    return (timeout);
}
/** @fn static uint32_t AdcInstance_WriteRegister(uint8_t instance,uint8_t address,uint8_t len,uint16_t timeout)
 *
 * @param instance
 * @param address
 * @param len
 * @param timeout
 * @return
 */
static uint32_t ExtADC_WriteRegister(uint8_t deviceID,uint8_t address,uint8_t * pRegValue,uint8_t regSize,uint16_t timeout)
{
    uint32_t errorReg = 0U;
    uint8_t i=0U;
    uint8_t dataArray[8] = {0U};
    while(timeout)
    {

        /* Get Error Register to check SPI ignore error flag */
        errorReg = ExtADC_ReadErrorRegister(deviceID);
        /*if(errorReg )
        {
            printf("%02x CError_In: %s()\r\n",errorReg,__FUNCTION__);
        }*/
        /* Is SPI ready? */
        if(0x00U == ((errorReg >> ERR_SPI_IGN_FLAG_SHIFT) & ERR_SPI_IGN_FLAG_MASK))
        {
            /* Set Command */
            dataArray[0] =  (uint8_t)(0x00U |                            /* Write */
                                      (address & COMMS_REG_SEL_MASK));
            /* Fill TX dataBuffer with value*/
            for(i = regSize;i > 0;i--)
            {
                dataArray[i] = (uint8_t)((ExtADC_Device[deviceID].registers[address] >> ((regSize - i) * 8U))&0XFFU);
            }
            if(0x01 == ExtADC_Device[deviceID].enableCRCMode)
            {

                dataArray[(regSize + 1)] = ExtADC_CalculateCRC(dataArray, (regSize + 1 ));


                SPIDriver_WriteArray(ExtADC_Device[deviceID].spiBusID,dataArray, (regSize+2U));
            }
            else
            {
                SPIDriver_WriteArray(ExtADC_Device[deviceID].spiBusID,dataArray, (regSize+1U));
            }
            break;

        }
        else
        {

            timeout--;
        }
    }
    if(0x00U == timeout)
    {
        printf("Timeout_In: %s()\r\n",__FUNCTION__);

    }

    return (timeout);
}
/** @fn static int16_t AdcInstance_Reset(uint8_t instance,uint16_t resTimeout)
 *
 * @param [in] instance     Instance Of ADC
 * @param [in] resTimeout   Tmieout of waiting for reset
 * @return  TODO:  -1 -2 0...
 */
static int16_t ExtADC_Reset(uint8_t deviceID,uint16_t resTimeout)
{
    int16_t retVal = 0;
    uint8_t statusReg = 0U;
    uint8_t id=0U;
    uint8_t state = 0;
    //   uint32_t tmp=0x00U;
    /* TODO: Instance check
     *
     */
    /* Set MOSI High for 64 Clocks -> 8 * 0XFFU todo: DMA??*/
    ExtADC_Device[deviceID].dataBuffer[0] = 0xFFU;
    ExtADC_Device[deviceID].dataBuffer[1] = 0xFFU;
    ExtADC_Device[deviceID].dataBuffer[2] = 0xFFU;
    ExtADC_Device[deviceID].dataBuffer[3] = 0xFFU;
    ExtADC_Device[deviceID].dataBuffer[4] = 0xFFU;
    ExtADC_Device[deviceID].dataBuffer[5] = 0xFFU;
    ExtADC_Device[deviceID].dataBuffer[6] = 0xFFU;
    ExtADC_Device[deviceID].dataBuffer[7] = 0xFFU;
    /* Send reset frame */
    SPIDriver_WriteArray(ExtADC_Device[deviceID].spiBusID, ExtADC_Device[deviceID].dataBuffer, 8U);
    /* After the reset frame sent the POR flag is going to HIGH while hardware reset is pending */
    /* Wait for POR flag to go HIGH in ADC's Status register */

    while(resTimeout)
        {

            retVal = ExtADC_ReadRegister(deviceID,ADDRESS_STATUS_REG,ExtADC_Device[deviceID].dataBuffer,SIZE_STATUS_REG,2048U);

            if(retVal > 0)
            {
                statusReg = (uint8_t)ExtADC_Device[deviceID].registers[ADDRESS_STATUS_REG];
               // print_byte_hex(statusReg);
               // PUTCHAR('\n');
                if(( 0x01U == ((statusReg >> STAT_POR_FLAG_SHIFT) & STAT_POR_FLAG_MASK))&&(0==state))
                {

                    state = 1;
                }
                else if(( 0x00U == ((statusReg >> STAT_POR_FLAG_SHIFT) & STAT_POR_FLAG_MASK))&&(1==state))
                {

                    break;
                }

                else
                {
                    resTimeout--;
                }
            }
            else
            {
                resTimeout--;
            }
        }

    /* Check Timeout */
    if(0x00U == resTimeout)
    {
        /* Error 1: Reset Timeout */
        retVal = -1;
        printf("Timeout in SPI Reset Wait LOW!!\r\n");
        print_byte_hex(statusReg);
        print_byte_hex(state);
    }
    else
    {

        /* In this initial state we cannot use CRC to check status register's value is valid so we need to check ADC's ID rwegister, which value is always 0x14*/
        /* Check ADC ID Register */
        retVal = ExtADC_ReadRegister(deviceID,ADDRESS_ID_REG,ExtADC_Device[deviceID].dataBuffer,SIZE_ID_REG,2048U);
        if(retVal > 0)
        {
            id = (uint8_t)ExtADC_Device[deviceID].registers[ADDRESS_ID_REG];
            if(id != 0x14U)
            {
                /* Error 2: ID doesn't Match */
                retVal = -2;
                printf("ID doesn't Match %d!!\r\n",id);
            }
            else
            {
                retVal = 0;
                //printf("ID Match %d!!\r\n",id);
            }
        }
        else
        {
            retVal = -3;
        }

    }
    return (retVal);
}

uint32_t ExtADC_GetPulseCNT(uint8_t deviceID)
{
    uint32_t retVal = 0x00U;
    retVal = ExtADC_Device[deviceID].pulseCNT;
    return retVal;
}

uint32_t ExtADC_GetDataCount(uint8_t deviceID)
{
    uint32_t retVal = 0x00U;
    retVal = ExtADC_Device[deviceID].convCHCnt;
    return retVal;
}
void ExtADC_ResetPulseCNT(uint8_t deviceID)
{
    ExtADC_Device[deviceID].pulseCNT = 0x00U;
}
/** @fn int16_t AdcInstance_RunMasterConvFSM(uint8_t instance)
 *  @brief Non Blocking Master Receive FSM for AD7124
 * @param instance
 * @return Undefined
 * @todo Return value can used for state
 */
int16_t ExtADC_RunMasterConvFSM(uint8_t deviceID)
{
    uint8_t ch=0u;
    uint8_t bit_nrdy=0u;
    uint8_t bit_err=0u;
    uint8_t i=0u;
    int16_t retVal = -10;

    // uint32_t recent = get_100us_tick();
    uint8_t b_cnt = 0u;
    int8_t nReady = 0x00;

    uint8_t additional = ((~ExtADC_Device[deviceID].continousReadMode)&0x01U);
    uint8_t crc = ExtADC_Device[deviceID].enableCRCMode;
    if(1)
    {
        switch(ExtADC_Device[deviceID].convPendingDat.chConvState)
        {
            /* ADC is waiting for MISO/NREADY pin go LOW */
        case EXTADC_ST_CH_WAIT_FOR_READY:
            SPIDriver_SetCS0Pin(ExtADC_Device[deviceID].spiBusID,0x00U);
            nReady = GPIO_GetPinValue(ExtADC_Device[deviceID].nReadyPort, ExtADC_Device[deviceID].nReadyPin);

            if(nReady < 0)
            {
                PUTCHAR('E');
                ERROR_HALT();
            }
            else
            {
                ExtADC_Device[deviceID].convPendingDat.pin_nrdy[NREADY_LAST] = ExtADC_Device[deviceID].convPendingDat.pin_nrdy[NREADY_CURRENT];
                ExtADC_Device[deviceID].convPendingDat.pin_nrdy[NREADY_CURRENT] = nReady;
            }
            if(( 0 == ExtADC_Device[deviceID].convPendingDat.pin_nrdy[NREADY_CURRENT])&&( 1 == ExtADC_Device[deviceID].convPendingDat.pin_nrdy[NREADY_LAST]))
                //if(( 1 == adc_instance[instanceID].convPendingDat.pin_nrdy[NREADY_CURRENT]))
            {
                ExtADC_Device[deviceID].pulseCNT++;
                /* Reset the values */
           //     PUTCHAR('_');
                ExtADC_Device[deviceID].convPendingDat.dataCount = 0u;
                for(i=additional;i < (ADC_CONV_DAT_LENGTH + crc);i++)
                {
                    ExtADC_Device[deviceID].dataBuffer[i] = 0u;
                }
                /* Start the SPI Read immedietly (shifting 0x00)*/
                if( additional)
                {
                      ExtADC_Device[deviceID].dataBuffer[0] = 0x42U;
                }
                retVal = SPIDriver_WriteDataRegister(ExtADC_Device[deviceID].spiBusID,ExtADC_Device[deviceID].dataBuffer[0]);
                if(retVal < 0)
                {
                    /* Hardware Error */
                    ERROR_HALT();
                }
                else
                {
                    ExtADC_Device[deviceID].convPendingDat.chConvState = EXTADC_ST_CH_GET_DATA_POST;
                }
            }
            else
            {
                //  SPIDriver_SetCS0Pin(adc_instance[instanceID].spiBusID,0x01U);
            }
            break;
            /* SPI Is is shifting out one data '0' byte to the ADC */
        case EXTADC_ST_CH_GET_DATA_PRE:
            retVal = SPIDriver_ShiftDataRegister(ExtADC_Device[deviceID].spiBusID);
            if(retVal < 0)
            {
                ERROR_HALT();
            }
            else
            {
                ExtADC_Device[deviceID].convPendingDat.chConvState = EXTADC_ST_CH_GET_DATA_POST;
            }
            break;
            /* SPI Is is shifting in one data byte from the ADC and check if finish*/
        case EXTADC_ST_CH_GET_DATA_POST:
            b_cnt = ExtADC_Device[deviceID].convPendingDat.dataCount;
            retVal = SPIDriver_GetDataRegister(ExtADC_Device[deviceID].spiBusID, &ExtADC_Device[deviceID].dataBuffer[b_cnt]);
            /* New Data byte Arrived */
            if(retVal > 0)
            {
                ExtADC_Device[deviceID].convPendingDat.dataCount++;


                /* All data has been arrived goto next state */
                if(ExtADC_Device[deviceID].convPendingDat.dataCount >= (ADC_CONV_DAT_LENGTH + additional + crc))   /* TODO: 3 if no use data status 4 if use */
                {
                    /* TODO: CRCCheck if( 1 == adc_instance[instance].crcCheck ){ .... }*/
                    ExtADC_Device[deviceID].convPendingDat.chConvState = EXTADC_ST_CH_FINISH;
                }
                /* Continue with Next Data byte */
                else
                {
                    ExtADC_Device[deviceID].convPendingDat.chConvState = EXTADC_ST_CH_GET_DATA_PRE;
                }
            }
            /* HW or programming error */
            else if(retVal < 0)
            {
                ERROR_HALT();
            }
            /* No new Data TODO: Decrement timeout */
            else
            {
                /* timeout--;if(!timeout)-> error */
            }
            break;
        case EXTADC_ST_CH_FINISH:
            ch =  (uint8_t)ExtADC_Device[deviceID].dataBuffer[(3 + additional)] & 0x0fu;
            bit_nrdy = (uint8_t)((ExtADC_Device[deviceID].dataBuffer[(3 + additional)] >> 7)&0x01u);
            bit_err  = (uint8_t)((ExtADC_Device[deviceID].dataBuffer[(3 + additional)] >> 6)&0x01u);
            if((0u == bit_nrdy)&&(0u == bit_err))
            {
                ExtADC_Device[deviceID].convChData[ ch].chData = ( ((uint32_t)ExtADC_Device[deviceID].dataBuffer[(2+ additional)]) |
                        ((uint32_t)ExtADC_Device[deviceID].dataBuffer[(1+ additional)] << 8u) |
                        ((uint32_t)ExtADC_Device[deviceID].dataBuffer[(0+additional)] << 16u));
                ExtADC_Device[deviceID].convChData[ch].chErrFlag = bit_err;
                ExtADC_Device[deviceID].convChData[ch].chNRDYFlag = bit_nrdy;
                ExtADC_Device[deviceID].convChData[ch].chID = ch;
                if(crc)
                {
                    ExtADC_Device[deviceID].dataBuffer[0] = 0x42U;
                    crc = ExtADC_CalculateCRC( ExtADC_Device[deviceID].dataBuffer, 6U);
                    if(crc != 0x00U)
                    {
                      //  printf("CRCERR %02x\r\n",ExtADC_Device[deviceID].dataBuffer[5]);
                       // while(1);
                    }
                }



                ExtADC_Device[deviceID].convPendingDat.chNewDat |= (1u << (uint16_t)ch);        /* Sign that new data arrived */
                ExtADC_Device[deviceID].convPendingDat.chPending &= ~(1u << (uint16_t)ch);      /* Clear chnalle from pending data */


                ExtADC_Device[deviceID].convPendingDat.chConvState = EXTADC_ST_CH_WAIT_FOR_READY;
                ExtADC_Device[deviceID].convCHCnt++;

                /*if(MAX_ADC_CHANNELS == adc_instance[instanceID].convDataCnt )
            {
                    adc_instance[instanceID].convDataCnt = 0x00U;
            }*/
                SPIDriver_SetCS0Pin( ExtADC_Device[deviceID].spiBusID,0x01U);
               // PUTCHAR('|');
               // PUTCHAR('C');
             // PUTCHAR(ch+'0');
               // PUTCHAR('-');
                //Debug_SCIPutchar('+');

                //printf("%02x%02x%02x%02x%02x\r\n",ExtADC_Device[deviceID].dataBuffer[0],ExtADC_Device[deviceID].dataBuffer[1],ExtADC_Device[deviceID].dataBuffer[2],ExtADC_Device[deviceID].dataBuffer[3],ExtADC_Device[deviceID].dataBuffer[4]);
            }
            else
            {
                PUTCHAR('E');
                /*Restart */
                ExtADC_Device[deviceID].convPendingDat.chConvState = EXTADC_ST_CH_WAIT_FOR_READY;
            }
            break;
        case EXTADC_ST_ERROR:

        default:
            break;
        }
    }

    return (retVal);
}
int16_t ExtADC_RunMasterConvFSMTest(uint8_t deviceID)
{
    uint8_t ch=0u;
    uint8_t bit_nrdy=0u;
    uint8_t bit_err=0u;
    uint8_t i=0u;
    int16_t retVal = -10;

    // uint32_t recent = get_100us_tick();
    uint8_t b_cnt = 0u;
    int8_t nReady = 0x00;

    uint8_t additional = ((~ExtADC_Device[deviceID].continousReadMode)&0x01U);
    uint8_t crc = ExtADC_Device[deviceID].enableCRCMode;
    //while( ExtADC_Device[deviceID].convPendingDat.chPending > 0)
    if(1)
    {
        switch(ExtADC_Device[deviceID].convPendingDat.chConvState)
        {
            /* ADC is waiting for MISO/NREADY pin go LOW */
        case EXTADC_ST_CH_WAIT_FOR_READY:
            SPIDriver_SetCS0Pin(ExtADC_Device[deviceID].spiBusID,0x00U);
            nReady = GPIO_GetPinValue(ExtADC_Device[deviceID].nReadyPort, ExtADC_Device[deviceID].nReadyPin);
            if(nReady < 0)
            {
                PUTCHAR('E');
                ERROR_HALT();
            }
            else
            {
                ExtADC_Device[deviceID].convPendingDat.pin_nrdy[NREADY_LAST] = ExtADC_Device[deviceID].convPendingDat.pin_nrdy[NREADY_CURRENT];
                ExtADC_Device[deviceID].convPendingDat.pin_nrdy[NREADY_CURRENT] = nReady;
            }
            if(( 0 == ExtADC_Device[deviceID].convPendingDat.pin_nrdy[NREADY_CURRENT])&&( 1 == ExtADC_Device[deviceID].convPendingDat.pin_nrdy[NREADY_LAST]))
                //if(( 1 == adc_instance[instanceID].convPendingDat.pin_nrdy[NREADY_CURRENT]))
            {
                ExtADC_Device[deviceID].pulseCNT++;
                /* Reset the values */
           //     PUTCHAR('_');
                ExtADC_Device[deviceID].convPendingDat.dataCount = 0u;
                for(i=additional;i < (ADC_CONV_DAT_LENGTH + crc);i++)
                {
                    ExtADC_Device[deviceID].dataBuffer[i] = 0u;
                }
                /* Start the SPI Read immedietly (shifting 0x00)*/
                if( additional)
                {
                      ExtADC_Device[deviceID].dataBuffer[0] = 0x42U;
                }
                //retVal = SPIDriver_WriteDataRegister(ExtADC_Device[deviceID].spiBusID,ExtADC_Device[deviceID].dataBuffer[0]);

                retVal = SPIDriver_WriteReadArray(ExtADC_Device[deviceID].spiBusID, ExtADC_Device[deviceID].dataBuffer, ( ADC_CONV_DAT_LENGTH + 2));
                if(retVal < 0)
                {
                    /* Hardware Error */
                    ERROR_HALT();
                }
                else
                {
                    //ExtADC_Device[deviceID].convPendingDat.chConvState = EXTADC_ST_CH_GET_DATA_POST;
                    ExtADC_Device[deviceID].convPendingDat.chConvState = EXTADC_ST_CH_FINISH;
                    ExtADC_Device[deviceID].convPendingDat.dataCount = ( ADC_CONV_DAT_LENGTH + additional + crc);
                }
            }
            else
            {

                //  SPIDriver_SetCS0Pin(adc_instance[instanceID].spiBusID,0x01U);
            }
            break;
            /* SPI Is is shifting out one data '0' byte to the ADC */
        case EXTADC_ST_CH_GET_DATA_PRE:
            retVal = SPIDriver_ShiftDataRegister(ExtADC_Device[deviceID].spiBusID);
            if(retVal < 0)
            {
                ERROR_HALT();
            }
            else
            {
                ExtADC_Device[deviceID].convPendingDat.chConvState = EXTADC_ST_CH_GET_DATA_POST;
            }
            break;
            /* SPI Is is shifting in one data byte from the ADC and check if finish*/
        case EXTADC_ST_CH_GET_DATA_POST:
            b_cnt = ExtADC_Device[deviceID].convPendingDat.dataCount;
            retVal = SPIDriver_GetDataRegister(ExtADC_Device[deviceID].spiBusID, &ExtADC_Device[deviceID].dataBuffer[b_cnt]);
            /* New Data byte Arrived */
            if(retVal > 0)
            {
                ExtADC_Device[deviceID].convPendingDat.dataCount++;


                /* All data has been arrived goto next state */
                if(ExtADC_Device[deviceID].convPendingDat.dataCount >= (ADC_CONV_DAT_LENGTH + additional + crc))   /* TODO: 3 if no use data status 4 if use */
                {
                    /* TODO: CRCCheck if( 1 == adc_instance[instance].crcCheck ){ .... }*/
                    ExtADC_Device[deviceID].convPendingDat.chConvState = EXTADC_ST_CH_FINISH;
                }
                /* Continue with Next Data byte */
                else
                {
                    ExtADC_Device[deviceID].convPendingDat.chConvState = EXTADC_ST_CH_GET_DATA_PRE;
                }
            }
            /* HW or programming error */
            else if(retVal < 0)
            {
                ERROR_HALT();
            }
            /* No new Data TODO: Decrement timeout */
            else
            {
                /* timeout--;if(!timeout)-> error */
            }
            break;
        case EXTADC_ST_CH_FINISH:
            ch =  (uint8_t)ExtADC_Device[deviceID].dataBuffer[(3 + additional)] & 0x0fu;
            bit_nrdy = (uint8_t)((ExtADC_Device[deviceID].dataBuffer[(3 + additional)] >> 7)&0x01u);
            bit_err  = (uint8_t)((ExtADC_Device[deviceID].dataBuffer[(3 + additional)] >> 6)&0x01u);
            if((0u == bit_nrdy)&&(0u == bit_err))
            {
                ExtADC_Device[deviceID].convChData[ ch].chData = ( ((uint32_t)ExtADC_Device[deviceID].dataBuffer[(2+ additional)]) |
                        ((uint32_t)ExtADC_Device[deviceID].dataBuffer[(1+ additional)] << 8u) |
                        ((uint32_t)ExtADC_Device[deviceID].dataBuffer[(0+additional)] << 16u));
                ExtADC_Device[deviceID].convChData[ch].chErrFlag = bit_err;
                ExtADC_Device[deviceID].convChData[ch].chNRDYFlag = bit_nrdy;
                ExtADC_Device[deviceID].convChData[ch].chID = ch;
                if(crc)
                {
                    if(additional)
                    {
                        ExtADC_Device[deviceID].dataBuffer[0] = 0x42U;
                    }
                    crc = ExtADC_CalculateCRC( ExtADC_Device[deviceID].dataBuffer,  ( ADC_CONV_DAT_LENGTH + additional + crc));
                    if(crc != 0x00U)
                    {
                      //  printf("CRCERR %02x\r\n",ExtADC_Device[deviceID].dataBuffer[5]);
                       // while(1);
                    }
                }



                ExtADC_Device[deviceID].convPendingDat.chNewDat |= (1u << (uint16_t)ch);        /* Sign that new data arrived */
                ExtADC_Device[deviceID].convPendingDat.chPending &= ~(1u << (uint16_t)ch);      /* Clear chnalle from pending data */


                ExtADC_Device[deviceID].convPendingDat.chConvState = EXTADC_ST_CH_WAIT_FOR_READY;
                ExtADC_Device[deviceID].convCHCnt++;

                /*if(MAX_ADC_CHANNELS == adc_instance[instanceID].convDataCnt )
            {
                    adc_instance[instanceID].convDataCnt = 0x00U;
            }*/
                SPIDriver_SetCS0Pin( ExtADC_Device[deviceID].spiBusID,0x01U);
               // PUTCHAR('|');
               // PUTCHAR('C');
           //PUTCHAR(ch+'0');
               // PUTCHAR('-');
                //Debug_SCIPutchar('+');

               // printf("%02x%02x%02x%02x%02x%02x\r\n",ExtADC_Device[deviceID].dataBuffer[0],ExtADC_Device[deviceID].dataBuffer[1],ExtADC_Device[deviceID].dataBuffer[2],ExtADC_Device[deviceID].dataBuffer[3],ExtADC_Device[deviceID].dataBuffer[4],ExtADC_Device[deviceID].dataBuffer[5]);
            }
            else
            {
                PUTCHAR('E');
                /*Restart */
                ExtADC_Device[deviceID].convPendingDat.chConvState = EXTADC_ST_CH_WAIT_FOR_READY;
            }
            break;
        case EXTADC_ST_ERROR:

        default:
            break;
        }
    }

    return (retVal);
}
/*ExtADC_RunSingleConversion(uint8_t cnt);
{
uint8_t deviceID=0x00U;
    // Setup registers
    // Start Conversion

    // Check nready
    // Readwrite
    // Check counter
    //Stop conversion
    // Check results
}*/
/** @fn AdcInstance_IsConvReady(uint8_t adc_id)
 *
 * @param[in] adc_id - ID of ADC-s from 0 to 4
 * @retval 1 All Channel's Conversion ready
 * @retval 0 Conversion is pending
 */
uint8_t ExtADC_IsConvReady(uint8_t deviceID)
{
    uint8_t flag=0u;

    //if(( ExtADC_Device[deviceID].convPendingDat.chEnable == ExtADC_Device[deviceID].convPendingDat.chNewDat)&& ExtADC_Device[deviceID].convPendingDat.chEnable > 0U)
    if(( ExtADC_Device[deviceID].convPendingDat.chPending == 0x00U)&& ExtADC_Device[deviceID].convPendingDat.chEnable > 0U)
    {
        flag = 1u;

    }
    else
    {
        flag = 0u;
    }


    return (flag);
}

/** @fn void AdcInstance_ClearConvFlag(uint8_t adc_id)
 * @brief Clear the chanell's conversion ready bits in their flag register
 * @param[in] adc_id
 */
void ExtADC_ClearConvFlag(uint8_t deviceID)
{
    ExtADC_Device[deviceID].convPendingDat.chNewDat = 0u;
    ExtADC_Device[deviceID].convPendingDat.chPending = ExtADC_Device[deviceID].convPendingDat.chEnable;
    ExtADC_Device[deviceID].convCHCnt=0x00U;
}
int16_t ExtADCEnableConversion(uint8_t deviceID)
{
    // SPIDriver_SetCS0Pin(adc_instance[instanceID].spiBusID,0x00U);
#if 10 == BOARD_HW_VER


#else
    GPIO_SetPinValue(  ExtADC_Device[deviceID].pNSynchPort,   ExtADC_Device[deviceID].nSynchPin, 0x01u);

#endif
    // printf("EN\r\n%d",gioGetBit(gioPORTA, 0x02U));
    return(0);
}
int16_t ExtADCDisableConversion(uint8_t deviceID)
{
#if 10 == BOARD_HW_VER


#else
    GPIO_SetPinValue(  ExtADC_Device[deviceID].pNSynchPort,   ExtADC_Device[deviceID].nSynchPin, 0x00u);

#endif
    return(0);
}
int16_t ExtADC_ResetConvFSM(uint8_t deviceID)
{
    uint16_t i=0U;
    ExtADC_Device[deviceID].convPendingDat.chConvState = EXTADC_ST_CH_WAIT_FOR_READY;  /* Initial State is waiting for Nready pin */
    ExtADC_Device[deviceID].convPendingDat.chNewDat = 0x00U;

    ExtADC_Device[deviceID].convPendingDat.chPending = ExtADC_Device[deviceID].convPendingDat.chEnable;
    ExtADC_Device[deviceID].convPendingDat.dataCount = 0u;
    ExtADC_Device[deviceID].convPendingDat.pin_nrdy[0] = 0u;
    ExtADC_Device[deviceID].convPendingDat.pin_nrdy[1] = 0u;
    //  ExtADC_Device[deviceID].convPendingDat.lastChID = 8u;                 /* Initial values is bigger than max to reset ch lost */
    ExtADC_Device[deviceID].convCHCnt = 0x00U;
    ExtADC_Device[deviceID].enableCRCMode = 0x00U;
    for(i=0u;i<9u;i++)
    {
        ExtADC_Device[deviceID].convChData[i].chData = 0x00u;
        ExtADC_Device[deviceID].convChData[i].chID = 16U;
        ExtADC_Device[deviceID].convChData[i].chErrFlag = 1u;
        ExtADC_Device[deviceID].convChData[i].chNRDYFlag = 1u;
    }
    return 0;
}

/** @fn static uint8_t adc_compute_crc(uint8_t pBuf[], uint8_t bufSize)
 *
 * @param aBuf
 * @param bufSize
 * @return
 */
static uint8_t ExtADC_CalculateCRC(uint8_t aBuf[], uint8_t bufSize)
{
    uint8_t bit_iter   = 0U;
    uint8_t array_iter   = 0U;
    uint8_t crc = 0U;
    while(bufSize)
    {
        for(bit_iter = 0x80U; bit_iter != 0U; bit_iter >>= 1U)
        {
            if(((crc & 0x80U) != 0U) != ((aBuf[array_iter] & bit_iter) != 0U)) /* MSB of CRC register XOR input Bit from Data */
            {
                crc <<= 1U;
                crc ^= AD7124_CRC8_POLYNOMIAL_REPRESENTATION;
            }
            else
            {
                crc <<= 1;
            }
        }
        array_iter++;
        bufSize--;
    }
    return crc;
}
