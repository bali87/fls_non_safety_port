/**********************************************************************
* (C) Copyright 2017, OptoForce Ltd.
* -------------------------------------------------------------------
* OPTOFORCE LIMITED PROPRIETARY INFORMATION
*
* Property of OptoForce ltd, Unauthorized reproduction and/or
* distribution is strictly prohibited. This product is protected
* under copyright law and trade secret law as an unpublished
* work
* (C) Copyright OptoForce. All rights reserved.
***********************************************************************/

/**
* @file adc.h
* @author Vincze Balazs
* @date 2017. okt. 12.
* @brief
*/
#ifndef DRIVERS_ADC_ADC_1H_
#define DRIVERS_ADC_ADC_1H_

#include <inttypes.h>
#include "spi_.h"
#include "board.h"

#define USED_ADC_CHANNELS   8u

#define USED_ADC_MODULES    4u

#define MAX_ADC_CHANNELS    (15U)
#define ADC_USE_CRC 0u

#if ADC_USE_CRC == (1u)
#define ADC_CONV_DAT_LENGTH 5U   /* 24 bit data + 8 bit status + 8bit crc */
#else
#define ADC_CONV_DAT_LENGTH 4U   /* 24 bit data + 8 bit status*/
#endif

#define AD7124_CRC8_POLYNOMIAL_REPRESENTATION (0x07u) /* x8 + x2 + x + 1 */

#define REG_COMMS   0x00u
#define REG_ID      0x05u

#define NWEN_SHIFT     7u
#define NWEN_MASK      0x80u
#define RNW_SHIFT       6u
#define RNW_MASK        0x40u
#define RS_SHIFT        0u
#define RS_MASK         0x3Fu

/* ADC 7124 Register shifts and masks */
/* COMMS reg. at 0x00 write*/
#define COMMS_NWEN_FLAG_SHIFT           7U
#define COMMS_NWEN_FLAG_MASK            0x01U
#define COMMS_RNW_FLAG_SHIFT            6U
#define COMMS_RNW_FLAG_MASK             0x01U
#define COMMS_REG_SEL_SHIFT             0U
#define COMMS_REG_SEL_MASK              0x3FU
/* Status reg. at 0x00 read*/
#define STAT_NRDY_FLAG_SHIFT            7U
#define STAT_NRDY_FLAG_MASK             0x01U
#define STAT_ERR_FLAG_SHIFT             6U
#define STAT_ERR_FLAG_MASK              0x01U
#define STAT_POR_FLAG_SHIFT             4U
#define STAT_POR_FLAG_MASK              0x01U
#define STAT_CH_ACTIVE_SHIFT            0U
#define STAT_CH_ACTIVE_MASK             0x0FU
/* Control reg. at 0x01 write-  read*/
#define AD_CTRL_DNRDY_DEL_FLAG_SHIFT   12U
#define AD_CTRL_DNRDY_DEL_FLAG_MASK    0x01U
#define AD_CTRL_CONTREAD_FLAG_SHIFT    11U
#define AD_CTRL_CONTREAD_FLAG_MASK     0x01U
#define AD_CTRL_DATSTAT_FLAG_SHIFT     10U
#define AD_CTRL_DATSTAT_FLAG_MASK      0x01U
#define AD_CTRL_CSEN_FLAG_SHIFT        9U
#define AD_CTRL_CSEN_FLAG_MASK         0x01U
#define AD_CTRL_REFEN_FLAG_SHIFT       8U
#define AD_CTRL_REFEN_FLAG_MASK        0x01U
#define AD_CTRL_POWMODE_SHIFT          6U
#define AD_CTRL_POWMODE_MASK           0x03U
#define AD_CTRL_MODE_SHIFT             2U
#define AD_CTRL_MODE_MASK              0x0FU
#define AD_CTRL_CLKSEL_SHIFT           0U
#define AD_CTRL_CLKSEL_MASK            0x03U
/* Error reg. at 0x06 read*/
#define ERR_LDO_CAP_FLAG_SHIFT         19U
#define ERR_LDO_CAP_FLAG_MASK          0x01U
#define ERR_ADC_CAL_FLAG_SHIFT         18U
#define ERR_ADC_CAL_FLAG_MASK          0x01U
#define ERR_ADC_CONV_FLAG_SHIFT        17U
#define ERR_ADC_CONV_FLAG_MASK         0x01U
#define ERR_ADC_SAT_FLAG_SHIFT         16U
#define ERR_ADC_SAT_FLAG_MASK          0x01U
#define ERR_AINP_OV_FLAG_SHIFT         15U
#define ERR_AINP_OV_FLAG_MASK          0x01U
#define ERR_AINP_UV_FLAG_SHIFT         14U
#define ERR_AINP_UV_FLAG_MASK          0x01U
#define ERR_AINM_OV_FLAG_SHIFT         13U
#define ERR_AINM_OV_FLAG_MASK          0x01U
#define ERR_AINM_UV_FLAG_SHIFT         12U
#define ERR_AINM_UV_FLAG_MASK          0x01U
#define ERR_REF_DET_FLAG_SHIFT         11U
#define ERR_REF_DET_FLAG_MASK          0x01U
#define ERR_DLDO_PSM_FLAG_SHIFT        9U
#define ERR_DLDO_PSM_FLAG_MASK         0x01U
#define ERR_ALDO_PSM_FLAG_SHIFT        7U
#define ERR_ALDO_PSMFLAG_MASK          0x01U
#define ERR_SPI_IGN_FLAG_SHIFT         6U
#define ERR_SPI_IGN_FLAG_MASK          0x01U
#define ERR_SPI_SCLK_CNT_FLAG_SHIFT    5U
#define ERR_SPI_SCLK_CNT_FLAG_MASK     0x01U
#define ERR_SPI_READ_FLAG_SHIFT        4U
#define ERR_SPI_READ_FLAG_MASK         0x01U
#define ERR_SPI_WRITE_FLAG_SHIFT       3U
#define ERR_SPI_WRITE_FLAG_MASK        0x01U
#define ERR_SPI_CRC_FLAG_SHIFT         2U
#define ERR_SPI_CRC_FLAG_MASK          0x01U
#define ERR_MM_CRC_FLAG_SHIFT          1U
#define ERR_MM_CRC_FLAG_MASK           0x01U
#define ERR_ROM_CRC_FLAG_SHIFT         0U
#define ERR_ROM_CRC_FLAG_MASK          0x01U
/* Error en reg. at 0x06 read*/
#define MCLK_CNT_EN_SHIFT               22
#define LDO_CAP_CHK_TEST_EN_SHIFT       21
#define LDO_CAP_CHK_SHIFT               20
#define ADC_CAL_ERR_EN_SHIFT            18
#define CONV_ERR_EN_SHIFT               17
#define ADC_SAT_ERR_EN_SHIFT            16
#define AINP_OV_ERR_EN_SHIFT            15
#define AINP_UV_ERR_EN_SHIFT            14
#define AINM_OV_ERR_EN_SHIFT            13
#define AINM_UV_ERR_EN_SHIFT            12
#define REF_DET_ERR_EN_SHIFT            11
#define DLDO_PSM_TRIP_TEST_EN_SHIFT     10
#define DLDO_PSM_ERR_ERR_SHIFT          9
#define ALDO_PSM_TRIP_TEST_EN_SHIFT     8
#define ALDO_PSM_ERR_EN_SHIFT           7
#define SPI_IGNORE_ERR_EN_SHIFT         6
#define SPI_SCLK_CNT_ERR_EN_SHIFT       5
#define SPI_READ_ERR_EN_SHIFT           4
#define SPI_WRITE_ERR_EN_SHIFT          3
#define SPI_CRC_ERR_EN_SHIFT            2
#define MM_CRC_ERR_EN_SHIFT             1
#define ROM_CRC_ERR_EN_SHIFT            0

#define MCLK_CNT_EN_MASK            0x01U
#define LDO_CAP_CHK_TEST_EN_MASK    0x01U
#define LDO_CAP_CHK_MASK            0x03U
#define ADC_CAL_ERR_EN_MASK         0x01U
#define CONV_ERR_EN_MASK            0x01U
#define ADC_SAT_ERR_EN_MASK         0x01U
#define AINP_OV_ERR_EN_MASK         0x01U
#define AINP_UV_ERR_EN_MASK         0x01U
#define AINM_OV_ERR_EN_MASK         0x01U
#define AINM_UV_ERR_EN_MASK         0x01U
#define REF_DET_ERR_EN_MASK         0x01U
#define DLDO_PSM_TRIP_TEST_EN_MASK  0x01U
#define DLDO_PSM_ERR_ERR_MASK       0x01U
#define ALDO_PSM_TRIP_TEST_EN_MASK  0x01U
#define ALDO_PSM_ERR_EN_MASK        0x01U
#define SPI_IGNORE_ERR_EN_MASK      0x01U
#define SPI_SCLK_CNT_ERR_EN_MASK    0x01U
#define SPI_READ_ERR_EN_MASK        0x01U
#define SPI_WRITE_ERR_EN_MASK       0x01U
#define SPI_CRC_ERR_EN_MASK         0x01U
#define MM_CRC_ERR_EN_MASK          0x01U
#define ROM_CRC_ERR_EN_MASK         0x01U
/* Channel Register from 0x09U */
#define CH_EN_FLAG_SHIFT                15U
#define CH_EN_FLAG_MASK                 0x01U
#define CH_SETUP_SEL_SHIFT              12U
#define CH_SETUP_SEL_MASK               0x07U
#define CH_AINP_SEL_SHIFT               5U
#define CH_AINP_SEL_MASK                0x1FU
#define CH_AINM_SEL_SHIFT               0U
#define CH_AINM_SEL_MASK                0x1FU
/* Configuration Register from 0x19U to  */
#define CONF_BIPOLAR_FLAG_SHIFT         11U
#define CONF_BIPOLAR_FLAG_MASK          0x01U
#define CONF_BURNOUT_SHIFT              9U
#define CONF_BURNOUT_MASK               0x03U
#define CONF_REFBUFP_FLAG_SHIFT         8U
#define CONF_REFBUFP_FLAG_MASK          0x01U
#define CONF_REFBUFM_FLAG_SHIFT         7U
#define CONF_REFBUFM_FLAG_MASK          0x01U
#define CONF_AINBUFP_FLAG_SHIFT         6U
#define CONF_AINBUFP_FLAG_MASK          0x01U
#define CONF_AINBUFM_FLAG_SHIFT         5U
#define CONF_AINBUFM_FLAG_MASK          0x01U
#define CONF_REFSEL_SHIFT               3U
#define CONF_REFSEL_MASK                0x03U
#define CONF_PGA_SHIFT                  0U
#define CONF_PGA_MASK                   0x07U
/* Filter reg. from 0x21 to 0x28 write-read*/
#define FILTER_TYPE_SEL_SHIFT           21U
#define FILTER_TYPE_SEL_MASK            0x07U
#define FILTER_REJ60_FLAG_SHIFT         20U
#define FILTER_REJ60_FLAG_MASK          0x01U
#define FILTER_POST_FILTER_SHIFT        17U
#define FILTER_POST_FILTER_MASK         0x07U
#define FILTER_SCYCLE_SHIFT             16U
#define FILTER_SCYCLE_MASK              0x01U
#define FILTER_FRATE_SHIFT              0U
#define FILTER_FRATE_MASK               0x7FFU
/* Address */
#define ADDRESS_COMM_REG      0x00
#define ADDRESS_STATUS_REG    0x00
#define ADDRESS_ADC_CTRL_REG  0x01
#define ADDRESS_DATA_REG      0x02
#define ADDRESS_IO_CTRL1_REG  0x03
#define ADDRESS_IO_CTRL2_REG  0x04
#define ADDRESS_ID_REG        0x05
#define ADDRESS_ERR_REG       0x06
#define ADDRESS_ERREN_REG     0x07
#define ADDRESS_CH0_MAP_REG   0x09
#define ADDRESS_CH1_MAP_REG   0x0A
#define ADDRESS_CH2_MAP_REG   0x0B
#define ADDRESS_CH3_MAP_REG   0x0C
#define ADDRESS_CH4_MAP_REG   0x0D
#define ADDRESS_CH5_MAP_REG   0x0E
#define ADDRESS_CH6_MAP_REG   0x0F
#define ADDRESS_CH7_MAP_REG   0x10
#define ADDRESS_CH8_MAP_REG   0x11
#define ADDRESS_CH9_MAP_REG   0x12
#define ADDRESS_CH10_MAP_REG  0x13
#define ADDRESS_CH11_MAP_REG  0x14
#define ADDRESS_CH12_MAP_REG  0x15
#define ADDRESS_CH13_MAP_REG  0x16
#define ADDRESS_CH14_MAP_REG  0x17
#define ADDRESS_CH15_MAP_REG  0x18
#define ADDRESS_CFG0_REG      0x19
#define ADDRESS_CFG1_REG      0x1A
#define ADDRESS_CFG2_REG      0x1B
#define ADDRESS_CFG3_REG      0x1C
#define ADDRESS_CFG4_REG      0x1D
#define ADDRESS_CFG5_REG      0x1E
#define ADDRESS_CFG6_REG      0x1F
#define ADDRESS_CFG7_REG      0x20
#define ADDRESS_FILT0_REG     0x21
#define ADDRESS_FILT1_REG     0x22
#define ADDRESS_FILT2_REG     0x23
#define ADDRESS_FILT3_REG     0x24
#define ADDRESS_FILT4_REG     0x25
#define ADDRESS_FILT5_REG     0x26
#define ADDRESS_FILT6_REG     0x27
#define ADDRESS_FILT7_REG     0x28
#define ADDRESS_OFFS0_REG     0x29
#define ADDRESS_OFFS1_REG     0x2A
#define ADDRESS_OFFS2_REG     0x2B
#define ADDRESS_OFFS3_REG     0x2C
#define ADDRESS_OFFS4_REG     0x2D
#define ADDRESS_OFFS5_REG     0x2E
#define ADDRESS_OFFS6_REG     0x2F
#define ADDRESS_OFFS7_REG     0x30
#define ADDRESS_GAIN0_REG     0x31
#define ADDRESS_GAIN1_REG     0x32
#define ADDRESS_GAIN2_REG     0x33
#define ADDRESS_GAIN3_REG     0x34
#define ADDRESS_GAIN4_REG     0x35
#define ADDRESS_GAIN5_REG     0x36
#define ADDRESS_GAIN6_REG     0x37
#define ADDRESS_GAIN7_REG     0x38
#define ADDRESS_MAX_REG       0x39
/* Size*/
#define SIZE_COMM_REG           0x01U
#define SIZE_STATUS_REG         0x01U
#define SIZE_ADC_CTRL_REG       0x02U
#define SIZE_DATA_REG           0x04U         /* Original 3 if dataStatus == 1 then 4 */
#define SIZE_IO_CTRL1_REG       0x03U
#define SIZE_IO_CTRL2_REG       0x03U
#define SIZE_ID_REG             0x01U
#define SIZE_ERR_REG            0x03U
#define SIZE_ERREN_REG          0x03U
#define SIZE_CH_MAP_REG         0x02U
#define SIZE_CFG_REG            0x02U
#define SIZE_FILT_REG           0x03U
#define SIZE_OFFS_REG           0x03U
#define SIZE_GAIN_REG           0x03U
#define ADC_CH_ENABLE           0x01U
#define ADC_CH_DISABLE          0x00U
#define BIPOLAR                 0x01U
#define UNIPOLAR                0x00U

typedef enum
{
    ADC_INSTANCE1 = 0x00U,
#if USED_ADC_MODULES == 1
    MAX_ADC_INSTANCE = 1
#elif USED_ADC_MODULES == 2
    ADC_INSTANCE2 = 0x01U,
    MAX_ADC_INSTANCE = 0x02U
#elif USED_ADC_MODULES == 3
    ADC_INSTANCE2 = 0x01U,
    ADC_INSTANCE3 = 0x02U,
    MAX_ADC_INSTANCE = 0x03U
#elif USED_ADC_MODULES == 4
    ADC_INSTANCE2 = 0x01U,
    ADC_INSTANCE3 = 0x02U,
    ADC_INSTANCE4 = 0x03U,
    MAX_ADC_INSTANCE = 0x04U
#else
#error "Only 4 ADC Instzances Can used with these hardware!"
#endif
}ExtADC_DeviceIDType;

typedef enum
{
    EXT_ADC_SEL_AIN0             =   0x00u,
    EXT_ADC_SEL_AIN1             =   0x01u,
    EXT_ADC_SEL_AIN2             =   0x02u,
    EXT_ADC_SEL_AIN3             =   0x03u,
    EXT_ADC_SEL_AIN4             =   0x04u,
    EXT_ADC_SEL_AIN5             =   0x05u,
    EXT_ADC_SEL_AIN6             =   0x06u,
    EXT_ADC_SEL_AIN7             =   0x07u,
    EXT_ADC_SEL_AIN8             =   0x08u,
    EXT_ADC_SEL_AIN9             =   0x09u,
    EXT_ADC_SEL_AIN10            =   0x0au,
    EXT_ADC_SEL_AIN11            =   0x0bu,
    EXT_ADC_SEL_AIN12            =   0x0cu,
    EXT_ADC_SEL_AIN13            =   0x0du,
    EXT_ADC_SEL_AIN14            =   0x0eu,
    EXT_ADC_SEL_AIN15            =   0x0fu,
    EXT_ADC_SEL_TEMP_SENS        =   0x10u,
    EXT_ADC_SEL_AVSS             =   0x11u,
    EXT_ADC_SEL_INTREF           =   0x12u,
    EXT_ADC_SEL_DGND             =   0x13u,
    EXT_ADC_SEL_MON_SUPPLY_POS   =   0x14u,
    EXT_ADC_SEL_MON_SUPPLY_NEG   =   0x15u,
    EXT_ADC_SEL_MON_IOV_POS      =   0x16u,
    EXT_ADC_SEL_MON_IOV_NEG      =   0x17u,
    EXT_ADC_SEL_MON_ALDO_POS     =   0x18u,
    EXT_ADC_SEL_MON_ALDO_NEG     =   0x19u,
    EXT_ADC_SEL_MON_DLDO_POS     =   0x1au,
    EXT_ADC_SEL_MON_DLDO_NEG     =   0x1bu,
    EXT_ADC_SEL_MON_V20MV_POS    =   0x1cu,
    EXT_ADC_SEL_MON_V20MV_NEG    =   0x1du,
    EXT_ADC_SEL_RESERVED1        =   0x1eu,
    EXT_ADC_SEL_RESERVED2        =   0x1fu
}ExtADC_PinSelectType;

typedef enum
{
    EXT_ADC_SEL_PD1          =   EXT_ADC_SEL_AIN1,
    EXT_ADC_SEL_PD2          =   EXT_ADC_SEL_AIN4,
    EXT_ADC_SEL_PD3          =   EXT_ADC_SEL_AIN7,
    EXT_ADC_SEL_PD4          =   EXT_ADC_SEL_AIN10,
    EXT_ADC_SEL_TEMP_MEAS    =   EXT_ADC_SEL_AIN5,
    EXT_ADC_SEL_LED_I        =   EXT_ADC_SEL_AIN8,
    EXT_ADC_SEL_LED_VFMEAS   =   EXT_ADC_SEL_AIN2,
    EXT_ADC_SEL_REFOUT       =   EXT_ADC_SEL_AIN11,
    EXT_ADC_SEL_INT_TEMP     =   EXT_ADC_SEL_TEMP_SENS
}adc_pin_select_t;
typedef enum
{   POWER_MODE_LOW =    0X00U,
    POWER_MODE_MID =    0X01U,
    POWER_MODE_FULL0 =  0X02U,
    POWER_MODE_FULL1 =  0X03U,
    POWER_MODE_MAX =    0X04U
}ExtADC_PowerModeType;
typedef enum
{
    FILTER_TYPE_SYNC4 =          0X00U,
    FILTER_TYPE_RES1 =           0X01U,
    FILTER_TYPE_SYNC3 =          0X02U,
    FILTER_TYPE_RES2 =           0X03U,
    FILTER_TYPE_FASTL_SYNC4 =    0X04U,
    FILTER_TYPE_FASTL_SYNC3 =    0X05U,
    FILTER_TYPE_RES3 =           0X06U,
    FILTER_TYPE_POST_EN =        0X07U,
    FILTER_TYPE_MAX =           0X08U
}ExtADC_FilterType;
typedef enum
{
    SETUP_SEL0       =          0X00U,
    SETUP_SEL1       =          0X01U,
    SETUP_SEL2       =          0X02U,
    SETUP_SEL3       =          0X03U,
    SETUP_SEL4       =          0X04U,
    SETUP_SEL5       =          0X05U,
    SETUP_SEL6       =          0X06U,
    SETUP_SEL7       =          0X07U,
    SETUP_SEL_MAX    =          0X08U
}ExtADC_SetupSelectType;

typedef enum
{
    OP_MODE_CONT_CONV =         0X00U,
    OP_MODE_SING_CONV =         0X01U,
    OP_MODE_STAND_BY =          0X02U,
    OP_MODE_POWER_DOWN =        0X03U,
    OP_MODE_POWER_IDLE =        0X04U,
    OP_MODE_INT_ZSCALE_CALIB =  0X05U,
    OP_MODE_INT_FSCALE_CALIB =  0X06U,
    OP_MODE_SYS_ZSCALE_CALIB =  0X07U,
    OP_MODE_SYS_FSCALE_CALIB =  0X08U,
    OP_MODE_MAX =               0X09U
}ExtADC_OpModeType;
typedef enum
{
    ADC_MASTER_MODE_UNKNOWN =   0X00U,
    ADC_MASTER_MODE_MASTER  =   0X01U,
    ADC_MASTER_MODE_SLAVE   =   0X02U,
    ADC_MASTER_MODE_MAX     =   0X03U
}ExtADC_MasterModeType;


typedef struct _adc_ch_dat_t
{
    uint32_t    ch_data;
    uint8_t     ch_num;
}adc_ch_dat_t;

extern spiDAT1_t    dataconfig;

uint8_t ExtADC_Init(uint8_t masterMode);
int16_t ExtADC_InitDeviceMaster(uint8_t deviceId);
extern int16_t ExtADC_RunMasterConvFSM(uint8_t deviceID);
int16_t ExtADC_RunMasterConvFSMTest(uint8_t deviceID);

uint8_t ExtADC_IsConvReady(uint8_t deviceId);
void ExtADC_ClearConvFlag(uint8_t deviceID);
int16_t ExtADC_GetFinishedChannels(uint8_t deviceID,adc_ch_dat_t * p_dat); //fixme move to .h file.�
int16_t ExtADCEnableConversion(uint8_t deviceID);
int16_t ExtADCDisableConversion(uint8_t deviceID);
uint32_t ExtADC_GetCHData(uint8_t deviceID,uint8_t chID);
void ExtADC_ResetPulseCNT(uint8_t deviceID);
uint32_t ExtADC_GetPulseCNT(uint8_t deviceID);
uint32_t ExtADC_GetDataCount(uint8_t deviceID);
int16_t ExtADC_GetOffsetRegister(uint8_t deviceID,uint8_t offset_id,uint32_t *offsetPtr);
#endif /* DRIVERS_ADC_ADC_1H_ */

