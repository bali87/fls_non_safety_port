#ifndef OPTOFORCE_FLS_EXTADCREGISTER_H
#define OPTOFORCE_FLS_EXTADCREGISTER_H

/**
 * @file
 * 
 * Header file of the unit ExtADCRegister.h. //fixme remove .h
 *
 * @version $Id$
 */

/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */

#include "Std.h"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */
typedef enum
{
    EXT_ADC_REGISTER_PERM_R  = 1U,
    EXT_ADC_REGISTER_PERM_W  = 2U,
    EXT_ADC_REGISTER_PERM_RW = 3U
}ExtADCRegister_PermType;
typedef struct _ExtADCRegister_RegType
{
    uint8_t                 size;
    uint32_t                data;
    ExtADCRegister_PermType permission;
}ExtADCRegister_RegType;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */
const ExtADCRegister_RegType ExtADCRegister_RegTable [0x39];



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */



/** @} */

#endif
