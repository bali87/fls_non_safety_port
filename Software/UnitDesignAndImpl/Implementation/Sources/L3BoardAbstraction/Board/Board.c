/**
 * @file
 *
 * Unit that manages:
 * - .....
 * - .........
 *
 * @version $Id$
 */
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */

#include <ExtADCDrv.h>
#include <GPIODrv.h>
#include <spi_.h>
#include "Std.h"
#include "Board.h"
#include "Debug.h"
//#include "MCU.h"
//#include "MCUConfig.h"
//#include "GPIO.h"
#include "SPIDriver.h"
//#include "I2C.h"
//#include "ExtADC.h"
#include "SysTimer.h"
//#include "UART.h"
#include "sys_common.h"
#include "sys_core.h"
#include "rti.h"
#include "sci.h"
#include "can.h"
#include "gio.h"
#include "reg_spi.h"
//#include "Process.h"
#include "RTIDriver.h"
#include "ComManager.h"
/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticDefines Static defines
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */

/** @enum Board_StateType
 *	@brief Enum State of module initialization.
 */
typedef enum
{
    BOARD_STATE_UNINITIALISED = 0u,
    BOARD_STATE_INITIALISED = 1u
} Board_StateType;
typedef struct
{
    master_mode_t masterMode;
}common_settings_t;

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */

#if 0
const uint8_t __attribute__( (space(prog), keep, address(0x4800) )) Board_ConfigData[16] =
    {
        HARDWARE_TYPE,
        FIRMWARE_VERSION, 0x0, 0x0, PROTOCOL_VERSION, 0u
    };
#endif
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */
/** @var static Board_StateType Board_State
    @brief State of module initialization.
 */
common_settings_t Board_CommonSettings;
static Board_StateType Board_State = BOARD_STATE_UNINITIALISED;
#if 1
static Board_SignalGroupType Board_SignalGroup[BOARD_SIGNAL_GROUP_COUNT];
static uint16 Board_SignalRef5V;
static uint8 Board_SensorFailureStatus = 0u;
#endif
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */

Std_ReturnType Board_Init (void)
{
    Std_ReturnType retVal;
    uint32_t masterIDPin ;
    _enable_interrupt_();
    /* Init GPIO */
     GPIO_Init();
      /* Master */
     GPIO_SetPinODR(BOARD_CAN_RS_PORT, BOARD_CAN_RS_PIN, 0X00u);
     GPIO_SetPinDirection(BOARD_CAN_RS_PORT, BOARD_CAN_RS_PIN, GPIO_OUT);
     GPIO_SetPinPUPD(BOARD_CAN_RS_PORT, BOARD_CAN_RS_PIN, 0x00U, 0x00U);
     GPIO_SetPinValue(BOARD_CAN_RS_PORT, BOARD_CAN_RS_PIN, 0x00U);
     /* Slave */
     GPIO_SetPinODR(BOARD_CAN_SHDN_PORT, BOARD_CAN_SHDN_PIN, 0X00u);
     GPIO_SetPinDirection(BOARD_CAN_SHDN_PORT, BOARD_CAN_SHDN_PIN, GPIO_OUT);
     GPIO_SetPinPUPD(BOARD_CAN_SHDN_PORT, BOARD_CAN_SHDN_PIN, 0x00U, 0x00U);
     GPIO_SetPinValue(BOARD_CAN_SHDN_PORT, BOARD_CAN_SHDN_PIN, 0x00U);

     GPIO_SetPinODR(BOARD_LED_GRN_PORT, BOARD_LED_GRN_PIN, 0X00u);
     GPIO_SetPinDirection(BOARD_LED_GRN_PORT, BOARD_LED_GRN_PIN, GPIO_OUT);
     GPIO_SetPinPUPD(BOARD_LED_GRN_PORT, BOARD_LED_GRN_PIN, 0x00U, 0x00U);
     GPIO_SetPinValue(BOARD_LED_GRN_PORT, BOARD_LED_GRN_PIN, 0x01U);

     GPIO_SetPinODR(BOARD_LED_BLU_PORT, BOARD_LED_BLU_PIN, 0X00u);
     GPIO_SetPinDirection(BOARD_LED_BLU_PORT, BOARD_LED_BLU_PIN, GPIO_OUT);
     GPIO_SetPinPUPD(BOARD_LED_BLU_PORT, BOARD_LED_BLU_PIN, 0x00U, 0x00U);
     GPIO_SetPinValue(BOARD_LED_BLU_PORT, BOARD_LED_BLU_PIN, 0x01U);

     GPIO_SetPinODR(BOARD_LED_RED_PORT, BOARD_LED_RED_PIN, 0X00u);
     GPIO_SetPinDirection(BOARD_LED_RED_PORT, BOARD_LED_RED_PIN, GPIO_OUT);
     GPIO_SetPinPUPD(BOARD_LED_RED_PORT, BOARD_LED_RED_PIN, 0x00U, 0x00U);
     GPIO_SetPinValue(BOARD_LED_RED_PORT, BOARD_LED_RED_PIN, 0x00U);

     GPIO_SetPinODR(BOARD_LED_RED_PORT, BOARD_LED_RED_PIN, 0X00u);
     GPIO_SetPinDirection(BOARD_LED_RED_PORT, BOARD_LED_RED_PIN, GPIO_OUT);
     GPIO_SetPinPUPD(BOARD_LED_RED_PORT, BOARD_LED_RED_PIN, 0x00U, 0x00U);
     GPIO_SetPinValue(BOARD_LED_RED_PORT, BOARD_LED_RED_PIN, 0x01U);

     GPIO_SetPinODR(BOARD_MSTNSLV_PORT, BOARD_MSTNSLV_PIN, 0X00u);
     GPIO_SetPinDirection(BOARD_MSTNSLV_PORT, BOARD_MSTNSLV_PIN, GPIO_IN);
     GPIO_SetPinPUPD(BOARD_MSTNSLV_PORT, BOARD_MSTNSLV_PIN, 0x00U, 0x00U);
     GPIO_SetPinValue(BOARD_MSTNSLV_PORT, BOARD_MSTNSLV_PIN, 0x01U);
#if (10 == BOARD_HW_VER)
     GPIO_SetPinODR(BOARD_EXTADC_SYNC_PORT, BOARD_EXTADC_SYNC_PIN, 0X00u);
     GPIO_SetPinDirection(BOARD_EXTADC_SYNC_PORT, BOARD_EXTADC_SYNC_PIN, GPIO_OUT);
     GPIO_SetPinPUPD(BOARD_EXTADC_SYNC_PORT, BOARD_EXTADC_SYNC_PIN, 0x00U, 0x00U);
     GPIO_SetPinValue(BOARD_EXTADC_SYNC_PORT, BOARD_EXTADC_SYNC_PIN, 0x01U);
#else
     GPIO_SetPinODR(BOARD_EXTADC1_SYNC_PORT, BOARD_EXTADC1_SYNC_PIN, 0X00u);
     GPIO_SetPinDirection(BOARD_EXTADC1_SYNC_PORT, BOARD_EXTADC1_SYNC_PIN, GPIO_OUT);
     GPIO_SetPinPUPD(BOARD_EXTADC1_SYNC_PORT, BOARD_EXTADC1_SYNC_PIN, 0x00U, 0x00U);
     GPIO_SetPinValue(BOARD_EXTADC1_SYNC_PORT, BOARD_EXTADC1_SYNC_PIN, 0x01U);

     GPIO_SetPinODR(BOARD_EXTADC2_SYNC_PORT, BOARD_EXTADC2_SYNC_PIN, 0X00u);
     GPIO_SetPinDirection(BOARD_EXTADC2_SYNC_PORT, BOARD_EXTADC2_SYNC_PIN, GPIO_OUT);
     GPIO_SetPinPUPD(BOARD_EXTADC2_SYNC_PORT, BOARD_EXTADC2_SYNC_PIN, 0x00U, 0x00U);
     GPIO_SetPinValue(BOARD_EXTADC2_SYNC_PORT, BOARD_EXTADC2_SYNC_PIN, 0x01U);

     GPIO_SetPinODR(BOARD_EXTADC3_SYNC_PORT, BOARD_EXTADC3_SYNC_PIN, 0X00u);
     GPIO_SetPinDirection(BOARD_EXTADC3_SYNC_PORT, BOARD_EXTADC3_SYNC_PIN, GPIO_OUT);
     GPIO_SetPinPUPD(BOARD_EXTADC3_SYNC_PORT, BOARD_EXTADC3_SYNC_PIN, 0x00U, 0x00U);
     GPIO_SetPinValue(BOARD_EXTADC3_SYNC_PORT, BOARD_EXTADC3_SYNC_PIN, 0x01U);
#endif

     /* Determine Master mode */
#if 1
      masterIDPin =  GPIO_GetPinValue(BOARD_MSTNSLV_PORT,BOARD_MSTNSLV_PIN);

       /*This is MASTER MCU */
       if(0X01U == masterIDPin) // 1 was correct changed for hercules board
       {
           GPIO_SetPinValue(BOARD_LED_RED_PORT, BOARD_LED_RED_PIN, 0x01U);
           GPIO_SetPinValue(BOARD_LED_GRN_PORT, BOARD_LED_GRN_PIN, 0x00U);
           // rs master ->Low
           Board_CommonSettings.masterMode = MASTER_MODE_MASTER;
           GPIO_SetPinValue(BOARD_CAN_RS_PORT, BOARD_CAN_RS_PIN, 0x00U);
       }
       /* This is SLAVE MCU */
       else
       {
           GPIO_SetPinValue(BOARD_LED_RED_PORT, BOARD_LED_RED_PIN, 0x01U);
           GPIO_SetPinValue(BOARD_LED_BLU_PORT, BOARD_LED_BLU_PIN, 0x00U);
           //can shdn -ylow
           GPIO_SetPinValue(BOARD_CAN_SHDN_PORT, BOARD_CAN_SHDN_PIN, 0x00U);
           Board_CommonSettings.masterMode = MASTER_MODE_SLAVE;
       }
#endif
       /* USART Init Debug ?*/
      sciInit();

    // CANDriver_Init(BOARD_CAN_TX_ID,BOARD_CAN_RX_ID,0);
     CANDrv_InitInstance(CAN_INS1,BOARD_CAN_TX_ID,BOARD_CAN_RX_ID,1u,32u,33u,16u);
      RTIDriver_Init();
      SPIDriver_Init();
      Board_CommonSettings.masterMode = Board_GetMasterMode();
         if(MASTER_MODE_MASTER == Board_CommonSettings.masterMode)
         {
             PUTS("Mode: Master Mode\r\n");
             retVal = ExtADC_Init(Board_CommonSettings.masterMode);  /*@todo: Add Master Mode */
         }
         else
         {
             while(1)
             {
                 /* @note:  Slave only implemented until enable can GPIO pin */

             }
            // PUTS("Mode: Slave Mode\r\n");
         }


     //


    /* Initialise static variables
        .....
     */

    Board_State = BOARD_STATE_INITIALISED;
    return (retVal);
}

void Board_CollectSignals (void)
{
    uint8_t groupIterator;
    uint8_t pdIterator;
    uint8_t flag = 0;
   /* while(1)
    {

        putchar(GPIO_GetPinValue(BOARD_EXTADC1_NRDY_PORT,BOARD_EXTADC1_NRDY_PIN)+'0');
        putchar('\n');


    }*/

    ExtADCEnableConversion(ADC_INSTANCE1);
    ExtADCEnableConversion(ADC_INSTANCE2);
    ExtADCEnableConversion(ADC_INSTANCE3);

    if(MASTER_MODE_MASTER == Board_CommonSettings.masterMode)
    {

       while(0u == flag)
       {
           flag = 1u;
           if( 0u == (flag &= ExtADC_IsConvReady(ADC_INSTANCE1)))
           {
              ExtADC_RunMasterConvFSM(ADC_INSTANCE1);              /*Non blocking adc receive FSM for converted data from ADC */

           }



           if( 0u == (flag &= ExtADC_IsConvReady(ADC_INSTANCE2)))
           {
                     ExtADC_RunMasterConvFSM(ADC_INSTANCE2);              /*Non blocking adc receive FSM for converted data from ADC */

           }


           if( 0u == (flag &= ExtADC_IsConvReady(ADC_INSTANCE3)))
                   {
                             ExtADC_RunMasterConvFSM(ADC_INSTANCE3);              /*Non blocking adc receive FSM for converted data from ADC */

                   }


         //  ComManager_RunRxFSM();
       }
       ExtADC_ClearConvFlag(ADC_INSTANCE1);
       ExtADCDisableConversion(ADC_INSTANCE1);
       ExtADC_ClearConvFlag(ADC_INSTANCE2);
       ExtADCDisableConversion(ADC_INSTANCE2);
       ExtADC_ClearConvFlag(ADC_INSTANCE3);
       ExtADCDisableConversion(ADC_INSTANCE3);



    }



    Board_SignalRef5V = (uint16) (ExtADC_GetCHData(ADC_INSTANCE1,6));

    Board_SignalGroup[BOARD_SG1].pdArray[0] = (uint16) (ExtADC_GetCHData(ADC_INSTANCE1,0) >> 7);
    Board_SignalGroup[BOARD_SG1].pdArray[1] = (uint16) (ExtADC_GetCHData(ADC_INSTANCE1,1) >> 7);;
    Board_SignalGroup[BOARD_SG1].pdArray[2] = (uint16) (ExtADC_GetCHData(ADC_INSTANCE1,2) >> 7);
    Board_SignalGroup[BOARD_SG1].pdArray[3] = (uint16) (ExtADC_GetCHData(ADC_INSTANCE1,3) >> 7);
    Board_SignalGroup[BOARD_SG1].tempDiode = (uint16_t)50000u-(uint16) ((BOARD_TEMP_PRE_MUL * ((uint16)  (ExtADC_GetCHData(ADC_INSTANCE1,6) >> 8))) / BOARD_TEMP_POST_DIV);


    Board_SignalGroup[BOARD_SG2].pdArray[0] = (uint16) (ExtADC_GetCHData(ADC_INSTANCE2,0) >> 7);
       Board_SignalGroup[BOARD_SG2].pdArray[1] = (uint16) (ExtADC_GetCHData(ADC_INSTANCE2,1) >> 7);;
       Board_SignalGroup[BOARD_SG2].pdArray[2] = (uint16) (ExtADC_GetCHData(ADC_INSTANCE2,2) >> 7);
       Board_SignalGroup[BOARD_SG2].pdArray[3] = (uint16) (ExtADC_GetCHData(ADC_INSTANCE2,3) >> 7);
       Board_SignalGroup[BOARD_SG2].tempDiode =  (uint16_t)50000u-(uint16) ((BOARD_TEMP_PRE_MUL * ( (uint16)  (ExtADC_GetCHData(ADC_INSTANCE2,6) >> 8))) / BOARD_TEMP_POST_DIV);

       Board_SignalGroup[BOARD_SG3].pdArray[0] = (uint16) (ExtADC_GetCHData(ADC_INSTANCE3,0) >> 7);
            Board_SignalGroup[BOARD_SG3].pdArray[1] = (uint16) (ExtADC_GetCHData(ADC_INSTANCE3,1) >> 7);;
            Board_SignalGroup[BOARD_SG3].pdArray[2] = (uint16) (ExtADC_GetCHData(ADC_INSTANCE3,2) >> 7);
            Board_SignalGroup[BOARD_SG3].pdArray[3] = (uint16) (ExtADC_GetCHData(ADC_INSTANCE3,3) >> 7);
            Board_SignalGroup[BOARD_SG3].tempDiode =  (uint16_t)50000u-(uint16) ((BOARD_TEMP_PRE_MUL * ( (uint16)  (ExtADC_GetCHData(ADC_INSTANCE3,6) >> 8))) / BOARD_TEMP_POST_DIV);
/*


        Board_SignalGroup[BOARD_SG4].pdArray[0] = (uint16) (adc_ch_dat_arr[BOARD_SG4][0].ch_data >> 8);
            Board_SignalGroup[BOARD_SG4].pdArray[1] = (uint16) (adc_ch_dat_arr[BOARD_SG4][1].ch_data >> 8);;
            Board_SignalGroup[BOARD_SG4].pdArray[2] = (uint16) (adc_ch_dat_arr[BOARD_SG4][2].ch_data >> 8);
            Board_SignalGroup[BOARD_SG4].pdArray[3] = (uint16) (adc_ch_dat_arr[BOARD_SG4][3].ch_data >> 8);
            Board_SignalGroup[BOARD_SG4].tempDiode = (uint16) ((BOARD_TEMP_PRE_MUL * (Board_SignalRef5V
                       - (uint16)  (adc_ch_dat_arr[BOARD_SG4][4].ch_data >> 8))) / BOARD_TEMP_POST_DIV);*/


    /*Check sensor failure */
    Board_SensorFailureStatus = BOARD_ERR_NO_ERROR;
    for (groupIterator = 0u; groupIterator < BOARD_SIGNAL_GROUP_COUNT; groupIterator++)
    {
        for (pdIterator = 0u; pdIterator < BOARD_PD_COUNT; pdIterator++)
        {
            if(Board_SignalGroup[groupIterator].pdArray[pdIterator] < 800u)
            {
                Board_SensorFailureStatus |= BOARD_ERR_SENS_FAIL;
            }
        }
    }






    //ExtADC_ClearConvFlag(ADC_INSTANCE3);
}

Std_ReturnType Board_GetSignalGroup (uint8 sgID, Board_SignalGroupType * sgPtr)
{
    Std_ReturnType retVal;
#if 1
    if ((sgID < BOARD_SIGNAL_GROUP_COUNT)&&(NULL != sgPtr))
    {
        sgPtr->pdArray[0] = Board_SignalGroup[sgID].pdArray[0];
        sgPtr->pdArray[1] = Board_SignalGroup[sgID].pdArray[1];
        sgPtr->pdArray[2] = Board_SignalGroup[sgID].pdArray[2];
        sgPtr->pdArray[3] = Board_SignalGroup[sgID].pdArray[3];
        sgPtr->tempDiode = Board_SignalGroup[sgID].tempDiode;
        retVal = E_OK;
    }
    else
    {
        retVal = E_NOT_OK;
    }
#endif
    return (retVal);
}

uint16 Board_GetRef5VSignal (void)
{
#if 1
    //uint64_t voltage = (((uint64_t)ExtADC_GetChannelValue(A2V5) << 13)/(0x780000u));  //1000 us

    return(Board_SignalRef5V);// (ExtADC_GetChannelValue (A2V5) / 960u);
#endif
}

uint8 Board_GetSensorFailureStatus(void)
{
#if 1
    return (Board_SensorFailureStatus);
#endif
}
master_mode_t Board_GetMasterMode(void)
{
    master_mode_t masterMode = Board_CommonSettings.masterMode;
return (masterMode);
}
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/** @} */


