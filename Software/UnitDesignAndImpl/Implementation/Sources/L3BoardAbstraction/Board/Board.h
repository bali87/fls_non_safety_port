#ifndef Optoforce_SHexHost_Board_H
#define Optoforce_SHexHost_Board_H

/**
 * @file
 *
 * Unit that manages:
 * - .....
 * - .........
 *
 * @version $Id$
 */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */
#include "Std.h"
//#include "process.h"
//#include "GPIO.h"
//#include "ExtADC.h"
#include "GPIODrv.h"
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */

#define BOARD_HW_VER                        (11u)




#define BOARD_CAN_RS_PORT    hetPORT1
#define BOARD_CAN_RS_PIN     30U
#define BOARD_CAN_SHDN_PORT  gioPORTA
#define BOARD_CAN_SHDN_PIN   3U
#define BOARD_LED_RED_PORT   gioPORTB
#define BOARD_LED_RED_PIN    7U
#define BOARD_LED_GRN_PORT   gioPORTB
#define BOARD_LED_GRN_PIN    4U
#define BOARD_LED_BLU_PORT   gioPORTB
#define BOARD_LED_BLU_PIN    5U
#define BOARD_MSTNSLV_PORT   gioPORTA /* TODO: Check it */
#define BOARD_MSTNSLV_PIN    0u

#define BOARD_ADC_SYNC_PORT  gioPORTA
#define BOARD_ADC_SYNC_PIN   2u

#if (10 == BOARD_HW_VER)

#define BOARD_EXTADC_SYNC_PORT  gioPORTA
#define BOARD_EXTADC_SYNC_PIN   2u


#define BOARD_EXTADC1_NRDY_PORT  spiPORT1
#define BOARD_EXTADC1_NRDY_PIN   8u

#define BOARD_EXTADC2_NRDY_PORT  spiPORT2
#define BOARD_EXTADC2_NRDY_PIN   8u

#define BOARD_EXTADC3_NRDY_PORT  spiPORT3
#define BOARD_EXTADC3_NRDY_PIN   8u

#define BOARD_EXTADC4_NRDY_PORT  spiPORT4
#define BOARD_EXTADC4_NRDY_PIN   8u

#else

#define BOARD_EXTADC1_SYNC_PORT  gioPORTA
#define BOARD_EXTADC1_SYNC_PIN   4u

#define BOARD_EXTADC2_SYNC_PORT  gioPORTA
#define BOARD_EXTADC2_SYNC_PIN   5u

#define BOARD_EXTADC3_SYNC_PORT  gioPORTA
#define BOARD_EXTADC3_SYNC_PIN   6u

#define BOARD_EXTADC4_SYNC_PORT  gioPORTA
#define BOARD_EXTADC4_SYNC_PIN   7u


#define BOARD_EXTADC1_NRDY_PORT  gioPORTB
#define BOARD_EXTADC1_NRDY_PIN   0u

#define BOARD_EXTADC2_NRDY_PORT  gioPORTB
#define BOARD_EXTADC2_NRDY_PIN   1u

#define BOARD_EXTADC3_NRDY_PORT  gioPORTB
#define BOARD_EXTADC3_NRDY_PIN   2u

#define BOARD_EXTADC4_NRDY_PORT  gioPORTB
#define BOARD_EXTADC4_NRDY_PIN   6u
#endif


#define BOARD_SIGNAL_GROUP_COUNT             3
#define BOARD_SIGNAL_GROUP_MEMBERS_COUNT     5u
#define BOARD_SIGNAL_GROUP_PD_COUNT          4u


#define BOARD_PRODUCT_TYPE                  (64u)


#define BOARD_TEMP_PRE_MUL      1ul//(20ul)
#define BOARD_TEMP_POST_DIV     1ul//(10ul)

#define BOARD_CAN_TX_ID         (0x100u)
#define BOARD_CAN_RX_ID         (0x101u)

#define FIRMWARE_VERSION_MAJOR  (2u)
#define FIRMWARE_VERSION_MINOR  (10u)
#define FIRMWARE_VERSION        ((FIRMWARE_VERSION_MAJOR*100) + FIRMWARE_VERSION_MINOR)
#define PROTOCOL_VERSION        (105u)
#define HARDWARE_TYPE           (60u)
#if 0
#define REQUIRED_DATA_FORMAT_VERSION    (1u)
    
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */
#endif
#if 0
typedef enum
{
    MASTER_MODE_UNKNOWN     = 0x00U,
    MASTER_MODE_MASTER       = 0x01U,
    MASTER_MODE_SLAVE      = 0x02U,
    MASTER_MODE_MAX         = 0x03U
}master_mode_t;
#endif
typedef enum {
    BOARD_SG1 = 0u,
    BOARD_SG2 = 1u,
    BOARD_SG3 = 2u,
    BOARD_SG4 = 3u,
    BOARD_SIGNAL_GROUP_NUM = 4u
} Board_SignalGroupIDType;

typedef enum {
    BOARD_PD1 = 0u,
    BOARD_PD2 = 1u,
    BOARD_PD3 = 2u,
    BOARD_PD4 = 3u,
    BOARD_PD_COUNT = 4u
} Board_PhotoDiodeIDType;


typedef struct {
    uint16 pdArray[BOARD_PD_COUNT];
    uint16 tempDiode;
    uint16 tempLED;
    uint16 ref;
} Board_SignalGroupType;

typedef enum {
    BOARD_ERR_NO_ERROR,
    BOARD_ERR_NOT_DETECTED,
    BOARD_ERR_SENS_FAIL,
    BOARD_ERR_TEMP_ERR
} Board_ErrorType;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */
/** @fn Std_ReturnType Board_Init(void)
 *
 * @return
 */
Std_ReturnType Board_Init(void);
/** @fn void Board_CollectSignals(void)
 *
 */
#if 1
void Board_CollectSignals(void);
/** @fn Std_ReturnType Board_GetSignalGroup(uint8 sgID,Board_SignalGroupType * sgPtr)
 *
 *  @param[in] sgID
 *  @param[out] sgPtr
 *  @return
 */
Std_ReturnType Board_GetSignalGroup(uint8 sgID, Board_SignalGroupType * sgPtr);
/**@fn Board_GetRef5VSignal
 *
 * @return
 */
uint16 Board_GetRef5VSignal(void);
/** @fn uint8 Board_GetSensorFailureStatus(void)
 *
 * @return
 */
uint8 Board_GetSensorFailureStatus(void);
/**
 *
 */
/** @} */
#endif
master_mode_t Board_GetMasterMode(void);
#endif /* Optoforce_SHexHost_Board_H */
