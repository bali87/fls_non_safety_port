/* ************************************************************************** */
/** Descriptive File Name

  @Company
    OnRobot

  @File Name
    flash_eep.h

  @Summary
    EEPROM in internal FLASH

  @Description
    Flash emulated EEPROM
 */
/* ************************************************************************** */

#ifndef _FLASH_EEP_H    /* Guard against multiple inclusion */
#define _FLASH_EEP_H

#include "Std.h"


/**
 *
 * @param address
 * @param data
 * @param length
 * @param blockSize
 * @return
 */
int FlashEeprom_Read(uint32 address, void* data, size_t length, int blockSize);
/**
 *
 * @param address
 * @param data
 * @param length
 * @param blockSize
 * @return
 */
int FlashEeprom_Write(uint32 address, void* data, size_t length, int blockSize);
/**
 *
 * @param address
 * @param length
 * @return
 */
uint16 FlashEeprom_ReadCRC(uint32 address, size_t length);
/**
 *
 * @return
 */
int FlashEeprom_Flush(void);

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
