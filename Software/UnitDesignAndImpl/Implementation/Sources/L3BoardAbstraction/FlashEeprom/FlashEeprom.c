/* ************************************************************************** */
/** Descriptive File Name

  @Company
    OnRobot

  @File Name
    flash_eep.h

  @Summary
    EEPROM in internal FLASH

  @Description
    Flash emulated EEPROM
 */
#include "CRC16.h"
#include "Std.h"

#include "FlashEeprom.h"
#include "NvMem.h"

static uint8_t blockBuffer[BLOCK_SIZE_DATA];
static char flashIsModified=0;
static uint32_t lastBlockAddress=-1;
char verifyWrite=0;


#define pbBlock(adr)  (adr & 0xFFFFF800)
#define pbOffset(adr) (adr & 0x000007FF)

static int FlashEeprom_ReadBlockToBuffer(uint32 address,void* buffer, size_t length);
static int FlashEeprom_VerifyBlock(uint32 address,void* buffer);
static int FlashEeprom_WritePageFromBuffer(uint32 address,uint8* buffer, size_t length);

static int FlashEeprom_ReadBlockToBuffer(uint32 address,void* buffer, size_t length)
{
	uint32 block;
	block = pbBlock(address);
    FlashEeprom_Read(block, buffer, length, 0);
	lastBlockAddress=block;
	flashIsModified=0;
	return 0;
}

static int FlashEeprom_WritePageFromBuffer(uint32 address,uint8_t* buffer, size_t length)
{
	uint32 block, r;
	block = pbBlock(address);
	NV_Mem_Erase_Page(block);
	for (r=0;r<NUM_ROWS_PER_BLOCK;r++) {
		uint32 tgoffs= block + (r*ROW_SIZE_ADDR)/2;
		uint16 srcAddr= (unsigned int)buffer + (r*ROW_SIZE_DATA);
		NV_Mem_Write_Row( tgoffs, (void*)srcAddr );
	}
	flashIsModified=0;
	return 0;
}

static int FlashEeprom_VerifyBlock(uint32 address,void* buffer)
{
	uint32 block;
	block = pbBlock(address);
	if (block!=lastBlockAddress)
		FlashEeprom_ReadBlockToBuffer(lastBlockAddress,blockBuffer,BLOCK_SIZE_DATA);
	return memcmp(buffer,blockBuffer,BLOCK_SIZE_DATA);
}


int FlashEeprom_Flush()
{
	char result=0;
	if ((flashIsModified==1) && (lastBlockAddress!=-1)) {
		FlashEeprom_WritePageFromBuffer(lastBlockAddress,blockBuffer,BLOCK_SIZE_DATA);
		
		if (verifyWrite) {
			uint8_t tmpBuf[BLOCK_SIZE_DATA]={0};
			FlashEeprom_ReadBlockToBuffer(lastBlockAddress,tmpBuf,BLOCK_SIZE_DATA);
			if (FlashEeprom_VerifyBlock(lastBlockAddress,tmpBuf))
				result=1;
		}
	}
	return result;
}


#if defined(__PIC24FJ256GA702__)

int FlashEeprom_Write(uint32 address, void* data, size_t length, int blockSize)
{
	uint32 updBlock=pbBlock(address);
	if (updBlock!=lastBlockAddress) {
		if (FlashEeprom_Flush())
			return 1;
		FlashEeprom_ReadBlockToBuffer(updBlock,blockBuffer,BLOCK_SIZE_DATA);
	}
	memcpy( (void*)(blockBuffer + pbOffset(address)), data,length );
	flashIsModified=1;
	return 0;
}

int FlashEeprom_Read(uint32 address, void* data, size_t length, int blockSize)
{
    uint8* b=data;
    while (length>0) {
		int read=0;
		int readLength=(blockSize)?blockSize:length;
		read=NV_Mem_Read_DWord_To_Buffer(address,b,readLength);
		b+=read;
		address=((address>>1)<<1)+(ADDRESS_SIZE/2);
		if (length>=read)
			length-=read;
		else
			length=0;
    }
    return 0;
}

uint16 FlashEeprom_ReadCRC(uint32 address, size_t length)
{
	uint8 data[4];
	uint16 crc=0;
    while (length>0) {
		int read=0;
		int readLength=(length>3)?3:length;
		
		read=NV_Mem_Read_DWord_To_Buffer(address,data,readLength);
		for (int i=0;i<read;i++)
			crc=CRC16_CalculateCRC(data,read,crc);
		
		address=((address>>1)<<1)+(ADDRESS_SIZE/2);
		if (length>=read)
			length-=read;
		else
			length=0;
    }
    return crc;
}

#elif defined(__32MX564F128H__)

int FlashEeprom_Write(uint32_t address, void* data, size_t length)
{
	unsigned int updPage=pbPage(address);
	if (updPage!=lastPageAddress) {
		if (FlashEeprom_Flush())
			return 1;
		FlashEeprom_ReadBlockToBuffer(updPage,blockBuffer,BYTE_PAGE_SIZE);
	}
	memcpy( (void*)(blockBuffer + pbOffset(address)), data,length );
	flashIsModified=1;
	return 0;
}

int FlashEeprom_Read(uint32_t address, void* data, size_t length)
{
	memcpy(data,(void*)address , length);
    return 0;
}

#endif
/* *****************************************************************************
 End of File
 */
