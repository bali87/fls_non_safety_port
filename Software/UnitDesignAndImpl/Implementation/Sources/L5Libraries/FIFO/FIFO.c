/**
 * @file
 *
 * Unit that implements:
 * - Ring Buffer FIFO functions on a given descriptor connected with an array of uint buffer
 *
 * @version $Id$
 */
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */
#include "Std.h"
//#include "MCU.h"
//#include "Board.h"
//#include "UART.h"
#include "FIFO.h"

/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticDefines Static defines
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */

Std_ReturnType FIFO_InitDescriptor (FIFO_DescrType * descrPtr, uint8 * buffPtr, uint16 buffLength)
{
    Std_ReturnType retVal;
    /* Check NULL pointers */
    if ((NULL == descrPtr) || (NULL == buffPtr))
    {
        /* Error Occured */
        retVal = E_NOT_OK;
    }
    else
    {
        /* Set ring buffer's address */
        descrPtr->buffPtr = buffPtr;
        /* Set buffer's size */
        descrPtr->buffLength = buffLength;
        /* Reset Head */
        descrPtr->head = 0u;
        /* Reset Tail */
        descrPtr->tail = 0u;
        /* Set state to Empty */
        descrPtr->state = FIFO_EMPTY;
        /* Sign that everything is OK */
        retVal = E_OK;

        descrPtr->errorCounter = 0u;
    }
    return (retVal);
}

Std_ReturnType FIFO_PutData (FIFO_DescrType * descrPtr, uint8 data)
{
    Std_ReturnType retVal =E_NOT_OK ;
#if 0
    /* Check FIFO Descriptor is not NULL */
    if (NULL == descrPtr)
    {
        retVal = STD_RETURN_ERROR;
    }
        /* Check FIFO was initialized */
    else if ((FIFO_UNINITIALIZED == descrPtr->state))
    {
        retVal = STD_RETURN_ERROR;
    }
        /* Preconditions OK */
    else
#endif
    {
        descrPtr->buffPtr[descrPtr->head] = data;
        /* Increment Head */
        descrPtr->head++;
        /* Truncate head */
        descrPtr->head = (descrPtr->head & (descrPtr->buffLength - 1u));
        /* Check FIFO is full */
#if 1
        if (FIFO_FULL == descrPtr->state)
        {
            /* Tail = Pre incremented Head so pop tail*/
            descrPtr->tail = descrPtr->head;
            retVal = E_NOT_OK;
            descrPtr->errorCounter++;
        }
#endif
        /* FIFO overrun */
        //if(descrPtr->head == descrPtr->tail)
        else if (descrPtr->head == descrPtr->tail)
        {
            /* Set State to FULL */
            descrPtr->state = FIFO_FULL;
            /* Increment Tail, remove oldest data byte */
       //     descrPtr->tail++;
            /* Truncate Tail */
        //    descrPtr->tail = (descrPtr->tail & (descrPtr->buffLength - 1u));
            /* Sign error */
            retVal = E_NOT_OK;
        }
        else
        {
            /* Sign that fifo is Not Empty*/
            descrPtr->state = FIFO_NOT_EMPTY;
            /* All OK */
            retVal = E_OK;
        }
    }
    return (retVal);
}

Std_ReturnType FIFO_GetData (FIFO_DescrType *descrPtr, uint8 *dataPtr)
{
    Std_ReturnType retVal=E_NOT_OK;
#if 0
    /* Check NULL pointer parameters */
    if ((NULL == descrPtr) || (NULL == dataPtr))
    {
        retVal = STD_RETURN_ERROR;
    }
        /* Check FIFO was initialized */
    else if ((FIFO_UNINITIALIZED == descrPtr->state))
    {
        retVal = STD_RETURN_ERROR;
    }
        /* Preconditions OK */
    else
#endif
    {
        /* Check Data available */
        if (FIFO_EMPTY == descrPtr->state)
        {
            /* FIFO is empty */
            retVal = E_NOT_OK;
        }
        else

        {
            /* Copy Data to pointer */
            *dataPtr = descrPtr->buffPtr[descrPtr->tail];

            /* Increment Tail */
            descrPtr->tail++;
            /* Truncate Tail */
            descrPtr->tail = (descrPtr->tail & (descrPtr->buffLength - 1u));
            if(FIFO_FULL ==  descrPtr->state)
            {

            }
            /* Check Equal Head*/
            if (descrPtr->tail == descrPtr->head)
            {
                /* If FIFO is empty */
                descrPtr->state = FIFO_EMPTY;
            }
            else
            {
                /* Else FIFO is not empty */
                descrPtr->state = FIFO_NOT_EMPTY;
            }
            retVal = E_OK;
        }
    }
    return (retVal);
}

Std_ReturnType FIFO_GetFreeSize (FIFO_DescrType *descrPtr, uint16 *sizePtr)
{
    Std_ReturnType retVal;
    /* Check NULL pointer parameters */
    if ((NULL == descrPtr) || (NULL == sizePtr))
    {
        retVal = E_NOT_OK;
    }
        /* Preconditions OK */
    else
    {
        if (FIFO_FULL == descrPtr->state)
        {
            *sizePtr = 0u;//descrPtr->buffLength;
        }
            /* Head before Tail */
        else if (descrPtr->head >= descrPtr->tail)
        {
            /* Calculate free size */
            *sizePtr = (descrPtr->buffLength - (descrPtr->head - descrPtr->tail));
        }
            /* Tail Before Head */
        else
        {
            /* Calculate free size */
            *sizePtr = descrPtr->tail - descrPtr->head;
        }
        retVal = E_OK;
    }
    return (retVal);
}

Std_ReturnType FIFO_ClearFIFO (FIFO_DescrType *descrPtr)
{
    Std_ReturnType retVal;

    if (NULL == descrPtr)
    {
        retVal = E_NOT_OK;
    }
    else
    {
        descrPtr->head = 0u;
        descrPtr->tail = 0u;
        descrPtr->state = FIFO_EMPTY;
        retVal = E_OK;
    }
    return (retVal);
}

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */



/** @} */


