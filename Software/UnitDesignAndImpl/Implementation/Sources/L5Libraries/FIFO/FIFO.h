#ifndef FIFO_H
#define FIFO_H

/**
 * @file
 *
 * Unit that implements:
 * - Ring Buffer FIFO functions interface
 *
 *
 * @version $Id$
 */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */
#include "Std.h"
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */
#define FIFO_IS_FULL(_fifoDesc)  (FIFO_FULL == _fifoDesc.state ? 1u : 0u)
#define FIFO_IS_EMPTY(_fifoDesc) (FIFO_EMPTY == _fifoDesc.state ? 1u : 0u)

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */
/**@enum FIFO_StateType
 * @brief FIFO's state machine's states
@startuml
	[*] -> FIFO_UNINITIALIZED
	FIFO_UNINITIALIZED --> FIFO_EMPTY : FIFO_InitDescriptor()
	FIFO_EMPTY --> FIFO_NOT_EMPTY : FIFO_PutData()
	FIFO_NOT_EMPTY --> FIFO_FULL : FIFO_PutData() && ((FIFO.head+1) == FIFO.tail)
	FIFO_FULL --> FIFO_NOT_EMPTY : FIFO_GetData()
	FIFO_NOT_EMPTY --> FIFO_EMPTY: FIFO_GetData() && (FIFO.head == FIFO.tail)
	FIFO_NOT_EMPTY --> FIFO_NOT_EMPTY: FIFO_GetData && (FIFO.head != FIFO.tail)
@enduml

 */
typedef enum
{
	/** Default state before initialization **/
	FIFO_UNINITIALIZED	= 0u,
	/** Empty state before initialization **/
	FIFO_EMPTY			= 1u,
	/** Not empty state after first write and before Full **/
	FIFO_NOT_EMPTY		= 2u,
	/** Full state **/
	FIFO_FULL          	= 3u
}FIFO_StateType;
/**@struct FIFO_DescrType
 * @brief FIFO Descriptor base of module's operations.
 */
typedef struct
{
	/** Ring Buffer's Head **/
	uint16 			head;
	/** Ring Buffer's Tail **/
	uint16 			tail;
	/** Pointer to data buffer set during initialization **/
	uint8          *buffPtr;
	/** Size of data buffer set during initialization **/
	uint16 			buffLength;
	/** Ring Buffer's state **/
	FIFO_StateType 	state;
    uint16 errorCounter;
}FIFO_DescrType;

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */

/** @fn Std_ReturnType FIFO_InitDescriptor(FIFO_DescrType * descrPtr, uint8 * buffPtr, uint16 buffLength)
 *
 * @param[inout] descrPtr:	Calling module's own FIFO Descriptor cannot be NULL pointer
 * @param[in] buffPtr:		Calling module's own data buffer which will used by FIFO cannot be NULL pointer
 * @param[in] buffLength: 	Calling module's own data buffer's length
 * @return In case of any NULL pointer parameter E_NOT_OK else E_OK;
 */
Std_ReturnType FIFO_InitDescriptor( FIFO_DescrType *descrPtr, uint8 *buffPtr, uint16 buffLength );
/** @fn Std_ReturnType FIFO_GetFreeSize(FIFO_DescrType * descrPtr,uint8_t * sizePtr)
 * @brief	Function return FIFO's ring buffer's free size
 * @param[inout] descrPtr:	Calling module's own FIFO Descriptor cannot be NULL pointer
 * @param[out] sizePtr:		Calling module's pointer to size
 * @return In case of any NULL pointer parameter else E_OK;
 */
Std_ReturnType FIFO_GetFreeSize( FIFO_DescrType *descrPtr, uint16 *sizePtr );
/** @fn Std_ReturnType FIFO_GetData(FIFO_DescrType * descrPtr,uint8 * dataPtr)
 * @brief FIFO Getter Function it pops one byte from it's ring buffer if not empty.
 * @param[inout] descrPtr:	Calling module's own FIFO Descriptor cannot be NULL pointer
 * @param[out] dataPtr: 	Calling module's pointer to data
 * @return In case of any NULL pointer parameter or if FIFO was empty before call E_NOT_OK else E_OK;
 */
Std_ReturnType FIFO_GetData( FIFO_DescrType *descrPtr, uint8 * dataPtr );
/** @fn Std_ReturnType FIFO_PutData(FIFO_DescrType * descrPtr,uint8 data)
 * @brief FIFO Putter Function it push one byte to it's ring buffer.
 * @param[inout] descrPtr:	Calling module's own FIFO Descriptor cannot be NULL pointer
 * @param[in] data:			Data byte should push into ring buffer
 * @return In case of any NULL pointer parameter or if FIFO was full before call E_NOT_OK else E_OK;
 * @note  If FIFO's ring buffer is full it removes one byte from the Tail change it to data and sign's E_NOT_OK
 */
Std_ReturnType FIFO_PutData( FIFO_DescrType *descrPtr, uint8 data );
/** @fn Std_ReturnType FIFO_ClearFIFO( FIFO_DescrType *descrPtr )
 * @brief Resets state of the FIFO to empty
 * @param[in] descrPtr:	Calling module's own FIFO Descriptor cannot be NULL pointer
 * @return In case of any NULL pointer parameter else E_OK;
 */
Std_ReturnType FIFO_ClearFIFO( FIFO_DescrType *descrPtr );

/** @} */

#endif /* FIFO_H */
