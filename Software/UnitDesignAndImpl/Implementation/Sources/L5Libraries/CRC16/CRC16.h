#ifndef __CRC16_H__
#define __CRC16_H__
#include "Std.h"

/** @fn uint16 CRC16_CalculateCRC(const void* inDataPtr, uint32 inDatalen, uint16 initialVal)

 * @brief Calculate a given array's CRC16 Value with initial value.
 * @param[in] inDataPtr
 * @param[in] inDatalen
 * @param[in] initialVal
 * @return
 */
uint16 CRC16_CalculateCRC(const void* inDataPtr, uint32 inDatalen, uint16 initialVal);

#endif
