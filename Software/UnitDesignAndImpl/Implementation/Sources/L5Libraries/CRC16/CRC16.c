#include "CRC16.h"
#include "Std.h"
/**
 *  Static table used for the table_driven implementation.
 *
 */
/**
 *
 */
static const unsigned short int crc_table[16] = { 0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef};


uint16 CRC16_CalculateCRC(const void* inDataPtr, uint32 inDatalen, uint16 initialVal)
{
	uint16 i;
	uint16 crc = initialVal;
	unsigned char* ptr=(unsigned char* )inDataPtr;
	
    while (inDatalen--)
    {
		i = (crc >> 12) ^ (*ptr >> 4);
		crc = crc_table[i & 0x0F] ^ (crc << 4);
		i = (crc >> 12) ^ (*ptr >> 0);
		crc = crc_table[i & 0x0F] ^ (crc << 4);
		ptr++;
    }
    return (crc & 0xFFFF);
}

/**
 *  CalculateCrc
 *
 *      Calculates CRC for the given data and len
 *
 */
#if 0
uint16_t CalculateCrc254(void* inData, uint32_t len)
{
    unsigned int i;
    unsigned short int crc = 0;
	
	uint8_t* data=inData;
    while (len--) {
		i = (crc >> 12) ^ (*data >> 4);
		crc = crc_table[i & 0x0F] ^ (crc << 4);

		i = (crc >> 12) ^ (*data >> 0);
		crc = crc_table[i & 0x0F] ^ (crc << 4);

		data++;
    }

    return (crc & 0xFFFF);
}


#endif

