#ifndef MY_FLOAT_H
#define MY_FLOAT_H

/**
 * @file
 * 
 * Header file of the unit ${FILENAME}. //fixme remove .h
 *
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */

//#define MY_FLOAT_INT24_MAX  8388607L 
//#define MY_FLOAT_INT24_MIN -8388608L
//#define MY_FLOAT_INT24_MAX  16777215L 
//#define MY_FLOAT_INT24_MIN -16777216L
//#define MY_FLOAT_INT47_MAX  140737488355327LL
//#define MY_FLOAT_INT47_MIN -140737488355328LL
#define MY_FLOAT_INT28_MAX  268435455L 
#define MY_FLOAT_INT28_MIN -268435456L
#define MY_FLOAT_INT55_MAX  36028797018963967LL
#define MY_FLOAT_INT55_MIN -36028797018963968LL


#define MY_FLOAT_S28_MUL2( y, x1, x2 )      ( y ).coeff = (( sint64 )( x1 ) * ( sint64 )( x2 )) >> 28; ( y ).expo = 2
//#define MY_FLOAT_SIMPLE_MUL3(x1, x2, x3, y) ( y ).coeff = (( int64_t )((( int64_t )( x1 ) * ( int64_t )( x2 )) >> 16 ) * ( int64_t )( x3 )) >> 8; ( y ).expo = 24
#define MY_FLOAT_S28_MUL3( y, x1, x2, x3 )  ( y ).coeff = (( sint64 )((( sint64 )( x1 ) * ( sint64 )( x2 )) >> 28 ) * ( sint64 )( x3 )) >> 28; ( y ).expo = 15


#define MY_FLOAT_SIMPLE_ADD2( y, op1, x2 )  do{( y ).coeff = ( x1 ).coeff + ( x2 ).coeff}while(0)

#define MY_FLOAT_FULL_ADD2_v1( y, x1, x2 )  do                                                                                          \
                                            {                                                                                           \
                                                if (((( uint16 )(( *(( uint16* )( &(( x1 ).expo )))) ^ ( *(( uint16* )( &(( x2 ).expo )))))) & 0x7FFFU ) < 24 )    \
                                                {                                                                                       \
                                                    if (( x1 ).expo > ( x2 ).expo )                                                     \
                                                    {                                                                                   \
                                                        ( y ).coeff = ( x1 ).coeff + (( x2 ).coeff >> (( x1 ).expo - ( x2 ).expo ));    \
                                                        ( y ).expo = ( x1 ).expo;                                                       \
                                                    }                                                                                   \
                                                    else                                                                                \
                                                    {                                                                                   \
                                                        ( y ).coeff = ( x2 ).coeff + (( x1 ).coeff >> (( x2 ).expo - ( x1 ).expo ));    \
                                                        ( y ).expo = ( x2 ).expo;                                                       \
                                                    }                                                                                   \
                                                }                                                                                       \
                                                else                                                                                    \
                                                {                                                                                       \
                                                    if ((( x1 ).expo > ( x2 ).expo ) && (( x1 ).coeff != 0 ))                           \
                                                    {                                                                                   \
                                                        ( y ).coeff = ( x1 ).coeff;                                                     \
                                                        ( y ).expo = ( x1 ).expo;                                                       \
                                                    }                                                                                   \
                                                    else                                                                                \
                                                    {                                                                                   \
                                                        ( y ).coeff = ( x2 ).coeff;                                                     \
                                                        ( y ).expo = ( x2 ).expo;                                                       \
                                                    }                                                                                   \
                                                }                                                                                       \
                                                if ( ( y ).coeff < 0x00800000L )                                                        \
                                                {                                                                                       \
                                                    ( y ).coeff >>= 1;                                                                  \
                                                    ( y ).expo - 1;                                                                     \
                                                }                                                                                       \
                                            } while ( 0 )

#define MY_FLOAT_FULL_ADD2( y, x1, x2 )     do                                                                                          \
                                            {                                                                                           \
                                                if ( 0 == ( x1 ).coeff )                                                                \
                                                {                                                                                       \
                                                    ( y ).coeff = ( x2 ).coeff;                                                         \
                                                    ( y ).expo = ( x2 ).expo;                                                           \
                                                }                                                                                       \
                                                else if ( 0 == ( x2 ).coeff )                                                           \
                                                {                                                                                       \
                                                    ( y ).coeff = ( x1 ).coeff;                                                         \
                                                    ( y ).expo = ( x1 ).expo;                                                           \
                                                }                                                                                       \
                                                else if (((( x1 ).expo ) - (( x2 ).expo ) < 28 ) && ((( x1 ).expo ) - (( x2 ).expo ) > -28 )) \
                                                {                                                                                       \
                                                    if (( x1 ).expo > ( x2 ).expo )                                                     \
                                                    {                                                                                   \
                                                        ( y ).coeff = ( x1 ).coeff + (( x2 ).coeff >> (( x1 ).expo - ( x2 ).expo ));    \
                                                        ( y ).expo = ( x1 ).expo;                                                       \
                                                    }                                                                                   \
                                                    else                                                                                \
                                                    {                                                                                   \
                                                        ( y ).coeff = ( x2 ).coeff + (( x1 ).coeff >> (( x2 ).expo - ( x1 ).expo ));    \
                                                        ( y ).expo = ( x2 ).expo;                                                       \
                                                    }                                                                                   \
                                                    if (((( y ).coeff ) >  MY_FLOAT_INT28_MAX ) || ((( y ).coeff ) < MY_FLOAT_INT28_MIN )) \
                                                    {                                                                                   \
                                                        ( y ).coeff >>= 1;                                                              \
                                                        ( y ).expo++;                                                                   \
                                                    }                                                                                   \
                                                }                                                                                       \
                                                else                                                                                    \
                                                {                                                                                       \
                                                    if (( x1 ).expo > ( x2 ).expo )                                                     \
                                                    {                                                                                   \
                                                        ( y ).coeff = ( x1 ).coeff;                                                     \
                                                        ( y ).expo = ( x1 ).expo;                                                       \
                                                    }                                                                                   \
                                                    else                                                                                \
                                                    {                                                                                   \
                                                        ( y ).coeff = ( x2 ).coeff;                                                     \
                                                        ( y ).expo = ( x2 ).expo;                                                       \
                                                    }                                                                                   \
                                                }                                                                                       \
                                            } while ( 0 )

//                                 else if (((( uint16 )(( *(( uint16* )( &(( x1 ).expo )))) ^ ( *(( uint16* )( &(( x2 ).expo )))))) & 0x7FFFU ) < 24 )    
//                                                    if ( ((*(( uint32* )(&(( y ).coeff )))) & 0x01000000UL ) != 0ul )                   
               
#define MY_FLOAT_SIMPLE_SUB2( y, op1, x2 )  do{( y ).coeff = ( x1 ).coeff - ( x2 ).coeff}while(0)

#define MY_FLOAT_FULL_SUB2( y, x1, x2 )     do                                                                                          \
                                            {                                                                                           \
                                                if ( 0 == ( x1 ).coeff )                                                                \
                                                {                                                                                       \
                                                    ( y ).coeff = -(( x2 ).coeff );                                                     \
                                                    ( y ).expo = ( x2 ).expo;                                                           \
                                                }                                                                                       \
                                                else if ( 0 == ( x2 ).coeff )                                                           \
                                                {                                                                                       \
                                                    ( y ).coeff = ( x1 ).coeff;                                                         \
                                                    ( y ).expo = ( x1 ).expo;                                                           \
                                                }                                                                                       \
                                                else if (((( x1 ).expo ) - (( x2 ).expo ) < 28 ) && ((( x1 ).expo ) - (( x2 ).expo ) > -28 )) \
                                                {                                                                                       \
                                                    if (( x1 ).expo > ( x2 ).expo )                                                     \
                                                    {                                                                                   \
                                                        ( y ).coeff = ( x1 ).coeff - (( x2 ).coeff >> (( x1 ).expo - ( x2 ).expo ));    \
                                                        ( y ).expo = ( x1 ).expo;                                                       \
                                                    }                                                                                   \
                                                    else                                                                                \
                                                    {                                                                                   \
                                                        ( y ).coeff = -(( x2 ).coeff ) + (( x1 ).coeff >> (( x2 ).expo - ( x1 ).expo ));\
                                                        ( y ).expo = ( x2 ).expo;                                                       \
                                                    }                                                                                   \
                                                    if (( y ).coeff == 0 )                                                              \
                                                    {                                                                                   \
                                                        ( y ).expo = 0;                                                                 \
                                                    }                                                                                   \
                                                    else                                                                                \
                                                    {                                                                                   \
                                                        while (((( y ).coeff ) < (( MY_FLOAT_INT28_MAX + 1 ) / 2 )) && ((( y ).coeff ) > MY_FLOAT_INT28_MIN / 2 )) \
                                                        {                                                                               \
                                                            ( y ).coeff *= 2;                                                           \
                                                            ( y ).expo--;                                                               \
                                                        };                                                                              \
                                                    }                                                                                   \
                                                }                                                                                       \
                                                else                                                                                    \
                                                {                                                                                       \
                                                    if (( x1 ).expo > ( x2 ).expo )                                                     \
                                                    {                                                                                   \
                                                        ( y ).coeff = ( x1 ).coeff;                                                     \
                                                        ( y ).expo = ( x1 ).expo;                                                       \
                                                    }                                                                                   \
                                                    else                                                                                \
                                                    {                                                                                   \
                                                        ( y ).coeff = -(( x2 ).coeff );                                                 \
                                                        ( y ).expo = ( x2 ).expo;                                                       \
                                                    }                                                                                   \
                                                    if ( 0 == ( x1 ).coeff )                                                            \
                                                    {                                                                                   \
                                                        ( y ).coeff = 0;                                                                \
                                                        ( y ).expo = 0;                                                                 \
                                                    }                                                                                   \
                                                    else if ( 0 == ( x2 ).coeff )                                                       \
                                                    {                                                                                   \
                                                        ( y ).coeff = 0;                                                                \
                                                        ( y ).expo = 0;                                                                 \
                                                    }                                                                                   \
                                                }                                                                                       \
                                            } while ( 0 )
#if 0

                                                        if ( -2147483648 == ( y ).coeff ) BOARD_SET_LED_BLUE_LOW(); 
#define MY_FLOAT_NORMALIZE( x )             do                                                                                          \
                                            {                                                                                           \
                                                uint16 shift;                                                                           \
                                                shift = (*(( uint32* )( &(( x1 ).coeff )))) & 0x8F000000;                               \
                                                if (( shift > 0u )                                                                      \
                                                    {                                                                                   \
                                                if (( *(( uint16* )( &(( x1                                                             \
                                                    if (( x1 ).expo > ( x2 ).expo )                                                     \
                                                    {                                                                                   \
                                                        ( y ).coeff = ( x1 ).coeff + (( x2 ).coeff >> (( x1 ).expo - ( x2 ).expo ));    \
                                                    }                                                                                   \
                                                    else                                                                                \
                                                    {                                                                                   \
                                                        ( y ).coeff = ( x2 ).coeff + (( x1 ).coeff >> (( x2 ).expo - ( x1 ).expo ));    \
                                                    }                                                                                   \
                                                }                                                                                       \
                                                else                                                                                    \
                                                {                                                                                       \
                                                    if (( x1 ).expo > ( x2 ).expo )                                                     \
                                                    {                                                                                   \
                                                        ( y ).coeff = ( x1 ).coeff;                                                     \
                                                    }                                                                                   \
                                                    else                                                                                \
                                                    {                                                                                   \
                                                        ( y ).coeff = ( x2 ).coeff;                                                     \
                                                    }                                                                                   \
                                                }                                                                                       \
                                            } while ( 0 )
#endif
#define MY_FLOAT_FULL_MUL2( y, x1, x2 )     do                                                                                          \
                                            {                                                                                           \
                                                if ( (( sint64 )((( sint64 )( x1 ).coeff ) * (( sint64 )( x2 ).coeff )) > MY_FLOAT_INT55_MAX ) || (( sint64 )((( sint64 )( x1 ).coeff ) * (( sint64 )( x2 ).coeff )) < MY_FLOAT_INT55_MIN ))    \
                                                {                                                                                       \
                                                    ( y ).coeff = ((( sint64 )( x1 ).coeff ) * (( sint64 )( x2 ).coeff )) >> 28;        \
                                                    ( y ).expo = ( x1 ).expo + ( x2 ).expo + 28;                                        \
                                                }                                                                                       \
                                                else                                                                                    \
                                                {                                                                                       \
                                                    ( y ).coeff = ((( sint64 )( x1 ).coeff ) * (( sint64 )( x2 ).coeff )) >> 27;        \
                                                    ( y ).expo = ( x1 ).expo + ( x2 ).expo + 27;                                        \
                                                }                                                                                       \
                                            } while ( 0 )                                                                               \
                                               
#define MY_FLOAT_SIMPLE_MUL2( y, x1, x2 )   do                                                                                          \
                                            {                                                                                           \
                                                ( y ).coeff = ((( x1 ).coeff >> 13 ) * (( x2 ).coeff >> 13 )) >> 2;                     \
                                                ( y ).expo = ( x1 ).expo + ( x2 ).expo + 28;                                            \
                                            } while ( 0 )                                                                               \

#define MY_FLOAT_SIMPLE_MUL3( y, x1, x2, x3 )   do                                                                                      \
                                            {                                                                                           \
                                                ( y ).coeff = ((((( x1 ).coeff >> 13 ) * (( x2 ).coeff >> 13 )) >> 15 ) * (( x3 ).coeff >> 13 )) >> 2; \
                                                ( y ).expo = ( x1 ).expo + ( x2 ).expo + ( x3 ).expo + 56;                              \
                                            } while ( 0 )                                                                               \
//                                                if ( ( sint64 )((( sint64 )( x1 ).coeff ) * (( sint64 )( x2 ).coeff )) < 0x0000800000000000LL )    


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */

typedef struct MyFloat_tag {
    sint32 coeff;
    sint16 expo;
} MyFloat;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */
 



/**
 * @addtogroup GlobalVaribles Global variables
 */
 



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */
/** @fn Std_ReturnType MyFloat_Float64ToMyFloat( float64 *inputDataPtr, MyFloat *outputDataPtr, uint16 dataLen )
 *
 * @param[in] inputDataPtr:
 * @param[out] outputDataPtr:
 * @param[in] dataLen:
 * @return  In case of any any error, it returns the error code (not implemented yet)
 */
Std_ReturnType MyFloat_Float64ToMyFloat( float64 *inputDataPtr, MyFloat *outputDataPtr, uint16 dataLen );
/** @fn Std_ReturnType MyFloat_Uint16ToMyFloat( uint16 *inputDataPtr, MyFloat *outputDataPtr, uint16 dataLen )
 * @param[in] inputDataPtr:
 * @param[out] outputDataPtr:
 * @param[in] dataLen:
 * @return  In case of any any error, it returns the error code (not implemented yet)
 */
Std_ReturnType MyFloat_Uint16ToMyFloat( uint16 *inputDataPtr, MyFloat *outputDataPtr, uint16 dataLen );

/** @} */

#endif /* MY_FLOAT_H */
