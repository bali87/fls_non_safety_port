
/**
 * @file
 *
 * Unit that manages the computation of the system delay /Unit that provides library functions for computation of delay.
 * fixme update the comment.
 *
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */

#include "Std.h"
#include "MyFloat.h"


/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticDefines Static defines
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



 /** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */


Std_ReturnType MyFloat_Float64ToMyFloat( float64 *inputDataPtr, MyFloat *outputDataPtr, uint16 dataLen )
{
    Std_ReturnType retVal = E_NOT_OK;

    uint16 idx;
    sint32 tempCoeff;
    sint16 tempExpo;
    float64 tempFloat;

    for ( idx = 0u; idx < dataLen; idx++ )
    {
        tempCoeff = 0;
        tempExpo = 0;
        tempFloat = inputDataPtr[ idx ];
        if ( tempFloat < 0.0L )
        {
            if ( tempFloat < ( float64 )( MY_FLOAT_INT28_MIN ) - 0.5L )
            {
                do
                {
                    tempFloat = tempFloat / 2.0L;
                    tempExpo++;
                } while ( tempFloat < (( float64 )( MY_FLOAT_INT28_MIN ) -0.5L ));
                tempCoeff = ( sint32 )( tempFloat - 0.5L );
            }
            else if ( tempFloat > ( float64 )( MY_FLOAT_INT28_MIN ) / 2.0L )
            {
                do
                {
                    tempFloat = tempFloat * 2.0L;
                    tempExpo--;
                } while ( tempFloat > (( float64 )( MY_FLOAT_INT28_MIN ) / 2.0L ));
                tempCoeff = ( sint32 )( tempFloat - 0.5L );
            }
            else
            {
                tempExpo = 0;
                tempCoeff = ( sint32 )( tempFloat - 0.5L );
            }
        }
        else if ( tempFloat > 0.0L )
        {
            if ( tempFloat > ( float64 )( MY_FLOAT_INT28_MAX ) + 0.5L )
            {
                do
                {
                    tempFloat = tempFloat / 2.0L;
                    tempExpo++;
                } while ( tempFloat > ( float64 )( MY_FLOAT_INT28_MAX ) + 0.5L );
                tempCoeff = ( sint32 )( tempFloat + 0.5L );
            }
            else if ( tempFloat < ( float64 )( MY_FLOAT_INT28_MAX ) / 2.0L )
            {
                do
                {
                    tempFloat = tempFloat * 2.0L;
                    tempExpo--;
                } while ( tempFloat < (( float64 )( MY_FLOAT_INT28_MAX ) / 2.0L ));
                tempCoeff = ( sint32 )( tempFloat + 0.5L );
            }
            else
            {
                tempCoeff = ( sint32 )( tempFloat + 0.5L );
                tempExpo = 0;
            }
        }
        else
        {
            tempCoeff = 0;
            tempExpo = 0;
        }
        outputDataPtr[ idx ].coeff = tempCoeff;
        outputDataPtr[ idx ].expo  = tempExpo;
    }

    retVal = E_OK;
    return retVal;
}

Std_ReturnType MyFloat_Uint16ToMyFloat( uint16 *inputDataPtr, MyFloat *outputDataPtr, uint16 dataLen )
{
    Std_ReturnType retVal = E_NOT_OK;

    uint16 idx = 0u;
    sint32 tempCoeff;
    sint16 tempExpo;
    uint32 tempData;

    for ( idx = 0u; idx < dataLen; idx++ )
    {
        tempCoeff = 0;
        tempExpo = 0;
        tempData = ( uint32 )( inputDataPtr[ idx ] );
        do
        {
            tempData = tempData << 1;
            tempExpo--;
//        } while ( tempData < MY_FLOAT_INT24_MAX / 2 );
//        } while ( 0 == tempData & ( ~MY_FLOAT_INT24_MAX ));
        } while ( 0 == ( tempData & 0xF8000000UL ));
        tempCoeff = ( sint32 )( tempData );
        outputDataPtr[ idx ].coeff = tempCoeff;
        outputDataPtr[ idx ].expo  = tempExpo;
    }

    retVal = E_OK;
    return retVal;
}


//Std_ReturnType MyFloat_MyFloatToSint16( MyFloat *inputDataPtr, sint16 *outputDataPtr, uint16 dataLen )
//{
//    Std_ReturnType retVal = E_NOT_OK;
//
//    uint16 idx = 0u;
//    sint32 tempCoeff;
//    sint16 tempExpo;
//    uint32 tempData;
//
//    for ( idx = 0u; idx < dataLen; idx++ )
//    {
//        tempCoeff = 0;
//        tempExpo = 0;
//        tempData = ( uint32 )( inputDataPtr[ idx ] );
//        do
//        {
//            tempData = tempData << 1;
//            tempExpo--;
////        } while ( tempData < MY_FLOAT_INT24_MAX / 2 );
////        } while ( 0 == tempData & ( ~MY_FLOAT_INT24_MAX ));
//        } while ( 0 == ( tempData & 0xF8000000UL ));
//        tempCoeff = ( sint32 )( tempData );
//        outputDataPtr[ idx ].coeff = tempCoeff;
//        outputDataPtr[ idx ].expo  = tempExpo;
//    }
//
//    retVal = E_OK;
//    return retVal;
//}


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */


