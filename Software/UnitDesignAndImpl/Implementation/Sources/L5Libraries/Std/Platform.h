#ifndef PLATFORM_H
#define PLATFORM_H
/* $Id: Platform.h 165 2018-02-20 13:34:48Z balazs.vincze $ */

#include "hal_stdtypes.h"
/* Target Pic24f... not Unix works on all cross compilers */
#define COMPILE_FOR_TARGET			(!defined (__unix__))
/* Host Cygwin and Linux */
#define COMPILE_FOR_HOST			(defined (__unix__))




#if COMPILE_FOR_HOST
#define interrupt
#define no_auto_psv
#define naked
#endif



#ifdef AIVT_ENABLED
    #define InterruptVector(a)   _## Alt ## a ## Interrupt
    #define Handler(a)   _## Alt ## a
#else
    #define InterruptVector(a)   _## a ## Interrupt
    #define Handler(a)   _## a
#endif

#if COMPILE_FOR_TARGET
#define IRQ_NO_PSV __attribute__ ( ( interrupt, no_auto_psv ) )
#define _ISR_NO_AUTO_PSV __attribute__((interrupt(no_auto_psv) ))
#else
#define IRQ_NO_PSV
#define	_ISR_NO_AUTO_PSV
#define	space(_spc)
#define	keep
#define	address(_addr)
#define __sfr__
#define __prog__
//#define __deprecated__
#define __unsafe__
#endif

/*
 CPU register type width
*/
//#define CPU_TYPE_8 8
//#define CPU_TYPE_16 16
//#define CPU_TYPE_32 32

/*
 Bit order definition
*/
//#define MSB_FIRST 0 /* Big endian bit ordering */
//#define LSB_FIRST 1 /* Little endian bit ordering */

/*
 Byte order definition
*/
//#define HIGH_BYTE_FIRST 0 /* Big endian byte ordering */
//#define LOW_BYTE_FIRST 1 /* Little endian byte ordering */

/*
 Word order definition
*/
//#define HIGH_WORD_FIRST 0 /* Big endian word ordering */
//#define LOW_WORD_FIRST 1 /* Little endian word ordering */


//#define CPU_TYPE CPU_TYPE_32
//#define CPU_BIT_ORDER LSB_FIRST
//#define CPU_BYTE_ORDER LOW_BYTE_FIRST
//#define CPU_WORD_ORDER LOW_WORD_FIRST

/*
 * limits of DataPtr types
 */
//#define MAXUINT8 (UINT8_MAX)
//#define MAXSINT8 (INT8_MAX)
//#define MINSINT8 (INT8_MIN)

//#define MAXUINT16 (UINT16_MAX)
//#define MAXSINT16 (INT16_MAX)
//#define MINSINT16 (INT16_MIN)

//#define MAXUINT32 (UINT32_MAX)
//#define MAXSINT32 (INT32_MAX)
//#define MINSINT32 (INT32_MIN)

#if 0
typedef signed char 		    sint8; 		/* -128 .. +127 */
typedef unsigned char 		    uint8; 	/* 0 .. 255 */
typedef signed short int 		sint16; 	/* -32768 .. +32767 */
typedef unsigned short int 		uint16; 	/* 0 .. 65535 */
typedef signed long 		    sint32; 	/* -2147483648 .. +2147483647 */
typedef unsigned long 		    uint32;	/* 0 .. 4294967295 */
typedef signed long long int 	sint64;
typedef unsigned long long int 	uint64;

typedef float 				    float32;
typedef long double			    float64;    /* for PIC24 only */
#endif
/* typedef unsigned long uint8_least;*/ 	/* At least 8 bit */
/* typedef unsigned long uint16_least;*/ /* At least 16 bit */
/* typedef unsigned long uint32_least;*/ /* At least 32 bit */
/* typedef signed long sint8_least;*/ 	/* At least 7 bit + 1 bit sign */
/* typedef signed long sint16_least;*/ 	/* At least 15 bit + 1 bit sign */
/* typedef signed long sint32_least;*/ 	/* At least 31 bit + 1 bit sign */

//typedef unsigned char boolean; /* for use with TRUE/FALSE */
//typedef unsigned char bool; /* for use with TRUE/FALSE */
/* conditional check */

#if COMPILE_FOR_HOST
#define asm(ARG)
#endif

//#ifndef _BOOL
//#define TRUE    ((bool) 1)
//#define FALSE   ((bool) 0)
//#define true    ((bool) 1)
//#define false   ((bool) 0)
//typedef bool BOOL;
//#define _BOOL _BOOL
//#endif

#endif /* PLATFORM_H */
