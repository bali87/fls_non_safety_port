/* $Id: Std.h 169 2018-03-08 16:25:36Z balazs.vincze $ */
#ifndef STD_H
#define STD_H

#include "Compiler.h"
#include "Platform.h"


#include <stdint.h>
#include <string.h>


//#define TRUE  1u
//#define FALSE 0u

#define	HIGH	1u
#define	LOW		0u
typedef enum
{
    MASTER_MODE_UNK = 0,
    MASTER_MODE_MASTER =  1,
    MASTER_MODE_SLAVE = 2
}master_mode_t;

typedef master_mode_t Std_MCUMasterModeType;
#if 0
typedef enum Std_ReturnType_tag
{
    STD_RETURN_OK            = 0u,
    STD_RETURN_ERROR         = 1u,
    STD_RETURN_NOT_AVAILABLE = 2u
} Std_ReturnType;
#endif
typedef enum Std_BoolType_tag
{
    STD_BOOL_FALSE           = 0u,
    STD_BOOL_TRUE            = 1u
} Std_BoolType;

#define SET_BIT(p,n) do{(p) |= (1 << (n));}while(0)
#define CLR_BIT(p,n) do{(p) &= ~((1) << (n));}while(0)

#define SET_MASK(p,n,m) do{((p) &= (~(_ ## p ## _ ## n ## _MASK)));\
						((p) |= (((m)<< (_ ## p ## _ ## n ## _POSITION))&(_ ## p ## _ ## n ## _MASK)));\
						}while(0)
/* TODO: check it!!! */
#ifndef __little_endian__
#define __little_endian__  1
#endif
#ifndef __LITTLE_ENDIAN__
#define __LITTLE_ENDIAN__  1
#endif

#endif /* STD_H */
