#include "CRC16.h"
#include "MemIf.h"
#include "FlashEeprom.h"
#include "Board.h"


char MemIf_LoadNVData(FlashDataCRC *data)
{
    FlashEeprom_Read(FLASHDATA_ADDRESS,data,sizeof(FlashDataCRC),0);
    uint16_t crc=CRC16_CalculateCRC(data,sizeof(FlashDataCRC)-2,0);
    if (crc==data->crc)
        if (REQUIRED_DATA_FORMAT_VERSION==data->header.data_format_version)
        {
            return 0;
        }
        else
        {
            return 2;
        }
    else
    {
        return 1;
    }

}

char MemIf_UpdataNVData(FlashDataCRC *data)
{
    uint16_t newCrc=CRC16_CalculateCRC(data,sizeof(FlashDataCRC)-2,0);
    data->crc=newCrc;
    FlashEeprom_Write(FLASHDATA_ADDRESS,data,sizeof(FlashDataCRC),0);
    return FlashEeprom_Flush();
}
