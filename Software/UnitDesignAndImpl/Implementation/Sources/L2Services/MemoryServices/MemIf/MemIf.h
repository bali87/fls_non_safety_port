#ifndef __FLASHDATA_H_
#define __FLASHDATA_H_

#include <stdint.h>

#define FLASHDATA_ADDRESS   0x20000UL

#define NUM_OF_OMD          3
#define NUM_OF_DIODES       4

#define IntType             int16_t
#define FloatType           long double

typedef FloatType GainData[NUM_OF_OMD][NUM_OF_DIODES];
typedef IntType BiasData[NUM_OF_OMD][NUM_OF_DIODES+1];

typedef struct __attribute__ ((packed)) {
    IntType CTS_P[6];
    IntType CTS_N[6];
    IntType  NC_P[6];
    IntType  NC_N[6];
} RangeData;

typedef FloatType TempCompItem[3];  //n2,n1,n0
typedef TempCompItem TempCompSensorData[NUM_OF_DIODES];
typedef TempCompSensorData TempCompData[NUM_OF_OMD];

typedef struct __attribute__ ((packed)) {
    FloatType matrix[67][6];
    FloatType offset[12];
} PcaData;

typedef struct __attribute__ ((packed)) {
    GainData        gain;
    BiasData        bias;
    RangeData       range;
    TempCompData    tempComp;
    PcaData         pca;
} FlashData;

typedef struct __attribute__ ((packed)) {
    uint16_t data_format_version;
    uint8_t sensor_calibrated;
    int8_t  proxy_sensor_offset;
    uint8_t dummy[12];   //align to 16
    uint8_t sensor_name[8];
    uint8_t sensor_name_dummy[8];
} SensorHeader;
/** @struct  FlashDataCRC;
 *
 */
typedef struct __attribute__ ((packed))
{
    SensorHeader header;
    FlashData data;
    uint16_t crc;
} FlashDataCRC;
/** @fn char MemIf_LoadNVData(FlashDataCRC *data);
 *
 * @param data
 * @return
 */
char MemIf_LoadNVData(FlashDataCRC *data);
/** @fn char MemIf_UpdataNVData(FlashDataCRC *data);
 *
 * @param data
 * @return
 */
char MemIf_UpdataNVData(FlashDataCRC *data);

#endif
