#ifndef COM_MANAGER_H
#define COM_MANAGER_H

/**
 * @file
 *
 * Unit that implements:
 * - ComManager Interface
 *
 * @version $Id$
 */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */
#include "Std.h"
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */


typedef enum ComManager_TPDOType_tag
{
    TPDO_TYPE_NORMAL   = 0u,
    TPDO_TYPE_EXTENDED = 10u,
	TPDO_MODE_RANGE_DEBUG = 1u
	//TPDO_MODE_TEST = 2u
} ComManager_TPDOType;
/**
 *
 */
typedef enum ComManager_SyncModeType_tag
{
    SYNC_MODE_INTERNAL = 0u,//!< SYNC_MODE_INTERNAL
    SYNC_MODE_EXTERNAL = 1u //!< SYNC_MODE_EXTERNAL
} ComManager_SyncModeType;
/**
 *
 */
typedef enum ComManager_ExternalTriggerType_tag
{
    EXTERNAL_TRIGGER_NOT_RECEIVED           = 0u,//!< EXTERNAL_TRIGGER_NOT_RECEIVED
    EXTERNAL_TRIGGER_RECEIVED_NOT_TRIGGERED = 1u,//!< EXTERNAL_TRIGGER_RECEIVED_NOT_TRIGGERED
    EXTERNAL_TRIGGER_RECEIVED_TRIGGERED     = 2u //!< EXTERNAL_TRIGGER_RECEIVED_TRIGGERED
} ComManager_ExternalTriggerType;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */
/** @fn void ComManager_Init(uint8 nodeID)
 *	@param[in] nodeID: Sensor's node ID
 *	@brief ComManager's Init Function
 */
Std_ReturnType ComManager_Init( uint8 nodeID );
/** @fn Std_ReturnType ComManager_ExecuteTasks( void )

 *
 * @return
 */
Std_ReturnType ComManager_ExecuteTasks ( void );
/**@fn Std_ReturnType ComManager_RunRxFSM (void)
 *
 * @return
 * @brief
 * @note
 */
Std_ReturnType ComManager_RunRxFSM( void );
/** @fn ComManager_SyncModeType ComManager_GetSyncMode( void )

 *
 * @return
 */
ComManager_SyncModeType ComManager_GetSyncMode( void );
/** @fn Std_ReturnType ComManager_SetSyncMode( ComManager_SyncModeType syncMode );

 *
 * @param syncMode
 * @return
 */
Std_ReturnType ComManager_SetSyncMode( ComManager_SyncModeType syncMode );
/** @fn ComManager_ExternalTriggerType ComManager_GetExternalTriggerState( void )

 *
 * @return
 */
ComManager_ExternalTriggerType ComManager_GetExternalTriggerState( void );


/** @fn void ComManager_ReportWaitUs(sint16 waitUs)
 *
 * @param waitUs
 */
void ComManager_ReportWaitUs(sint16 waitUs);
/** @fn void ComManager_SetCompMode(uint8 compMode)
 *
 * 	@param compMode
 */
void ComManager_SetCompMode(uint8 compMode);
/** @fn void ComManager_SetPOC(uint32 poc)
 *
 * 	@param poc
 */
void ComManager_SetPOC(uint16 poc);
/** @fn void ComManager_ResetCommunication(void)
 *
 */
void ComManager_ResetCommunication(void);

ComManager_TPDOType ComManager_GetRunMode(void);
/** @} */

#endif /* COM_MANAGER_H */
