/**
 * @file
 *
 * Unit that manages:
 * - Communication with sensor's environment over UART
 *
 * @version $Id$
 */
/**@}*/
/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */
#include "Std.h"
#include "ComManager.h"
#include "CANDrv.h"
//#include "Bootloader.h"
#include "SignalProcessing.h"
//#include "FTCalcPca.h"
#include "SysTimer.h"
//#include "MCU.h"
#include "Board.h"
#include "FTCalcPca.h"
//#include "RangeMeasurement.h"
//#include "Timer2.h"
#include "rti.h"
#include "CalibrationDataHandler.h"
#include "CANDrv.h"
/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticDefines Static defines
 */

#define TPDO_INTERVAL_MS    (8u)

#define CMD_READ            (171u)
#define CMD_WRITE           (170u)
#define INDX_CMD            (0u)
#define INDX_ADDRESS_HI     (1u)
#define INDX_ADDRESS_LO     (2u)
#define INDX_LENGTH         (3u)
#define INDX_DATA           (4u)

#define INDX_DAQCFG_FILT     (5u)
#define INDX_DAQCFG_DBGRLS   (9u)
#define _stringify(t) #t
#define _expand(t) _stringify(t)

#define FT_TPDO_ADDRESS     (1800u)
#define RANGE_TPDO_ADDRESS  (5030u)

#define FT_TPDO_LEN_NORMAL      (16u)
#define FT_TPDO_LEN_EXTD       (96u)
#define RANGE_TPDO_LEN    (8u)

#define COM_ERROR_CHKSUM_WR (200u)
#define COM_ERROR_CHKSUM_RD (201u)
#define COM_ERROR_TIMEOUT (202u)

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */
/** @enum ComManager_ObjDictAddressType
 *  @brief ComManager Objects's Addresses Enumeration
 */
typedef enum
{
    ADDRESS_PRODUCT_TYPE = 10u,         //!< ADDRESS_PRODUCT_TYPE
    ADDRESS_SENSOR_NUMBER = 11u,        //!< ADDRESS_SENSOR_NUMBER
    ADDRESS_COM_TYPE = 12u,             //!< ADDRESS_COM_TYPE
    ADDRESS_HW_VER = 13u,               //!< ADDRESS_HW_VER
    ADDRESS_FW_VER = 14u,               //!< ADDRESS_FW_VER
    ADDRESS_PROTOCOL_VER = 15u,         //!< ADDRESS_PROTOCOL_VER
    ADDRESS_HW_REALTYPE = 16u,          //!< ADDRESS_HW_REALTYPE
    ADDRESS_HW_TYPEREQ = 17u,           //!< ADDRESS_HW_TYPEREQ
    ADDRESS_DQ_SERIAL_START = 18u,      //!< ADDRESS_DQ_SERIAL_START
    ADDRESS_TPDO_SPEED = 50u,           //!< ADDRESS_TPDO_SPEED
    ADDRESS_FILTER_TYPE = 51u,          //!< ADDRESS_FILTER_TYPE
    ADDRESS_ZERO = 52u,                 //!< ADDRESS_ZERO
    ADDRESS_COMP_MODE = 54u,            //!< ADDRESS_COMP_MODE
    ADDRESS_RLS_DBG = 55u,              //!< ADDRESS_RLS_DBG
    ADDRESS_POC_HI = 70u,               //!< ADDRESS_POC_HI
    ADDRESS_POC_LO = 71u,               //!< ADDRESS_POC_LO
    ADDRESS_UARTSPEED_HI = 72u,         //!< ADDRESS_UARTSPEED_HI
    ADDRESS_UARTSPEED_LO = 73u,         //!< ADDRESS_UARTSPEED_LO
    ADDRESS_COM_ERROR = 80u,            //!< ADDRESS_COM_ERROR
    ADDRESS_DAQ_ERROR = 81u,            //!< ADDRESS_DAQ_ERROR
    ADDRESS_ERROR_COUNTER = 83u,        //!< ADDRESS_ERROR_COUNTER
    ADDRESS_WAIT_US_HI = 84u,           //!< ADDRESS_WAIT_US_HI
    ADDRESS_WAIT_US_LO = 85u,           //!< ADDRESS_WAIT_US_LO
    ADDRESS_WAIT_US_MIN_HI = 86u,       //!< ADDRESS_WAIT_US_MIN_HI
    ADDRESS_WAIT_US_MIN_LO = 87u,       //!< ADDRESS_WAIT_US_MIN_LO
    ADDRESS_REF_5V_HI = 88u,            //!< ADDRESS_REF_5V_HI
    ADDRESS_REF_5V_LO = 89u,            //!< ADDRESS_REF_5V_LO
    ADDRESS_HW_VER_FROM = 94u,          //!< ADDRESS_HW_VER_FROM
    ADDRESS_HW_VER_TO = 95u,            //!< ADDRESS_HW_VER_TO
    ADDRESS_TPDO_MAIN_ADDR_HI = 100u,   //!< ADDRESS_TPDO_MAIN_ADDR_HI
    ADDRESS_TPDO_MAIN_ADDR_LO = 101u,   //!< ADDRESS_TPDO_MAIN_ADDR_LO
    ADDRESS_TPDO_MAIN_LEN = 102u,       //!< ADDRESS_TPDO_MAIN_LEN
    ADDRESS_TPDO_MAIN_EN = 103u,        //!< ADDRESS_TPDO_MAIN_EN
    ADDRESS_TPDO1_ADDR_HI = 104u,       //!< ADDRESS_TPDO1_ADDR_HI
    ADDRESS_TPDO1_ADDR_LO = 105u,       //!< ADDRESS_TPDO1_ADDR_LO
    ADDRESS_TPDO1_LEN = 106u,           //!< ADDRESS_TPDO1_LEN
    ADDRESS_TPDO1_EN = 107u,            //!< ADDRESS_TPDO1_EN
    ADDRESS_TPDO2_ADDR_HI = 108u,       //!< ADDRESS_TPDO2_ADDR_HI
    ADDRESS_TPDO2_ADDR_LO = 109u,       //!< ADDRESS_TPDO2_ADDR_LO
    ADDRESS_TPDO2_LEN = 110u,           //!< ADDRESS_TPDO2_LEN
    ADDRESS_TPDO2_EN = 111u,            //!< ADDRESS_TPDO2_EN
    ADDRESS_TPDO3_ADDR_HI = 112u,       //!< ADDRESS_TPDO3_ADDR_HI
    ADDRESS_TPDO3_ADDR_LO = 113u,       //!< ADDRESS_TPDO3_ADDR_LO
    ADDRESS_TPDO3_LEN = 114u,           //!< ADDRESS_TPDO3_LEN
    ADDRESS_TPDO3_EN = 115u,            //!< ADDRESS_TPDO3_EN
    ADDRESS_TPDO4_ADDR_HI = 116u,       //!< ADDRESS_TPDO4_ADDR_HI
    ADDRESS_TPDO4_ADDR_LO = 117u,       //!< ADDRESS_TPDO4_ADDR_LO
    ADDRESS_TPDO4_LEN = 118u,           //!< ADDRESS_TPDO4_LEN
    ADDRESS_TPDO4_EN = 119u,            //!< ADDRESS_TPDO4_EN
    ADDRESS_TPDO5_ADDR_HI = 120u,       //!< ADDRESS_TPDO5_ADDR_HI
    ADDRESS_TPDO5_ADDR_LO = 121u,       //!< ADDRESS_TPDO5_ADDR_LO
    ADDRESS_TPDO5_LEN = 122u,           //!< ADDRESS_TPDO5_LEN
    ADDRESS_TPDO5_EN = 123u,            //!< ADDRESS_TPDO5_EN
    ADDRESS_TPDO6_ADDR_HI = 124u,       //!< ADDRESS_TPDO6_ADDR_HI
    ADDRESS_TPDO6_ADDR_LO = 125u,       //!< ADDRESS_TPDO6_ADDR_LO
    ADDRESS_TPDO6_LEN = 126u,           //!< ADDRESS_TPDO6_LEN
    ADDRESS_TPDO6_EN = 127u,            //!< ADDRESS_TPDO6_EN
    ADDRESS_TPDO7_ADDR_HI = 128u,       //!< ADDRESS_TPDO7_ADDR_HI
    ADDRESS_TPDO7_ADDR_LO = 129u,       //!< ADDRESS_TPDO7_ADDR_LO
    ADDRESS_TPDO7_LEN = 130u,           //!< ADDRESS_TPDO7_LEN
    ADDRESS_TPDO7_EN = 131u,            //!< ADDRESS_TPDO7_EN
    ADDRESS_TPDO8_ADDR_HI = 132u,       //!< ADDRESS_TPDO8_ADDR_HI
    ADDRESS_TPDO8_ADDR_LO = 133u,       //!< ADDRESS_TPDO8_ADDR_LO
    ADDRESS_TPDO8_LEN = 134u,           //!< ADDRESS_TPDO8_LEN
    ADDRESS_TPDO8_EN = 135u,            //!< ADDRESS_TPDO8_EN

    ADDRESS_RESTART = 168u,             //!< ADDRESS_RESTART
    ADDRESS_TRIGGER = 190u,             //!< ADDRESS_TRIGGER
    ADDRESS_PROP_HEX_STATUS_HI = 272u,  //!< ADDRESS_PROP_HEX_STATUS_HI
    ADDRESS_PROP_HEX_STATUS_LO = 273u,  //!< ADDRESS_PROP_HEX_STATUS_LO
    ADDRESS_NOMINAL_CAPACITY_BEGIN=350u,//!< ADDRESS_NOMINAL_CAPACITY_BEGIN
    ADDRESS_NOMINAL_CAPACITY_END=361u,  //!< ADDRESS_NOMINAL_CAPACITY_END
    ADDRESS_COUNTS_BEGIN=450u,          //!< ADDRESS_COUNTS_BEGIN
    ADDRESS_COUNTS_END=461u,            //!< ADDRESS_COUNTS_END
    ADDRESS_SAMPLECOUNTER_HI=1200u,     //!< ADDRESS_SAMPLECOUNTER_HI
    ADDRESS_SAMPLECOUNTER_LO=1201u,     //!< ADDRESS_SAMPLECOUNTER_LO
    ADDRESS_GLOBALSTATUS_HI=1202u,      //!< ADDRESS_GLOBALSTATUS_HI
    ADDRESS_GLOBALSTATUS_LO=1203u,      //!< ADDRESS_GLOBALSTATUS_LO
    ADDRESS_FTDATA_BEGIN=1400u,         //!< ADDRESS_FTDATA_BEGIN
    ADDRESS_FTDATA_END=1411u,           //!< ADDRESS_FTDATA_END
    ADDRESS_RAWDATA_BEGIN=1500u,        //!< ADDRESS_RAWDATA_BEGIN
    ADDRESS_RAWDATA_END=1529u,          //!< ADDRESS_RAWDATA_END
    ADDRESS_RAWDATA4_BEGIN=1530u,       //!< ADDRESS_RAWDATA4_BEGIN
    ADDRESS_RAWDATA4_END=1539u,         //!< ADDRESS_RAWDATA4_END
    ADDRESS_CSDATA_BEGIN=1600u,         //!< ADDRESS_CSDATA_BEGIN
    ADDRESS_CSDATA_END=1629u,           //!< ADDRESS_CSDATA_END
    ADDRESS_TPDODATA_BEGIN=1800u,       //!< ADDRESS_TPDODATA_BEGIN
    ADDRESS_RANGE_MM=5030u,             //!< ADDRESS_RANGE_MM
    ADDRESS_RANGE_SAMPLE_HI=5031u,      //!< ADDRESS_RANGE_SAMPLE_HI
    ADDRESS_RANGE_SAMPLE_LO=5032u,      //!< ADDRESS_RANGE_SAMPLE_LO
    ADDRESS_RANGE_STATUS=5033u          //!< ADDRESS_RANGE_STATUS

} ComManager_ObjDictAddressType;

/** @enum ComManager_StateType
 *	@brief Enum State of module initialization.
 */
typedef enum _ComManager_StateType
{
    COMMANAGER_STATE_UNINITIALIZED = 0u, //!< COMMANAGER_STATE_UNINITIALIZED
    COMMANAGER_STATE_INITIALIZED = 1u //!< COMMANAGER_STATE_INITIALIZED
} ComManager_StateType;
/** @enum ComManager_RxStateType
 *  @brief ComManager Rx State Machine's states enumeration
 */
typedef enum
{
    RX_STATE_WAIT_PREAMBLE = 0u,//!< RX_STATE_WAIT_PREAMBLE
    RX_STATE_WAIT_HDR = 1U,     //!< RX_STATE_WAIT_HDR
    RX_STATE_CHECKSUM = 2U,     //!< RX_STATE_CHECKSUM
    RX_STATE_PROCESS = 3U,      //!< RX_STATE_PROCESS
    RX_STATE_RESET = 4u        //!< RX_STATE_RESET
} ComManager_RxStateType;
/** @struct ComManager_DecodedMsgType
 * 	@brief Decoded message descriptor
 */
typedef struct
{
    uint16 preamble;
    uint16 chksumCalculated;
    uint16 address;
    uint8 expectedLen;
    uint16 checksumGiven;
    uint32_t startTime;
    uint16 bodyLen;
}
ComManager_DecodedMsgType;

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */
/** COMPENSATION FILE NAME CONCATENATION *************************************/

uint8 SerialString[8] = "HEXHA018";//_expand (SENSOR_SERIAL); // Serial number read  from the define (header file)


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */
/** @var static ComManager_StateType ComManager_State
        @brief State of module initialization.
 */
static ComManager_RxStateType ComManager_RxState;
static ComManager_DecodedMsgType ComManager_DecodedMsg;
static uint16 GlobalStatusWord;
static uint16 SensorStatusWord;
static ComManager_StateType ComManager_State = COMMANAGER_STATE_UNINITIALIZED;
static uint16 ComManager_NodeID = 0u;
static uint16 ComManager_SampleCounter = 0u;
static uint16 powerOnCounter = 0u;
static ComManager_TPDOType ComManager_TpdoMode = TPDO_TYPE_NORMAL;

static ComManager_SyncModeType ComManager_SyncMode = SYNC_MODE_INTERNAL;
static ComManager_ExternalTriggerType ComManager_ExternalTrigger = EXTERNAL_TRIGGER_NOT_RECEIVED;


static uint8 ComManager_ResponseBuffer[256] = {0u};
static uint8 ComManager_RequestBuffer[256] = {0u};
static uint8 ComManager_SkipResponse = 0;


static uint8 ComManager_TpdoSpeed = 8;//;SYS_TIMER_MAIN_CYCLE_TIME;
static uint8 ComManager_ErrorCounter = 0u;
static uint8 ComManager_LastError = 0u;
static uint8 ComManager_DaqError = 0u;

static uint8 ComManager_CompMode = 0u;

static sint16 ComManager_WaitUs = 0u;
static sint16 ComManager_WaitUsMin = 32767;


static uint8_t ComManager_RawDataArray[40] = {0u};
static uint8_t ComManager_CompDataArray[30];
static uint8_t ComManager_FTDataArray[12];



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */

//static Std_ReturnType ComManager_CalcCheckSum (uint8 *dataPtr, uint16 dataLen, uint16 *checkSum);
/**@fn static Std_ReturnType ComManager_SendTPDO( ComManager_TPDOType TPDOType )
 *
 * @param[in] TPDOType
 * @return
 * @brief
 * @note
 */
static Std_ReturnType ComManager_SendTPDO (ComManager_TPDOType TPDOType);
/** @fn static Std_ReturnType ComManager_ProcessDecodedMsg(DecodedMsgType * packetPtr)
 *
 * 	@param packetPtr
 * 	@return
 */
static Std_ReturnType ComManager_ProcessDecodedMsg (ComManager_DecodedMsgType * packetPtr);



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */

Std_ReturnType ComManager_Init (uint8 nodeID)
{
    ComManager_NodeID = nodeID;
    ComManager_State = COMMANAGER_STATE_INITIALIZED;
    ComManager_SampleCounter = 0u;
    ComManager_RxState = RX_STATE_RESET;
    SysTimer_SetTimePeriod( SYS_TIMER_COM_TIMEOUT, 100u );
    return 0;
}

Std_ReturnType ComManager_ExecuteTasks (void)
{

    if (0u != ComManager_TpdoSpeed)
    {
        ComManager_SendTPDO (ComManager_TpdoMode);
    }
    ComManager_RunRxFSM ();
    if (EXTERNAL_TRIGGER_RECEIVED_TRIGGERED == ComManager_ExternalTrigger)
    {
        ComManager_SyncMode = SYNC_MODE_EXTERNAL;
    }
    ComManager_SampleCounter += TPDO_INTERVAL_MS;

    return (E_OK);
}

Std_ReturnType ComManager_RunRxFSM (void)
{
    uint8_t i;

    Std_ReturnType retVal = E_OK;

    while (E_OK == retVal)
    {
        switch (ComManager_RxState)
        {
        case RX_STATE_RESET:
            /* Start measuring Idle time */
            ComManager_DecodedMsg.startTime = SysTimer_GetSysTimerValue ();
            SysTimer_StartTimer( SYS_TIMER_COM_TIMEOUT, SYS_TIMER_ONE_SHOT );

            ComManager_RxState = RX_STATE_WAIT_PREAMBLE;
            //break;
        case RX_STATE_WAIT_PREAMBLE:
            retVal = CANDriver_GetMessage (CAN_INS1,ComManager_RequestBuffer, 1u);
            if ((E_OK == retVal) && ((CMD_WRITE == ComManager_RequestBuffer[INDX_CMD]) || (CMD_READ == ComManager_RequestBuffer[INDX_CMD])))
            {
                /* Start measuring message processing's time */
                ComManager_DecodedMsg.startTime = rtiGetCurrentTick(0);//Timer2_GetValue ();
                ComManager_DecodedMsg.preamble = ComManager_RequestBuffer[INDX_CMD];
                ComManager_RxState = RX_STATE_WAIT_HDR;
                /* No Break try to continue next state  WAIT_HDR*/
            }
            else
            {
                /* No received message in last 40ms (only in external trigger mode) */
                if (((SysTimer_GetSysTimerValue () - ComManager_DecodedMsg.startTime) > 40000u) && (SYNC_MODE_EXTERNAL == ComManager_SyncMode))
                {

                    // ComManager_ErrorCounter++;
                    //  ComManager_LastError = 230u; //Trigger Lost
                    //                         BREAKPNT();
                    //ComManager_RxState = RX_STATE_RESET;
                }

                break;
            }

        case RX_STATE_WAIT_HDR:

            retVal = CANDriver_GetMessage (CAN_INS1,&ComManager_RequestBuffer[INDX_ADDRESS_HI], 3u);
         //   CANDrv_ReadFullBuffer(&ComManager_RequestBuffer[INDX_ADDRESS_HI]);
            if ((E_OK == retVal))
            {
                ComManager_DecodedMsg.address = (((uint16) ComManager_RequestBuffer[INDX_ADDRESS_HI]) << 8) | (uint16) (ComManager_RequestBuffer[INDX_ADDRESS_LO]);
                ComManager_DecodedMsg.expectedLen = (ComManager_RequestBuffer[INDX_LENGTH]);
                ComManager_RxState = RX_STATE_CHECKSUM;
                /* Determine packet body's length */
                if (CMD_WRITE == ComManager_DecodedMsg.preamble)
                {
                    ComManager_DecodedMsg.bodyLen = (ComManager_DecodedMsg.expectedLen + 2u);
                }
                else
                {
                    ComManager_DecodedMsg.bodyLen = 2u;
                }
                /* No break try to get data + cheksum  */
            }
            else
            {
                /* Timeout in Communication */
                //if ((SysTimer_GetSysTimerValue () - ComManager_DecodedMsg.startTime) > 100u)
                if ( SYS_TIMER_IS_EXPIRED == SysTimer_GetTimerStatus( SYS_TIMER_COM_TIMEOUT ))
                {
                    ComManager_RxState = RX_STATE_RESET;
                    ComManager_ErrorCounter++;
                    ComManager_LastError = COM_ERROR_TIMEOUT;
                    //                         BREAKPNT();
                }
                break;
            }

        case RX_STATE_CHECKSUM:
            retVal = CANDriver_GetMessage (CAN_INS1,&ComManager_RequestBuffer[INDX_DATA], ComManager_DecodedMsg.bodyLen);
          //  CANDrv_ReadFullBuffer(&ComManager_RequestBuffer[INDX_DATA]);

            if ((E_OK == retVal))
            {
                ComManager_DecodedMsg.checksumGiven = ((((uint16) ComManager_RequestBuffer[ (INDX_DATA + ComManager_DecodedMsg.bodyLen - 2) ]) << 8)
                                                       | ((uint16) (ComManager_RequestBuffer[ (INDX_DATA + ComManager_DecodedMsg.bodyLen - 1)])));
                ComManager_DecodedMsg.chksumCalculated = 0u;

                for (i = 0u; i < (ComManager_DecodedMsg.bodyLen + INDX_DATA - 2u); i++)
                {
                    ComManager_DecodedMsg.chksumCalculated += ComManager_RequestBuffer[i];
                }
                if (ComManager_DecodedMsg.chksumCalculated == ComManager_DecodedMsg.checksumGiven)
                {
                    ComManager_RxState = RX_STATE_PROCESS;

                }
                /* Checksum Error*/
                else
                {



                    ComManager_ErrorCounter++;
                    if (CMD_WRITE == ComManager_RequestBuffer[INDX_CMD])
                    {
                        ComManager_LastError = COM_ERROR_CHKSUM_WR; //Checksum Error Read
                    }
                    else
                    {
                        ComManager_LastError = COM_ERROR_CHKSUM_RD; //Checksum Error Read
                    }
                    ComManager_RxState = RX_STATE_RESET;
                    break;
                }
            }
            else
            {
                /* Timeout in Communication */
                // if ((SysTimer_GetSysTimerValue () - ComManager_DecodedMsg.startTime) > 100u)
                if ( SYS_TIMER_IS_EXPIRED == SysTimer_GetTimerStatus( SYS_TIMER_COM_TIMEOUT ))
                {
                    ComManager_RxState = RX_STATE_RESET;
                    ComManager_ErrorCounter++;
                    ComManager_LastError = COM_ERROR_TIMEOUT;
                    //                         BREAKPNT();
                }
                break;
            }
        case RX_STATE_PROCESS:
            ComManager_ProcessDecodedMsg (&ComManager_DecodedMsg);
            /* TODO Check Tx Fifo */
            ComManager_RxState = RX_STATE_RESET;
            break;
        default:
            //Error
            break;
        }
    }
    return (E_OK);
}

ComManager_SyncModeType ComManager_GetSyncMode (void)
{
    return (ComManager_SyncMode);
}

Std_ReturnType ComManager_SetSyncMode (ComManager_SyncModeType syncMode)
{
    ComManager_SyncMode = syncMode;
    return (E_OK);
}

ComManager_ExternalTriggerType ComManager_GetExternalTriggerState (void)
{
    ComManager_ExternalTriggerType retVal = ComManager_ExternalTrigger;
    if (EXTERNAL_TRIGGER_NOT_RECEIVED != ComManager_ExternalTrigger)
    {
        ComManager_ExternalTrigger = EXTERNAL_TRIGGER_NOT_RECEIVED;
    }
    return (retVal);
}

void ComManager_ReportWaitUs (sint16 waitUs)
{
    ComManager_WaitUs = waitUs;
    if (waitUs < ComManager_WaitUsMin)
    {
        ComManager_WaitUsMin = waitUs;

    }
}




void ComManager_SetCompMode(uint8 compMode)
{
    ComManager_CompMode = compMode;
}



void ComManager_SetPOC(uint16 poc)
{
    powerOnCounter = poc;
}

void ComManager_ResetCommunication(void)
{
   // UART_ClearBuffers();
}
ComManager_TPDOType ComManager_GetRunMode(void)
{
    return (ComManager_TpdoMode);
}
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */


static Std_ReturnType ComManager_SendTPDO (ComManager_TPDOType TPDOType)
{
    Std_ReturnType retVal = E_NOT_OK;
    static ComManager_DecodedMsgType tpdoQuery;
    uint8 negFtOverload;
    uint8 posFtOverload;

    uint8_t daqTypeErr = 0u;

    SignalProcessing_GetRawData (ComManager_RawDataArray);
    SignalProcessing_GetCompensatedData (ComManager_CompDataArray);

    FTCalcPca_GetFTData (ComManager_FTDataArray);
    posFtOverload = FTCalcPca_GetNegOverload ();
    negFtOverload = FTCalcPca_GetPosOverload ();

    if ((Board_GetRef5VSignal () < 4800u))
    {
        ComManager_DaqError = 2u;
    }
    else
    {
        ComManager_DaqError = 0u;
    }
    if (ComManager_DaqError > 0)
        daqTypeErr |= 1u;
    if (ComManager_LastError > 0)
        daqTypeErr |= 2u;

    GlobalStatusWord = ((((posFtOverload | negFtOverload) << 4)&0x3F0u)
                        | ((Board_GetSensorFailureStatus () << 10)& 0x1C00u)
                        | ((daqTypeErr << 13)& 0xE000u)
                       );

    SensorStatusWord = (1u << 15
                        | ((posFtOverload & 0x38u) << 6)
                        | ((negFtOverload & 0x38u) << 3)
                        | ((posFtOverload & 0x07u) << 3)
                        | ((negFtOverload & 0x07u) << 0)
                        | ((Board_GetSensorFailureStatus () << 12)& 0x7000u)
                       );
    if ((TPDO_TYPE_NORMAL == TPDOType)||(TPDO_MODE_RANGE_DEBUG == TPDOType))
    {

        tpdoQuery.address = FT_TPDO_ADDRESS;
        tpdoQuery.preamble = CMD_READ;
        tpdoQuery.expectedLen = FT_TPDO_LEN_NORMAL;
        ComManager_ProcessDecodedMsg (&tpdoQuery);

       /* tpdoQuery.address = RANGE_TPDO_ADDRESS;
        tpdoQuery.preamble = CMD_READ;
        tpdoQuery.expectedLen = RANGE_TPDO_LEN;
        ComManager_ProcessDecodedMsg (&tpdoQuery);*/

        retVal = E_OK;

    }
    else if (TPDO_TYPE_EXTENDED == TPDOType)
    {
        tpdoQuery.address = FT_TPDO_ADDRESS;
        tpdoQuery.preamble = CMD_READ;
        tpdoQuery.expectedLen = FT_TPDO_LEN_EXTD;
        ComManager_ProcessDecodedMsg (&tpdoQuery);

        retVal = E_OK;
    }
    else
    {
        retVal = E_NOT_OK;
    }

    return (retVal);
}


static Std_ReturnType ComManager_ProcessDecodedMsg (ComManager_DecodedMsgType * packetPtr)
{

    Std_ReturnType retVal = E_OK;
    uint16 i = 0u;
    uint16 currentAddress = packetPtr->address;
    uint16 chkSum = 0u;
    //uint8_t * msgPtr;
    // uint8_t remainder = packetPtr->expectedLen;
    static uint8 tempData;
    static uint16 tempData16;
    tempData = 0u;
    if (NULL == packetPtr)
    {
        retVal = E_NOT_OK;
    }
    else
    {
        retVal = E_OK;
        if (packetPtr->preamble == CMD_READ)
        {
            ComManager_ResponseBuffer[0] = 170u;
            ComManager_ResponseBuffer[1] = (uint8) (packetPtr->address >> 8)&0xFFu;
            ComManager_ResponseBuffer[2] = (uint8) (packetPtr->address)&0xFFu;
            ComManager_ResponseBuffer[3] = (uint8) packetPtr->expectedLen;
            chkSum += ComManager_ResponseBuffer[0];
            chkSum += ComManager_ResponseBuffer[1];
            chkSum += ComManager_ResponseBuffer[2];
            chkSum += ComManager_ResponseBuffer[3];
            //msgPtr = &response[4];
            i = 0u;
            while (i < packetPtr->expectedLen)
            {
                switch (currentAddress)
                {
                case ADDRESS_PRODUCT_TYPE:
                    ComManager_ResponseBuffer[(4 + i)] = BOARD_PRODUCT_TYPE;
                    break;
                case ADDRESS_SENSOR_NUMBER:
                    ComManager_ResponseBuffer[(4 + i)] = 3u;
                    break;
                case ADDRESS_COM_TYPE:
                    ComManager_ResponseBuffer[(4 + i)] = 2u;
                    break;
                case ADDRESS_HW_VER:
                    ComManager_ResponseBuffer[(4 + i)] = 160;
                    break;
                case ADDRESS_FW_VER:
                    ComManager_ResponseBuffer[(4 + i)] = 211;
                    break;
                case ADDRESS_PROTOCOL_VER:
                    ComManager_ResponseBuffer[(4 + i)] = PROTOCOL_VERSION;
                    break;
                case ADDRESS_HW_REALTYPE:
                    ComManager_ResponseBuffer[(4 + i)] = HARDWARE_TYPE;
                    break;
                case ADDRESS_HW_TYPEREQ:
                    ComManager_ResponseBuffer[(4 + i)] = HARDWARE_TYPE;
                    break;
                case ADDRESS_DQ_SERIAL_START ... 25u:
                    ComManager_ResponseBuffer[(4 + i)] = SerialString[(currentAddress - 18u)];
                    break;
                case ADDRESS_TPDO_SPEED:
                    ComManager_ResponseBuffer[(4 + i)] = ComManager_TpdoSpeed;
                    break;
                case ADDRESS_FILTER_TYPE:
                    ComManager_ResponseBuffer[(4 + i)] = SignalProcessing_GetFilterType ();
                    break;
                case ADDRESS_ZERO:
                    ComManager_ResponseBuffer[(4 + i)] = FTCalcPca_GetZeroStatus();
                    break;
                case ADDRESS_COMP_MODE:
                    ComManager_ResponseBuffer[(4 + i)] = (CalibrationDataHandler_IsCalibrated()? 7u:0u);
                    break;
                case ADDRESS_RLS_DBG:
                    ComManager_ResponseBuffer[(4 + i)] = ComManager_TpdoMode;
                    break;
                case ADDRESS_POC_HI:
                    ComManager_ResponseBuffer[(4 + i)] = (uint8) ((powerOnCounter >> 8)&0xFFu);
                    break;
                case ADDRESS_POC_LO:
                    ComManager_ResponseBuffer[(4 + i)] = (uint8) ((powerOnCounter >> 0)&0xFFu);
                    break;

                case ADDRESS_UARTSPEED_HI:
                    ComManager_ResponseBuffer[(4 + i)] = (uint8) ((2500 >> 8)&0xFFu);
                    break;
                case ADDRESS_UARTSPEED_LO:
                    ComManager_ResponseBuffer[(4 + i)] = (uint8) ((2500 >> 0)&0xFFu);
                    break;


                case ADDRESS_COM_ERROR:
                    ComManager_ResponseBuffer[(4 + i)] = ComManager_LastError;
                    break;
                case ADDRESS_DAQ_ERROR:
                    ComManager_ResponseBuffer[(4 + i)] = ComManager_DaqError;
                    break;
                case ADDRESS_ERROR_COUNTER:
                    ComManager_ResponseBuffer[(4 + i)] = ComManager_ErrorCounter;
                    break;
                case ADDRESS_WAIT_US_HI:
                    ComManager_ResponseBuffer[(4 + i)] = (uint8) ((ComManager_WaitUs >> 8)&0xFFu);
                    break;
                case ADDRESS_WAIT_US_LO:
                    ComManager_ResponseBuffer[(4 + i)] = (uint8) ((ComManager_WaitUs >> 0)&0xFFu);
                    break;
                case ADDRESS_WAIT_US_MIN_HI:
                    ComManager_ResponseBuffer[(4 + i)] = (uint8) ((ComManager_WaitUsMin >> 8)&0xFFu);
                    break;
                case ADDRESS_WAIT_US_MIN_LO:
                    ComManager_ResponseBuffer[(4 + i)] = (uint8) ((ComManager_WaitUsMin >> 0)&0xFFu);
                    break;

                case ADDRESS_REF_5V_HI://88u:
                    ComManager_ResponseBuffer[(4 + i)] = (Board_GetRef5VSignal () >> 8)&0xFFu;
                    break;
                case ADDRESS_REF_5V_LO://89u:
                    ComManager_ResponseBuffer[(4 + i)] = (Board_GetRef5VSignal () >> 0)&0xFFu;
                    break;
                case ADDRESS_HW_VER_FROM://94u:
                    ComManager_ResponseBuffer[(4 + i)] = 31;
                    break;
                case ADDRESS_HW_VER_TO://95u:
                    ComManager_ResponseBuffer[(4 + i)] = 31;
                    break;

                case ADDRESS_TPDO_MAIN_ADDR_HI://100u:
                    ComManager_ResponseBuffer[(4 + i)] = (1800u >> 8)&0xFFu;
                    break;
                case ADDRESS_TPDO_MAIN_ADDR_LO://101u:
                    ComManager_ResponseBuffer[(4 + i)] = (1800u >> 0)&0xFFu;
                    break;
                case ADDRESS_TPDO_MAIN_LEN://102u:
                    if (TPDO_TYPE_NORMAL == ComManager_TpdoMode)
                    {
                        ComManager_ResponseBuffer[(4 + i)] = (FT_TPDO_LEN_NORMAL)&0xFFu;
                    }
                    else
                    {
                        ComManager_ResponseBuffer[(4 + i)] = (FT_TPDO_LEN_EXTD)&0xFFu;
                    }
                    break;
                case ADDRESS_TPDO_MAIN_EN://103
                    ComManager_ResponseBuffer[(4 + i)] = 1u;
                    break;
                case ADDRESS_TPDO1_ADDR_HI://104u:
                    ComManager_ResponseBuffer[(4 + i)] = (1200u >> 8)&0xFFu;
                    break;
                case ADDRESS_TPDO1_ADDR_LO://105u:
                    ComManager_ResponseBuffer[(4 + i)] = (1200u >> 0)&0xFFu;
                    break;
                case ADDRESS_TPDO1_LEN://106u:
                    ComManager_ResponseBuffer[(4 + i)] = 4u;
                    break;
                case ADDRESS_TPDO1_EN://:107u:
                    ComManager_ResponseBuffer[(4 + i)] = 1u;
                    break;
                case ADDRESS_TPDO2_ADDR_HI:
                    ComManager_ResponseBuffer[(4 + i)] = (1400u >> 8)&0xFFu;
                    break;
                case ADDRESS_TPDO2_ADDR_LO:
                    ComManager_ResponseBuffer[(4 + i)] = (1400u >> 0)&0xFFu;
                    break;
                case ADDRESS_TPDO2_LEN:
                    ComManager_ResponseBuffer[(4 + i)] = 12u;
                    break;
                case ADDRESS_TPDO2_EN:
                    ComManager_ResponseBuffer[(4 + i)] = 1u;
                    break;
                case ADDRESS_TPDO3_ADDR_HI:
                    ComManager_ResponseBuffer[(4 + i)] = (1500u >> 8)&0xFFu;
                    break;
                case ADDRESS_TPDO3_ADDR_LO:
                    ComManager_ResponseBuffer[(4 + i)] = (1500u >> 0)&0xFFu;
                    break;
                case ADDRESS_TPDO3_LEN:
                    ComManager_ResponseBuffer[(4 + i)] = 40u;
                    break;
                case ADDRESS_TPDO3_EN:
                    ComManager_ResponseBuffer[(4 + i)] = (TPDO_TYPE_EXTENDED == ComManager_TpdoMode)? 1u : 0u;
                    break;
                case ADDRESS_TPDO4_ADDR_HI:
                    ComManager_ResponseBuffer[(4 + i)] = (1600u >> 8)&0xFFu;
                    break;
                case ADDRESS_TPDO4_ADDR_LO:
                    ComManager_ResponseBuffer[(4 + i)] = (1600u >> 0)&0xFFu;
                    break;
                case ADDRESS_TPDO4_LEN:
                    ComManager_ResponseBuffer[(4 + i)] = 40u;
                    break;
                case ADDRESS_TPDO4_EN:
                    ComManager_ResponseBuffer[(4 + i)] = (TPDO_TYPE_EXTENDED == ComManager_TpdoMode)? 1u : 0u;
                    break;
                case ADDRESS_TRIGGER:
                    ComManager_ResponseBuffer[(4 + i)] = (uint8) ComManager_ExternalTrigger;
                    break;
                case ADDRESS_PROP_HEX_STATUS_HI:
                    ComManager_ResponseBuffer[(4 + i)] = (uint8) (SensorStatusWord >> 8);
                    break;
                case ADDRESS_PROP_HEX_STATUS_LO:
                    ComManager_ResponseBuffer[(4 + i)] = (uint8) (SensorStatusWord >> 0);
                    break;
                case ADDRESS_NOMINAL_CAPACITY_BEGIN ... ADDRESS_NOMINAL_CAPACITY_END:
                    if (!(currentAddress % 2))
                    {
                        ComManager_ResponseBuffer[(4 + i)] = (uint8) ((CalibDataHandler_GetPosNomCap ((currentAddress - ADDRESS_NOMINAL_CAPACITY_BEGIN) / 2) >> 8)&0xFFu);
                    }
                    else
                    {
                        ComManager_ResponseBuffer[(4 + i)] = (uint8) ((CalibDataHandler_GetPosNomCap ((currentAddress - ADDRESS_NOMINAL_CAPACITY_BEGIN) / 2) >> 0)&0xFFu);
                    }
                    break;
                case 362u:
                    ComManager_ResponseBuffer[(4 + i)] = (uint8) ((CalibDataHandler_GetNegNomCap (2u) >> 8)&0xFFu);
                    break;
                case 363u:
                    ComManager_ResponseBuffer[(4 + i)] = (uint8) ((CalibDataHandler_GetNegNomCap (2u) >> 0)&0xFFu);
                    break;
                case ADDRESS_COUNTS_BEGIN ... ADDRESS_COUNTS_END:
                    if (!(currentAddress % 2))
                    {
                        ComManager_ResponseBuffer[(4 + i)] = (uint8) ((CalibDataHandler_GetPosCount ((currentAddress - ADDRESS_COUNTS_BEGIN) / 2) >> 8)&0xFFu);
                    }
                    else
                    {
                        ComManager_ResponseBuffer[(4 + i)] = (uint8) ((CalibDataHandler_GetPosCount ((currentAddress - ADDRESS_COUNTS_BEGIN) / 2) >> 0)&0xFFu);
                    }
                    break;
                case 462u:
                    ComManager_ResponseBuffer[(4 + i)] = (uint8) ((CalibDataHandler_GetNegCount (2u) >> 8)&0xFFu);
                    break;
                case 463u:
                    ComManager_ResponseBuffer[(4 + i)] = (uint8) ((CalibDataHandler_GetNegCount (2u) >> 0)&0xFFu);
                    break;
                case ADDRESS_TPDODATA_BEGIN:
                case ADDRESS_SAMPLECOUNTER_HI:
                    ComManager_ResponseBuffer[(4 + i)] = (ComManager_SampleCounter >> 8)&0xFFu;
                    break;
                case (ADDRESS_TPDODATA_BEGIN+1u):
                case ADDRESS_SAMPLECOUNTER_LO:
                    ComManager_ResponseBuffer[(4 + i)] = (ComManager_SampleCounter >> 0)&0xFFu;
                    break;
                case (ADDRESS_TPDODATA_BEGIN+2u):
                case ADDRESS_GLOBALSTATUS_HI:
                    ComManager_ResponseBuffer[(4 + i)] = (GlobalStatusWord >> 8)&0xFFu;
                    break;
                case (ADDRESS_TPDODATA_BEGIN+3u):
                case ADDRESS_GLOBALSTATUS_LO:
                    ComManager_ResponseBuffer[(4 + i)] = (GlobalStatusWord >> 0)&0xFFu;
                    break;
                case ADDRESS_FTDATA_BEGIN ... ADDRESS_FTDATA_END:
                    ComManager_ResponseBuffer[(4 + i)] = ComManager_FTDataArray[(currentAddress - ADDRESS_FTDATA_BEGIN)];
                    break;
                case ADDRESS_RAWDATA_BEGIN ... ADDRESS_RAWDATA_END:
                    ComManager_ResponseBuffer[(4 + i)] = ComManager_RawDataArray[(currentAddress - ADDRESS_RAWDATA_BEGIN)];
                    break;

                case ADDRESS_CSDATA_BEGIN ... ADDRESS_CSDATA_END:
                    ComManager_ResponseBuffer[(4 + i)] = ComManager_CompDataArray[(currentAddress - ADDRESS_CSDATA_BEGIN)];
                    break;
                case (ADDRESS_TPDODATA_BEGIN+04u) ... (ADDRESS_TPDODATA_BEGIN+15u):
                                ComManager_ResponseBuffer[(4 + i)] = ComManager_FTDataArray[(currentAddress - 1804u)];
                    break;
                case (ADDRESS_TPDODATA_BEGIN+16u) ... (ADDRESS_TPDODATA_BEGIN+45u):
                                ComManager_ResponseBuffer[(4 + i)] = ComManager_RawDataArray[(currentAddress - 1816u)];
                    break;
                case (ADDRESS_TPDODATA_BEGIN+56u) ... (ADDRESS_TPDODATA_BEGIN+85u):
                                ComManager_ResponseBuffer[(4 + i)] = ComManager_CompDataArray[(currentAddress - 1856u)];
                    break;


                default:
                    ComManager_ResponseBuffer[(4 + i)] = 0u;
                    break;

                }
                chkSum += ComManager_ResponseBuffer[(4 + i)];
                i++;
                currentAddress = (packetPtr->address + i);
            }
            ComManager_ResponseBuffer[(4 + i)] = ((chkSum >> 8)&0xFFu);
            ComManager_ResponseBuffer[(4 + i + 1)] = ((chkSum)&0xFFu);

            CANDriver_PutMessage(CAN_INS1,ComManager_ResponseBuffer, (4 + i + 2));
        }
        else if (packetPtr->preamble == CMD_WRITE)
{
            ComManager_ResponseBuffer[0] = CMD_WRITE;
            ComManager_ResponseBuffer[1] = (uint8) 0u;
            ComManager_ResponseBuffer[2] = (uint8) ADDRESS_COM_ERROR;
            ComManager_ResponseBuffer[3] = (uint8) 1u;
            ComManager_ResponseBuffer[4] = (uint8) 0u;
            chkSum += ComManager_ResponseBuffer[0];
            chkSum += ComManager_ResponseBuffer[1];
            chkSum += ComManager_ResponseBuffer[2];
            chkSum += ComManager_ResponseBuffer[3];
            chkSum += ComManager_ResponseBuffer[4];
            ComManager_SkipResponse = 0;
            i = 0u;
            while (i < (packetPtr->expectedLen))
            {
                switch (currentAddress)
                {
                case ADDRESS_TRIGGER:
                    ComManager_SkipResponse = 1;
                    if (ComManager_RequestBuffer[(4 + i)] >= 1u)
                    {
                        ComManager_ExternalTrigger = EXTERNAL_TRIGGER_RECEIVED_TRIGGERED;
                    }
                    else
                    {
                        ComManager_ExternalTrigger = EXTERNAL_TRIGGER_RECEIVED_NOT_TRIGGERED;
                    }
                    break;
                case ADDRESS_TPDO_SPEED:
                    if (ComManager_RequestBuffer[(4 + i)] >= (uint8_t)0u)
                    {
                        ComManager_TpdoSpeed = ComManager_RequestBuffer[(4 + i)];
                    }
                    if (ComManager_TpdoSpeed > 0)
                        ComManager_TpdoSpeed = 8;
                    break;
                case ADDRESS_FILTER_TYPE:

                    SignalProcessing_SetFilterType (ComManager_RequestBuffer[(4 + i)]);
                    break;
                case ADDRESS_ZERO:
                    if (0u == ComManager_RequestBuffer[(4 + i)])
                    {
                        FTCalcPca_Unzero ();
                    }
                    else if (255u == ComManager_RequestBuffer[(4 + i)])
                    {
                        FTCalcPca_Zero ();
                    }
                    break;
                case ADDRESS_COMP_MODE:
                    ComManager_CompMode = ComManager_RequestBuffer[(4 + i)];
                    break;
                case ADDRESS_RLS_DBG:
                    ComManager_TpdoMode = ComManager_RequestBuffer[(4 + i)];
                    break;
                case ADDRESS_COM_ERROR:
                    ComManager_LastError = ComManager_RequestBuffer[(4 + i)];
                    break;
                case ADDRESS_ERROR_COUNTER:
                    ComManager_ErrorCounter = ComManager_RequestBuffer[(4 + i)];
                    break;
                case ADDRESS_WAIT_US_MIN_HI:
                    ComManager_WaitUsMin = 32767; // It restarts the minimum value when any write is issued to this address
                    break;
                case ADDRESS_RESTART:
                    if (ComManager_RequestBuffer[(4 + i)] == 'B')
                    {
                        //Bootloader_SetTrigger(1);
                    }
                    else
                    {
                     //   Bootloader_SetTrigger(0);
                    }
                  //  asm("RESET");
                    break;

                case (ADDRESS_RAWDATA4_BEGIN+6u):
					if(TPDO_MODE_RANGE_DEBUG == ComManager_TpdoMode)
					{
							tempData16 = (sint16)ComManager_RequestBuffer[(4 + i)] << 8u;
							i++;
							currentAddress++;
							/** No break **/
					}
                case (ADDRESS_RAWDATA4_BEGIN+7u):
					if(TPDO_MODE_RANGE_DEBUG == ComManager_TpdoMode)
					{
						tempData16 |= (sint16)ComManager_RequestBuffer[(4 + i)];
						//RangeMeasurment_SetOffsetCalibrationData((sint16)tempData16) ;

					}
               break;
                case (ADDRESS_RAWDATA4_BEGIN+8u):
					if(TPDO_MODE_RANGE_DEBUG == ComManager_TpdoMode)
					{
              			tempData16 = (sint16)ComManager_RequestBuffer[(4 + i)] << 8u;
                        i++;
					  currentAddress++;
					}
					  /** No break **/
			  case (ADDRESS_RAWDATA4_BEGIN+9u):
					if(TPDO_MODE_RANGE_DEBUG == ComManager_TpdoMode)
					{
						tempData16 |= (sint16)ComManager_RequestBuffer[(4 + i)];
						//RangeMeasurment_SetXTalkCalibrationData((sint16)tempData16) ;
					}
              break;

                default:
                    break;
                }
                i++;
                currentAddress++;
            }
            ComManager_ResponseBuffer[(5)] = ((chkSum >> 8)&0xFFu);
            ComManager_ResponseBuffer[(6)] = ((chkSum)&0xFFu);
            if (0u == ComManager_SkipResponse) // For the backpack trigger packet do not generate response
            {

                CANDriver_PutMessage(CAN_INS1,ComManager_ResponseBuffer, (7u));
            }
        }
    }

    return (retVal);
}


/** @} */



