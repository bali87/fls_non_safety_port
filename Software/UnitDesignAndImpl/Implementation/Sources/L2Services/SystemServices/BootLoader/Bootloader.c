/*
 * File:   bootloader.c
 * Author: Koncseg Laszlo
 *
 * Created on 2018. j�lius 11., 9:47
 */
#include <Bootloader.h>
#include "MCU.h"
#include "FlashEeprom.h"


static uint32_t Bootloader_UserAppResetVector = 0;
uint32_t BootLoader_ResetAddress[2] = {0,0};


void  Bootloader_SetTrigger(char startBootloadMode)
{
    uint32_t trg = startBootloadMode? TRG_VALUE :0;
    FlashEeprom_Write(TRG_MEM_FLAG_ADDR,&trg,3,0);
    FlashEeprom_Flush();
}

char Bootloader_CheckTrigger(void)
{
    uint32_t trg = 0xFF;
    FlashEeprom_Read(TRG_MEM_FLAG_ADDR,&trg,3,0);
    if (trg==TRG_VALUE) {
        Bootloader_SetTrigger(0);
        return 1;
    }
    else 
        return 0;
}


char  Bootloader_IsValidAppPresent(void)
{
    uint32_t appStart = 0x00FFFFFF;
    FlashEeprom_Read(USER_APP_RESET_ADDRESS,&appStart,3,0);
	
	if ((appStart == 0x00FFFFFF) || (appStart == 0x00000000)) {
		return 0;
	}
	else {
		uint32_t appLength=0,appCrc = 0;
		FlashEeprom_Read(USER_APP_CRC_ADDRESS+0,&appCrc,3,0);
		FlashEeprom_Read(USER_APP_CRC_ADDRESS+4,&appLength,3,0);
		
		uint32_t calcCrc=FlashEeprom_ReadCRC(APP_FLASH_BASE_ADDRESS,(APP_FLASH_BASE_ADDRESS+appLength));
		if (calcCrc==appCrc) {
			Bootloader_UserAppResetVector = appStart;
			return 1;
		}
		else
			return 0;
	}
}

void  Bootloader_JumpToApp(void)
{
    asm("goto %0" : : "r"(Bootloader_UserAppResetVector));
}

uint8_t Bootloader_SetValidAppTrigger(void)
{
    FlashEeprom_Write(USER_APP_RESET_ADDRESS,BootLoader_ResetAddress,8,0);
    return FlashEeprom_Flush();
//    NV_Mem_Write_DWord(USER_APP_RESET_ADDRESS, ResetVector[0],ResetVector[1]);
}

void Bootloader_SaveResetVector(uint32_t *v)
{
    BootLoader_ResetAddress[0]=v[0];
    BootLoader_ResetAddress[1]=v[1];
}
