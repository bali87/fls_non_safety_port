#ifndef BOOTLOADER_FLAG_H
#define BOOTLOADER_FLAG_H
#include "Std.h"
#include "BootloaderSettings.h"

extern uint32_t BootLoader_ResetAddress[2];
/** @fn void  Bootloader_SetTrigger(char startBootloadMode);
 *
 * @param startBootloadMode
 */
void  Bootloader_SetTrigger(char startBootloadMode);
//void  Clear_Bootloader_Trigger(void);
/** @fn char Bootloader_CheckTrigger(void);
 *
 * @return
 */
char Bootloader_CheckTrigger(void);
/** @fn char  Bootloader_IsValidAppPresent(void);
 *
 * @return
 */
char  Bootloader_IsValidAppPresent(void);
/** @fn void  Bootloader_JumpToApp(void);
 *
 */
void  Bootloader_JumpToApp(void);
/** @fn uint8_t Bootloader_SetValidAppTrigger();
 *
 * @return
 */
uint8_t Bootloader_SetValidAppTrigger();
/** @fn void Bootloader_SaveResetVector(uint32_t *v);
 *
 * @param v
 */
void Bootloader_SaveResetVector(uint32_t *v);

#endif
