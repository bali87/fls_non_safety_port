#ifndef BOOTLOADER_SETTINGS_H
#define BOOTLOADER_SETTINGS_H

#define APP_FLASH_BASE_ADDRESS            0x4800UL  // in word 
#define APP_FLASH_END_ADDRESS             0x29FFFUL   // in word


#define USER_APP_RESET_ADDRESS            0x29800UL     // 3 bytes
#define USER_APP_CRC_ADDRESS              0x29804UL     // 2 bytes


#define TRG_MEM_FLAG_ADDR                 0x2A000UL     // 3 bytes
#define TRG_VALUE                         0x61

// Hardware Type info flash address
#define HW_TYPE_FLASH_ADDRESS             APP_FLASH_BASE_ADDRESS

#endif
