
/**
 * @file
 *
 * Unit that manages the computation of the system delay /Unit that provides library functions for computation of delay.
 * fixme update the comment.
 * 
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */

#include "Std.h"
//#include "MCU.h"
#include "RTIDriver.h"
//#include "Timer2.h"
#include "SysTimer.h"
#include "rti.h"



/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticDefines Static defines
 */


 
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */
/**
 *
 */
typedef struct SysTimer_TimerDescriptor_tag
{
    uint64 startValue;
    uint16 timeOutValue;
    SysTimer_ModeType timerMode;
    SysTimer_ReturnType timerState;
} SysTimer_TimerDescriptor;

 /** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */



/** @} */
/* ================================================================================================================== */
 
/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */ 

//static volatile uint64 SysTimer_MainCounter;
/**
 *
 */
uint64 SysTimer_MainCounter;
/**
 *
 */
static SysTimer_TimerDescriptor SysTimer_TimerSlots[SYS_TIMER_SLOT_NUMBER];

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */ 



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */ 
/** @fn Std_ReturnType SysTimer_Init( void )
* @brief SysTimer init function, it also calls init funtions of the underlying Timer1 and Timer2 drivers.
* @param none
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType SysTimer_Init( void )
{
    uint16 idx;

    SysTimer_MainCounter = 0;
    
    for ( idx = 0; idx < SYS_TIMER_SLOT_NUMBER; idx++ )
    {
        SysTimer_TimerSlots[idx].startValue = 0;
        SysTimer_TimerSlots[idx].timeOutValue = 0;
        SysTimer_TimerSlots[idx].timerMode = SYS_TIMER_ONE_SHOT;
        SysTimer_TimerSlots[idx].timerState = SYS_TIMER_IS_STOPPED;
    }
    
    return (0);
};

/** @fn Std_ReturnType SysTimer_SetTimePeriod( uint16 timerSlot, uint16 timerPeriod )
* @brief Sets time-out period of the given timer slot.
* @param [in] timeSlot: Identifier of the timer slot to be set
* @param [in] timerPeriod: Time-out value in ms.
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType SysTimer_SetTimePeriod( uint16 timerSlot, uint16 timerPeriod )
{
    SysTimer_TimerSlots[timerSlot].timeOutValue = timerPeriod;
    return 0;
};

/** @fn Std_ReturnType SysTimer_StartTimer( uint16 timerSlot, SysTimer_ModeType timerMode )
* @brief Starts the given timer.
* @param [in] timeSlot: Identifier of the timer slot to be started
* @param [in] timerMode: One shot or free running operation.
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType SysTimer_StartTimer( uint16 timerSlot, SysTimer_ModeType timerMode )
{
    SysTimer_TimerSlots[timerSlot].startValue = SysTimer_MainCounter;
    SysTimer_TimerSlots[timerSlot].timerState = SYS_TIMER_IS_RUNNING;
    SysTimer_TimerSlots[timerSlot].timerMode = timerMode;
    return 0;
};

/** @fn Std_ReturnType SysTimer_StopTimer( uint16 timerSlot )
* @brief Stops the given timer.
* @param [in] timeSlot: Identifier of the timer slot to be stopped.
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType SysTimer_StopTimer( uint16 timerSlot )
{
    SysTimer_TimerSlots[timerSlot].timerState = SYS_TIMER_IS_STOPPED;
    return 0;
};

/** @fn SysTimer_ReturnType SysTimer_GetTimerStatus( uint16 timerSlot )
* @brief Evaluate the state of the given timer slot, then returns it's state.
* @param [in] timeSlot: Identifier of the timer slot to be checked.
* @return In case of any any error, it returns the error code (not implemented yet)
* @note In free running mode, it also sets the state of the timer slot to "RUNNING".
*/
SysTimer_ReturnType SysTimer_GetTimerStatus( uint16 timerSlot )
{
    uint64 currentTime;
    SysTimer_ReturnType retVal;
    
    currentTime = SysTimer_MainCounter;

    if ( SYS_TIMER_IS_RUNNING == SysTimer_TimerSlots[timerSlot].timerState )
    {
        if ( SysTimer_TimerSlots[timerSlot].startValue +
             SysTimer_TimerSlots[timerSlot].timeOutValue  <= currentTime )
        {
            if ( SYS_TIMER_FREE_RUNNING == SysTimer_TimerSlots[timerSlot].timerMode )
            {
                SysTimer_TimerSlots[timerSlot].timerState = SYS_TIMER_IS_RUNNING;
                SysTimer_TimerSlots[timerSlot].startValue = currentTime;
                retVal = SYS_TIMER_IS_EXPIRED;
            }
            else
            {
                SysTimer_TimerSlots[timerSlot].timerState = SYS_TIMER_IS_EXPIRED;
                retVal = SYS_TIMER_IS_EXPIRED;
            }
        }
        else
        {
            retVal = SYS_TIMER_IS_RUNNING;
        }
    }
    else
    {
        if ( SYS_TIMER_IS_EXPIRED == SysTimer_TimerSlots[timerSlot].timerState )
        {
             retVal = SYS_TIMER_IS_EXPIRED;
        }
        else
        {
            retVal = SYS_TIMER_IS_STOPPED;
        }
    }
    
    return retVal;
};

uint32 SysTimer_GetSysTimerValue(void)
{
    return (uint32)SysTimer_MainCounter;
}
/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */ 

//void __attribute__ ( ( interrupt, no_auto_psv ) ) _T1Interrupt (  )
//{
//    /* Check if the Timer Interrupt/Status is set */
//
//    //***User Area Begin
//
//    // ticker function call;
//    // ticker is 1 -> Callback function gets called everytime this ISR executes
//    //TMR1_CallBack();
//    SysTimer_MainCounter++;
//
//    //***User Area End
//    IFS0bits.T1IF = FALSE;
//}

