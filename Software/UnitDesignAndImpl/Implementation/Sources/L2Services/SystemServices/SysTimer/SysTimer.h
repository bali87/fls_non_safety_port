#ifndef SYS_TIMER_H
#define SYS_TIMER_H

/**
 * @file
 * 
 * Header file of the unit ${FILENAME}. //fixme remove .h
 *
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */

//#include "Timer2.h"

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */

#define SYS_TIMER_SLOT_NUMBER 10u
#define SYS_TIMER_MAINCOUNTER SysTimerMainCounter;

#define SYS_TIMER_MAIN_TIMER 0u
#define SYS_TIMER_SYNC_TIME_OUT 1u
#define SYS_TIMER_COM_TIMEOUT 2u

#define SYS_TIMER_MAIN_CYCLE_TIME 8u
//#define SYS_TIMER_MAIN_CYCLE_TIME 20u

#define SYS_TIMER_GET_CORE_TIME() Timer2_GetValue()

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */

typedef enum SysTimer_ReturnType_tag
{
    SYS_TIMER_IS_RUNNING    = 1u,
    SYS_TIMER_IS_STOPPED    = 2u,
    SYS_TIMER_IS_EXPIRED    = 3u
} SysTimer_ReturnType;


typedef enum SysTimer_TimerMode_tag
{
    SYS_TIMER_ONE_SHOT      = 1u,
    SYS_TIMER_FREE_RUNNING  = 2u,
} SysTimer_ModeType;

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */
 



/**
 * @addtogroup GlobalVaribles Global variables
 */
 
extern uint64 SysTimer_MainCounter;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */
/**
 *
 * @return
 */
Std_ReturnType SysTimer_Init( void );
/**
 *
 * @param timerSlot
 * @param timerPeriod
 * @return
 */
Std_ReturnType SysTimer_SetTimePeriod( uint16 timerSlot, uint16 timerPeriod );
/**
 *
 * @param timerSlot
 * @param timerMode
 * @return
 */
Std_ReturnType SysTimer_StartTimer( uint16 timerSlot, SysTimer_ModeType timerMode );
/**
 *
 * @param timerSlot
 * @return
 */
Std_ReturnType SysTimer_StopTimer( uint16 timerSlot );
/**
 *
 * @param timerSlot
 * @return
 */
SysTimer_ReturnType SysTimer_GetTimerStatus( uint16 timerSlot );
/**
 *
 * @return
 */
uint32 SysTimer_GetSysTimerValue(void);
/** @} */

#endif /* SYS_TIMER_H */
