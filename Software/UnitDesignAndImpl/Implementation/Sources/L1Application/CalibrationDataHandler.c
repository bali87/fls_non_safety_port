
/**
 * @file
 *
 * Unit that manages the computation of the system delay /Unit that provides library functions for computation of delay.
 * fixme update the comment.
 *
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */

#include "Std.h"
#include "MyFloat.h"
#include "Board.h"
#include "CalibrationDataHandler.h"
#include "ComManager.h"
//#include "RangeMeasurement.h"
//#include "FlashEeprom.h"

//#define LOAD_FROM_FLASH

#ifdef LOAD_FROM_FLASH
#include "MemIf.h"
static RangeData rangeData;

#else

#define _stringify(t) #t
#define _expand(t) _stringify(t)
#include "Gain.h"
#include "Range.h"
#include "PCA.h"
#include "Temp.h"
#if 1//GAIN_COMPENSATION == AVAILABLE
	//#include _expand(../../CalibrationData/SENSOR_SERIAL/SENSOR_SERIAL-gain.h)
//#include "HEXSA012-gain.h"
#endif

#if 1//TEMP_COMPENSATION == AVAILABLE
	//#include _expand(../../CalibrationData/HEXSA012/HEXSA012-temp.h)
//#include "HEXSA012-temp.h"
#endif

#if 1//RANGE_FILE == AVAILABLE
    //#include _expand(../../CalibrationData/SENSOR_SERIAL/SENSOR_SERIAL-range.h)

#endif

#if 1//PCA_CALIBRATION == AVAILABLE
    //#include _expand(../../CalibrationData/SENSOR_SERIAL/SENSOR_SERIAL-pca.h)


#endif

#endif
static uint8 calibrated=1u;
/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticDefines Static defines
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */


Std_ReturnType CalibDataHandler_GetOffsetCompParams( CalibDataHandler_CalibrationDataType *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
#ifndef LOAD_FROM_FLASH


    uint16 idx ;
    static float64 tempFloat;
    static MyFloat tempMyFloat;

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_MEMBERS_COUNT; idx++ )
    {
        tempFloat = ( float64 )( b1[ idx + 1 ] );
        MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
        dataPtr->offsetCompParams[ 0 ][ idx ].coeff = tempMyFloat.coeff;
        dataPtr->offsetCompParams[ 0 ][ idx ].expo = tempMyFloat.expo;
    }

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_MEMBERS_COUNT; idx++ )
    {
        tempFloat = ( float64 )( b2[ idx + 1 ] );
        MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
        dataPtr->offsetCompParams[ 1 ][ idx ].coeff = tempMyFloat.coeff;
        dataPtr->offsetCompParams[ 1 ][ idx ].expo = tempMyFloat.expo;
    }

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_MEMBERS_COUNT; idx++ )
    {
        tempFloat = ( float64 )( b3[ idx + 1 ] );
        MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
        dataPtr->offsetCompParams[ 2 ][ idx ].coeff = tempMyFloat.coeff;
        dataPtr->offsetCompParams[ 2 ][ idx ].expo = tempMyFloat.expo;
    }

    retVal = E_OK;
#endif

    return (retVal);
};

Std_ReturnType CalibDataHandler_GetOffsetCompParams_f( CalibDataHandler_CalibrationDataType_f *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
#ifndef LOAD_FROM_FLASH

    uint16 idx;

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_MEMBERS_COUNT; idx++ )
    {
        dataPtr->offsetCompParams[ 0 ][ idx ] =  b1[ idx + 1 ];
    }

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_MEMBERS_COUNT; idx++ )
    {
        dataPtr->offsetCompParams[ 1 ][ idx ] = b2[ idx + 1 ];
    }

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_MEMBERS_COUNT; idx++ )
    {
        dataPtr->offsetCompParams[ 2 ][ idx ] = b3[ idx + 1 ];
    }

    retVal = E_OK;
#endif

    return (retVal);
};


Std_ReturnType CalibDataHandler_GetGainCompParams( CalibDataHandler_CalibrationDataType *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
#ifndef LOAD_FROM_FLASH

    uint16 idx;
    static float64 tempFloat;
    static MyFloat tempMyFloat;

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
    {
        tempFloat = ( float64 )( g1[ idx + 1 ] );
        MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
        dataPtr->gainCompParams[ 0 ][ idx ].coeff = tempMyFloat.coeff;
        dataPtr->gainCompParams[ 0 ][ idx ].expo = tempMyFloat.expo;
    }

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
    {
        tempFloat = ( float64 )( g2[ idx + 1 ] );
        MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
        dataPtr->gainCompParams[ 1 ][ idx ].coeff = tempMyFloat.coeff;
        dataPtr->gainCompParams[ 1 ][ idx ].expo = tempMyFloat.expo;
    }

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
    {
        tempFloat = ( float64 )( g3[ idx + 1 ] );
        MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
        dataPtr->gainCompParams[ 2 ][ idx ].coeff = tempMyFloat.coeff;
        dataPtr->gainCompParams[ 2 ][ idx ].expo = tempMyFloat.expo;
    }

    retVal = E_OK;
#endif

    return (retVal);
};

Std_ReturnType CalibDataHandler_GetGainCompParams_f( CalibDataHandler_CalibrationDataType_f *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
#ifndef LOAD_FROM_FLASH

    uint16 idx;

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
    {
        dataPtr->gainCompParams[ 0 ][ idx ] = ( float32 )( g1[ idx + 1 ] );
    }

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
    {
        dataPtr->gainCompParams[ 1 ][ idx ] = ( float32 )( g2[ idx + 1 ] );
    }

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
    {
        dataPtr->gainCompParams[ 2 ][ idx ] = ( float32 )( g3[ idx + 1 ] );
    }

    retVal = E_OK;
#endif

    return (retVal);
};


Std_ReturnType CalibDataHandler_GetTempCompParams( CalibDataHandler_CalibrationDataType *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
#ifndef LOAD_FROM_FLASH

    uint16 idx, subIdx;
    static float64 tempFloat;
    static MyFloat tempMyFloat;

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
    {
        for ( subIdx = 0; subIdx < 3; subIdx++)
        {
            tempFloat = ( float64 )( p1[ idx ][ subIdx ] );
            MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
            dataPtr->tempCompParams[ 0 ][ idx ][ subIdx ].coeff = tempMyFloat.coeff;
            dataPtr->tempCompParams[ 0 ][ idx ][ subIdx ].expo = tempMyFloat.expo;
        }
    }

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
    {
        for ( subIdx = 0; subIdx < 3; subIdx++)
        {
            tempFloat = ( float64 )( p2[ idx ][ subIdx ] );
            MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
            dataPtr->tempCompParams[ 1 ][ idx ][ subIdx ].coeff = tempMyFloat.coeff;
            dataPtr->tempCompParams[ 1 ][ idx ][ subIdx ].expo = tempMyFloat.expo;
        }
    }

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
    {
        for ( subIdx = 0; subIdx < 3; subIdx++)
        {
            tempFloat = ( float64 )( p3[ idx ][ subIdx ] );
            MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
            dataPtr->tempCompParams[ 2 ][ idx ][ subIdx ].coeff = tempMyFloat.coeff;
            dataPtr->tempCompParams[ 2 ][ idx ][ subIdx ].expo = tempMyFloat.expo;
        }
    }

    retVal = E_OK;
#endif

    return (retVal);
};

Std_ReturnType CalibDataHandler_GetTempCompParams_f( CalibDataHandler_CalibrationDataType_f *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
#ifndef LOAD_FROM_FLASH

    uint16 idx, subIdx;

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
    {
        for ( subIdx = 0; subIdx < 3; subIdx++)
        {
            dataPtr->tempCompParams[ 0 ][ idx ][ subIdx ] = ( float32 )( p1[ idx ][ subIdx ] );
        }
    }

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
    {
        for ( subIdx = 0; subIdx < 3; subIdx++)
        {
            dataPtr->tempCompParams[ 1 ][ idx ][ subIdx ] = ( float32 )( p2[ idx ][ subIdx ] );
        }
    }

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
    {
        for ( subIdx = 0; subIdx < 3; subIdx++)
        {
            dataPtr->tempCompParams[ 2 ][ idx ][ subIdx ] = ( float32 )( p3[ idx ][ subIdx ] );
        }
    }

    retVal = E_OK;
#endif

    return (retVal);
};


Std_ReturnType CalibDataHandler_GetSensorLimits( CalibDataHandler_CalibrationDataType *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
#ifndef LOAD_FROM_FLASH

    uint16 idx;
    static float64 tempFloat;
    static MyFloat tempMyFloat;

    for ( idx = 0; idx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; idx++ )
    {
        tempFloat = ( float64 )( CTS_P[ idx ] );
        MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
        dataPtr->sensorLimits.NominalCapacityPosCounts[ idx ].coeff = tempMyFloat.coeff;
        dataPtr->sensorLimits.NominalCapacityPosCounts[ idx ].expo = tempMyFloat.expo;
    }

    for ( idx = 0; idx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; idx++ )
    {
        tempFloat = ( float64 )( CTS_N[ idx ] );
        MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
        dataPtr->sensorLimits.NominalCapacityNegCounts[ idx ].coeff = tempMyFloat.coeff;
        dataPtr->sensorLimits.NominalCapacityNegCounts[ idx ].expo = tempMyFloat.expo;
    }

    for ( idx = 0; idx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; idx++ )
    {
        tempFloat = ( float64 )( NC_P[ idx ] );
        MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
        dataPtr->sensorLimits.NominalCapacityPosUnits[ idx ].coeff = tempMyFloat.coeff;
        dataPtr->sensorLimits.NominalCapacityPosUnits[ idx ].expo = tempMyFloat.expo;
    }

    for ( idx = 0; idx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; idx++ )
    {
        tempFloat = ( float64 )( NC_N[ idx ] );
        MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
        dataPtr->sensorLimits.NominalCapacityNegUnits[ idx ].coeff = tempMyFloat.coeff;
        dataPtr->sensorLimits.NominalCapacityNegUnits[ idx ].expo = tempMyFloat.expo;
    }

    retVal = E_OK;
#endif

    return (retVal);
};

Std_ReturnType CalibDataHandler_GetSensorLimits_f( CalibDataHandler_CalibrationDataType_f *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
#ifndef LOAD_FROM_FLASH

    uint16 idx;

    for ( idx = 0; idx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; idx++ )
    {
        dataPtr->sensorLimits.NominalCapacityPosCounts[ idx ] = CTS_P[ idx ];
    }

    for ( idx = 0; idx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; idx++ )
    {
        dataPtr->sensorLimits.NominalCapacityNegCounts[ idx ] = CTS_N[ idx ];
    }

    for ( idx = 0; idx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; idx++ )
    {
        dataPtr->sensorLimits.NominalCapacityPosUnits[ idx ] = NC_P[ idx ];
    }

    for ( idx = 0; idx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; idx++ )
    {
        dataPtr->sensorLimits.NominalCapacityNegUnits[ idx ] = NC_P[ idx ];
    }

    retVal = E_OK;
#endif

    return (retVal);
};


Std_ReturnType CalibDataHandler_GetPcaMatrix( CalibDataHandler_PcaDataType *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
#ifndef LOAD_FROM_FLASH

    uint16 idx, subIdx;
    static float64 tempFloat;
    static MyFloat tempMyFloat;

    for ( idx = 0; idx < CALIB_DATA_HANDLER_PCA_VIRTUAL_SIGNAL_COUNT; idx++ )
    {
        for ( subIdx = 0; subIdx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; subIdx++)
        {
            tempFloat = ( float64 )( pcamatrix[ idx ][ subIdx ] );
            MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
            dataPtr->pcaMatrix[ idx ][ subIdx ].coeff = tempMyFloat.coeff;
            dataPtr->pcaMatrix[ idx ][ subIdx ].expo = tempMyFloat.expo;
        }
    }

    retVal = E_OK;
#endif

    return (retVal);
};

Std_ReturnType CalibDataHandler_GetPcaMatrix_f( CalibDataHandler_PcaDataType_f *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
#ifndef LOAD_FROM_FLASH

    uint16 idx, subIdx;

    for ( idx = 0; idx < CALIB_DATA_HANDLER_PCA_VIRTUAL_SIGNAL_COUNT; idx++ )
    {
        for ( subIdx = 0; subIdx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; subIdx++)
        {
            dataPtr->pcaMatrix[ idx ][ subIdx ] = ( float32 )( pcamatrix[ idx ][ subIdx ] );
        }
    }

    retVal = E_OK;
#endif

    return (retVal);
};


Std_ReturnType CalibDataHandler_GetPcaOffset( CalibDataHandler_PcaDataType *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
#ifndef LOAD_FROM_FLASH

    uint16 idx;
    static float64 tempFloat;
    static MyFloat tempMyFloat;

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_COUNT * BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
    {
        tempFloat = ( float64 )( pcaoffset[ idx ] );
        MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
        dataPtr->pcaOffset[ idx ].coeff = tempMyFloat.coeff;
        dataPtr->pcaOffset[ idx ].expo = tempMyFloat.expo;
        //        dataPtr->pcaOffset[ idx ] = ( sint32 )(( float64 )( pcaoffset[ idx ] ) * 8192 );
    }

    retVal = E_OK;
#endif

    return (retVal);
};

Std_ReturnType CalibDataHandler_GetPcaOffset_f( CalibDataHandler_PcaDataType_f *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
#ifndef LOAD_FROM_FLASH

    uint16 idx;

    for ( idx = 0; idx < BOARD_SIGNAL_GROUP_COUNT * BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
    {
        dataPtr->pcaOffset[ idx ] = ( float32 )( pcaoffset[ idx ] );
    }

    retVal = E_OK;
#endif

    return (retVal);
};


sint16 CalibDataHandler_GetPosCount( uint8 id )
{
    int16_t tmp;
#ifdef LOAD_FROM_FLASH

    tmp=rangeData.CTS_P[id];
#else

    tmp=CTS_P[id];
#endif

    if (tmp==0)
        tmp=1;
    return (tmp);
}

sint16 CalibDataHandler_GetNegCount( uint8 id )
{
    int16_t tmp;
#ifdef LOAD_FROM_FLASH

    tmp=rangeData.CTS_N[id];
#else

    tmp=CTS_N[id];
#endif

    if (tmp==0)
        tmp=-1;
    return (tmp);

}

sint16 CalibDataHandler_GetPosNomCap( uint8 id )
{
    int16_t tmp;
#ifdef LOAD_FROM_FLASH

    tmp=rangeData.NC_P[id];
#else

    tmp=NC_P[id];
#endif

    if (tmp==0)
        tmp=1;
    return (tmp);
}

sint16 CalibDataHandler_GetNegNomCap( uint8 id )
{
    int16_t tmp;
#ifdef LOAD_FROM_FLASH

    tmp=rangeData.NC_N[id];
#else

    tmp=NC_N[id];
#endif

    if (tmp==0)
        tmp=1;
    return (tmp);
}


Std_ReturnType CalibDataHandler_LoadData_f( CalibDataHandler_CalibrationDataType_f *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;

#ifdef LOAD_FROM_FLASH

    uint8 idx,subIdx;
    uint16 grpIdx;
    FlashDataCRC d;
    if (MemIf_LoadNVData(&d)==0)
    {
        /* LOAD RANGE*/
        for ( idx = 0; idx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; idx++ )
        {
            dataPtr->sensorLimits.NominalCapacityPosCounts[ idx ] = d.data.range.CTS_P[ idx ];
            rangeData.CTS_P[idx]=d.data.range.CTS_P[idx];
        }

        for ( idx = 0; idx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; idx++ )
        {
            dataPtr->sensorLimits.NominalCapacityNegCounts[ idx ] = ( float64 )( d.data.range.CTS_N[ idx ] );
            rangeData.CTS_N[idx]=d.data.range.CTS_N[idx];
        }

        for ( idx = 0; idx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; idx++ )
        {
            dataPtr->sensorLimits.NominalCapacityPosUnits[ idx ] = ( float64 )( d.data.range.NC_P[ idx ] );
            rangeData.NC_P[idx]=d.data.range.NC_P[idx];
        }

        for ( idx = 0; idx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; idx++ )
        {
            dataPtr->sensorLimits.NominalCapacityNegUnits[ idx ]  = ( float64 )( d.data.range.NC_N[ idx ] );
            rangeData.NC_N[idx]=d.data.range.NC_N[idx];
        }

        /* TEMPCOMP */
        for ( grpIdx=0; grpIdx < BOARD_SIGNAL_GROUP_COUNT; grpIdx++ )
        {
            for ( idx = 0; idx < BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
            {
                for ( subIdx = 0; subIdx < 3; subIdx++)
                {
                    dataPtr->tempCompParams[ grpIdx ][ idx ][ subIdx ] = ( float64 )( d.data.tempComp[ grpIdx ][ idx ][ subIdx ] );
                }
            }
        }

        /* GAIN */
        for ( grpIdx=0; grpIdx < BOARD_SIGNAL_GROUP_COUNT; grpIdx++ )
        {
            for ( idx = 0; idx < BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
            {
                dataPtr->gainCompParams[ grpIdx ][ idx ] = ( float64 )( d.data.gain[ grpIdx ][ idx ] );
            }
        }
        /* OFFSET */
        for ( grpIdx=0; grpIdx < BOARD_SIGNAL_GROUP_COUNT; grpIdx++ )
        {
            for ( idx = 0; idx < BOARD_SIGNAL_GROUP_MEMBERS_COUNT; idx++ )
            {
                dataPtr->offsetCompParams[ grpIdx ][ idx ] = d.data.bias[ grpIdx ][ idx ];
            }
        }
    }
    retVal = E_OK;
#endif

    return (retVal);
}


Std_ReturnType CalibDataHandler_LoadPCA( CalibDataHandler_PcaDataType *pcaPtr )
{
    Std_ReturnType retVal = E_NOT_OK;

#ifdef LOAD_FROM_FLASH

    uint8 idx,subIdx;
    static float64 tempFloat;
    static MyFloat tempMyFloat;
    FlashDataCRC d;
    if (MemIf_LoadNVData(&d)==0)
    {
        /* PCA MATRIX */
        for ( idx = 0; idx < CALIB_DATA_HANDLER_PCA_VIRTUAL_SIGNAL_COUNT; idx++ )
        {
            for ( subIdx = 0; subIdx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; subIdx++)
            {
                tempFloat = ( float64 )( d.data.pca.matrix[idx][subIdx] );
                MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
                pcaPtr->pcaMatrix[ idx ][ subIdx ].coeff = tempMyFloat.coeff;
                pcaPtr->pcaMatrix[ idx ][ subIdx ].expo = tempMyFloat.expo;
            }
        }

        /*PCA OFFSET*/
        for ( idx = 0; idx < BOARD_SIGNAL_GROUP_COUNT * BOARD_SIGNAL_GROUP_PD_COUNT; idx++ )
        {
            tempFloat = ( float64 )( d.data.pca.offset[ idx ] );
            MyFloat_Float64ToMyFloat( &tempFloat, &tempMyFloat, 1u );
            pcaPtr->pcaOffset[ idx ].coeff = tempMyFloat.coeff;
            pcaPtr->pcaOffset[ idx ].expo = tempMyFloat.expo;
        }

    }
    retVal = E_OK;
#endif

    return (retVal);
}

void SensorSerial_Init(void)
{
#ifdef LOAD_FROM_FLASH
    uint8 i;
    FlashDataCRC d;
    if (MemIf_LoadNVData(&d)==0)
    {
        extern uint8 SerialString[8];
        for (i=0;i<8;i++)
        {
            SerialString[i]=d.header.sensor_name[i];
        }

        //ComManager_SetCompMode( (d.header.sensor_calibrated==1)? 7:0 );
        calibrated = d.header.sensor_calibrated;
        RangeMeasurement_SetRangingOffset(d.header.proxy_sensor_offset);
    }

    uint16_t poc;
#define POC_ADDRESS                       (FLASHDATA_ADDRESS+0x3000)     // 2 bytes

    FlashEeprom_Read(POC_ADDRESS,&poc,2,0);
    poc++;
    ComManager_SetPOC(poc);
    FlashEeprom_Write(POC_ADDRESS,&poc,2,0);
    FlashEeprom_Flush();

#endif

}
/**
 *
 * @return
 */
uint8 CalibrationDataHandler_IsCalibrated(void)
{
    return(calibrated);
}

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */


