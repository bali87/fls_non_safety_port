#ifndef FT_CALC_PCA_H
#define FT_CALC_PCA_H

#include "CalibrationDataHandler.h"
#if 1
/**
 * @file
 * 
 * Header file of the unit ${FILENAME}. //fixme remove .h
 *
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */

typedef struct FTCalcPca_FTValuesType_tag
{
    sint16 ForceValues[ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT / 2 ];
    sint16 TorqueValues[ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT / 2 ];
} FTCalcPca_FTValuesType;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */
 



/**
 * @addtogroup GlobalVaribles Global variables
 */
 


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */
/** @fn Std_ReturnType FTCalcPca_Init( void )
 * @brief
 * @param
 * @return In case of any any error, it returns the error code (not implemented yet)
 */
Std_ReturnType FTCalcPca_Init( void );
/** @fn Std_ReturnType FTCalcPca_Calculate( void )
 * @brief
 * @param [in]
 * @param [in]
 * @return In case of any any error, it returns the error code (not implemented yet)
 */
Std_ReturnType FTCalcPca_Calculate( void );

Std_ReturnType FTCalcPca_Calculate_f( void );
/** @fn Std_ReturnType FTCalcPca_GetFTValues( FTPcaCalc_FTValuesType *dataPtr )
 * @brief
 * @param [in]
 * @param [in]
 * @return In case of any any error, it returns the error code (not implemented yet)
 */
Std_ReturnType FTCalcPca_GetFTValues( FTCalcPca_FTValuesType *dataPtr );

Std_ReturnType FTCalcPca_GetFTData( uint8 *dataPtr );

Std_ReturnType FTCalcPca_GetFTValues_f( FTCalcPca_FTValuesType *dataPtr );

Std_ReturnType FTCalcPca_GetFTData_f( uint8 *dataPtr );

Std_ReturnType FTCalcPca_GetFTDataDiff( uint8 *dataPtr );

Std_ReturnType FTCalcPca_Zero( void);

Std_ReturnType FTCalcPca_Unzero( void);

uint8 FTCalcPca_GetZeroStatus (void);

uint8 FTCalcPca_GetNegOverload(void);

uint8 FTCalcPca_GetPosOverload(void);
/** @} */
#endif
#endif /* FT_CALC_PCAG_H */
