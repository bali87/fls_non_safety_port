
/**
 * @file
 *
 * Unit that manages the computation of the system delay /Unit that provides library functions for computation of delay.
 * fixme update the comment.
 *
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */

#include <math.h>
#include "Std.h"
#include "MyFloat.h"
//#include "MCU.h"
#include "Board.h"
#include "CalibrationDataHandler.h"
#include "SignalProcessing.h"
#include "FTCalcPca.h"


//#if (${HEADER_FILENAME})
//#[[#include]]# "${HEADER_FILENAME}"
//#end


/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticDefines Static defines
 */

#define S1_1      ( FTCalcPca_InputSignals[ 0 ].pdArray[ 0 ] )
#define S1_2      ( FTCalcPca_InputSignals[ 0 ].pdArray[ 1 ] )
#define S1_3      ( FTCalcPca_InputSignals[ 0 ].pdArray[ 2 ] )
#define S1_4      ( FTCalcPca_InputSignals[ 0 ].pdArray[ 3 ] )
#define S2_1      ( FTCalcPca_InputSignals[ 1 ].pdArray[ 0 ] )
#define S2_2      ( FTCalcPca_InputSignals[ 1 ].pdArray[ 1 ] )
#define S2_3      ( FTCalcPca_InputSignals[ 1 ].pdArray[ 2 ] )
#define S2_4      ( FTCalcPca_InputSignals[ 1 ].pdArray[ 3 ] )
#define S3_1      ( FTCalcPca_InputSignals[ 2 ].pdArray[ 0 ] )
#define S3_2      ( FTCalcPca_InputSignals[ 2 ].pdArray[ 1 ] )
#define S3_3      ( FTCalcPca_InputSignals[ 2 ].pdArray[ 2 ] )
#define S3_4      ( FTCalcPca_InputSignals[ 2 ].pdArray[ 3 ] )

#define S1_1_f    ( FTCalcPca_InputSignals_f[ 0 ].pdArray[ 0 ] )
#define S1_2_f    ( FTCalcPca_InputSignals_f[ 0 ].pdArray[ 1 ] )
#define S1_3_f    ( FTCalcPca_InputSignals_f[ 0 ].pdArray[ 2 ] )
#define S1_4_f    ( FTCalcPca_InputSignals_f[ 0 ].pdArray[ 3 ] )
#define S2_1_f    ( FTCalcPca_InputSignals_f[ 1 ].pdArray[ 0 ] )
#define S2_2_f    ( FTCalcPca_InputSignals_f[ 1 ].pdArray[ 1 ] )
#define S2_3_f    ( FTCalcPca_InputSignals_f[ 1 ].pdArray[ 2 ] )
#define S2_4_f    ( FTCalcPca_InputSignals_f[ 1 ].pdArray[ 3 ] )
#define S3_1_f    ( FTCalcPca_InputSignals_f[ 2 ].pdArray[ 0 ] )
#define S3_2_f    ( FTCalcPca_InputSignals_f[ 2 ].pdArray[ 1 ] )
#define S3_3_f    ( FTCalcPca_InputSignals_f[ 2 ].pdArray[ 2 ] )
#define S3_4_f    ( FTCalcPca_InputSignals_f[ 2 ].pdArray[ 3 ] )


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */

//static float32 sumVirtualSignals_f[ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT ];
static MyFloat sumVirtualSignals[ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT ];
static MyFloat tempMyFloat_pca;


static SignalProcessing_SignalGroupType FTCalcPca_InputSignals[ BOARD_SIGNAL_GROUP_COUNT ];
//static SignalProcessing_SignalGroupType_f FTCalcPca_InputSignals_f[ BOARD_SIGNAL_GROUP_COUNT ];

static CalibDataHandler_PcaDataType FTCalcPca_PcaData;
//static CalibDataHandler_PcaDataType_f FTCalcPca_PcaData_f;

static MyFloat FTCalcPca_VirtualSignals[ CALIB_DATA_HANDLER_PCA_VIRTUAL_SIGNAL_COUNT ];
//static float32 FTCalcPca_VirtualSignals_f[ CALIB_DATA_HANDLER_PCA_VIRTUAL_SIGNAL_COUNT ];

static FTCalcPca_FTValuesType FTCalcPca_FTValues;
//static FTCalcPca_FTValuesType FTCalcPca_FTValues_f;
static FTCalcPca_FTValuesType FTCalcPca_FTZeroValues;
static uint8_t FTCalcPca_ZeroStatus;
static uint8 FTCalcPca_PosOverload;
static uint8 FTCalcPca_NegOverload;
static float FtCalcPca_ForcePosOverloadLimit[3];
static float FtCalcPca_ForceNegOverloadLimit[3];
static float FtCalcPca_TorquePosOverloadLimit[3];
static float FtCalcPca_TorqueNegOverloadLimit[3];
static float FtCalcPca_ForcePostMultiplier[3];
static float FtCalcPca_TorquePostMultiplier[3];

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */
/** @fn static Std_ReturnType FTCalcPca_PcaOffsetComp( void )
 * @brief
 * @param [in]
 * @param [in]
 * @return In case of any any error, it returns the error code (not implemented yet)
 */
static Std_ReturnType FTCalcPca_PcaOffsetComp (void);
/** @fn static Std_ReturnType FTCalcPca_PcaMatrixCalc( void )
 * @brief
 * @param [in]
 * @param [in]
 * @return In case of any any error, it returns the error code (not implemented yet)
 */
static Std_ReturnType FTCalcPca_CalcPcaMatrix (void);
static Std_ReturnType FTCalcPca_CheckOverload (void);
//static Std_ReturnType FTCalcPca_PcaOffsetComp_f( void );
//static Std_ReturnType FTCalcPca_CalcPcaMatrix_f( void );


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */


Std_ReturnType FTCalcPca_Init (void)
{
    Std_ReturnType retVal = E_NOT_OK;
	
	if (E_NOT_OK == CalibDataHandler_LoadPCA(&FTCalcPca_PcaData))
	{
	    CalibDataHandler_GetPcaMatrix (&FTCalcPca_PcaData);
	    CalibDataHandler_GetPcaOffset (&FTCalcPca_PcaData);
	}

#if 0
    CalibDataHandler_GetPcaMatrix_f (&FTCalcPca_PcaData_f);
    CalibDataHandler_GetPcaOffset_f (&FTCalcPca_PcaData_f);
#endif

    FTCalcPca_FTZeroValues.ForceValues[0] = 0;
	FTCalcPca_FTZeroValues.ForceValues[1] = 0;
	FTCalcPca_FTZeroValues.ForceValues[2] = 0;
    FTCalcPca_FTZeroValues.TorqueValues[0] = 0; 
	FTCalcPca_FTZeroValues.TorqueValues[1] = 0;
	FTCalcPca_FTZeroValues.TorqueValues[2] = 0;
	FTCalcPca_ZeroStatus = 0u;

    FtCalcPca_ForcePosOverloadLimit[0] = (float) CalibDataHandler_GetPosCount (0) * 1.5f;
    FtCalcPca_ForcePosOverloadLimit[1] = (float) CalibDataHandler_GetPosCount (1) * 1.5f;
    FtCalcPca_ForcePosOverloadLimit[2] = (float) CalibDataHandler_GetPosCount (2) * 1.5f;

    FtCalcPca_ForceNegOverloadLimit[0] = (float) CalibDataHandler_GetNegCount (0) * 1.5f;
    FtCalcPca_ForceNegOverloadLimit[1] = (float) CalibDataHandler_GetNegCount (1) * 1.5f;
    FtCalcPca_ForceNegOverloadLimit[2] = (float) CalibDataHandler_GetNegCount (2) * 1.5f;

    FtCalcPca_TorquePosOverloadLimit[0] = (float) CalibDataHandler_GetPosCount (3) * 1.5f;
    FtCalcPca_TorquePosOverloadLimit[1] = (float) CalibDataHandler_GetPosCount (4) * 1.5f;
    FtCalcPca_TorquePosOverloadLimit[2] = (float) CalibDataHandler_GetPosCount (5) * 1.5f;

    FtCalcPca_TorqueNegOverloadLimit[0] = (float) CalibDataHandler_GetNegCount (3) * 1.5f;
    FtCalcPca_TorqueNegOverloadLimit[1] = (float) CalibDataHandler_GetNegCount (4) * 1.5f;
    FtCalcPca_TorqueNegOverloadLimit[2] = (float) CalibDataHandler_GetNegCount (5) * 1.5f;

    FtCalcPca_ForcePostMultiplier[0] = (float) CalibDataHandler_GetPosCount (0) / (float) CalibDataHandler_GetPosNomCap (0);
    FtCalcPca_ForcePostMultiplier[1] = (float) CalibDataHandler_GetPosCount (1) / (float) CalibDataHandler_GetPosNomCap (1);
    FtCalcPca_ForcePostMultiplier[2] = (float) CalibDataHandler_GetPosCount (2) / (float) CalibDataHandler_GetPosNomCap (2);

    FtCalcPca_TorquePostMultiplier[0] = (float) ((float) CalibDataHandler_GetPosCount (3)*1000.0f) / (float) CalibDataHandler_GetPosNomCap (3);
    FtCalcPca_TorquePostMultiplier[1] = (float) ((float) CalibDataHandler_GetPosCount (4)*1000.0f) / (float) CalibDataHandler_GetPosNomCap (4);
    FtCalcPca_TorquePostMultiplier[2] = (float) ((float) CalibDataHandler_GetPosCount (5)*1000.0f) / (float) CalibDataHandler_GetPosNomCap (5);
    FTCalcPca_PosOverload = 0u;
    FTCalcPca_NegOverload = 0u;
    retVal = E_OK;
    return (retVal);
};

Std_ReturnType FTCalcPca_Calculate (void)
{
    Std_ReturnType retVal = E_NOT_OK;

    FTCalcPca_PcaOffsetComp ();
    FTCalcPca_CalcPcaMatrix ();
    FTCalcPca_CheckOverload ();
    retVal = E_OK;
    return (retVal);
}

#if 0
Std_ReturnType FTCalcPca_Calculate_f (void)
{
    Std_ReturnType retVal = E_NOT_OK;

    FTCalcPca_PcaOffsetComp_f ();
    FTCalcPca_CalcPcaMatrix_f ();

    retVal = E_OK;
    return retVal;
}
#endif

Std_ReturnType FTCalcPca_GetFTValues (FTCalcPca_FTValuesType *dataPtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    retVal = E_OK;
    return retVal;
}
Std_ReturnType FTCalcPca_GetFTData (uint8 *dataPtr)
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 inputIdx, ouputIdx = 0;
    // static uint16 tempArray[ 6 ];

    for (inputIdx = 0; inputIdx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; inputIdx++)
    {
        //  tempArray[ inputIdx ] = ( uint16 )(((( sint32* )( &FTCalcPca_FTValues))[ inputIdx ] ) + 30000 );

        dataPtr[ ouputIdx ] = (uint8) (((((uint16*) (&FTCalcPca_FTValues))[ inputIdx ])-(((uint16*) (&FTCalcPca_FTZeroValues))[ inputIdx ])) >> 8);
        ouputIdx++;
        dataPtr[ ouputIdx ] = (uint8) (((((uint16*) (&FTCalcPca_FTValues))[ inputIdx ])-(((uint16*) (&FTCalcPca_FTZeroValues))[ inputIdx ])) &0x00FF);
        ouputIdx++;
        //        dataPtr[ ouputIdx ] = ( uint8 )(((( uint16* )( &FTCalcPca_FTValues ))[ inputIdx ] ) >> 8 );
        //        ouputIdx++;
        //        dataPtr[ ouputIdx ] = ( uint8 )(((( uint16* )( &FTCalcPca_FTValues ))[ inputIdx ] ) &0x00FF );
        //        ouputIdx++;
    }

    retVal = E_OK;
    return retVal;
}
Std_ReturnType FTCalcPca_Zero (void)
{
    Std_ReturnType retVal = E_NOT_OK;

    FTCalcPca_FTZeroValues.ForceValues[0] = FTCalcPca_FTValues.ForceValues[0];
    FTCalcPca_FTZeroValues.ForceValues[1] = FTCalcPca_FTValues.ForceValues[1];
    FTCalcPca_FTZeroValues.ForceValues[2] = FTCalcPca_FTValues.ForceValues[2];

    FTCalcPca_FTZeroValues.TorqueValues[0] = FTCalcPca_FTValues.TorqueValues[0];
    FTCalcPca_FTZeroValues.TorqueValues[1] = FTCalcPca_FTValues.TorqueValues[1];
    FTCalcPca_FTZeroValues.TorqueValues[2] = FTCalcPca_FTValues.TorqueValues[2];

    FTCalcPca_ZeroStatus = 1u;
    retVal = E_OK;
    return retVal;
}
Std_ReturnType FTCalcPca_Unzero (void)
{
    Std_ReturnType retVal = E_NOT_OK;
    FTCalcPca_FTZeroValues.ForceValues[0] = 0;
	FTCalcPca_FTZeroValues.ForceValues[1] = 0;
	FTCalcPca_FTZeroValues.ForceValues[2] = 0;
    FTCalcPca_FTZeroValues.TorqueValues[0] = 0;
	FTCalcPca_FTZeroValues.TorqueValues[1] = 0;
    FTCalcPca_FTZeroValues.TorqueValues[2] = 0;
    FTCalcPca_ZeroStatus = 0u;
    retVal = E_OK;
    return retVal;
}
uint8 FTCalcPca_GetZeroStatus (void)
{

   return (FTCalcPca_ZeroStatus);

}
#if 0
Std_ReturnType FTCalcPca_GetFTDataDiff (uint8 *dataPtr)
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 inputIdx, ouputIdx = 0;
    static uint16 tempArray[6];

    tempArray[ 0 ] = (uint16) ((FTCalcPca_FTValues_f.ForceValues[ 0 ] - FTCalcPca_FTValues.ForceValues[ 0 ]) + 30000);
    tempArray[ 1 ] = (uint16) ((FTCalcPca_FTValues_f.ForceValues[ 1 ] - FTCalcPca_FTValues.ForceValues[ 1 ]) + 30000);
    tempArray[ 2 ] = (uint16) ((FTCalcPca_FTValues_f.ForceValues[ 2 ] - FTCalcPca_FTValues.ForceValues[ 2 ]) + 30000);
    tempArray[ 3 ] = (uint16) ((FTCalcPca_FTValues_f.TorqueValues[ 0 ] - FTCalcPca_FTValues.TorqueValues[ 0 ]) + 30000);
    tempArray[ 4 ] = (uint16) ((FTCalcPca_FTValues_f.TorqueValues[ 1 ] - FTCalcPca_FTValues.TorqueValues[ 1 ]) + 30000);
    tempArray[ 5 ] = (uint16) ((FTCalcPca_FTValues_f.TorqueValues[ 2 ] - FTCalcPca_FTValues.TorqueValues[ 2 ]) + 30000);

    for (inputIdx = 0; inputIdx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; inputIdx++)
    {
        dataPtr[ ouputIdx ] = (uint8) ((((uint16*) (&tempArray))[ inputIdx ]) >> 8);
        ouputIdx++;
        dataPtr[ ouputIdx ] = (uint8) ((((uint16*) (&tempArray))[ inputIdx ]) &0x00FF);
        ouputIdx++;
    }

    retVal = E_OK;
    return retVal;
}
#endif

#if 0
Std_ReturnType FTCalcPca_GetFTValues_f (FTCalcPca_FTValuesType *dataPtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    dataPtr->ForceValues[ 0 ] = FTCalcPca_FTValues_f.ForceValues[ 0 ];
    dataPtr->ForceValues[ 1 ] = FTCalcPca_FTValues_f.ForceValues[ 1 ];
    dataPtr->ForceValues[ 2 ] = FTCalcPca_FTValues_f.ForceValues[ 2 ];
    dataPtr->TorqueValues[ 0 ] = FTCalcPca_FTValues_f.TorqueValues[ 0 ];
    dataPtr->TorqueValues[ 1 ] = FTCalcPca_FTValues_f.TorqueValues[ 1 ];
    dataPtr->TorqueValues[ 2 ] = FTCalcPca_FTValues_f.TorqueValues[ 2 ];

    retVal = E_OK;
    return retVal;
}
#endif

#if 0
Std_ReturnType FTCalcPca_GetFTData_f (uint8 *dataPtr)
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 inputIdx, ouputIdx = 0;
    static uint16 tempArray[ 6 ];

    for (inputIdx = 0; inputIdx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; inputIdx++)
    {
        tempArray[ inputIdx ] = (uint16) ((((sint32*) (&FTCalcPca_FTValues_f))[ inputIdx ]) + 30000);
        dataPtr[ ouputIdx ] = (uint8) ((((uint16*) (&tempArray))[ inputIdx ]) >> 8);
        ouputIdx++;
        dataPtr[ ouputIdx ] = (uint8) ((((uint16*) (&tempArray))[ inputIdx ]) &0x00FF);
        ouputIdx++;
        //        dataPtr[ ouputIdx ] = ( uint8 )(((( uint16* )( &FTCalcPca_FTValues_f ))[ inputIdx ] ) >> 8 );
        //        ouputIdx++;
        //        dataPtr[ ouputIdx ] = ( uint8 )(((( uint16* )( &FTCalcPca_FTValues_f ))[ inputIdx ] ) &0x00FF );
        //        ouputIdx++;
    }

    retVal = E_OK;
    return retVal;
}
#endif
uint8 FTCalcPca_GetPosOverload (void)
{
    return (FTCalcPca_PosOverload);
}
uint8 FTCalcPca_GetNegOverload (void)
{
    return (FTCalcPca_NegOverload);
}
/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */

static Std_ReturnType FTCalcPca_CheckOverload (void)
{
    Std_ReturnType retVal = E_NOT_OK;
    uint8 i;

    for (i = 0u; i < 3; i++)
    {
        FTCalcPca_PosOverload &= ~(1u << (5u - i));
        FTCalcPca_NegOverload &= ~(1u << (5u - i));
        if (FTCalcPca_FTValues.ForceValues[i] > FtCalcPca_ForcePosOverloadLimit[i])
        {
            FTCalcPca_PosOverload |= (1u << (5u - i));

        }
        else if (FTCalcPca_FTValues.ForceValues[i] < FtCalcPca_ForceNegOverloadLimit[i])
        {
            FTCalcPca_NegOverload |= (1u << (5u - i));

        }

    }
    for (i = 0u; i < 3; i++)
    {
        FTCalcPca_PosOverload &= ~(1u << (2u - i));
        FTCalcPca_NegOverload &= ~(1u << (2u - i));
        if (FTCalcPca_FTValues.TorqueValues[i] > FtCalcPca_TorquePosOverloadLimit[i])
        {
            FTCalcPca_PosOverload |= (1u << (2u - i));
        }
        else if (FTCalcPca_FTValues.TorqueValues[i] < FtCalcPca_TorqueNegOverloadLimit[i])
        {
            FTCalcPca_NegOverload |= (1u << (2u - i));
        }

    }


    retVal = E_OK;
    return retVal;
}

static Std_ReturnType FTCalcPca_PcaOffsetComp (void)
{
    Std_ReturnType retVal = E_NOT_OK;

    SignalProcessing_GetCompensatedSignals (FTCalcPca_InputSignals);

    MY_FLOAT_FULL_SUB2 (S1_1, S1_1, FTCalcPca_PcaData.pcaOffset[ 0 ]);
    MY_FLOAT_FULL_SUB2 (S1_2, S1_2, FTCalcPca_PcaData.pcaOffset[ 1 ]);
    MY_FLOAT_FULL_SUB2 (S1_3, S1_3, FTCalcPca_PcaData.pcaOffset[ 2 ]);
    MY_FLOAT_FULL_SUB2 (S1_4, S1_4, FTCalcPca_PcaData.pcaOffset[ 3 ]);

    MY_FLOAT_FULL_SUB2 (S2_1, S2_1, FTCalcPca_PcaData.pcaOffset[ 4 ]);
    MY_FLOAT_FULL_SUB2 (S2_2, S2_2, FTCalcPca_PcaData.pcaOffset[ 5 ]);
    MY_FLOAT_FULL_SUB2 (S2_3, S2_3, FTCalcPca_PcaData.pcaOffset[ 6 ]);
    MY_FLOAT_FULL_SUB2 (S2_4, S2_4, FTCalcPca_PcaData.pcaOffset[ 7 ]);

    MY_FLOAT_FULL_SUB2 (S3_1, S3_1, FTCalcPca_PcaData.pcaOffset[ 8 ]);
    MY_FLOAT_FULL_SUB2 (S3_2, S3_2, FTCalcPca_PcaData.pcaOffset[ 9 ]);
    MY_FLOAT_FULL_SUB2 (S3_3, S3_3, FTCalcPca_PcaData.pcaOffset[ 10 ]);
    MY_FLOAT_FULL_SUB2 (S3_4, S3_4, FTCalcPca_PcaData.pcaOffset[ 11 ]);

    //    S1_1 = S1_1 - FTCalcPca_PcaData.pcaOffset[ 0 ];
    //    S1_2 = S1_2 - FTCalcPca_PcaData.pcaOffset[ 1 ];
    //    S1_3 = S1_3 - FTCalcPca_PcaData.pcaOffset[ 2 ];
    //    S1_4 = S1_4 - FTCalcPca_PcaData.pcaOffset[ 3 ];
    //
    //    S2_1 = S2_1 - FTCalcPca_PcaData.pcaOffset[ 4 ];
    //    S2_2 = S2_2 - FTCalcPca_PcaData.pcaOffset[ 5 ];
    //    S2_3 = S2_3 - FTCalcPca_PcaData.pcaOffset[ 6 ];
    //    S2_4 = S2_4 - FTCalcPca_PcaData.pcaOffset[ 7 ];
    //
    //    S3_1 = S3_1 - FTCalcPca_PcaData.pcaOffset[ 8 ];
    //    S3_2 = S3_2 - FTCalcPca_PcaData.pcaOffset[ 9 ];
    //    S3_3 = S3_3 - FTCalcPca_PcaData.pcaOffset[ 10 ];
    //    S3_4 = S3_4 - FTCalcPca_PcaData.pcaOffset[ 11 ];

    retVal = E_OK;
    return retVal;
}

#if 0
static Std_ReturnType FTCalcPca_PcaOffsetComp_f (void)
{
    Std_ReturnType retVal = E_NOT_OK;

    SignalProcessing_GetCompensatedSignals_f (&FTCalcPca_InputSignals_f);

    S1_1_f = S1_1_f - FTCalcPca_PcaData_f.pcaOffset[ 0 ];
    S1_2_f = S1_2_f - FTCalcPca_PcaData_f.pcaOffset[ 1 ];
    S1_3_f = S1_3_f - FTCalcPca_PcaData_f.pcaOffset[ 2 ];
    S1_4_f = S1_4_f - FTCalcPca_PcaData_f.pcaOffset[ 3 ];

    S2_1_f = S2_1_f - FTCalcPca_PcaData_f.pcaOffset[ 4 ];
    S2_2_f = S2_2_f - FTCalcPca_PcaData_f.pcaOffset[ 5 ];
    S2_3_f = S2_3_f - FTCalcPca_PcaData_f.pcaOffset[ 6 ];
    S2_4_f = S2_4_f - FTCalcPca_PcaData_f.pcaOffset[ 7 ];

    S3_1_f = S3_1_f - FTCalcPca_PcaData_f.pcaOffset[ 8 ];
    S3_2_f = S3_2_f - FTCalcPca_PcaData_f.pcaOffset[ 9 ];
    S3_3_f = S3_3_f - FTCalcPca_PcaData_f.pcaOffset[ 10 ];
    S3_4_f = S3_4_f - FTCalcPca_PcaData_f.pcaOffset[ 11 ];

    retVal = E_OK;
    return retVal;
}
#endif

static Std_ReturnType FTCalcPca_CalcPcaMatrix (void)
{
    Std_ReturnType retVal = E_NOT_OK;
    //    static MyFloat sumVirtualSignals[ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT ] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    //    static MyFloat tempMyFloat;
    uint16 columnIdx, rowIdx;

    FTCalcPca_VirtualSignals[0].coeff = S1_1.coeff;
    FTCalcPca_VirtualSignals[0].expo = S1_1.expo;
    FTCalcPca_VirtualSignals[1].coeff = S1_2.coeff;
    FTCalcPca_VirtualSignals[1].expo = S1_2.expo;
    FTCalcPca_VirtualSignals[2].coeff = S1_3.coeff;
    FTCalcPca_VirtualSignals[2].expo = S1_3.expo;
    FTCalcPca_VirtualSignals[3].coeff = S1_4.coeff;
    FTCalcPca_VirtualSignals[3].expo = S1_4.expo;

    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[4], S1_1, S1_1);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[5], S1_2, S1_2);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[6], S1_3, S1_3);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[7], S1_4, S1_4);

    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[8], S1_1, S1_1, S1_1);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[9], S1_2, S1_2, S1_2);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[10], S1_3, S1_3, S1_3);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[11], S1_4, S1_4, S1_4);

    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[12], S1_1, S1_2);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[13], S1_1, S1_3);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[14], S1_1, S1_4);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[15], S1_2, S1_3);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[16], S1_2, S1_4);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[17], S1_3, S1_4);

    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[18], S1_2, S1_3, S1_4);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[19], S1_1, S1_3, S1_4);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[20], S1_1, S1_2, S1_4);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[21], S1_1, S1_2, S1_3);



    FTCalcPca_VirtualSignals[22].coeff = S2_1.coeff;
    FTCalcPca_VirtualSignals[22].expo = S2_1.expo;
    FTCalcPca_VirtualSignals[23].coeff = S2_2.coeff;
    FTCalcPca_VirtualSignals[23].expo = S2_2.expo;
    FTCalcPca_VirtualSignals[24].coeff = S2_3.coeff;
    FTCalcPca_VirtualSignals[24].expo = S2_3.expo;
    FTCalcPca_VirtualSignals[25].coeff = S2_4.coeff;
    FTCalcPca_VirtualSignals[25].expo = S2_4.expo;

    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[26], S2_1, S2_1);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[27], S2_2, S2_2);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[28], S2_3, S2_3);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[29], S2_4, S2_4);

    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[30], S2_1, S2_1, S2_1);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[31], S2_2, S2_2, S2_2);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[32], S2_3, S2_3, S2_3);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[33], S2_4, S2_4, S2_4);

    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[34], S2_1, S2_2);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[35], S2_1, S2_3);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[36], S2_1, S2_4);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[37], S2_2, S2_3);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[38], S2_2, S2_4);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[39], S2_3, S2_4);

    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[40], S2_2, S2_3, S2_4);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[41], S2_1, S2_3, S2_4);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[42], S2_1, S2_2, S2_4);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[43], S2_1, S2_2, S2_3);



    FTCalcPca_VirtualSignals[44].coeff = S3_1.coeff;
    FTCalcPca_VirtualSignals[44].expo = S3_1.expo;
    FTCalcPca_VirtualSignals[45].coeff = S3_2.coeff;
    FTCalcPca_VirtualSignals[45].expo = S3_2.expo;
    FTCalcPca_VirtualSignals[46].coeff = S3_3.coeff;
    FTCalcPca_VirtualSignals[46].expo = S3_3.expo;
    FTCalcPca_VirtualSignals[47].coeff = S3_4.coeff;
    FTCalcPca_VirtualSignals[47].expo = S3_4.expo;

    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[48], S3_1, S3_1);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[49], S3_2, S3_2);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[50], S3_3, S3_3);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[51], S3_4, S3_4);

    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[52], S3_1, S3_1, S3_1);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[53], S3_2, S3_2, S3_2);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[54], S3_3, S3_3, S3_3);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[55], S3_4, S3_4, S3_4);

    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[56], S3_1, S3_2);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[57], S3_1, S3_3);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[58], S3_1, S3_4);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[59], S3_2, S3_3);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[60], S3_2, S3_4);
    MY_FLOAT_SIMPLE_MUL2 (FTCalcPca_VirtualSignals[61], S3_3, S3_4);

    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[62], S3_2, S3_3, S3_4);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[63], S3_1, S3_3, S3_4);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[64], S3_1, S3_2, S3_4);
    MY_FLOAT_SIMPLE_MUL3 (FTCalcPca_VirtualSignals[65], S3_1, S3_2, S3_3);

    FTCalcPca_VirtualSignals[66].coeff = (sint32) 1 << 27;
    FTCalcPca_VirtualSignals[66].expo = -27;

    //    BOARD_SET_LED_RED_HIGH();

    for (columnIdx = 0; columnIdx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; columnIdx++)
    {
        sumVirtualSignals[ columnIdx ].coeff = 0;
        sumVirtualSignals[ columnIdx ].expo = 0;
        for (rowIdx = 0; rowIdx < CALIB_DATA_HANDLER_PCA_VIRTUAL_SIGNAL_COUNT; rowIdx++)
        {
            //            BOARD_SET_LED_RED_HIGH();
            //            MY_FLOAT_FULL_MUL2( tempMyFloat_pca, FTCalcPca_VirtualSignals[ rowIdx ], FTCalcPca_PcaData.pcaMatrix[ rowIdx ][ columnIdx ]);
            MY_FLOAT_SIMPLE_MUL2 (tempMyFloat_pca, FTCalcPca_VirtualSignals[ rowIdx ], FTCalcPca_PcaData.pcaMatrix[ rowIdx ][ columnIdx ]);
            //            BOARD_SET_LED_RED_LOW();
            //            BOARD_SET_LED_GREEN_HIGH();
            MY_FLOAT_FULL_ADD2 (sumVirtualSignals[ columnIdx ], sumVirtualSignals[ columnIdx ], tempMyFloat_pca);
            //            BOARD_SET_LED_GREEN_LOW();
        }
    }

    //    BOARD_SET_LED_RED_LOW();

    ////        temp.coeff = ((( hex->virtual_signal_s[idx_s].coeff ) >> 8 ) * (( pca_matrix_s[idx_s][column].coeff ) >> 8 )) >> 8;
    ////        temp.coeff = (( int16_t )(( hex->virtual_signal_s[idx_s].coeff ) >> 8 ) * ( int16_t )(( pca_matrix_s[idx_s][column].coeff ) >> 8 )) >> 8;
    //        temp.coeff = ((( int64_t )( hex->virtual_signal_s[idx_s].coeff )) * (( int64_t )( pca_matrix_s[idx_s][column].coeff ))) >> 24;
    //        temp.expo = hex->virtual_signal_s[idx_s].expo + pca_matrix_s[idx_s][column].expo;
    ////        temp.coeff++;
    ////        temp.expo++;
    //        if ((( sum_s.expo - temp.expo ) < 24 ) || (( sum_s.expo - temp.expo ) > -24 ))
    //        {
    //            if ( sum_s.expo > temp.expo )
    //            {
    //                sum_s.coeff = sum_s.coeff + ( temp.coeff >> ( sum_s.expo - temp.expo ));
    //            }
    //            else
    //            {
    //                sum_s.coeff = temp.coeff + ( sum_s.coeff >> ( temp.expo - sum_s.expo ));
    //                sum_s.expo = temp.expo;
    //            }
    //        }
    //        else if ( sum_s.expo - temp.expo <= -24 )
    //        {
    //            sum_s.coeff = temp.coeff;
    //            sum_s.expo = temp.expo;
    //        }
    ////        sum_f = sum_f + ldexp( hex->virtual_signal_s[idx_s].coeff, hex->virtual_signal_s[idx_s].expo ) * ldexp( pca_matrix_s[idx_s][column].coeff, pca_matrix_s[idx_s][column].expo );
    //    }
    //
    ////    if ( sum_s.expo < 0 )
    ////    {
    ////        hex->ft2[column] = (( float )( sum_s.coeff )) / (( -1 ) * sum_s.expo );
    ////    }
    ////    else
    ////    {
    ////        hex->ft2[column] = (( float )( sum_s.coeff )) * sum_s.expo;
    ////    }
    //
    //    hex->ft2[column] = ldexp( sum_s.coeff, sum_s.expo + 24 );

    FTCalcPca_FTValues.ForceValues[ 0 ] = (sint16) (ldexp (sumVirtualSignals[ 0 ].coeff, sumVirtualSignals[ 0 ].expo) * FtCalcPca_ForcePostMultiplier[0]);
    FTCalcPca_FTValues.ForceValues[ 1 ] = (sint16) (ldexp (sumVirtualSignals[ 1 ].coeff, sumVirtualSignals[ 1 ].expo) * FtCalcPca_ForcePostMultiplier[1]);
    FTCalcPca_FTValues.ForceValues[ 2 ] = (sint16) (ldexp (sumVirtualSignals[ 2 ].coeff, sumVirtualSignals[ 2 ].expo) * FtCalcPca_ForcePostMultiplier[2]);
    FTCalcPca_FTValues.TorqueValues[ 0 ] = (sint16) (ldexp (sumVirtualSignals[ 3 ].coeff, sumVirtualSignals[ 3 ].expo) * FtCalcPca_TorquePostMultiplier[0]);
    FTCalcPca_FTValues.TorqueValues[ 1 ] = (sint16) (ldexp (sumVirtualSignals[ 4 ].coeff, sumVirtualSignals[ 4 ].expo) * FtCalcPca_TorquePostMultiplier[1]);
    FTCalcPca_FTValues.TorqueValues[ 2 ] = (sint16) (ldexp (sumVirtualSignals[ 5 ].coeff, sumVirtualSignals[ 5 ].expo) * FtCalcPca_TorquePostMultiplier[2]);
#if 0
    FTCalcPca_FTValues.ForceValues[ 0 ] = (sint16) (ldexp (sumVirtualSignals[ 0 ].coeff, sumVirtualSignals[ 0 ].expo) * 100.0f);
    FTCalcPca_FTValues.ForceValues[ 1 ] = (sint16) (ldexp (sumVirtualSignals[ 1 ].coeff, sumVirtualSignals[ 1 ].expo) * 100.0f);
    FTCalcPca_FTValues.ForceValues[ 2 ] = (sint16) (ldexp (sumVirtualSignals[ 2 ].coeff, sumVirtualSignals[ 2 ].expo) * 100.0f);
    FTCalcPca_FTValues.TorqueValues[ 0 ] = (sint16) (ldexp (sumVirtualSignals[ 3 ].coeff, sumVirtualSignals[ 3 ].expo) * 1000.0f);
    FTCalcPca_FTValues.TorqueValues[ 1 ] = (sint16) (ldexp (sumVirtualSignals[ 4 ].coeff, sumVirtualSignals[ 4 ].expo) * 1000.0f);
    FTCalcPca_FTValues.TorqueValues[ 2 ] = (sint16) (ldexp (sumVirtualSignals[ 5 ].coeff, sumVirtualSignals[ 5 ].expo) * 1000.0f);
#endif
    //    FTCalcPca_FTValues.ForceValues[ 0 ]  = ( sint16 )(( sumVirtualSignals[ 0 ].coeff >> sumVirtualSignals[ 0 ].expo ) * 100 );
    //    FTCalcPca_FTValues.ForceValues[ 1 ]  = ( sint16 )(( sumVirtualSignals[ 1 ].coeff >> sumVirtualSignals[ 1 ].expo ) * 100 );
    //    FTCalcPca_FTValues.ForceValues[ 2 ]  = ( sint16 )(( sumVirtualSignals[ 2 ].coeff >> sumVirtualSignals[ 2 ].expo ) * 100 );
    //    FTCalcPca_FTValues.TorqueValues[ 0 ] = ( sint16 )(( sumVirtualSignals[ 3 ].coeff >> sumVirtualSignals[ 3 ].expo ) * 1000 );
    //    FTCalcPca_FTValues.TorqueValues[ 1 ] = ( sint16 )(( sumVirtualSignals[ 4 ].coeff >> sumVirtualSignals[ 4 ].expo ) * 1000 );
    //    FTCalcPca_FTValues.TorqueValues[ 2 ] = ( sint16 )(( sumVirtualSignals[ 5 ].coeff >> sumVirtualSignals[ 5 ].expo ) * 1000 );

    retVal = E_OK;
    return retVal;
}

#if 0
static Std_ReturnType FTCalcPca_CalcPcaMatrix_f (void)
{
    Std_ReturnType retVal = E_NOT_OK;
    //    static float32 sumVirtualSignals_f[ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT ] = { 0.0f };
    uint16 columnIdx, rowIdx;

    FTCalcPca_VirtualSignals_f[0] = S1_1_f;
    FTCalcPca_VirtualSignals_f[1] = S1_2_f;
    FTCalcPca_VirtualSignals_f[2] = S1_3_f;
    FTCalcPca_VirtualSignals_f[3] = S1_4_f;

    FTCalcPca_VirtualSignals_f[4] = S1_1_f * S1_1_f;
    FTCalcPca_VirtualSignals_f[5] = S1_2_f * S1_2_f;
    FTCalcPca_VirtualSignals_f[6] = S1_3_f * S1_3_f;
    FTCalcPca_VirtualSignals_f[7] = S1_4_f * S1_4_f;

    FTCalcPca_VirtualSignals_f[8] = S1_1_f * S1_1_f * S1_1_f;
    FTCalcPca_VirtualSignals_f[9] = S1_2_f * S1_2_f * S1_2_f;
    FTCalcPca_VirtualSignals_f[10] = S1_3_f * S1_3_f * S1_3_f;
    FTCalcPca_VirtualSignals_f[11] = S1_4_f * S1_4_f * S1_4_f;

    FTCalcPca_VirtualSignals_f[12] = S1_1_f * S1_2_f;
    FTCalcPca_VirtualSignals_f[13] = S1_1_f * S1_3_f;
    FTCalcPca_VirtualSignals_f[14] = S1_1_f * S1_4_f;
    FTCalcPca_VirtualSignals_f[15] = S1_2_f * S1_3_f;
    FTCalcPca_VirtualSignals_f[16] = S1_2_f * S1_4_f;
    FTCalcPca_VirtualSignals_f[17] = S1_3_f * S1_4_f;

    FTCalcPca_VirtualSignals_f[18] = S1_2_f * S1_3_f * S1_4_f;
    FTCalcPca_VirtualSignals_f[19] = S1_1_f * S1_3_f * S1_4_f;
    FTCalcPca_VirtualSignals_f[20] = S1_1_f * S1_2_f * S1_4_f;
    FTCalcPca_VirtualSignals_f[21] = S1_1_f * S1_2_f * S1_3_f;



    FTCalcPca_VirtualSignals_f[22] = S2_1_f;
    FTCalcPca_VirtualSignals_f[23] = S2_2_f;
    FTCalcPca_VirtualSignals_f[24] = S2_3_f;
    FTCalcPca_VirtualSignals_f[25] = S2_4_f;

    FTCalcPca_VirtualSignals_f[26] = S2_1_f * S2_1_f;
    FTCalcPca_VirtualSignals_f[27] = S2_2_f * S2_2_f;
    FTCalcPca_VirtualSignals_f[28] = S2_3_f * S2_3_f;
    FTCalcPca_VirtualSignals_f[29] = S2_4_f * S2_4_f;

    FTCalcPca_VirtualSignals_f[30] = S2_1_f * S2_1_f * S2_1_f;
    FTCalcPca_VirtualSignals_f[31] = S2_2_f * S2_2_f * S2_2_f;
    FTCalcPca_VirtualSignals_f[32] = S2_3_f * S2_3_f * S2_3_f;
    FTCalcPca_VirtualSignals_f[33] = S2_4_f * S2_4_f * S2_4_f;

    FTCalcPca_VirtualSignals_f[34] = S2_1_f * S2_2_f;
    FTCalcPca_VirtualSignals_f[35] = S2_1_f * S2_3_f;
    FTCalcPca_VirtualSignals_f[36] = S2_1_f * S2_4_f;
    FTCalcPca_VirtualSignals_f[37] = S2_2_f * S2_3_f;
    FTCalcPca_VirtualSignals_f[38] = S2_2_f * S2_4_f;
    FTCalcPca_VirtualSignals_f[39] = S2_3_f * S2_4_f;

    FTCalcPca_VirtualSignals_f[40] = S2_2_f * S2_3_f * S2_4_f;
    FTCalcPca_VirtualSignals_f[41] = S2_1_f * S2_3_f * S2_4_f;
    FTCalcPca_VirtualSignals_f[42] = S2_1_f * S2_2_f * S2_4_f;
    FTCalcPca_VirtualSignals_f[43] = S2_1_f * S2_2_f * S2_3_f;



    FTCalcPca_VirtualSignals_f[44] = S3_1_f;
    FTCalcPca_VirtualSignals_f[45] = S3_2_f;
    FTCalcPca_VirtualSignals_f[46] = S3_3_f;
    FTCalcPca_VirtualSignals_f[47] = S3_4_f;

    FTCalcPca_VirtualSignals_f[48] = S3_1_f * S3_1_f;
    FTCalcPca_VirtualSignals_f[49] = S3_2_f * S3_2_f;
    FTCalcPca_VirtualSignals_f[50] = S3_3_f * S3_3_f;
    FTCalcPca_VirtualSignals_f[51] = S3_4_f * S3_4_f;

    FTCalcPca_VirtualSignals_f[52] = S3_1_f * S3_1_f * S3_1_f;
    FTCalcPca_VirtualSignals_f[53] = S3_2_f * S3_2_f * S3_2_f;
    FTCalcPca_VirtualSignals_f[54] = S3_3_f * S3_3_f * S3_3_f;
    FTCalcPca_VirtualSignals_f[55] = S3_4_f * S3_4_f * S3_4_f;

    FTCalcPca_VirtualSignals_f[56] = S3_1_f * S3_2_f;
    FTCalcPca_VirtualSignals_f[57] = S3_1_f * S3_3_f;
    FTCalcPca_VirtualSignals_f[58] = S3_1_f * S3_4_f;
    FTCalcPca_VirtualSignals_f[59] = S3_2_f * S3_3_f;
    FTCalcPca_VirtualSignals_f[60] = S3_2_f * S3_4_f;
    FTCalcPca_VirtualSignals_f[61] = S3_3_f * S3_4_f;

    FTCalcPca_VirtualSignals_f[62] = S3_2_f * S3_3_f * S3_4_f;
    FTCalcPca_VirtualSignals_f[63] = S3_1_f * S3_3_f * S3_4_f;
    FTCalcPca_VirtualSignals_f[64] = S3_1_f * S3_2_f * S3_4_f;
    FTCalcPca_VirtualSignals_f[65] = S3_1_f * S3_2_f * S3_3_f;

    FTCalcPca_VirtualSignals_f[66] = 1.0f;

    //    BOARD_SET_LED_GREEN_HIGH();

    for (columnIdx = 0; columnIdx < CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT; columnIdx++)
    {
        sumVirtualSignals_f[ columnIdx ] = 0.0f;
        for (rowIdx = 0; rowIdx < CALIB_DATA_HANDLER_PCA_VIRTUAL_SIGNAL_COUNT; rowIdx++)
        {
            sumVirtualSignals_f[ columnIdx ] += FTCalcPca_VirtualSignals_f[ rowIdx ] * FTCalcPca_PcaData_f.pcaMatrix[ rowIdx ][ columnIdx ];
        }
    }

    //    BOARD_SET_LED_GREEN_LOW();

    FTCalcPca_FTValues_f.ForceValues[ 0 ] = (sint16) (sumVirtualSignals_f[ 0 ] * 100.0f);
    FTCalcPca_FTValues_f.ForceValues[ 1 ] = (sint16) (sumVirtualSignals_f[ 1 ] * 100.0f);
    FTCalcPca_FTValues_f.ForceValues[ 2 ] = (sint16) (sumVirtualSignals_f[ 2 ] * 100.0f);
    FTCalcPca_FTValues_f.TorqueValues[ 0 ] = (sint16) (sumVirtualSignals_f[ 3 ] * 1000.0f);
    FTCalcPca_FTValues_f.TorqueValues[ 1 ] = (sint16) (sumVirtualSignals_f[ 4 ] * 1000.0f);
    FTCalcPca_FTValues_f.TorqueValues[ 2 ] = (sint16) (sumVirtualSignals_f[ 5 ] * 1000.0f);

    retVal = E_OK;
    return retVal;
}
#endif
