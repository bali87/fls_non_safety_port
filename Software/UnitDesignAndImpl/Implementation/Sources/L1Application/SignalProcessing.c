
/**
 * @file
 *
 * Unit that manages the computation of the system delay /Unit that provides library functions for computation of delay.
 * fixme update the comment.
 *
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup Includes Includes
 * @{
 */

#include <math.h>
#include "Std.h"
//#include "MCU.h"
#include "Board.h"
#include "MyFloat.h"
#include "CalibrationDataHandler.h"
#include "SignalProcessing.h"


//#if (${HEADER_FILENAME})
//#[[#include]]# "${HEADER_FILENAME}"
//#end


/** @} */
/* ================================================================================================================== */
/**
 * @addtogroup StaticDefines Static defines
 */




/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticTypes Static types
 *  @{
 */


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticConstants Static constants
 */

//#define SIGNAL_PROCESSING_SIGNAL_GROUP_MEMBERS_COUNT 5u

#define SIGNAL_PROCESSING_FILTER_COEFF_1_5_HZ       0.0725927873837182L
#define SIGNAL_PROCESSING_FILTER_COEFF_5_HZ         0.2212107437624510L
#define SIGNAL_PROCESSING_FILTER_COEFF_15_HZ        0.5135199323588330L
//#define SIGNAL_PROCESSING_FILTER_COEFF_15_HZ        ( 1.0L / 400.0L )
#define SIGNAL_PROCESSING_FILTER_COEFF_50_HZ        0.8159737626920680L
#define SIGNAL_PROCESSING_FILTER_COEFF_NO_FILTER    1.0L

#define SIGNAL_PROCESSING_FILTER_COEFF_TEMP         ( 1.0L / 400.0L )

#define SIGNAL_PROCESSING_DEFAULT_FILTER_COEFF      SIGNAL_PROCESSING_FILTER_COEFF_15_HZ

//#define SIGNAL_PROCESSING_FILTER_PASS_LIMIT         64u
#define SIGNAL_PROCESSING_FILTER_PASS_LIMIT         256.0f

/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticVariables Static variables
 *  @{
 */

static Board_SignalGroupType SignalProcessing_BoardRawSignals[ BOARD_SIGNAL_GROUP_COUNT ];

//static SignalProcessing_SignalGroupType SignalProcessing_RawSignals[ BOARD_SIGNAL_GROUP_COUNT ];
//static SignalProcessing_SignalGroupType SignalProcessing_FilteredSignals[ BOARD_SIGNAL_GROUP_COUNT ];
//static SignalProcessing_SignalGroupType SignalProcessing_GainCompensatedSignals[ BOARD_SIGNAL_GROUP_COUNT ];
//static SignalProcessing_SignalGroupType SignalProcessing_OffsetCompensatedSignals[ BOARD_SIGNAL_GROUP_COUNT ];
//static SignalProcessing_SignalGroupType SignalProcessing_TempCompensatedSignals[ BOARD_SIGNAL_GROUP_COUNT ];

//static MyFloat SignalProcessing_SignalFilterCoeff[ 2 ];
//static MyFloat SignalProcessing_TempFilterCoeff[ 2 ];

static SignalProcessing_SignalGroupType_f SignalProcessing_RawSignals_f[ BOARD_SIGNAL_GROUP_COUNT ];
static SignalProcessing_SignalGroupType_f SignalProcessing_FilteredSignals_f[ BOARD_SIGNAL_GROUP_COUNT ];
static SignalProcessing_SignalGroupType_f SignalProcessing_GainCompensatedSignals_f[ BOARD_SIGNAL_GROUP_COUNT ];
static SignalProcessing_SignalGroupType_f SignalProcessing_OffsetCompensatedSignals_f[ BOARD_SIGNAL_GROUP_COUNT ];
static SignalProcessing_SignalGroupType_f SignalProcessing_TempCompensatedSignals_f[ BOARD_SIGNAL_GROUP_COUNT ];

static float32 SignalProcessing_SignalFilterCoeff_f[ 2 ];
static float32 SignalProcessing_TempFilterCoeff_f[ 2 ];

static SignalProcessing_FilterType SignalProcesssing_FilterType;

//static CalibDataHandler_CalibrationDataType SignalProcessing_CalibrationData;

static CalibDataHandler_CalibrationDataType_f SignalProcessing_CalibrationData_f;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDeclarations Static functions declarations
 * @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDefinitions Global functions definitions
 *  @{
 */


Std_ReturnType SignalProcessing_Init( void )
{

    Std_ReturnType retVal = E_NOT_OK;

#if 0

    float64 tempFloat;
    tempFloat = 1.0L - SIGNAL_PROCESSING_DEFAULT_FILTER_COEFF;
    MyFloat_Float64ToMyFloat( &tempFloat, &( SignalProcessing_SignalFilterCoeff[ 0 ] ), 1u );
    tempFloat = SIGNAL_PROCESSING_DEFAULT_FILTER_COEFF;
    MyFloat_Float64ToMyFloat( &tempFloat, &( SignalProcessing_SignalFilterCoeff[ 1 ] ), 1u );
    tempFloat = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_TEMP;
    MyFloat_Float64ToMyFloat( &tempFloat, &( SignalProcessing_TempFilterCoeff[ 0 ] ), 1u );
    tempFloat = SIGNAL_PROCESSING_FILTER_COEFF_TEMP;
    MyFloat_Float64ToMyFloat( &tempFloat, &( SignalProcessing_TempFilterCoeff[ 1 ] ), 1u );
#endif

    SignalProcessing_SignalFilterCoeff_f[ 0 ] = 1.0L - SIGNAL_PROCESSING_DEFAULT_FILTER_COEFF;
    SignalProcessing_SignalFilterCoeff_f[ 1 ] = SIGNAL_PROCESSING_DEFAULT_FILTER_COEFF;
    SignalProcessing_TempFilterCoeff_f[ 0 ] = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_TEMP;
    SignalProcessing_TempFilterCoeff_f[ 1 ] = SIGNAL_PROCESSING_FILTER_COEFF_TEMP;

    //    SignalProcessing_FilteredSignals[ 0 ].tempDiode.coeff = 0x00F00000;
    //    SignalProcessing_FilteredSignals[ 0 ].tempDiode.expo = -20;

#if 0

    CalibDataHandler_GetOffsetCompParams( &SignalProcessing_CalibrationData );
    CalibDataHandler_GetGainCompParams( &SignalProcessing_CalibrationData );
    CalibDataHandler_GetTempCompParams( &SignalProcessing_CalibrationData );
    CalibDataHandler_GetSensorLimits( &SignalProcessing_CalibrationData );
#endif

    if (E_NOT_OK == CalibDataHandler_LoadData_f( &SignalProcessing_CalibrationData_f ) )
    {
        CalibDataHandler_GetOffsetCompParams_f( &SignalProcessing_CalibrationData_f );
        CalibDataHandler_GetGainCompParams_f( &SignalProcessing_CalibrationData_f );
        CalibDataHandler_GetTempCompParams_f( &SignalProcessing_CalibrationData_f );
        CalibDataHandler_GetSensorLimits_f( &SignalProcessing_CalibrationData_f );
    }

    retVal = E_OK;
    return retVal;
};


#if 0
Std_ReturnType SignalProcessing_Filtering( void )
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 groupIdx;
    uint16 memberIdx;
    static MyFloat tempMyFloat1;
    static MyFloat tempMyFloat2;

    for ( groupIdx = 0u; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        Board_GetSignalGroup(( uint8 )groupIdx, &( SignalProcessing_BoardRawSignals[ groupIdx ] ));
        MyFloat_Uint16ToMyFloat( ( uint16* )( &(SignalProcessing_BoardRawSignals[ groupIdx ])), ( MyFloat* )( &(SignalProcessing_RawSignals[ groupIdx ])), BOARD_SIGNAL_GROUP_MEMBERS_COUNT );

        for ( memberIdx = 0u; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT ; memberIdx++ )
        {
            MY_FLOAT_FULL_MUL2( tempMyFloat1, SignalProcessing_FilteredSignals[ groupIdx ].pdArray[ memberIdx ], SignalProcessing_SignalFilterCoeff[ 0 ] );
            MY_FLOAT_FULL_MUL2( tempMyFloat2, SignalProcessing_RawSignals[ groupIdx ].pdArray[ memberIdx ], SignalProcessing_SignalFilterCoeff[ 1 ] );
            MY_FLOAT_FULL_ADD2( SignalProcessing_FilteredSignals[ groupIdx ].pdArray[ memberIdx ], tempMyFloat1, tempMyFloat2 );
        }

        if ( 0 == SignalProcessing_FilteredSignals[ groupIdx ].tempDiode.coeff )
        {
            SignalProcessing_FilteredSignals[ groupIdx ].tempDiode.coeff =  SignalProcessing_RawSignals[ groupIdx ].tempDiode.coeff;
            SignalProcessing_FilteredSignals[ groupIdx ].tempDiode.expo =  SignalProcessing_RawSignals[ groupIdx ].tempDiode.expo;
        }
        else
        {
            MY_FLOAT_FULL_MUL2( tempMyFloat1, SignalProcessing_FilteredSignals[ groupIdx ].tempDiode, SignalProcessing_TempFilterCoeff[ 0 ] );
            MY_FLOAT_FULL_MUL2( tempMyFloat2, SignalProcessing_RawSignals[ groupIdx ].tempDiode, SignalProcessing_TempFilterCoeff[ 1 ] );
            MY_FLOAT_FULL_ADD2( SignalProcessing_FilteredSignals[ groupIdx ].tempDiode, tempMyFloat1, tempMyFloat2 );
        }
    }

    retVal = E_OK;
    return retVal;
}
#endif

Std_ReturnType SignalProcessing_Filtering_f( void )
{
    Std_ReturnType retVal = E_NOT_OK;

    uint16 groupIdx;
    uint16 memberIdx;
    retVal = E_OK;

    for ( groupIdx = 0u; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        Board_GetSignalGroup(( uint8 )groupIdx, &( SignalProcessing_BoardRawSignals[ groupIdx ] ));

        for ( memberIdx = 0u; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT ; memberIdx++ )
        {
            SignalProcessing_RawSignals_f[ groupIdx ].pdArray[ memberIdx ] = ( float32 )SignalProcessing_BoardRawSignals[ groupIdx ].pdArray[ memberIdx ];
            SignalProcessing_FilteredSignals_f[ groupIdx ].pdArray[ memberIdx ] =
            SignalProcessing_FilteredSignals_f[ groupIdx ].pdArray[ memberIdx ] * SignalProcessing_SignalFilterCoeff_f[ 0 ] +
            SignalProcessing_RawSignals_f[ groupIdx ].pdArray[ memberIdx ] * SignalProcessing_SignalFilterCoeff_f[ 1 ];
        }

        SignalProcessing_RawSignals_f[ groupIdx ].tempDiode = ( float32 )SignalProcessing_BoardRawSignals[ groupIdx ].tempDiode;

        if ( 0.0f == SignalProcessing_FilteredSignals_f[ groupIdx ].tempDiode )
        {
            SignalProcessing_FilteredSignals_f[ groupIdx ].tempDiode =  SignalProcessing_RawSignals_f[ groupIdx ].tempDiode;
        }
        else
        {
            SignalProcessing_FilteredSignals_f[ groupIdx ].tempDiode =
                SignalProcessing_FilteredSignals_f[ groupIdx ].tempDiode * SignalProcessing_TempFilterCoeff_f[ 0 ] +
                SignalProcessing_RawSignals_f[ groupIdx ].tempDiode * SignalProcessing_TempFilterCoeff_f[ 1 ];
        }
    }

    return retVal;
}


#if 0
Std_ReturnType SignalProcessing_OffsetComp( void )
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 groupIdx;
    uint16 memberIdx;
    //    static sint32 testVar0, testVar1;
    //    static uint32 testVar2, testVar3;

    for ( groupIdx = 0u; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        for ( memberIdx = 0u; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT ; memberIdx++ )
        {
            MY_FLOAT_FULL_SUB2( SignalProcessing_OffsetCompensatedSignals[ groupIdx ].pdArray[ memberIdx ],
                                SignalProcessing_FilteredSignals[ groupIdx ].pdArray[ memberIdx ],
                                SignalProcessing_CalibrationData.offsetCompParams[ groupIdx ][ memberIdx ]);
        }

        MY_FLOAT_FULL_SUB2( SignalProcessing_OffsetCompensatedSignals[ groupIdx ].tempDiode,
                            SignalProcessing_FilteredSignals[ groupIdx ].tempDiode,
                            SignalProcessing_CalibrationData.offsetCompParams[ groupIdx ][ memberIdx ]);
    }
    //    SignalProcessing_FilteredSignals[ 0 ].pdArray[ 0 ].coeff = -1 * ( SignalProcessing_FilteredSignals[ 0 ].pdArray[ 0 ].coeff );
    //    testVar1 = ( SignalProcessing_FilteredSignals[ 0 ].pdArray[ 0 ] ).coeff;
    //    testVar2 = *(( uint32* )(&(( SignalProcessing_FilteredSignals[ 0 ].pdArray[ 0 ] ).coeff )));
    //    testVar3 = (*(( uint32* )(&(( SignalProcessing_FilteredSignals[ 0 ].pdArray[ 0 ] ).coeff )))) & 0xFFFF0000UL;
    //    testVar0 = ( SignalProcessing_FilteredSignals[ 0 ].pdArray[ 0 ] ).coeff;

    retVal = E_OK;
    return retVal;
}
#endif

Std_ReturnType SignalProcessing_OffsetComp_f( void )
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 groupIdx;
    uint16 memberIdx;

    for ( groupIdx = 0; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        for ( memberIdx = 0; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT; memberIdx++ )
        {
            SignalProcessing_OffsetCompensatedSignals_f[ groupIdx ].pdArray[ memberIdx ] =
                SignalProcessing_FilteredSignals_f[ groupIdx ].pdArray[ memberIdx ] -
                SignalProcessing_CalibrationData_f.offsetCompParams[ groupIdx ][ memberIdx ];
        }

        SignalProcessing_OffsetCompensatedSignals_f[ groupIdx ].tempDiode =
            SignalProcessing_FilteredSignals_f[ groupIdx ].tempDiode -
            SignalProcessing_CalibrationData_f.offsetCompParams[ groupIdx ][ memberIdx ];
    }

    retVal = E_OK;
    return retVal;
}


#if 0
Std_ReturnType SignalProcessing_GainComp( void )
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 groupIdx;
    uint16 memberIdx;
    static MyFloat tempMyFloat;

    /* 30000 */
    tempMyFloat.coeff = 15360000;
    tempMyFloat.expo = -9;

    for ( groupIdx = 0u; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        for ( memberIdx = 0u; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT ; memberIdx++ )
        {
            MY_FLOAT_FULL_MUL2( SignalProcessing_GainCompensatedSignals[ groupIdx ].pdArray[ memberIdx ],
                                SignalProcessing_OffsetCompensatedSignals[ groupIdx ].pdArray[ memberIdx ],
                                SignalProcessing_CalibrationData.gainCompParams[ groupIdx ][ memberIdx ]);

            MY_FLOAT_FULL_ADD2( SignalProcessing_GainCompensatedSignals[ groupIdx ].pdArray[ memberIdx ],
                                SignalProcessing_GainCompensatedSignals[ groupIdx ].pdArray[ memberIdx ],
                                tempMyFloat );
        }
    }

    retVal = E_OK;
    return retVal;
}
#endif

Std_ReturnType SignalProcessing_GainComp_f( void )
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 groupIdx;
    uint16 memberIdx;

    for ( groupIdx = 0; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        for ( memberIdx = 0; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT; memberIdx++ )
        {
            SignalProcessing_GainCompensatedSignals_f[ groupIdx ].pdArray[ memberIdx ] =
                SignalProcessing_OffsetCompensatedSignals_f[ groupIdx ].pdArray[ memberIdx ] *
                SignalProcessing_CalibrationData_f.gainCompParams[ groupIdx ][ memberIdx ] + 30000;
        }
    }

    retVal = E_OK;
    return retVal;
}


#if 0
Std_ReturnType SignalProcessing_TempComp( void )
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 groupIdx;
    uint16 memberIdx;
    static float64 tempFloat;
    static MyFloat tempMyFloatOffset;
    static MyFloat tempMyFloat1;
    static MyFloat tempMyFloat2;
    static MyFloat tempMyFloat3;
    static MyFloat tempMyFloat4;

    /* 30000 */
    tempMyFloatOffset.coeff = 245760000;
    tempMyFloatOffset.expo = -13;

    for ( groupIdx = 0u; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        MY_FLOAT_FULL_MUL2( tempMyFloat1, SignalProcessing_OffsetCompensatedSignals[ groupIdx ].tempDiode, SignalProcessing_OffsetCompensatedSignals[ groupIdx ].tempDiode );
        for ( memberIdx = 0u; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT ; memberIdx++ )
        {
            MY_FLOAT_FULL_MUL2( tempMyFloat2,
                                SignalProcessing_CalibrationData.tempCompParams[ groupIdx ][ memberIdx ][ 0 ],
                                tempMyFloat1);

            MY_FLOAT_FULL_MUL2( tempMyFloat3,
                                SignalProcessing_CalibrationData.tempCompParams[ groupIdx ][ memberIdx ][ 1 ],
                                SignalProcessing_OffsetCompensatedSignals[ groupIdx ].tempDiode);

            MY_FLOAT_FULL_ADD2( tempMyFloat4,
                                tempMyFloat2,
                                tempMyFloat3 );

            MY_FLOAT_FULL_ADD2( tempMyFloat2,
                                SignalProcessing_CalibrationData.tempCompParams[ groupIdx ][ memberIdx ][ 2 ],
                                tempMyFloat4 );

            MY_FLOAT_FULL_MUL2( SignalProcessing_TempCompensatedSignals[ groupIdx ].pdArray[ memberIdx ],
                                SignalProcessing_GainCompensatedSignals[ groupIdx ].pdArray[ memberIdx ],
                                tempMyFloatOffset);

            tempFloat = 0.0L;
            tempFloat = ldexp( ( float64 )( SignalProcessing_TempCompensatedSignals[ groupIdx ].pdArray[ memberIdx ].coeff ), SignalProcessing_TempCompensatedSignals[ groupIdx ].pdArray[ memberIdx ].expo );
            tempFloat = tempFloat / ldexp( tempMyFloat2.coeff, tempMyFloat2.expo );
            //            BOARD_SET_LED_RED_HIGH();
            MyFloat_Float64ToMyFloat( &tempFloat, &( SignalProcessing_TempCompensatedSignals[ groupIdx ].pdArray[ memberIdx ]), 1u );
            //            BOARD_SET_LED_RED_LOW();
        }
    }

    retVal = E_OK;
    return retVal;
}
#endif

Std_ReturnType SignalProcessing_TempComp_f( void )
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 groupIdx;
    uint16 memberIdx;
    float32 tempFloat1;
    float32 tempFloat2;

    for ( groupIdx = 0; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        tempFloat1 = SignalProcessing_OffsetCompensatedSignals_f[ groupIdx ].tempDiode * SignalProcessing_OffsetCompensatedSignals_f[ groupIdx ].tempDiode;
        for ( memberIdx = 0; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT; memberIdx++ )
        {
            if(CalibrationDataHandler_IsCalibrated())
            {


                tempFloat2 = SignalProcessing_CalibrationData_f.tempCompParams[ groupIdx ][ memberIdx ][ 0 ] * tempFloat1 +
                             SignalProcessing_CalibrationData_f.tempCompParams[ groupIdx ][ memberIdx ][ 1 ] * SignalProcessing_OffsetCompensatedSignals_f[ groupIdx ].tempDiode +
                             SignalProcessing_CalibrationData_f.tempCompParams[ groupIdx ][ memberIdx ][ 2 ];

                SignalProcessing_TempCompensatedSignals_f[ groupIdx ].pdArray[ memberIdx ] =
                SignalProcessing_GainCompensatedSignals_f[ groupIdx ].pdArray[ memberIdx ] * ( 30000.0f / tempFloat2 );
            }
            /* Simple copy raw values to compensated values */
            else
            {
                SignalProcessing_TempCompensatedSignals_f[ groupIdx ].pdArray[ memberIdx ] =  SignalProcessing_RawSignals_f[ groupIdx ].pdArray[ memberIdx ];
            }
        }
    }

    retVal = E_OK;
    return retVal;
}


#if 0
Std_ReturnType SignalProcessing_GetRawSignals( SignalProcessing_SignalGroupType_u16 *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 groupIdx;
    uint16 memberIdx;

    for ( groupIdx = 0; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        for ( memberIdx = 0; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT; memberIdx++ )
        {
            dataPtr[ groupIdx ].pdArray[ memberIdx ]  = ( uint16 )( ldexp( SignalProcessing_FilteredSignals[ groupIdx ].pdArray[ memberIdx ].coeff, SignalProcessing_FilteredSignals[ groupIdx ].pdArray[ memberIdx ].expo ));
        }
        dataPtr[ groupIdx ].tempDiode  = ( uint16 )( ldexp( SignalProcessing_FilteredSignals[ groupIdx ].tempDiode.coeff, SignalProcessing_FilteredSignals[ groupIdx ].tempDiode.expo ));
    }

    (( uint8* )dataPtr )[ 0 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_TempCompensatedSignals[ 0 ].pdArray[ 0 ].coeff, SignalProcessing_TempCompensatedSignals[ 0 ].pdArray[ 0 ].expo )) >> 8 );
    (( uint8* )dataPtr )[ 1 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_TempCompensatedSignals[ 0 ].pdArray[ 0 ].coeff, SignalProcessing_TempCompensatedSignals[ 0 ].pdArray[ 0 ].expo )) & 0x00FF );
    (( uint8* )dataPtr )[ 2 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_FilteredSignals[ 0 ].tempDiode.coeff, SignalProcessing_FilteredSignals[ 0 ].tempDiode.expo )) >> 8 );
    (( uint8* )dataPtr )[ 3 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_FilteredSignals[ 0 ].tempDiode.coeff, SignalProcessing_FilteredSignals[ 0 ].tempDiode.expo )) & 0x00FF );

    (( uint8* )dataPtr )[ 4 ] = ( uint8 )(( uint16 )( SignalProcessing_TempCompensatedSignals_f[ 0 ].pdArray[ 0 ] ) >> 8 );
    (( uint8* )dataPtr )[ 5 ] = ( uint8 )(( uint16 )( SignalProcessing_TempCompensatedSignals_f[ 0 ].pdArray[ 0 ] ) & 0x00FF );
    (( uint8* )dataPtr )[ 6 ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ 0 ].tempDiode ) >> 8 );
    (( uint8* )dataPtr )[ 7 ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ 0 ].tempDiode ) & 0x00FF );

    //        dataPtr[ 0 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_GainCompensatedSignals[ 0 ].pdArray[ 0 ].coeff, SignalProcessing_GainCompensatedSignals[ 0 ].pdArray[ 0 ].expo )) >> 8 );
    //        dataPtr[ 1 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_GainCompensatedSignals[ 0 ].pdArray[ 0 ].coeff, SignalProcessing_GainCompensatedSignals[ 0 ].pdArray[ 0 ].expo )) & 0x00FF );
    //        dataPtr[ 2 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_FilteredSignals[ 0 ].tempDiode.coeff, SignalProcessing_FilteredSignals[ 0 ].tempDiode.expo )) >> 8 );
    //        dataPtr[ 3 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_FilteredSignals[ 0 ].tempDiode.coeff, SignalProcessing_FilteredSignals[ 0 ].tempDiode.expo )) & 0x00FF );
    //
    //        dataPtr[ 4 ] = ( uint8 )(( uint16 )( SignalProcessing_GainCompensatedSignals_f[ 0 ].pdArray[ 0 ] ) >> 8 );
    //        dataPtr[ 5 ] = ( uint8 )(( uint16 )( SignalProcessing_GainCompensatedSignals_f[ 0 ].pdArray[ 0 ] ) & 0x00FF );
    //        dataPtr[ 6 ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ 0 ].tempDiode ) >> 8 );
    //        dataPtr[ 7 ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ 0 ].tempDiode ) & 0x00FF );

    //        dataPtr[ 0 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_OffsetCompensatedSignals[ 0 ].pdArray[ 0 ].coeff, SignalProcessing_OffsetCompensatedSignals[ 0 ].pdArray[ 0 ].expo )) >> 8 );
    //        dataPtr[ 1 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_OffsetCompensatedSignals[ 0 ].pdArray[ 0 ].coeff, SignalProcessing_OffsetCompensatedSignals[ 0 ].pdArray[ 0 ].expo )) & 0x00FF );
    //        dataPtr[ 2 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_OffsetCompensatedSignals[ 0 ].tempDiode.coeff, SignalProcessing_OffsetCompensatedSignals[ 0 ].tempDiode.expo )) >> 8 );
    //        dataPtr[ 3 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_OffsetCompensatedSignals[ 0 ].tempDiode.coeff, SignalProcessing_OffsetCompensatedSignals[ 0 ].tempDiode.expo )) & 0x00FF );
    //
    //        dataPtr[ 4 ] = ( uint8 )(( uint16 )( SignalProcessing_OffsetCompensatedSignals_f[ 0 ].pdArray[ 0 ] ) >> 8 );
    //        dataPtr[ 5 ] = ( uint8 )(( uint16 )( SignalProcessing_OffsetCompensatedSignals_f[ 0 ].pdArray[ 0 ] ) & 0x00FF );
    //        dataPtr[ 6 ] = ( uint8 )(( uint16 )( SignalProcessing_OffsetCompensatedSignals_f[ 0 ].tempDiode ) >> 8 );
    //        dataPtr[ 7 ] = ( uint8 )(( uint16 )( SignalProcessing_OffsetCompensatedSignals_f[ 0 ].tempDiode ) & 0x00FF );

    //        dataPtr[ 0 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_FilteredSignals[ 0 ].pdArray[ 0 ].coeff, SignalProcessing_FilteredSignals[ 0 ].pdArray[ 0 ].expo )) >> 8 );
    //        dataPtr[ 1 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_FilteredSignals[ 0 ].pdArray[ 0 ].coeff, SignalProcessing_FilteredSignals[ 0 ].pdArray[ 0 ].expo )) & 0x00FF );
    //        dataPtr[ 2 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_FilteredSignals[ 0 ].tempDiode.coeff, SignalProcessing_FilteredSignals[ 0 ].tempDiode.expo )) >> 8 );
    //        dataPtr[ 3 ] = ( uint8 )(( uint16 )( ldexp( SignalProcessing_FilteredSignals[ 0 ].tempDiode.coeff, SignalProcessing_FilteredSignals[ 0 ].tempDiode.expo )) & 0x00FF );
    //
    //        dataPtr[ 4 ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ 0 ].pdArray[ 0 ] ) >> 8 );
    //        dataPtr[ 5 ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ 0 ].pdArray[ 0 ] ) & 0x00FF );
    //        dataPtr[ 6 ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ 0 ].tempDiode ) >> 8 );
    //        dataPtr[ 7 ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ 0 ].tempDiode ) & 0x00FF );

    retVal = E_OK;
    return retVal;
}
#endif

Std_ReturnType SignalProcessing_GetRawData( uint8 *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 groupIdx;
    uint16 memberIdx;
    uint16 idx = 0;
    ;

    for ( groupIdx = 0; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        for ( memberIdx = 0; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT; memberIdx++ )
        {
            dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ groupIdx ].pdArray[ memberIdx ] ) >> 8 );
            idx++;
            dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ groupIdx ].pdArray[ memberIdx ] ) & 0x00FF );
            idx++;
        }
        dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ groupIdx ].tempDiode ) >> 8 );
        idx++;
        dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ groupIdx ].tempDiode ) & 0x00FF );
        idx++;
    }

    retVal = E_OK;
    return retVal;
}

Std_ReturnType SignalProcessing_GetRawSignals_f( SignalProcessing_SignalGroupType_u16 *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 groupIdx;
    uint16 memberIdx;

    for ( groupIdx = 0; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        for ( memberIdx = 0; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT; memberIdx++ )
        {
            dataPtr[ groupIdx ].pdArray[ memberIdx ]  = ( uint16 )( SignalProcessing_FilteredSignals_f[ groupIdx ].pdArray[ memberIdx ]);
        }
        dataPtr[ groupIdx ].tempDiode  = ( uint16 )( SignalProcessing_FilteredSignals_f[ groupIdx ].tempDiode );
    }

    retVal = E_OK;
    return retVal;
}

Std_ReturnType SignalProcessing_GetRawData_f( uint8 *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 groupIdx;
    uint16 memberIdx;
    uint16 idx = 0;
    ;

    for ( groupIdx = 0; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        for ( memberIdx = 0; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT; memberIdx++ )
        {
            dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ groupIdx ].pdArray[ memberIdx ] ) >> 8 );
            idx++;
            dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ groupIdx ].pdArray[ memberIdx ] ) & 0x00FF );
            idx++;
        }
        dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ groupIdx ].tempDiode ) >> 8 );
        idx++;
        dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ groupIdx ].tempDiode ) & 0x00FF );
        idx++;
    }

    retVal = E_OK;
    return retVal;
}



Std_ReturnType SignalProcessing_GetCompensatedSignals( SignalProcessing_SignalGroupType *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 groupIdx;
    uint16 memberIdx;

    for ( groupIdx = 0; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        for ( memberIdx = 0; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT; memberIdx++ )
        {
            dataPtr[ groupIdx ].pdArray[ memberIdx ].coeff = ( sint32 )( SignalProcessing_TempCompensatedSignals_f[ groupIdx ].pdArray[ memberIdx ] * 8192 );
            dataPtr[ groupIdx ].pdArray[ memberIdx ].expo  = -13;
        }

        dataPtr[ groupIdx ].tempDiode.coeff = ( sint32 )( SignalProcessing_FilteredSignals_f[ groupIdx ].tempDiode * 8192 );
        dataPtr[ groupIdx ].tempDiode.expo  = -13;
    }

    //    for ( groupIdx = 0; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    //    {
    //        for ( memberIdx = 0; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT; memberIdx++ )
    //        {
    //            dataPtr[ groupIdx ].pdArray[ memberIdx ]  = ( uint16 )( SignalProcessing_TempCompensatedSignals_f[ groupIdx ].pdArray[ memberIdx ] );
    //        }
    //
    //        dataPtr[ groupIdx ].tempDiode  = ( uint16 )( SignalProcessing_FilteredSignals_f[ groupIdx ].tempDiode );
    //    }

    //    for ( groupIdx = 0; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    //    {
    //        for ( memberIdx = 0; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT; memberIdx++ )
    //        {
    //            MyFloat_Float64ToMyFloat( &( SignalProcessing_TempCompensatedSignals_f[ groupIdx ].pdArray[ memberIdx ]), &( SignalProcessing_TempCompensatedSignals[ groupIdx ].pdArray[ memberIdx ]), 1u );
    //            dataPtr[ groupIdx ].pdArray[ memberIdx ].coeff = SignalProcessing_TempCompensatedSignals[ groupIdx ].pdArray[ memberIdx ].coeff;
    //            dataPtr[ groupIdx ].pdArray[ memberIdx ].expo  = SignalProcessing_TempCompensatedSignals[ groupIdx ].pdArray[ memberIdx ].expo;
    //        }
    //        MyFloat_Float64ToMyFloat( &( SignalProcessing_TempCompensatedSignals_f[ groupIdx ].tempDiode ), &( SignalProcessing_TempCompensatedSignals[ groupIdx ].tempDiode ), 1u );
    //        dataPtr[ groupIdx ].tempDiode.coeff = SignalProcessing_TempCompensatedSignals[ groupIdx ].tempDiode.coeff;
    //        dataPtr[ groupIdx ].tempDiode.expo  = SignalProcessing_TempCompensatedSignals[ groupIdx ].tempDiode.expo;
    //    }
    //
    retVal = E_OK;
    return retVal;
}

Std_ReturnType SignalProcessing_GetCompensatedData( uint8 *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 groupIdx;
    uint16 memberIdx;
    uint16 idx = 0;
    ;

    for ( groupIdx = 0; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        for ( memberIdx = 0; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT; memberIdx++ )
        {
            dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_TempCompensatedSignals_f[ groupIdx ].pdArray[ memberIdx ] ) >> 8 );
            idx++;
            dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_TempCompensatedSignals_f[ groupIdx ].pdArray[ memberIdx ] ) & 0x00FF );
            idx++;
        }
        //dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_TempCompensatedSignals_f[ groupIdx ].tempDiode ) >> 8 );
        dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ groupIdx ].tempDiode ) >> 8 );
        idx++;
        //dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_TempCompensatedSignals_f[ groupIdx ].tempDiode ) & 0x00FF );
        dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_FilteredSignals_f[ groupIdx ].tempDiode ) & 0x00FF );
        idx++;
    }

    retVal = E_OK;
    return retVal;
}

Std_ReturnType SignalProcessing_GetCompensatedSignals_f( SignalProcessing_SignalGroupType_f *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 groupIdx;
    uint16 memberIdx;

    for ( groupIdx = 0; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        for ( memberIdx = 0; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT; memberIdx++ )
        {
            dataPtr[ groupIdx ].pdArray[ memberIdx ] = SignalProcessing_TempCompensatedSignals_f[ groupIdx ].pdArray[ memberIdx ];
        }

        dataPtr[ groupIdx ].tempDiode = SignalProcessing_FilteredSignals_f[ groupIdx ].tempDiode;
    }

    retVal = E_OK;
    return retVal;
}

Std_ReturnType SignalProcessing_GetCompensatedData_f( uint8 *dataPtr )
{
    Std_ReturnType retVal = E_NOT_OK;
    uint16 groupIdx;
    uint16 memberIdx;
    uint16 idx = 0;
    ;

    for ( groupIdx = 0; groupIdx < BOARD_SIGNAL_GROUP_COUNT; groupIdx++ )
    {
        for ( memberIdx = 0; memberIdx < BOARD_SIGNAL_GROUP_PD_COUNT; memberIdx++ )
        {
            dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_TempCompensatedSignals_f[ groupIdx ].pdArray[ memberIdx ] ) >> 8 );
            idx++;
            dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_TempCompensatedSignals_f[ groupIdx ].pdArray[ memberIdx ] ) & 0x00FF );
            idx++;
        }
        dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_TempCompensatedSignals_f[ groupIdx ].tempDiode ) >> 8 );
        idx++;
        dataPtr[ idx ] = ( uint8 )(( uint16 )( SignalProcessing_TempCompensatedSignals_f[ groupIdx ].tempDiode ) & 0x00FF );
        idx++;
    }

    retVal = E_OK;
    return retVal;
}



Std_ReturnType SignalProcessing_SetFilterType( SignalProcessing_FilterType filterType )
{
    Std_ReturnType retVal = E_NOT_OK;
    //    static float64 tempFloat;

    //        switch ( filterType )
    //        {
    //            case SIGNAL_PROCESSING_FILTER_NO_FILTER:
    //                tempFloat = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_NO_FILTER;
    //                MyFloat_Float64ToMyFloat( &tempFloat, &( SignalProcessing_SignalFilterCoeff[ 0 ] ), 1u );
    //                tempFloat = SIGNAL_PROCESSING_FILTER_COEFF_NO_FILTER;
    //                MyFloat_Float64ToMyFloat( &tempFloat, &( SignalProcessing_SignalFilterCoeff[ 1 ] ), 1u );
    //                SignalProcessing_SignalFilterCoeff_f[ 0 ] = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_NO_FILTER;
    //                SignalProcessing_SignalFilterCoeff_f[ 1 ] = SIGNAL_PROCESSING_FILTER_COEFF_NO_FILTER;
    //                break;
    //
    //            case SIGNAL_PROCESSING_FILTER_1_5_HZ:
    //                tempFloat = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_1_5_HZ;
    //                MyFloat_Float64ToMyFloat( &tempFloat, &( SignalProcessing_SignalFilterCoeff[ 0 ] ), 1u );
    //                tempFloat = SIGNAL_PROCESSING_FILTER_COEFF_1_5_HZ;
    //                MyFloat_Float64ToMyFloat( &tempFloat, &( SignalProcessing_SignalFilterCoeff[ 1 ] ), 1u );
    //                SignalProcessing_SignalFilterCoeff_f[ 0 ] = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_1_5_HZ;
    //                SignalProcessing_SignalFilterCoeff_f[ 1 ] = SIGNAL_PROCESSING_FILTER_COEFF_1_5_HZ;
    //                break;
    //
    //            case SIGNAL_PROCESSING_FILTER_5_HZ:
    //                tempFloat = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_5_HZ;
    //                MyFloat_Float64ToMyFloat( &tempFloat, &( SignalProcessing_SignalFilterCoeff[ 0 ] ), 1u );
    //                tempFloat = SIGNAL_PROCESSING_FILTER_COEFF_5_HZ;
    //                MyFloat_Float64ToMyFloat( &tempFloat, &( SignalProcessing_SignalFilterCoeff[ 1 ] ), 1u );
    //                SignalProcessing_SignalFilterCoeff_f[ 0 ] = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_5_HZ;
    //                SignalProcessing_SignalFilterCoeff_f[ 1 ] = SIGNAL_PROCESSING_FILTER_COEFF_5_HZ;
    //                break;
    //
    //            case SIGNAL_PROCESSING_FILTER_15_HZ:
    //                tempFloat = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_15_HZ;
    //                MyFloat_Float64ToMyFloat( &tempFloat, &( SignalProcessing_SignalFilterCoeff[ 0 ] ), 1u );
    //                tempFloat = SIGNAL_PROCESSING_FILTER_COEFF_15_HZ;
    //                MyFloat_Float64ToMyFloat( &tempFloat, &( SignalProcessing_SignalFilterCoeff[ 1 ] ), 1u );
    //                SignalProcessing_SignalFilterCoeff_f[ 0 ] = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_15_HZ;
    //                SignalProcessing_SignalFilterCoeff_f[ 1 ] = SIGNAL_PROCESSING_FILTER_COEFF_15_HZ;
    //                break;
    //
    //            case SIGNAL_PROCESSING_FILTER_50_HZ:
    //                tempFloat = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_50_HZ;
    //                MyFloat_Float64ToMyFloat( &tempFloat, &( SignalProcessing_SignalFilterCoeff[ 0 ] ), 1u );
    //                tempFloat = SIGNAL_PROCESSING_FILTER_COEFF_50_HZ;
    //                MyFloat_Float64ToMyFloat( &tempFloat, &( SignalProcessing_SignalFilterCoeff[ 1 ] ), 1u );
    //                SignalProcessing_SignalFilterCoeff_f[ 0 ] = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_50_HZ;
    //                SignalProcessing_SignalFilterCoeff_f[ 1 ] = SIGNAL_PROCESSING_FILTER_COEFF_50_HZ;
    //                break;
    //
    //            default:
    //                break;
    //        }
    SignalProcesssing_FilterType = filterType;
    switch ( filterType )
    {
    case SIGNAL_PROCESSING_FILTER_NO_FILTER:
        SignalProcessing_SignalFilterCoeff_f[ 0 ] = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_NO_FILTER;
        SignalProcessing_SignalFilterCoeff_f[ 1 ] = SIGNAL_PROCESSING_FILTER_COEFF_NO_FILTER;
        break;

    case SIGNAL_PROCESSING_FILTER_1_5_HZ:
        SignalProcessing_SignalFilterCoeff_f[ 0 ] = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_1_5_HZ;
        SignalProcessing_SignalFilterCoeff_f[ 1 ] = SIGNAL_PROCESSING_FILTER_COEFF_1_5_HZ;
        break;

    case SIGNAL_PROCESSING_FILTER_5_HZ:
        SignalProcessing_SignalFilterCoeff_f[ 0 ] = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_5_HZ;
        SignalProcessing_SignalFilterCoeff_f[ 1 ] = SIGNAL_PROCESSING_FILTER_COEFF_5_HZ;
        break;

    case SIGNAL_PROCESSING_FILTER_15_HZ:
        SignalProcessing_SignalFilterCoeff_f[ 0 ] = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_15_HZ;
        SignalProcessing_SignalFilterCoeff_f[ 1 ] = SIGNAL_PROCESSING_FILTER_COEFF_15_HZ;
        break;

    case SIGNAL_PROCESSING_FILTER_50_HZ:
        SignalProcessing_SignalFilterCoeff_f[ 0 ] = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_50_HZ;
        SignalProcessing_SignalFilterCoeff_f[ 1 ] = SIGNAL_PROCESSING_FILTER_COEFF_50_HZ;
        //                SignalProcessing_SignalFilterCoeff_f[ 0 ] = 1.0L - SIGNAL_PROCESSING_FILTER_COEFF_TEMP;
        //                SignalProcessing_SignalFilterCoeff_f[ 1 ] = SIGNAL_PROCESSING_FILTER_COEFF_TEMP;
        break;

    default:
        break;
    }

    retVal = E_OK;
    return retVal;
}
SignalProcessing_FilterType SignalProcessing_GetFilterType( void )
{

    return (SignalProcesssing_FilterType);
}


Std_ReturnType SignalProcessing_UpdateCalibrationData( void )
{
    Std_ReturnType retVal = E_NOT_OK;

    retVal = E_OK;
    return retVal;
}


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup StaticFunctionsDefinitions Static functions definitions
 *  @{
 */


