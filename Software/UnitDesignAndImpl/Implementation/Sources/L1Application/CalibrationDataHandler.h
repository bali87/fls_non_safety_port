#ifndef CALIBRATION_DATA_HANDLER_H
#define CALIBRATION_DATA_HANDLER_H
#if 1
/**
 * @file
 * 
 * Header file of the unit ${FILENAME}. //fixme remove .h
 *
 * @version #[[$Id$]]#
 */
#include "MyFloat.h"
#include "Board.h"
/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */

#define CALIB_DATA_HANDLER_PCA_VIRTUAL_SIGNAL_COUNT 67u
#define CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT   6u

#define NOT_AVAILABLE               0u
#define AVAILABLE                   1u
//
#define NOT_CALIBRATED              0u
#define CALIBRATED                  1u


#define SENSOR_SERIAL               HEXSA012

#define CALIB_STATE                 CALIBRATED  // NOT_CALIBRATED or CALIBRATED

#if CALIB_STATE == NOT_CALIBRATED
#define GAIN_COMPENSATION           NOT_AVAILABLE
#define TEMP_COMPENSATION           NOT_AVAILABLE
#define RANGE_FILE                  NOT_AVAILABLE
#define PCA_CALIBRATION             NOT_AVAILABLE
#elif CALIB_STATE == CALIBRATED
#define GAIN_COMPENSATION           AVAILABLE
#define TEMP_COMPENSATION           AVAILABLE
#define RANGE_FILE                  AVAILABLE
#define PCA_CALIBRATION             AVAILABLE    
//#define PROXY_ENABLED
#endif


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */

typedef struct CalibDataHandler_SensorLimitsType_tag {
    MyFloat NominalCapacityPosCounts[ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT ];
    MyFloat NominalCapacityNegCounts[ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT ];
    MyFloat NominalCapacityPosUnits[ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT ];
    MyFloat NominalCapacityNegUnits[ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT ];
} CalibDataHandler_SensorLimitsType;

typedef struct CalibDataHandler_CalibrationDataType_tag {
    MyFloat offsetCompParams[ BOARD_SIGNAL_GROUP_COUNT ][ BOARD_SIGNAL_GROUP_MEMBERS_COUNT  ];
    MyFloat gainCompParams[ BOARD_SIGNAL_GROUP_COUNT ][ BOARD_SIGNAL_GROUP_PD_COUNT  ];
    MyFloat tempCompParams[ BOARD_SIGNAL_GROUP_COUNT ][ BOARD_SIGNAL_GROUP_PD_COUNT ][ 3 ];
    CalibDataHandler_SensorLimitsType sensorLimits;
} CalibDataHandler_CalibrationDataType;

typedef struct CalibDataHandler_PcaDataType_tag {
    MyFloat pcaMatrix[ CALIB_DATA_HANDLER_PCA_VIRTUAL_SIGNAL_COUNT ][ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT ];
    MyFloat pcaOffset[ BOARD_SIGNAL_GROUP_COUNT * BOARD_SIGNAL_GROUP_PD_COUNT ];
} CalibDataHandler_PcaDataType;



typedef struct CalibDataHandler_SensorLimitsType_tag_f {
    sint16 NominalCapacityPosCounts[ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT ];
    sint16 NominalCapacityNegCounts[ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT ];
    sint16 NominalCapacityPosUnits[ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT ];
    sint16 NominalCapacityNegUnits[ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT ];
} CalibDataHandler_SensorLimitsType_f;

typedef struct CalibDataHandler_CalibrationDataType_f_tag {
    uint16  offsetCompParams[ BOARD_SIGNAL_GROUP_COUNT ][ BOARD_SIGNAL_GROUP_MEMBERS_COUNT  ];
    float32 gainCompParams[ BOARD_SIGNAL_GROUP_COUNT ][ BOARD_SIGNAL_GROUP_PD_COUNT  ];
    float32 tempCompParams[ BOARD_SIGNAL_GROUP_COUNT ][ BOARD_SIGNAL_GROUP_PD_COUNT  ][ 3 ];
    CalibDataHandler_SensorLimitsType_f sensorLimits;
} CalibDataHandler_CalibrationDataType_f;

typedef struct CalibDataHandler_PcaDataType_f_tag {
    float32 pcaMatrix[ CALIB_DATA_HANDLER_PCA_VIRTUAL_SIGNAL_COUNT ][ CALIB_DATA_HANDLER_PCA_OUTPUT_SIGNAL_COUNT ];
    float32 pcaOffset[ BOARD_SIGNAL_GROUP_COUNT * BOARD_SIGNAL_GROUP_PD_COUNT];
} CalibDataHandler_PcaDataType_f;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */
 



/**
 * @addtogroup GlobalVaribles Global variables
 */
 



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */
/** @fn Std_ReturnType CalibDataHandler_GetOffsetCompParams( CalibDataHandler_CalibrationDataType *dataPtr )
* @brief
* @param
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType CalibDataHandler_GetOffsetCompParams( CalibDataHandler_CalibrationDataType *dataPtr );
/** @fn Std_ReturnType CalibDataHandler_GetGainCompParams( CalibDataHandler_CalibrationDataType *dataPtr )
* @brief
* @param
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType CalibDataHandler_GetGainCompParams( CalibDataHandler_CalibrationDataType *dataPtr );
/** @fn Std_ReturnType CalibDataHandler_GetTempCompParams( CalibDataHandler_CalibrationDataType *dataPtr )
* @brief
* @param
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType CalibDataHandler_GetTempCompParams( CalibDataHandler_CalibrationDataType *dataPtr );
/** @fn Std_ReturnType CalibDataHandler_GetSensorLimits( CalibDataHandler_CalibrationDataType *dataPtr )
* @brief
* @param
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType CalibDataHandler_GetSensorLimits( CalibDataHandler_CalibrationDataType *dataPtr );
/** @fn Std_ReturnType CalibDataHandler_GetPcaMatrix( CalibDataHandler_PcaDataType *dataPtr )
* @brief
* @param
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType CalibDataHandler_GetPcaMatrix( CalibDataHandler_PcaDataType *dataPtr );
/** @fn Std_ReturnType CalibDataHandler_GetPcaOffset( CalibDataHandler_PcaDataType *dataPtr )
* @brief
* @param
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType CalibDataHandler_GetPcaOffset( CalibDataHandler_PcaDataType *dataPtr );

Std_ReturnType CalibDataHandler_GetOffsetCompParams_f( CalibDataHandler_CalibrationDataType_f *dataPtr );
Std_ReturnType CalibDataHandler_GetGainCompParams_f( CalibDataHandler_CalibrationDataType_f *dataPtr );
Std_ReturnType CalibDataHandler_GetTempCompParams_f( CalibDataHandler_CalibrationDataType_f *dataPtr );
Std_ReturnType CalibDataHandler_GetSensorLimits_f( CalibDataHandler_CalibrationDataType_f *dataPtr );
Std_ReturnType CalibDataHandler_GetPcaMatrix_f( CalibDataHandler_PcaDataType_f *dataPtr );
Std_ReturnType CalibDataHandler_GetPcaOffset_f( CalibDataHandler_PcaDataType_f *dataPtr );


sint16 CalibDataHandler_GetPosCount( uint8 id );
sint16 CalibDataHandler_GetNegCount( uint8 id );
sint16 CalibDataHandler_GetPosNomCap( uint8 id );
sint16 CalibDataHandler_GetNegNomCap( uint8 id );
Std_ReturnType CalibDataHandler_LoadData_f( CalibDataHandler_CalibrationDataType_f *dataPtr );
Std_ReturnType CalibDataHandler_LoadPCA( CalibDataHandler_PcaDataType *pcaPtr );
void SensorSerial_Init(void);

uint8 CalibrationDataHandler_IsCalibrated(void);
/** @} */
#endif
#endif /* CALIBRATION_DATA_HANDLER_H */
