#ifndef SIGNAL_PROCESSING_H
#define SIGNAL_PROCESSING_H
#if 1
/**
 * @file
 * 
 * Header file of the unit ${FILENAME}. //fixme remove .h
 *
 * @version #[[$Id$]]#
 */

/* ================================================================================================================== */

/**
 * @addtogroup GlobalIncludes Global includes
 *  @{
 */

#include "MyFloat.h"
#include "Board.h"


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalDefines Global defines
 *  @{
 */



/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalTypes Global types
 *  @{
 */

typedef enum SignalProcessing_FilterType_tag
{
    SIGNAL_PROCESSING_FILTER_NO_FILTER  = 0u,
    SIGNAL_PROCESSING_FILTER_1_5_HZ     = 6u,
    SIGNAL_PROCESSING_FILTER_5_HZ       = 5u,
    SIGNAL_PROCESSING_FILTER_15_HZ      = 4u,
    SIGNAL_PROCESSING_FILTER_50_HZ      = 3u
} SignalProcessing_FilterType;

typedef struct SignalProcessing_SignalGroupType_tag
{
	MyFloat pdArray[ BOARD_SIGNAL_GROUP_PD_COUNT ];
	MyFloat tempDiode;
} SignalProcessing_SignalGroupType;

typedef struct SignalProcessing_SignalGroupType_s32_tag
{
	sint32 pdArray[ BOARD_SIGNAL_GROUP_PD_COUNT ];
	sint32 tempDiode;
} SignalProcessing_SignalGroupType_s32;

typedef struct SignalProcessing_SignalGroupType_u16_tag
{
	uint16 pdArray[ BOARD_SIGNAL_GROUP_PD_COUNT ];
	uint16 tempDiode;
}SignalProcessing_SignalGroupType_u16;

typedef struct SignalProcessing_SignalGroupType_f_tag
{
	float32 pdArray[ BOARD_SIGNAL_GROUP_PD_COUNT ];
	float32 tempDiode;
}SignalProcessing_SignalGroupType_f;


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalConstants Global constants
 */
 



/**
 * @addtogroup GlobalVaribles Global variables
 */
 


/** @} */
/* ================================================================================================================== */

/**
 * @addtogroup GlobalFunctionsDeclarations Global functions declarations
 *  @{
 */
/** @fn Std_ReturnType SignalProcessing_Init( void )
* @brief
* @param
* @return In case of any any error, it returns the error code (not implemented yet)
*/

Std_ReturnType SignalProcessing_Init( void );
/** @fn Std_ReturnType SignalProcessing_Filtering( void )
* @brief
* @param [in]
* @param [in]
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType SignalProcessing_Filtering( void );
/** @fn Std_ReturnType SignalProcessing_OffsetComp( void )
* @brief
* @param [in]
* @param [in]
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType SignalProcessing_OffsetComp( void );
/** @fn Std_ReturnType SignalProcessing_GainComp( void )
* @brief
* @param [in]
* @param [in]
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType SignalProcessing_GainComp( void );
/** @fn Std_ReturnType SignalProcessing_TempComp( void )
* @brief
* @param [in]
* @param [in]
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType SignalProcessing_TempComp( void );
/** @fn Std_ReturnType SignalProcessing_GetRawSignals( uint8 *dataPtr, uint16 dataLen )
* @brief
* @param [in]
* @param [in]
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType SignalProcessing_GetRawSignals( SignalProcessing_SignalGroupType_u16 *dataPtr );
Std_ReturnType SignalProcessing_GetRawData( uint8 *dataPtr );
/** @fn Std_ReturnType SignalProcessing_GetCompensadetSignals( uint8 *dataPtr )
* @brief
* @param [in]
* @param [in]
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType SignalProcessing_GetCompensatedSignals( SignalProcessing_SignalGroupType *dataPtr );
Std_ReturnType SignalProcessing_GetCompensatedData( uint8 *dataPtr );
Std_ReturnType SignalProcessing_SetFilterType( SignalProcessing_FilterType filterType );
SignalProcessing_FilterType SignalProcessing_GetFilterType( void );
Std_ReturnType SignalProcessing_UpdateCalibrationData( void );

Std_ReturnType SignalProcessing_Filtering_f( void );
Std_ReturnType SignalProcessing_OffsetComp_f( void );
Std_ReturnType SignalProcessing_GainComp_f( void );
Std_ReturnType SignalProcessing_TempComp_f( void );
Std_ReturnType SignalProcessing_GetRawSignals_f( SignalProcessing_SignalGroupType_u16 *dataPtr );
Std_ReturnType SignalProcessing_GetRawData_f( uint8 *dataPtr );
Std_ReturnType SignalProcessing_GetCompensatedSignals_f( SignalProcessing_SignalGroupType_f *dataPtr );
Std_ReturnType SignalProcessing_GetCompensatedData_f( uint8 *dataPtr );
/** @fn Std_ReturnType SignalProcessing_SetFilterType( sint16 filterType )
* @brief
* @param [in]
* @param [in]
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType SignalProcessing_SetFilterType_f( SignalProcessing_FilterType filterType );
/** @fn Std_ReturnType SignalProcessing_UpdateCalibrationData( void )
* @brief
* @param [in]
* @param [in]
* @return In case of any any error, it returns the error code (not implemented yet)
*/
Std_ReturnType SignalProcessing_UpdateCalibrationData_f( void );


/** @} */
#endif
#endif /* SIGNAL_SPROCESSING_H */
