float g1[5]   =   {0.0000f, 1.0534f, 1.0658f, 1.0497f, 1.1107f};
int b1[6]   =   {    0, 29745, 30635, 30100, 30139, 28943};

//HEXSA012 sensor parameters for gain compensation
float g2[5]   =   {0.0000f, 0.9899f, 0.9445f, 1.0017f, 1.0933f};
int b2[6]   =   {    0, 30018, 29917, 28442, 28975, 28939};

//HEXSA012 sensor parameters for gain compensation
float g3[5]   =   {0.0000f, 0.9135f, 0.9573f, 1.1750f, 0.7770f};
int b3[6]   =   {    0, 25763, 28967, 30452, 26801, 29002};
