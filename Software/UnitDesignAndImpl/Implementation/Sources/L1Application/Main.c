
#include "Std.h"
//#include <MCUConfig.h>
#include "Board.h"
//#include "CalibrationDataHandler.h"

#include "SysTimer.h"
#include "ComManager.h"

#include "SignalProcessing.h"
#include "FTCalcPca.h"


#include "Main.h"

#include "sys_common.h"

#include "Debug.h"
//#define MAIN_CYCLE_TIME 8u
#define MAIN_SYNC_TIME_OUT_VALUE 200u







/** @fn int main (void)
 *
 * @return
*/
int main (void)
{
uint8_t buffer[128] = {0u};
uint16_t dataLen = 0u;
uint16_t i=0u;
    sint16 wait;
//    static Std_ReturnType rangeSensorStatus = STD_RETURN_ERROR;
//    static ComManager_SyncModeType syncMode = SYNC_MODE_INTERNAL;
 //   static ComManager_ExternalTriggerType externalTrigger = EXTERNAL_TRIGGER_NOT_RECEIVED;
 //   static RangeMeasurement_RangingStateType rangingState = RANGING_STATE_IDLE;
    static ComManager_TPDOType runMode;

    Board_Init();
    SysTimer_Init ();
    SensorSerial_Init();





    ComManager_Init (0u);
    SignalProcessing_Init();
    FTCalcPca_Init();

 //   GPIO_SetPinValue( BOARD_LED_RED_PORT, BOARD_LED_RED_PIN, LOW );
 //   GPIO_SetPinValue( BOARD_LED_GREEN_PORT, BOARD_LED_GREEN_PIN, HIGH );
  //  GPIO_SetPinValue( BOARD_LED_BLUE_PORT, BOARD_LED_BLUE_PIN, HIGH );


    ComManager_ResetCommunication();

    SysTimer_SetTimePeriod( SYS_TIMER_MAIN_TIMER, SYS_TIMER_MAIN_CYCLE_TIME );
    SysTimer_StartTimer( SYS_TIMER_MAIN_TIMER, SYS_TIMER_FREE_RUNNING );
    //SysTimer_SetTimePeriod( SYS_TIMER_SYNC_TIME_OUT, MAIN_SYNC_TIME_OUT_VALUE );

    while (1)
    {


/*
        Timer2_Reset();
        if ( RANGING_STATE_TRIGGERED == rangingState )
        {
            rangeSensorStatus = RangeMeasurement_StartOneShotMeasure();
            if ( STD_RETURN_OK == rangeSensorStatus )
            {
                rangingState = RANGING_STATE_STARTED;
            }
            else
            {
                rangingState = RANGING_STATE_ERROR;
            }
        }
        ExtADC_StartConversion ();
        while ( !ExtADC_IsConvFinished() )
        {}

        ExtADC_StopConversion(EXTADC_STOPPED);
*/
       Board_CollectSignals();
        SignalProcessing_Filtering_f();
        SignalProcessing_OffsetComp_f();
        SignalProcessing_GainComp_f();
        SignalProcessing_TempComp_f();

        FTCalcPca_Calculate();
/*
        if ( RANGING_STATE_DONE == rangingState )
        {
            runMode = ComManager_GetRunMode();

            if(TPDO_MODE_RANGE_DEBUG == runMode)
            {
                rangeSensorStatus = RangeMeasurement_ReadOutRangingResult(1u);
            }
          
            else
            {
                rangeSensorStatus = RangeMeasurement_ReadOutRangingResult(0u);
            }
            if ( STD_RETURN_OK == rangeSensorStatus )
            {
                rangingState = RANGING_STATE_IDLE;
            }
            else
            {
                rangingState = RANGING_STATE_ERROR;
            }

        }

*/
     //   PUTS("Tick!");
       /* dataLen = CANDrv_ReadFullBuffer(buffer);
        i = 0u;
        if(dataLen)
        {


            while(i < dataLen)
            {
                PUTCHAR(buffer[i] / 100 + '0');
                PUTCHAR(buffer[i] % 100 / 10 + '0');
                PUTCHAR(buffer[i] % 10 + '0');
                PUTCHAR(' ');
                i++;
            }
            PUTCHAR('\r');
            PUTCHAR('\n');
        }*/

         ComManager_ExecuteTasks();
/*
        wait = 8000-Timer2_GetValue();
        ComManager_ReportWaitUs(wait);

        syncMode = ComManager_GetSyncMode();
        if ( SYNC_MODE_EXTERNAL == syncMode )
        {
            GPIO_SetPinValue( BOARD_LED_RED_PORT, BOARD_LED_RED_PIN, HIGH );
            GPIO_SetPinValue( BOARD_LED_GREEN_PORT, BOARD_LED_GREEN_PIN, LOW );
            GPIO_SetPinValue( BOARD_LED_BLUE_PORT, BOARD_LED_BLUE_PIN, HIGH );

            SysTimer_StartTimer( SYS_TIMER_SYNC_TIME_OUT, SYS_TIMER_ONE_SHOT );
            do
            {
                ComManager_RunRxFSM();
                externalTrigger = ComManager_GetExternalTriggerState();
                if ( SYS_TIMER_IS_EXPIRED == SysTimer_GetTimerStatus( SYS_TIMER_SYNC_TIME_OUT ))
                {
                    syncMode = SYNC_MODE_INTERNAL;
                    ComManager_SetSyncMode( SYNC_MODE_INTERNAL );
                    break;
                }
            }
            while ( EXTERNAL_TRIGGER_NOT_RECEIVED == externalTrigger );

            if (( EXTERNAL_TRIGGER_RECEIVED_TRIGGERED == externalTrigger ) && ( RANGING_STATE_IDLE == rangingState ))
            {
                rangingState = RANGING_STATE_TRIGGERED;
            }
            else if (( EXTERNAL_TRIGGER_RECEIVED_NOT_TRIGGERED == externalTrigger ) && ( RANGING_STATE_STARTED == rangingState ))
            {
                rangingState = RANGING_STATE_DONE;
            }
            else if ( RANGING_STATE_ERROR != rangingState )
            {
                rangingState = RANGING_STATE_IDLE;
            }
        }
        else
        {
            GPIO_SetPinValue( BOARD_LED_RED_PORT, BOARD_LED_RED_PIN, HIGH );    // 3us
            GPIO_SetPinValue( BOARD_LED_GREEN_PORT, BOARD_LED_GREEN_PIN, HIGH); // 3us
            GPIO_SetPinValue( BOARD_LED_BLUE_PORT, BOARD_LED_BLUE_PIN, LOW);    // 3us
*/
            while ( SYS_TIMER_IS_EXPIRED != SysTimer_GetTimerStatus( SYS_TIMER_MAIN_TIMER ))
            {
                ComManager_RunRxFSM();
            }
           // PUTS("Tick\r\n");
/*
            if ( RANGING_STATE_IDLE == rangingState )
            {
                rangingState = RANGING_STATE_TRIGGERED;
            }
            else if ( RANGING_STATE_STARTED == rangingState )
            {
                rangingState = RANGING_STATE_DONE;
            }
            else if ( RANGING_STATE_ERROR != rangingState )
            {
                rangingState = RANGING_STATE_IDLE;
            }
        }

*/
    }

    return (-1);
}

