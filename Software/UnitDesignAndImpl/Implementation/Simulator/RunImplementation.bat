@echo off
set DEBUG_MODE="%1"

echo "running program on simulator"

set SIMULATION_INPUT=%CD%\RunConfig.txt
SET SIMULATION_OUTPUT=..\SUI_SimulatorOutput.txt"
SET SIMULATION_MDBLOG=..\SUI_SimulatorMDBLog.txt"


echo "deleting old files ..."
del /q %SIMULATION_OUTPUT%
del /q %SIMULATION_MDBLOG%
del /q *.xml*
echo "... deleted"

echo "starting the program through microchip debugger ..."
mdb.bat %SIMULATION_INPUT% >> %SIMULATION_MDBLOG%
echo "... program finished"

rem check if file is generated. If not then exit with error code.
if not exist %SIMULATION_OUTPUT% goto exit
if not exist %SIMULATION_MDBLOG% goto exit

echo "deleting unnecessary xml files..."
del /q *.xml*
echo "... deleted"

rem exit without error
echo "... running program on simulator finished successfully"
if %DEBUG_MODE% NEQ "skip" pause
exit /b

rem exit with error
:exit
echo "... running program on simulator finished with ERROR"
if %DEBUG_MODE% NEQ "skip" pause
exit /b 1

