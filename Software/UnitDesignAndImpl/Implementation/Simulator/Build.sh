#!/bin/bash

. ../../../../General/BuildServer/Tools/CommonScripts/Generic.sh

enable_interactive_mode $1

echo "Running SUDI compilation on Simulator (cygwin) ..."

create_and_cd_build_dir

# set relative paths with respect to build dir
OUTPUT_TXT=../../Implementation_SimulatorBuild.txt

echo "Removing old files ..."
rm -fr ${OUTPUT_TXT} *.hex

echo "Running cmake ..."
# -G "Unix Makefiles"
cmake -DCMAKE_TOOLCHAIN_FILE=${MCU_SIM_TOOLCHAIN_FILE} -DSUBPHASE=SUDI ${CMAKELISTS_DIR} 2>&1 | tee ${OUTPUT_TXT} || EXIT=1
check "cmake"

echo "Running make ..."
make 2>&1 | tee -a ${OUTPUT_TXT} || EXIT=1
check "make"

collect_hex
check_output ${OUTPUT_TXT}

echo "... running SUI compilation on Simulator (cygwin) done."
pause

