@echo off

cygpath.exe %CD% > cygpath.txt
set /p CWD=< cygpath.txt
del /q cygpath.txt
bash.exe -l -c "cd %CWD%; ./Build.sh INTERACTIVE_MODE"
