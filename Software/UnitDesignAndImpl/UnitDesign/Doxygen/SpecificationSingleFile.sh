#!/bin/bash

BROWSER=chrome

echo "Running Doxygen generation ..."

echo "Removing old files ..."
rm -fr ${OUTPUT_CHM} warnings.log

echo "Creating temp directory ..."
tmp=`mktemp -d`

echo "Copying images to temp directory"
find ../../Implementation/Sources/ -name "*.png" -exec cp {} $tmp \;

echo "Creating doxygen configuration file ..."
TMPDOX=SUDSSingleFile.dox
cp SUDS.dox $TMPDOX
filename=`basename $1`
filename_we=${filename/.*/}
echo "FILE_PATTERNS = ${filename_we}.c ${filename_we}.h" >> $TMPDOX
echo "GENERATE_HTMLHELP = NO" >> $TMPDOX
echo "HTML_OUTPUT = c:/cygwin64${tmp}" >> $TMPDOX

echo "Running doxygen ..."
doxygen $TMPDOX

echo "Opening doxygen html documentation in a browser ..."
${BROWSER} "file://c:/cygwin64${tmp}/index.html"

